# This template file defines jobs related to npm projects using Angular.

variables:
  # IMPORTANT: Variables that are defined using variable expansion cannot be use in `rules:if` and
  # `rules:changes` clauses. Therefore, variables defined in this template file and used in
  # `rules:if` or `rules:changes` clauses, among others, explicitly don't make use of variable
  # expansion. These variables are marked with [RULES]. Care must be taken when overriding these
  # variables. Otherwise, some predefined rules may not work as intended.

  ##################################################################################################
  # Definition of variables for general configuration of the pipeline jobs and the tools used
  # with them.
  # These variables are not intended to be overridden by jobs extending the job templates.
  ##################################################################################################
  NPM_ANGULAR_DOCKER_IMAGE: trion/ng-cli-karma:17.1.3

  ##################################################################################################
  # Definition of variables for project-specific configuration of the pipeline jobs.
  # These variables can be overridden as needed by jobs extending the job templates.
  ##################################################################################################
  # Path to the project directory (relative to ${CI_PROJECT_DIR}).
  # Can either be empty or has to end with a `/`.
  PROJECT_DIR: ""
  # [RULES] Path to the project's `package.json` file (relative to ${CI_PROJECT_DIR}).
  PACKAGE_JSON_PATH: "package.json"
  # Path to the directory containing coverage report files (relative to ${PROJECT_DIR}).
  COVERAGE_OUTPUT_RELATIVE_PATH: "coverage"
  # Comma-SEPARATED list of paths to LCOV coverage report files. Paths may be absolute or
  # relative to ${PROJECT_DIR}.
  LCOV_REPORT_PATHS: "${COVERAGE_OUTPUT_RELATIVE_PATH}/lcov.info"
  # Path to ESLint report file (relative to ${PROJECT_DIR}).
  ESLINT_REPORT_FILE: "eslint-report.xml"
  # Whether to enforce successful quality jobs (e.g. sonar analysis) on the default branch. A value
  # of `true` effectively doesn't allow quality jobs to fail on the default branch.
  ENFORCE_QA_ON_DEFAULT_BRANCH: "false"
  # Path to the reference file containing all third-party licenses (relative to ${PROJECT_DIR}).
  REFERENCE_LICENSES_FILE: "third-party-licenses/third-party-licenses.csv"
  # Path to the generated file containing all third-party licenses - i.e. the output of the
  # `license-checker` module (relative to ${PROJECT_DIR}).
  GENERATED_LICENSES_FILE: "generated/third-party-licenses.csv"

# Job template for compiling the source code.
.npm-angular-build:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - npm install
    - npm run build
  artifacts:
    # Save the files produced by this job as job artifacts and allow jobs in subsequent stages to
    # download them.
    paths:
      - "${PROJECT_DIR}dist/"
    expire_in: 1 week
  rules:
    # The build is never allowed to fail.
    - when: on_success
      allow_failure: false

# Job template for running the linter.
.npm-angular-lint:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - npm install
    # Run the linter
    - npm run lint -- -o "${ESLINT_REPORT_FILE}" -f jslint-xml
  artifacts:
    # Save the files produced by this job as job artifacts and allow jobs in subsequent stages to
    # download them.
    paths:
      - "${PROJECT_DIR}${ESLINT_REPORT_FILE}"
    expire_in: 1 week
  rules:
    # Lint is never allowed to fail.
    # Lint is allowed to fail temporary as naming-convention cannot follow for eCMR attributes.
    - when: on_success
      allow_failure: true

# Job template for running unit and integration tests.
.npm-angular-test:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - npm install
    - npm run test -- --code-coverage=true --progress=false --watch=false
  coverage: '/Lines \W+: (\d+\.\d+)%.*/'
  artifacts:
    # Save the files produced by this job as job artifacts and allow jobs in subsequent stages to
    # download them.
    paths:
      - "${PROJECT_DIR}${COVERAGE_OUTPUT_RELATIVE_PATH}"
    expire_in: 1 week
  rules:
    # Tests are never allowed to fail.
    - when: on_success
      allow_failure: false

# Job template for checking for changed licenses of third-party dependencies.
# Note: This job uses the `license-checker` module to evaluate the project's third-party licenses.
.npm-angular-third-party-license-check:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - echo "REFERENCE_LICENSES_FILE=${REFERENCE_LICENSES_FILE}"
    - echo "GENERATED_LICENSES_FILE=${GENERATED_LICENSES_FILE}"
    - cd "${PROJECT_DIR}"
    # `npx license-checker` traverses the `node_modules` folder to evaluate the project's licenses,
    # therefore "npm install" is needed.
    - npm install
    - npx license-checker --unknown --csv --out "${GENERATED_LICENSES_FILE}"
    # Ensure a newline on each of the license files to equalise them
    - sed -i -e '$a\' "${GENERATED_LICENSES_FILE}"; sed -i -e '$a\' "${REFERENCE_LICENSES_FILE}"
    - cmp --silent "${REFERENCE_LICENSES_FILE}" "${GENERATED_LICENSES_FILE}" || export LICENSES_CHANGED=true
    - 'if [ ! -z ${LICENSES_CHANGED} ]; then
        echo Some licenses used by the third-party dependencies have changed.;
        echo Please refer to the README and generate/update them accordingly.;
        git diff --no-index --unified=0 "${REFERENCE_LICENSES_FILE}" "$GENERATED_LICENSES_FILE";
      fi'
  rules:
    # License check must succeed in default branches and MRs.
    - if: ($CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH) || $CI_MERGE_REQUEST_IID
      allow_failure: false
    # In all other cases, failure is permitted.
    - when: on_success
      allow_failure: true

# Job template for deploying project artifacts to a package registry.
.npm-angular-archive:
  image: ${NPM_ANGULAR_DOCKER_IMAGE}
  before_script:
    # Output environment variables used in this job. (Can be useful for debugging.)
    - echo "PROJECT_DIR=${PROJECT_DIR}"
    - cd "${PROJECT_DIR}"
    - echo "ecmr/angular-frontend:registry=https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/" > .npmrc
    - echo "//${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_ID}/packages/npm/:_authToken=${CI_JOB_TOKEN}" >> .npmrc
  script:
    - npm publish
  rules:
    # Only archive if the project's `package.json` file has changed. Usually this should be the
    # case, among other things, when there is a new version.
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - "${PACKAGE_JSON_PATH}"
      # This is allowed to fail so that default branch pipelines can be repeated
      allow_failure: false

