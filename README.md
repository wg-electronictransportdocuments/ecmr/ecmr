## Project Overview

This project is an example for a digital _convention relative au contrat de transport international de marchandises par route_ (eCMR) service which can be used for road transports, especially designed for cross-border relations. It can also be seen as a digital implementation of the analog CMR. The aim of the development project is to map all eCMR-related transport operations on the system and to make the planned services available at the relevant stages of the supply chain. This includes the initial creation of the eCMR (e.g. by the customer), the recording of transport information and the storage of the eCMR. The data integrity is given by the usage of the blockchain. The downstream users (consignor, forwarder and consignee) have access to the eCMR, as soon as it is shared with them, and can supplement information, share the document with other users and add signatures. The services developed in the project focus on handling and forwarding the eCMR.

The eCMR service is decomposed into two main services

* **angular-frontend** - The web-interface for the user to interact with the service
* **java-backend** - The management and storage of data that comes from the frontend, the user management and the connection to the blockchain

and is complemented by a hash-token chain service.
For further detail on the functionalities and features of the services, see the documentation and the respective README files.

The current first version (1.2) contains the functional components of the angular-frontend and the java-backend.

|Component| Status |
|---------|--------|
|angular-frontend| 1.2.7  |
|java-backend| 1.1.0  |


### Release-Roadmap
* **Release 1.1:** Development from the Silicon Economy Logistics Ecosystems research project with core functions: Creating, changing and forwarding eCMR. Supporting electronic signature. Writing changes on the blockchain. 
* **Release 1.2:** Currently under development

#### Requirements

* Java 17 (for the backend)
* Node.js 20.11.0  (for the frontend)
* Docker

#### Optional components

* Keyloak (for advanced user management)
* PostgreSQL (for persistance, interchangable)
* GitLab CI/CD (Continuous Integration and Continuous Delivery)

## Setting up the project

  * To run the project for testing purposes, take a look at the [local deployment options](./local-deployment/README.md)
  * For production deployment, you will need to setup a database (default: PostgreSQL) and configure the services environment to your needs. Please take a look at the respective services' README files to do so.
  * Setup an own gitlab-ci.yml, if you are using a gitlab-ci.
  * Configure the config files (/config/) in the services according to your technical requirements. Pay attention to the URLs to the required services and set them to your needs.
  * For local execution: Build the docker containers.

## Running the project

To run the project, either view the specific services' README files or take a look at the [local-deployment](./local-deployment) docker config.

### General Deployment

The eCMR service is intended to be deployed to Kubernetes.

A scripted deployment of each of its components is possible and extensively used by CI/CD for the OKD distribution of Kubernetes. The respective scripts are developed and maintained alongside the source code (within /helm directory) and are part of the same repositories.

To use the Kubernetes Deployment within your GitLab CI/CD Pipeline corresponding deployment configurations must be created for backend and frontend in the root .gitlab-ci.yml.

#### Installation Database for Deployment

The eCMR backend service makes extensive use of a dedicated database. The setup of database tables and (config dependant) injection of testdata is realized by JPA functions. Depending on chosen configuration, an internal H2 database is created and filled with eCMR example data.

## License

Licensed under the Open Logistics Foundation License 1.3.
For details on the licensing terms, see the LICENSE file.

## Licenses of third-party dependencies

For information about licenses of third-party dependencies, please refer to the `README.md` files of the corresponding
components.

## Contact information

Maintainer: 
* Jens Leveling <jens.leveling@iml.fraunhofer.de>

Development Team:

* Product Owner: Patrick Becker, Maximilian Schellert
* Scrum Master: Lukas Lehmann
* Developer: Hermann Foot, Thomas Kirks, Julius Mackowiak, Björn Krämer, Edgar Wilzer
