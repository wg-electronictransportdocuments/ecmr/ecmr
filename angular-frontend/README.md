<!--
Workaround for the license-checker tool and this project's license:

The license-checker tool performs different steps to determine the license of npm packages.
When generating a third-party license report for this project, the tool tries to determine this project's license, too.
Since the Open Logistics Foundation License is currently not contained in the SPDX License List, the license-checker tool fails and ultimately falls back to scanning the README.md file for some license information.
Strangely, the tool then chooses the first URL it can find as the license for this project.
Therefore, this comment is used to have a reliable place for the first link in this document.
In the generated license report a URL as a license makes no sense, of course, but for this project this information is not really relevant anyway.

https://openlogisticsfoundation.org

This is a known issue with the license-checker tool: https://github.com/davglass/license-checker/issues/125
 -->

# eCMR Frontend application

This project was generated with Angular CLI version 11.1.4 and has been updated to Angular version 17.

## Versions

The versions of the runtime environment and the most relevant tools and frameworks used are listed below:

* trion/ng-cli-karma: 17.1.3 (CI Pipeline Base Image)
* Node: 20.11.0 (as used by the trion/ng-cli-karma:17.1.3image to build the application)
* npm: 10.2.4   (as used by the given Node version)
* Angular CLI: 17.1.3

## Developent environment
The frontend application can be run as is without the configuration of any parameters.
It is possible though to set custom values via environment variables with no need to edit source code.
In the frontend application, environment variables are registered under

- `./src/environments/environment.ts`
- `./src/environments/environment.prod.ts`
- `./src/assets/env.js`
- `./src/assets/env.template.js`

`environment.ts` also provides default values which are provided if environment variables are omitted
`environment.prod.ts` overwrites `environment.ts` if the `production` configuration profile is chosen.
`env.js` and `env.template.js` provide environment variables to the client browser during runtime using webpack.

The following list of environment variables can be set to affect the applications behavior:

| Environment Variable           | Type      | Description                                                                                                                                                                                                                                                                                             | Default Value            |
| ------------------------------ | --------- |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| ------------------------ |
| `BACKEND_URL`                  | _string_  | Resource locator of backend instance                                                                                                                                                                                                                                                                    | `http://localhost:8080/` |
| `FRONTEND_URL`                 | _string_  | Resource locator of self                                                                                                                                                                                                                                                                                | `http://localhost:4200/` |
| `INSTANCE_USERMANAGEMENT_TYPE` | _enum_    | Implementation name enum. Possible values are `localdev` and `keycloak.`                                                                                                                                                                                                                                | `localdev`               |
| `INSTANCE_SSL`                 | _boolean_ | Wether or not to expect an SSL certificate. Some web crypto based functionalities require a safe context, so when the SSL-toggle is set to false, they're switched off. This toggle is for testing and development purpose only, and should obviously not be set to `true` in a production environment. | `false`                  |
| `INSTANCE_TITLE`               | _string_  | Instance name, which is rendererd in the application header, as well as the tab name.                                                                                                                                                                                                                   | `eCMR Frontend`          |
| `INSTANCE_COLOR_THEME`         | _enum_    | String that controls the chosen color theme. Possible values are `green`, `blue`, `red`, `yellow` and `custom`                                                                                                                                                                                          | `green`                  |
| `KEYCLOAK_URL`                 | _string_  | Resource locator of keycloak instance                                                                                                                                                                                                                                                                   | `http://localhost:8181/` |
| `KEYCLOAK_REALM`               | _string_  | Keycloak realm identifier                                                                                                                                                                                                                                                                               | `frontend`               |
| `KEYCLOAK_FRONTEND_RESOURCE`   | _string_  | Keycloak frontend resource identifier                                                                                                                                                                                                                                                                   | `ecmr-keycloak`          |


`./environment-files/` contains a collection of scripts which export preconfigured variables when run in a console.
The current version of the frontend brings an abstract interface for authentication with two separate implementations.
An implementation may be chosen by altering the `userManagement.type` property within the provided modes in `./src/environments/environments.ts`, assuming the server is started in dev-mode (by omitting any `--prod` tag when launching).
The first implementation `local-dev` uses a very simplified authentication process which relies on hardcoded users for each role in the eCMR process.
The `keycloak` implementation connects to a keycloak server (tested with v18.0.2), which has to be defined using environment variables, according to the previously mentioned scripts.
As configured in the concurrent CI/CD-files (gitlab, helm) it is also possible to link to Kubernetes secrets.
>**NOTE:** The backend services uses the same abstraction logic to provide this functionality.

## Development server

Run `ng serve` for a dev server.
Navigate to `http://localhost:4200/`.
By default `ng serve` runs the application in dev mode, which sets the base url for the endpoints (back- and frontend) to localhost, too.
Refer to the angular documentation on further information.

## Build

Run `ng build` to build the project.
The build artifacts will be stored in the `dist/` directory.
In the production configuration Docker file, `nginx` is set up to build the application this way prior to serving the `dist/` directory's contents inside the docker container.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
Use the `--code-coverage` flag to analyze code coverage.

Run `ng test --watch false --progress false --code-coverage --browsers=ChromeHeadless` for testing in any environment without an X Server.


## Docker


### Build Docker Image (locally)

Use
```
docker build --build-arg "mode=dev" -t ecmr-frontend .
```
inside the frontend directory to build a local docker image of the frontend application in development mode.
If there's a need to test the connection to the production ingress route of the backend, it's also possible to omit the `--build-arg` flag and build the image in production-mode via
```
docker build -t ecmr-frontend .
```

### Run Docker Image (locally)

Run
```
docker run -p 4200:8080 --name ecmr-frontend-container -it ecmr-frontend
```
to run the image locally on port 4200 (which is optional, but Angular's default).

## Licensing

### Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of this project itself.
The directory contains the following files:
- `third-party-licenses.csv`
Contains the licenses used by this project's third-party dependencies.
The content of this file is/can be **generated**.
- `third-party-licenses-complementary.csv`
Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
The content of this file is maintained **manually**.
- `third-party-licenses-summary.txt`
Contains a summary of all licenses used by the third-party dependencies in this project.
The content of this file is/can be **generated**.

### Generating third-party license reports

**_Please note:
If using Windows, the `Powershell` Command Line may generate wrong character formats.
Using a standard `CMD` Terminal is recommended instead._**

This project uses the [license-checker](https://www.npmjs.com/package/license-checker) to generate a file containing the licenses used by the third-party dependencies.
The `third-party-licenses/third-party-licenses.csv` file can be generated using the following command:
```
npx license-checker --unknown --csv --out ./third-party-licenses/third-party-licenses.csv
```
Third-party dependencies for which the licenses cannot be determined automatically by the license-checker have to be documented manually in `third-party-licenses/third-party-licenses-complementary.csv`.
In the `third-party-licenses/third-party-licenses.csv` file these third-party dependencies have an "UNKNOWN" license.
The `third-party-licenses/third-party-license-summary.txt` file can be generated using the following command:
```
npx license-checker --unknown --summary > ./third-party-licenses/third-party-licenses-summary.txt
```
