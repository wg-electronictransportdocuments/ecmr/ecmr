export BACKEND_URL="http://localhost:8081/"
export FRONTEND_URL="http://localhost:4201/"

export INSTANCE_USERMANAGEMENT_TYPE="localdev"
export INSTANCE_SSL="true"
export INSTANCE_TITLE="eCMR - Instance B"
export INSTANCE_COLOR_THEME="blue"

export KEYCLOAK_FRONTEND_RESOURCE="frontend"
export KEYCLOAK_REALM="ecmr-keycloak-1"
export KEYCLOAK_URL="http://localhost:8181/"
