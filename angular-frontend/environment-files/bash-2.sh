export BACKEND_URL="http://localhost:8082/"
export FRONTEND_URL="http://localhost:4202/"

export INSTANCE_USERMANAGEMENT_TYPE="localdev"
export INSTANCE_SSL="false"
export INSTANCE_TITLE="eCMR - Instance C"
export INSTANCE_COLOR_THEME="yellow"

export KEYCLOAK_FRONTEND_RESOURCE="frontend"
export KEYCLOAK_REALM="ecmr-keycloak-2"
export KEYCLOAK_URL="http://localhost:8181/"
