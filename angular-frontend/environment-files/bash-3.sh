export BACKEND_URL="http://localhost:8083/"
export FRONTEND_URL="http://localhost:4203/"

export INSTANCE_USERMANAGEMENT_TYPE="localdev"
export INSTANCE_SSL="false"
export INSTANCE_TITLE="eCMR - Instance D"
export INSTANCE_COLOR_THEME="red"

export KEYCLOAK_FRONTEND_RESOURCE="frontend"
export KEYCLOAK_REALM="ecmr-keycloak-3"
export KEYCLOAK_URL="http://localhost:8181/"
