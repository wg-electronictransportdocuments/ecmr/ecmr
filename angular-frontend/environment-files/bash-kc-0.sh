export BACKEND_URL="http://localhost:8080/"
export FRONTEND_URL="http://localhost:4200/"

export INSTANCE_USERMANAGEMENT_TYPE="keycloak"
export INSTANCE_SSL="false"
export INSTANCE_TITLE="eCMR - Debug 0"
export INSTANCE_COLOR_THEME="green"

export KEYCLOAK_FRONTEND_RESOURCE="frontend"
export KEYCLOAK_REALM="ecmr-keycloak-0"
export KEYCLOAK_URL="http://localhost:8090/"
