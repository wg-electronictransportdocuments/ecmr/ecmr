export BACKEND_URL="http://localhost:8081/"
export FRONTEND_URL="http://localhost:4201/"

export INSTANCE_USERMANAGEMENT_TYPE="keycloak"
export INSTANCE_SSL="false"
export INSTANCE_TITLE="eCMR - Debug 1"
export INSTANCE_COLOR_THEME="blue"

export KEYCLOAK_FRONTEND_RESOURCE="frontend"
export KEYCLOAK_REALM="ecmr-keycloak-1"
export KEYCLOAK_URL="http://localhost:8090/"
