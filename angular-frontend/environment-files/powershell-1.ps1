$env:BACKEND_URL = "http://localhost:8081/"
$env:FRONTEND_URL = "http://localhost:4201/"

$env:INSTANCE_USERMANAGEMENT_TYPE = "localdev"
$env:INSTANCE_SSL = "false"
$env:INSTANCE_TITLE = "eCMR - Instance B"
$env:INSTANCE_COLOR_THEME = "blue"

$env:KEYCLOAK_FRONTEND_RESOURCE = "frontend"
$env:KEYCLOAK_REALM = "ecmr-keycloak-1"
$env:KEYCLOAK_URL = "http://localhost:8181/"
