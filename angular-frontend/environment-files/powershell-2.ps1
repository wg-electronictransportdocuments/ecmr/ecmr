$env:BACKEND_URL = "http://localhost:8082/"
$env:FRONTEND_URL = "http://localhost:4202/"

$env:INSTANCE_USERMANAGEMENT_TYPE = "localdev"
$env:INSTANCE_SSL = "false"
$env:INSTANCE_TITLE = "eCMR - Instance C"
$env:INSTANCE_COLOR_THEME = "yellow"

$env:KEYCLOAK_FRONTEND_RESOURCE = "frontend"
$env:KEYCLOAK_REALM = "ecmr-keycloak-2"
$env:KEYCLOAK_URL = "http://localhost:8181/"
