$env:BACKEND_URL = "http://localhost:8083/"
$env:FRONTEND_URL = "http://localhost:4203/"

$env:INSTANCE_USERMANAGEMENT_TYPE = "localdev"
$env:INSTANCE_SSL = "false"
$env:INSTANCE_TITLE = "eCMR - Instance D"
$env:INSTANCE_COLOR_THEME = "red"

$env:KEYCLOAK_FRONTEND_RESOURCE = "frontend"
$env:KEYCLOAK_REALM = "ecmr-keycloak-3"
$env:KEYCLOAK_URL = "http://localhost:8181/"
