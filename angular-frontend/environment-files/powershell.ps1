$env:BACKEND_URL = "http://localhost:8080/"
$env:FRONTEND_URL = "http://localhost:4200/"

$env:INSTANCE_USERMANAGEMENT_TYPE = "localdev"
$env:INSTANCE_SSL = "false"
$env:INSTANCE_TITLE = "eCMR - Instance A"
$env:INSTANCE_COLOR_THEME = "green"

$env:KEYCLOAK_FRONTEND_RESOURCE = "frontend"
$env:KEYCLOAK_REALM = "ecmr-keycloak"
$env:KEYCLOAK_URL = "http://localhost:8181/"
