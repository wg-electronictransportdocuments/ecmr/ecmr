/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SettingsPageComponent } from './pages/settings-page/settings-page.component';
import { ErrorPageComponent } from './pages/error-page/error-page.component';
import { ImprintLegalComponent } from './shared/components/imprint-legal/imprint-legal.component';
import { PrivacyDisclaimerComponent } from './shared/components/privacy-disclaimer/privacy-disclaimer.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { OverviewPageComponent } from './pages/overview-page/overview-page.component';
import { EditorPageComponent } from './pages/editor-page/editor-page.component';
import { ViewerPageComponent } from './pages/viewer-page/viewer-page.component';
import { AuthGuard } from './modules/user-management/guards/auth.guard';
import { ClaimEcmrPageComponent } from './pages/claim-ecmr-page/claim-ecmr-page.component';
import { HistoryViewerPageComponent } from './pages/history-viewer-page/history-viewer-page.component';
import { endpoints } from './shared/services/http/endpoints';
import { SharePageComponent } from './pages/share-page/share-page.component';
import { HistoryOverviewPageComponent } from './pages/history-overview-page/history-overview-page.component';
import { environment } from '../environments/environment';
import { ArchivePageComponent } from './pages/archive-page/archive-page.component';
import { TemplatesOverviewPageComponent } from './pages/templates-overview-page/templates-overview-page.component';
import { GuestCarrierComponent } from './pages/guest-carrier/guest-carrier.component';
import { GuestCarrierTanComponent } from './pages/guest-carrier-tan/guest-carrier-tan.component';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: endpoints.frontend.ecmrOverview,
    },
    {
        path: endpoints.frontend.ecmrOverview,
        component: OverviewPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Overview',
    },
    {
        path: endpoints.frontend.shareEcmr,
        component: SharePageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Share Overview',
    },
    {
        path: endpoints.frontend.ecmrTemplatesOverview,
        component: TemplatesOverviewPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Templates'
    },
    {
        path: endpoints.frontend.editEcmr + ':id',
        component: EditorPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Editor',
    },
    {
        path: endpoints.frontend.editEcmr + ':id' + "/tan/:tan",
        component: EditorPageComponent,
        canActivate: [],
        title: environment.instanceTitle + ' - Carrier',
    },
    {
        path: endpoints.frontend.copyEcmr + ':id',
        component: EditorPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Copy eCMR'
    },
    {
        path: endpoints.frontend.ecmrHistory + ':id',
        component: HistoryOverviewPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - History Overview',
    },
    {
        path: endpoints.frontend.showEcmr + ':id',
        component: ViewerPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Viewer',
    },
    {
        path: endpoints.frontend.ecmrHistory + ':id/:numberOfVersions/:version',
        component: HistoryViewerPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - History Viewer',
    },
    {
        path: endpoints.frontend.claimEcmr + ':id',
        component: ClaimEcmrPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Claim eCMR',
    },
    {
        path: endpoints.frontend.settings,
        component: SettingsPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Settings',
    },
    {
        path: endpoints.frontend.privacy,
        component: PrivacyDisclaimerComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Privacy',
    },
    {
        path: endpoints.frontend.imprintLegal,
        component: ImprintLegalComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Imprint and Legal Matter',
    },
    {
        path: endpoints.frontend.login,
        component: LoginPageComponent,
        title: environment.instanceTitle + ' - Login',
    },
    {
        path: endpoints.frontend.archive,
        component: ArchivePageComponent,
        title: environment.instanceTitle + ' - Archive',
    },
    {
        path: endpoints.frontend.guestCarrier + ':id',
        component: GuestCarrierComponent,
        canActivate: [],
        title: environment.instanceTitle + ' - Carrier',
    },
    {
        path: endpoints.frontend.guestCarrierTan + ':id',
        component: GuestCarrierTanComponent,
        canActivate: [],
        title: environment.instanceTitle + ' - Carrier',
    },
    {
        path: '**',
        component: ErrorPageComponent,
        canActivate: [AuthGuard],
        title: environment.instanceTitle + ' - Error',
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
})
export class AppRoutingModule {
}
