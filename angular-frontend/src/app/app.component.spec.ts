/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import {MatDialog, MatDialogModule} from '@angular/material/dialog';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatListModule } from '@angular/material/list';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { AuthService } from './modules/user-management/services/auth.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SnackbarService } from './shared/services/state/snackbar.service';
import { ThemeService } from './shared/services/state/theme.service';
import { SidenavService } from './shared/services/state/sidenav.service';
import { CookieConsentService } from './shared/services/state/cookie-consent.service';
import { UserService } from './shared/services/http/user.service';
import { Overlay, OverlayModule } from '@angular/cdk/overlay';
import { RouterTestingModule } from '@angular/router/testing';
import {
  authServiceStub,
  cookieServiceStub, encryptionServiceStub,
  loadingServiceStub,
  sidenavServiceStub,
  snackbarServiceStub,
  themeServiceStub,
  userProfileServiceStub,
  userServiceStub,
} from './shared/mockups/service-stubs';
import { UserProfileService } from './modules/user-management/services/user-profile.service';
import { LoadingStateService } from './shared/services/state/loading-state.service';
import {
  mockBreakpointObserver,
  resize,
} from './shared/mockups/mock-breakpoint-observer';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {EncryptionService} from './shared/services/encryption/encryption.service';

// TODO: More specific testing needed
describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        RouterTestingModule,
        MatDialogModule,
        MatSidenavModule,
        MatToolbarModule,
        MatIconModule,
        MatCardModule,
        BrowserAnimationsModule,
        MatListModule,
        OverlayModule,
      ],
      declarations: [AppComponent],
      providers: [
        { provide: AuthService, useValue: authServiceStub() },
        { provide: BreakpointObserver, useValue: mockBreakpointObserver },
        { provide: CookieConsentService, useValue: cookieServiceStub() },
        { provide: EncryptionService, useValue: encryptionServiceStub },
        { provide: LoadingStateService, useValue: loadingServiceStub() },
        { provide: MatDialog},
        { provide: MatSnackBar },
        { provide: Overlay },
        { provide: SidenavService, useValue: sidenavServiceStub() },
        { provide: SnackbarService, useValue: snackbarServiceStub() },
        { provide: ThemeService, useValue: themeServiceStub() },
        { provide: UserProfileService, useValue: userProfileServiceStub() },
        { provide: UserService, useValue: userServiceStub() },
      ],
    }).compileComponents();
  });

  describe('Responsiveness', () => {
    it('should activate handset-layout with "XSmall" BP', () => {
      resize(Breakpoints.XSmall);
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      expect(app).toBeTruthy();
    });
    it('should activate handset-layout with "Small" BP', () => {
      resize(Breakpoints.Small);
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      expect(app).toBeTruthy();
    });
    it('should activate desktop-layout with other BPs', () => {
      resize(Breakpoints.Medium);
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      expect(app).toBeTruthy();
    });
  });

  describe('Cookie Consent', () => {
    it('should be obligatory', () => {
      TestBed.overrideProvider(CookieConsentService, {
        useValue: cookieServiceStub(false),
      });
      // TODO: Expect modal Dialog to appear
      const fixture = TestBed.createComponent(AppComponent);
      const app = fixture.componentInstance;
      expect(app).toBeTruthy();
    });
  });
});
