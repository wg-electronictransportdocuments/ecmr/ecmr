/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component} from '@angular/core';
import {BreakpointObserver, Breakpoints} from '@angular/cdk/layout';
import {MatDrawerMode} from '@angular/material/sidenav';
import {MatDialog} from '@angular/material/dialog';
import {UserCompanyDto} from './shared/models/dtos/requests/get-user-dto';
import {UserProfile} from './shared/models/entities/user-profile';
import {UserProfileService} from './modules/user-management/services/user-profile.service';
import {ThemeService} from './shared/services/state/theme.service';
import {SidenavService} from './shared/services/state/sidenav.service';
import {ModeIcon} from './shared/models/ModeIcon';

import {animate, state, style, transition, trigger,} from '@angular/animations';
import {CookieDialogComponent} from './shared/dialogs/cookie-dialog/cookie-dialog.component';
import {CookieConsentService} from './shared/services/state/cookie-consent.service';
import {take} from 'rxjs/operators';
import {AuthService} from './modules/user-management/services/auth.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import {SnackbarService} from './shared/services/state/snackbar.service';
import {UserService} from './shared/services/http/user.service';
import {LoadingStateService} from './shared/services/state/loading-state.service';
import {Overlay, OverlayRef} from '@angular/cdk/overlay';
import {ComponentPortal} from '@angular/cdk/portal';
import {MatSpinner} from '@angular/material/progress-spinner';
import {endpoints} from './shared/services/http/endpoints';
import {EncryptionService} from './shared/services/encryption/encryption.service';
import {environment} from '../environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('indicatorRotation', [
      state('collapsed, void', style({transform: 'rotate(0deg)'})),
      state('expanded', style({transform: 'rotate(180deg)'})),
      transition(
        'expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
    /** Animation that expands and collapses the panel content. */
    trigger('listHeight', [
      state('collapsed, void', style({height: '0px', visibility: 'hidden'})),
      state('expanded', style({height: '*', visibility: 'visible'})),
      transition(
        'expanded <=> collapsed, void => collapsed',
        animate('225ms cubic-bezier(0.4,0.0,0.2,1)')
      ),
    ]),
  ],
})
export class AppComponent {
  title = environment.instanceTitle;
  titleLtSm = this.title;

  ecmrOverviewUrlFragment = '/' + endpoints.frontend.ecmrOverview;
  ecmrTemplateOverviewUrlFragment = '/' + endpoints.frontend.ecmrTemplatesOverview;
  privacyUrlFragment = '/' + endpoints.frontend.privacy;
  imprintLegalUrlFragment = '/' + endpoints.frontend.imprintLegal;
  archiveOverviewUrlFragment = '/' + endpoints.frontend.archive;

  authStatus: boolean;
  userProfile: Promise<UserProfile>;
  userCompany: UserCompanyDto = undefined;

  breakpointStateLtMd = false;
  modeIcon: ModeIcon;
  currentClassName: string;

  sidenavMode: MatDrawerMode = 'side';
  sidenavState = true;
  desktopSidenavState;
  loadingAnimOverlayRef: OverlayRef;

  constructor(
    public authService: AuthService,
    public breakpointObserver: BreakpointObserver,
    public cookieConsentService: CookieConsentService,
    public encryptionService: EncryptionService,
    public loadingStateService: LoadingStateService,
    public matDialog: MatDialog,
    public matSnackBar: MatSnackBar,
    public overlay: Overlay,
    public sidenavService: SidenavService,
    public snackbarService: SnackbarService,
    public themeService: ThemeService,
    public userProfileService: UserProfileService,
    public userService: UserService
  ) {
    this.authStatus = authService.authStatus;

    if (this.authStatus) {
      this.userProfile = userProfileService.userProfile;
      this.userProfile?.then(profile => {
        this.userService.updateUserCompany$(profile.id);
        this.userService.userCompany$.subscribe(r => (this.userCompany = r));
      });
      this.userProfileService.updateUserRoles$();
      // encryptionService.loadLocalStorage();
    }

    this.loadingAnimOverlayRef = this.overlay.create({
      hasBackdrop: true,
      backdropClass: 'loading-animation-backdrop',
      positionStrategy: this.overlay
        .position()
        .global()
        .centerHorizontally()
        .centerVertically(),
    });

    this.breakpointObserver
      .observe([Breakpoints.Small, Breakpoints.XSmall])
      .subscribe(result => {
        if (result.matches) {
          this.activateHandsetLayout();
          this.breakpointStateLtMd = true;
        } else {
          this.deactivateHandsetLayout();
          this.breakpointStateLtMd = false;
        }
      });

    // Application State
    this.sidenavService.getSidenavState().subscribe(r => {
      this.desktopSidenavState = r;
      if (this.breakpointStateLtMd) {
        this.activateHandsetLayout();
      } else {
        this.deactivateHandsetLayout();
      }
    });
    themeService.getModeName$().subscribe(() => {
      this.modeIcon = themeService.getNextModeIcon();
      this.applyClass(themeService.getClassName());
    });
    this.cookieConsentService
      .getCookieConsent()
      .pipe(take(1))
      .subscribe(r => {
        if (!r) {
          const cookieDialogRef = this.matDialog.open(CookieDialogComponent, {
            disableClose: true,
          });
          cookieDialogRef.afterClosed().subscribe(() => {
            this.cookieConsentService.setCookieConsent(true);
          });
        }
      });
    this.snackbarService.getStatusMessage$().subscribe(r => {
      this.matSnackBar.open(r, 'close', {
        duration: this.snackbarService.getNextDuration(),
      });
    });
    this.loadingStateService.getLoadingState$().subscribe(
      r => {
        this.toggleLoadingAnimation(r);
      },
      error => {
        this.snackbarService.pushNewStatus(error);
      }
    );
  }

  applyClass(className: string): void {
    if (this.currentClassName !== className) {
      document.body.classList.add(className);
      document.body.classList.remove(this.currentClassName);
      this.currentClassName = className;
    }
  }

  onClickLogout(): void {
    this.encryptionService.unsetKeys();
    this.authService.logout();
  }

  private activateHandsetLayout() {
    this.sidenavMode = 'over';
    this.sidenavState = false;
  }

  private deactivateHandsetLayout() {
    this.sidenavMode = 'side';
    this.sidenavState = this.desktopSidenavState;
  }

  private toggleLoadingAnimation(show: boolean): void {
    if (show) {
      this.loadingAnimOverlayRef.attach(new ComponentPortal(MatSpinner));
    } else {
      this.loadingAnimOverlayRef.detach();
    }
  }

}
