/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {ErrorPageComponent} from './pages/error-page/error-page.component';
import {FocusBlurDirective} from './shared/directives/focus-blur.directive';
import {ImprintLegalComponent} from './shared/components/imprint-legal/imprint-legal.component';
import {PrivacyDisclaimerComponent} from './shared/components/privacy-disclaimer/privacy-disclaimer.component';
import {SettingsPageComponent} from './pages/settings-page/settings-page.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list';
import {CommonModule} from '@angular/common';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatSelectModule} from '@angular/material/select';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatDialogModule} from '@angular/material/dialog';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserManagementModule} from './modules/user-management/user-management.module';
import {ThemeService} from './shared/services/state/theme.service';
import {SidenavService} from './shared/services/state/sidenav.service';
import {CookieConsentService} from './shared/services/state/cookie-consent.service';
import {CookieDialogComponent} from './shared/dialogs/cookie-dialog/cookie-dialog.component';
import {LoginPageComponent} from './pages/login-page/login-page.component';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {OverviewPageComponent} from './pages/overview-page/overview-page.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatTableModule} from '@angular/material/table';
import {MatOptionModule, MatRippleModule} from '@angular/material/core';
import {MatSortModule} from '@angular/material/sort';
import {EditorPageComponent} from './pages/editor-page/editor-page.component';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatFormFieldModule} from '@angular/material/form-field';
import {UserService} from './shared/services/http/user.service';
import {JwtModule} from '@auth0/angular-jwt';
import {HttpClientModule} from '@angular/common/http';
import {ShareQrDialogComponent} from './shared/dialogs/share-qr-dialog/share-qr-dialog.component';
import {QRCodeModule} from 'angularx-qrcode';
import {ShareEcmrService} from './shared/services/state/share-ecmr.service';
import {TableStateService} from './shared/services/state/table-state.service';
import {ViewerPageComponent} from './pages/viewer-page/viewer-page.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatBadgeModule} from '@angular/material/badge';
import {ViewerComponent} from './shared/components/viewer-component/viewer.component';
import {EcmrService} from './shared/services/http/ecmr.service';
import {EcmrListService} from './shared/services/http/ecmr-list.service';
import {ConfirmationDialogComponent} from './shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import {ClaimEcmrPageComponent} from './pages/claim-ecmr-page/claim-ecmr-page.component';
import {MatTooltipModule} from '@angular/material/tooltip';
import {ClipboardModule} from '@angular/cdk/clipboard';
import {HistoryViewerPageComponent} from './pages/history-viewer-page/history-viewer-page.component';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {SharePageComponent} from './pages/share-page/share-page.component';
import {HistoryOverviewPageComponent} from './pages/history-overview-page/history-overview-page.component';
import {PrintPdfDialogComponent} from './shared/dialogs/print-pdf-dialog/print-pdf-dialog.component';
import {ImportDialogComponent} from './shared/dialogs/import-dialog/import-dialog.component';
import {SignaturePadModule} from 'angular-signature-pad-v2';
import {ZXingScannerModule} from '@zxing/ngx-scanner';
import {SignOnGlassDialogComponent} from './shared/dialogs/sign-on-glass-dialog/sign-on-glass-dialog.component';
import {DropdownButtonComponent} from './shared/components/dropdown-button/dropdown-button.component';
import {MatMenuModule} from '@angular/material/menu';
import {ArchivePageComponent} from './pages/archive-page/archive-page.component';
import {TemplatesOverviewPageComponent} from './pages/templates-overview-page/templates-overview-page.component';
import {TemplateNameDialogComponent} from './shared/dialogs/template-name-dialog/template-name-dialog.component';
import {
  LoadFromTemplateDialogComponent
} from './shared/dialogs/load-from-template-dialog/load-from-template-dialog.component';
import {MatButtonToggleModule} from '@angular/material/button-toggle';

@NgModule({
  declarations: [
    AppComponent,
    ErrorPageComponent,
    FocusBlurDirective,
    ImprintLegalComponent,
    PrivacyDisclaimerComponent,
    SettingsPageComponent,
    CookieDialogComponent,
    OverviewPageComponent,
    LoginPageComponent,
    EditorPageComponent,
    ShareQrDialogComponent,
    ViewerPageComponent,
    ViewerComponent,
    ConfirmationDialogComponent,
    ClaimEcmrPageComponent,
    HistoryViewerPageComponent,
    PrintPdfDialogComponent,
    SharePageComponent,
    HistoryOverviewPageComponent,
    ImportDialogComponent,
    SignOnGlassDialogComponent,
    DropdownButtonComponent,
    ArchivePageComponent,
    TemplatesOverviewPageComponent,
    TemplateNameDialogComponent,
    LoadFromTemplateDialogComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,

    // Custom Modules:
    MatToolbarModule,
    MatIconModule,
    MatSidenavModule,
    MatListModule,
    CommonModule,
    MatButtonModule,
    ReactiveFormsModule,
    MatSelectModule,
    FormsModule,
    MatSlideToggleModule,
    MatCardModule,
    MatDialogModule,
    MatInputModule,
    MatCheckboxModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatTableModule,
    MatRippleModule,
    MatSortModule,
    MatFormFieldModule,
    MatGridListModule,
    MatPaginatorModule,
    MatOptionModule,
    JwtModule,
    HttpClientModule,
    QRCodeModule,
    MatBadgeModule,
    MatTooltipModule,
    ClipboardModule,
    AngularSvgIconModule.forRoot(),
    UserManagementModule,
    SignaturePadModule,
    ZXingScannerModule,
    MatMenuModule,
    MatButtonToggleModule,
  ],
  providers: [
    CookieConsentService,
    ThemeService,
    SidenavService,
    UserService,
    ShareEcmrService,
    TableStateService,
    EcmrService,
    EcmrListService,
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
}
