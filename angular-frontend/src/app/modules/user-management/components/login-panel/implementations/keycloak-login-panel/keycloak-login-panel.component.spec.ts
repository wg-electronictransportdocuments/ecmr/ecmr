/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KeycloakLoginPanelComponent } from './keycloak-login-panel.component';
import { MatCardModule } from '@angular/material/card';

describe('KeycloakLoginPanelComponent', () => {
  let component: KeycloakLoginPanelComponent;
  let fixture: ComponentFixture<KeycloakLoginPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatCardModule],
      declarations: [KeycloakLoginPanelComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KeycloakLoginPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
