/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { AuthService } from '../../../../services/auth.service';

@Component({
  selector: 'app-keycloak-login-panel',
  templateUrl: './keycloak-login-panel.component.html',
  styleUrls: ['./keycloak-login-panel.component.scss'],
})
export class KeycloakLoginPanelComponent {
  authStatus: boolean;

  constructor(public authService: AuthService) {
    this.authStatus = authService.authStatus;
  }
}
