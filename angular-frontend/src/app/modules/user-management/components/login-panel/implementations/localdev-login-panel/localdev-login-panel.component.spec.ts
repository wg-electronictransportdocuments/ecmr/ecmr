/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LocaldevLoginPanelComponent } from './localdev-login-panel.component';
import { AuthService } from '../../../../services/auth.service';
import {
  authServiceStub,
  userProfileServiceStub,
} from '../../../../../../shared/mockups/service-stubs';
import { UserProfileService } from '../../../../services/user-profile.service';
import { MatCardModule } from '@angular/material/card';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { MatSelectModule } from '@angular/material/select';
import { MatOptionModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';

describe('LocaldevLoginPanelComponent', () => {
  let component: LocaldevLoginPanelComponent;
  let fixture: ComponentFixture<LocaldevLoginPanelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [LocaldevLoginPanelComponent],
      imports: [
        MatCardModule,
        FormsModule,
        MatSelectModule,
        MatOptionModule,
        MatButtonModule,
        BrowserAnimationsModule,
      ],
      providers: [
        { provide: AuthService, useValue: authServiceStub() },
        { provide: UserProfileService, useValue: userProfileServiceStub() },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LocaldevLoginPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
