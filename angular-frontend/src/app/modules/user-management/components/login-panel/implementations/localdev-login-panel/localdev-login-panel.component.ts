/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { UserProfile } from '../../../../../../shared/models/entities/user-profile';
import { userProfiles } from '../../../../environments/implementations/localdev-environment';
import { AuthService } from '../../../../services/auth.service';
import { UserProfileService } from '../../../../services/user-profile.service';

@Component({
  selector: 'app-localdev-login-panel',
  templateUrl: './localdev-login-panel.component.html',
  styleUrls: ['./localdev-login-panel.component.scss'],
})
export class LocaldevLoginPanelComponent {
  userProfiles = [];
  selectedUserProfile: UserProfile;

  constructor(
    public authService: AuthService,
    public userProfileService: UserProfileService
  ) {
    this.userProfiles = userProfiles;
    this.userProfileService.userProfile.then(profile => {
      if (profile) {
        const index = this.userProfiles.findIndex(
          search => search.profile.username === profile.username
        );
        this.selectedUserProfile =
          index >= 0 ? this.userProfiles[index].profile : undefined;
      } else {
        this.selectedUserProfile = this.userProfiles[5].profile;
      }
    });
  }

  onLogin(): void {
    this.userProfileService.userProfile = new Promise<UserProfile>(resolve =>
      resolve(this.selectedUserProfile)
    );
    this.authService.login();
  }

  onLogout(): void {
    this.userProfileService.userProfile = new Promise<UserProfile>(resolve =>
      resolve(undefined)
    );
    this.authService.logout();
  }

  reset() {
    localStorage.clear();
    window.location.reload();
  }
}
