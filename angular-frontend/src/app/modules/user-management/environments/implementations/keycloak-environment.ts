/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const keycloakEnvironment = {
  keycloakUrl: $ENV.KEYCLOAK_URL,
  keycloakResource: $ENV.KEYCLOAK_FRONTEND_RESOURCE,
  keycloakRealm: $ENV.KEYCLOAK_REALM,
};
