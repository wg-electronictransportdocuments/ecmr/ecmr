/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { UserProfile } from '../../../../shared/models/entities/user-profile';

export const userProfiles: { label: string; profile: UserProfile }[] = [
  {
    label: 'Nils Vetter',
    profile: {
      username: 'nils.vetter@ecmr.de',
      id: '123-456-789-1',
      email: 'nils.vetter@ecmr.de',
      firstName: 'Nils',
      lastName: 'Vetter',
    },
  },
  {
    label: 'Annette Greiner',
    profile: {
      username: 'annette.greiner@ecmr.de',
      id: '123-456-789-2',
      email: 'annette.greiner@ecmr.de',
      firstName: 'Annette',
      lastName: 'Greiner',
    },
  },
  {
    label: 'Franz Pankerl',
    profile: {
      username: 'franz.pankerl@ecmr.de',
      id: '123-456-789-3',
      email: 'franz.pankerl@ecmr.de',
      firstName: 'Franz',
      lastName: 'Pankerl',
    },
  },
  {
    label: 'Uwe Senft',
    profile: {
      username: 'uwe.senft@ecmr.de',
      id: '123-456-789-4',
      email: 'uwe.senft@ecmr.dee',
      firstName: 'Uwe',
      lastName: 'Senft',
    },
  },
  {
    label: 'Judith Leineweber',
    profile: {
      username: 'judith.leineweber@ecmr.de',
      id: '123-456-789-5',
      email: 'judith.leineweber@ecmr.de',
      firstName: 'Judith',
      lastName: 'Leineweber',
    },
  },
  {
    label: 'Silvia Pagels',
    profile: {
      username: 'silvia.pagels@ecmr.de',
      id: '123-456-789-6',
      email: 'silvia.pagels@ecmr.de',
      firstName: 'Silvia',
      lastName: 'Pagels',
    },
  },
];
