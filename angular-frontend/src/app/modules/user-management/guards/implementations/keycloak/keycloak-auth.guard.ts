/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import {
  KeycloakAuthGuard as KcAuthGuard,
  KeycloakService,
} from 'keycloak-angular';
import {endpoints} from '../../../../../shared/services/http/endpoints';
import {AuthService} from '../../../services/auth.service';

@Injectable({
  providedIn: 'root',
})
export class KeycloakAuthGuard extends KcAuthGuard {
  loginUrlFragment = '/' + endpoints.frontend.login;

  constructor(
    public keycloakService: KeycloakService,
    public router: Router,
    public authService: AuthService
  ) {
    super(router, keycloakService);
  }

  async isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    this.authService.redirectUrl = state.url;
    this.authService.redirectId = route.params?.id;
    if (!this.authenticated) {
      this.router.navigate([this.loginUrlFragment]);
    }
    return this.authenticated;
  }
}
