/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { LocaldevAuthGuard } from './localdev-auth.guard';

describe('LocaldevAuthGuard', () => {
  let guard: LocaldevAuthGuard;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    guard = TestBed.inject(LocaldevAuthGuard);
  });

  it('should be created', () => {
    expect(guard).toBeTruthy();
  });
});
