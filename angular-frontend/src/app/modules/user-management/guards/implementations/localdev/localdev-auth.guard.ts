/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { endpoints } from '../../../../../shared/services/http/endpoints';
import { AuthService } from '../../../services/auth.service';
import { AuthGuard } from '../../auth.guard';

@Injectable({
  providedIn: 'root',
})
export class LocaldevAuthGuard extends AuthGuard {
  loginUrlFragment = '/' + endpoints.frontend.login;

  constructor(public router: Router, public authService: AuthService) {
    super();
  }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    return this.isAccessAllowed(route, state);
  }

  async isAccessAllowed(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<boolean | UrlTree> {
    this.authService.redirectUrl = state.url;
    this.authService.redirectId = route.params?.id;
    const authenticated = await this.authService.authStatus;
    if (!authenticated) {
      this.router.navigate([this.loginUrlFragment]);
    }
    return authenticated;
  }
}
