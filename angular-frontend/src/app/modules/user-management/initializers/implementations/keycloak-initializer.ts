/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { KeycloakService } from 'keycloak-angular';
import { environment as env } from '../../../../../environments/environment';

export const initializeKeycloak = (keycloak: KeycloakService) => () =>
  keycloak.init({
    config: {
      url: env.keycloakUrl,
      realm: env.keycloakRealm,
      clientId: env.keycloakResource,
    },
    initOptions: {
      onLoad: 'check-sso',
      silentCheckSsoRedirectUri:
        window.location.origin + '/assets/silent-check-sso.html',
    },
    enableBearerInterceptor: false,
  });
