/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { from, lastValueFrom, Observable } from 'rxjs';
import { AuthInterceptor } from '../../auth.interceptor';

@Injectable()
export class KeycloakAuthInterceptor extends AuthInterceptor {
  constructor(public keycloakService: KeycloakService) {
    super();
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this.handle(req, next));
  }

  async handle(request: HttpRequest<unknown>, next: HttpHandler) {
    const token = await this.keycloakService.getToken();
    let authenticatedRequest;
    if (token) {
      authenticatedRequest = request.clone({
        setHeaders: { authorization: 'bearer ' + token },
      });
    } else {
      authenticatedRequest = request;
    }
    return await lastValueFrom(next.handle(authenticatedRequest));
  }
}
