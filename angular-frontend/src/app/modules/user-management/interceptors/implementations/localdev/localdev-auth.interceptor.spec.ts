/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { LocaldevAuthInterceptor } from './localdev-auth.interceptor';

describe('LocaldevAuthInterceptor', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [LocaldevAuthInterceptor],
    })
  );

  it('should be created', () => {
    const interceptor: LocaldevAuthInterceptor = TestBed.inject(
      LocaldevAuthInterceptor
    );
    expect(interceptor).toBeTruthy();
  });
});
