/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { from, lastValueFrom, Observable } from 'rxjs';
import { UserProfileService } from '../../../services/user-profile.service';
import { AuthInterceptor } from '../../auth.interceptor';

@Injectable()
export class LocaldevAuthInterceptor extends AuthInterceptor {
  constructor(public userProfileService: UserProfileService) {
    super();
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return from(this.handle(req, next));
  }

  async handle(request: HttpRequest<unknown>, next: HttpHandler) {
    const userProfile = await this.userProfileService.userProfile;
    let authenticatedRequest;
    if (userProfile) {
      authenticatedRequest = request.clone({
        setHeaders: { authorization: userProfile.username },
      });
    } else {
      authenticatedRequest = request;
    }
    return await lastValueFrom(next.handle(authenticatedRequest));
  }
}
