/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export abstract class AuthService {
  /** AuthService has to redirect to loginPage on missing authentication */
  abstract loginPageUrlFragment: string;
  /** AuthService has to redirect to overviewPage on successful authentication */
  abstract overviewPageUrlFragment: string;
  /** AuthService has to redirect to respective page on successful authentication when another page than login was requested before */
  abstract redirectUrl: string;
  /** AuthService has to redirect to respective eCMR-page on successful authentication when another page than login was requested before */
  abstract redirectId: string;

  protected constructor() {}

  /** AuthService has to provide an async function to alter the authStatus from false to true */
  abstract login(): Promise<void>;

  /** AuthService has to provide an async function to alter the authStatus from true to false */
  abstract logout(): Promise<void>;

  /** AuthService has to provide async access to the authStatus */
  abstract get authStatus(): boolean;
}
