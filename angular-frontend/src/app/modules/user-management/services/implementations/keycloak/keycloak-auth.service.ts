/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { environment as env } from '../../../../../../environments/environment';
import { endpoints } from '../../../../../shared/services/http/endpoints';
import { AuthService } from '../../auth.service';

@Injectable({
  providedIn: 'root',
})
export class KeycloakAuthService extends AuthService {
  public loginPageUrlFragment = endpoints.frontend.login;
  public overviewPageUrlFragment = endpoints.frontend.ecmrOverview;

  public redirectUrl: string;
  public redirectId: string;

  constructor(public keycloakService: KeycloakService) {
    super();
  }

  public async login() {
    if (this.redirectUrl) {
      this.redirectUrl =
        env.frontendUrl +
        this.redirectUrl.replace(
          endpoints.frontend.editEcmr,
          endpoints.frontend.showEcmr
        );
      await this.keycloakService.login({
        redirectUri: this.redirectUrl,
      });
    } else {
      await this.keycloakService.login({
        redirectUri: env.frontendUrl + this.overviewPageUrlFragment,
      });
    }
  }

  public async logout() {
    await this.keycloakService.logout(
      env.frontendUrl + this.loginPageUrlFragment
    );
  }

  public get authStatus(): boolean {
    return this.keycloakService.isLoggedIn();
  }
}
