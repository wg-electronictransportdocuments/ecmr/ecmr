/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserProfile } from '../../../../../shared/models/entities/user-profile';
import { UserProfileService } from '../../user-profile.service';

@Injectable({
  providedIn: 'root',
})
export class KeycloakUserProfileService extends UserProfileService {
  disponentRoleKey = 'disponent';
  superuserRoleKey = 'superuser';
  carrierRoleKey = 'fahrer';
  userRoles: BehaviorSubject<string[]> = new BehaviorSubject<string[]>(null);

  isSuperuser: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  // TODO: begone!
  isDisponent: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  isCarrier: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private keycloakService: KeycloakService) {
    super();
  }

  public get userProfile(): Promise<UserProfile> {
    return this.keycloakService.loadUserProfile() as Promise<UserProfile>;
  }

  public get isSuperuser$(): Observable<boolean> {
    return this.isSuperuser as Observable<boolean>;
  }

  public get isDisponent$(): Observable<boolean> {
    return this.isDisponent as Observable<boolean>;
  }

  public get isCarrier$(): Observable<boolean> {
    return this.isCarrier as Observable<boolean>;
  }

  public get userRoles$(): Observable<string[]> {
    return this.userRoles.asObservable();
  }

  public updateUserRoles$(addRoles?: string[]): void {
    const allRoles = this.keycloakService.getUserRoles(true);
    if(addRoles) {
      allRoles.concat(addRoles);
    }
    const ecmrRoles: string[] = [];
    allRoles.forEach(role => {
      if (role.indexOf('ecmr-') === 0) {
        ecmrRoles.push(role.slice(5));
        if (role.slice(5) === this.superuserRoleKey) {
          this.isSuperuser.next(true);
        }
        if (role.slice(5) === this.disponentRoleKey) {
          this.isDisponent.next(true);
        }
        if(role.slice(5) === this.carrierRoleKey) {
          this.isCarrier.next(true);
        }
      }
    });
    this.userRoles.next(ecmrRoles);
  }

  public updateUserRolesForCarrier$(): void {
      this.updateUserRoles$(['ecmr-fahrer']);
  }
}
