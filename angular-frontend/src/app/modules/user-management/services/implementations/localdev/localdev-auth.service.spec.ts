/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { LocaldevAuthService } from './localdev-auth.service';

describe('LocaldevAuthService', () => {
  let service: LocaldevAuthService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LocaldevAuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
