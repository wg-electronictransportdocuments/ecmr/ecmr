/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { endpoints } from '../../../../../shared/services/http/endpoints';
import { LoadingStateService } from '../../../../../shared/services/state/loading-state.service';
import { AuthService } from '../../auth.service';

@Injectable({
  providedIn: 'root',
})
export class LocaldevAuthService extends AuthService {
  readonly localStorageKey = 'authStatus';

  public loginPageUrlFragment = endpoints.frontend.login;
  public overviewPageUrlFragment = endpoints.frontend.ecmrOverview;
  public redirectUrl: string;
  public redirectId: string;

  _authStatus: boolean;

  constructor(
    public router: Router,
    public loadingStateService: LoadingStateService
  ) {
    super();
    this._authStatus = this.getLocalStorage();
  }

  get authStatus(): boolean {
    return this._authStatus;
  }

  public async login(): Promise<void> {
    this.loadingStateService.setLoadingState$(true);
    this._authStatus = true;
    this.setLocalStorage(this._authStatus);
    if (this.redirectUrl) {
      this.redirectUrl = this.redirectUrl.replace(
        endpoints.frontend.editEcmr,
        endpoints.frontend.showEcmr
      );
      window.location.href = this.redirectUrl;
    } else {
      window.location.href = this.overviewPageUrlFragment;
    }
  }

  public async logout(): Promise<void> {
    this.loadingStateService.setLoadingState$(true);
    this._authStatus = false;
    this.setLocalStorage(this._authStatus);
    window.location.href = this.loginPageUrlFragment;
  }

  private setLocalStorage(authStatus: boolean) {
    localStorage.setItem(this.localStorageKey, authStatus.toString());
  }

  private getLocalStorage(): boolean {
    return localStorage.getItem(this.localStorageKey) === 'true';
  }

  private modifyRoutingStrategy(reload: boolean) {
    this.router.routeReuseStrategy.shouldReuseRoute = () => !reload;
    this.router.onSameUrlNavigation = reload ? 'reload' : 'ignore';
  }
}
