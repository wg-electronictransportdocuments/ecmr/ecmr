/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { UserProfile } from '../../../../../shared/models/entities/user-profile';
import { SnackbarService } from '../../../../../shared/services/state/snackbar.service';
import { UserProfileService } from '../../user-profile.service';
import { resolve } from '@angular/compiler-cli';

@Injectable({
  providedIn: 'root',
})
export class LocaldevUserProfileService extends UserProfileService {
  readonly localStorageKey = 'userprofile';
  isDisponent: BehaviorSubject<boolean>;
  isSuperuser: BehaviorSubject<boolean>;
  userRoles: Subject<string[]>;

  private _userProfile: UserProfile = undefined;

  constructor(public snackbarService: SnackbarService) {
    super();
    this._userProfile = this.getLocalStorage();
  }

  get isDisponent$(): Observable<boolean> {
    return of(false);
  }

  get isSuperuser$(): Observable<boolean> {
    return of(true);
  }

  public get userRoles$(): Observable<string[]> {
    return this.userRoles.asObservable();
  }

  get userProfile(): Promise<UserProfile> {
    return new Promise(resolve => resolve(this._userProfile));
  }

  set userProfile(userProfile: Promise<UserProfile>) {
    userProfile.then(profile => {
      this._userProfile = profile;
      this.setLocalStorage(this._userProfile);
    });
  }

  updateUserRoles$(addRoles?: string[]): void {}

  private setLocalStorage(userProfile: UserProfile) {
    localStorage.setItem(this.localStorageKey, JSON.stringify(userProfile));
  }

  private getLocalStorage(): UserProfile {
    let profile: UserProfile;
    const storage = localStorage.getItem(this.localStorageKey);
    if (storage) {
      try {
        profile = JSON.parse(storage) as UserProfile;
      } catch (e) {
        this.snackbarService.pushNewStatus(
          'Corrupt Local Browser Storage. Login has been reset.'
        );
      }
    }
    return profile;
  }

  updateUserRolesForCarrier$(): void {}
}
