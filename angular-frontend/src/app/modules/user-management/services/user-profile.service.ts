/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { UserProfile } from '../../../shared/models/entities/user-profile';

@Injectable({
  providedIn: 'root',
})
export abstract class UserProfileService {
  /** UserProfileService has to provide the possible userRoles */
  abstract userRoles: Subject<string[]>;
  /** UserProfileService has to provide the user's superuser property */
  abstract isSuperuser: BehaviorSubject<boolean>;
  /** UserProfileService has to provide the user's disponent property */
  abstract isDisponent: BehaviorSubject<boolean>;

  /** UserProfileService has to provide an async function to retrieve the current user profile */
  abstract get userProfile(): Promise<UserProfile>;

  // TODO: begone!
  /** UserProfileService has to provide an async function to return the disponent property */
  abstract get isDisponent$(): Observable<boolean>;

  /** UserProfileService has to provide an async function to return the superuser property */
  abstract get isSuperuser$(): Observable<boolean>;

  abstract get userRoles$(): Observable<string[]>;

  /** UserProfileService has to provide an async function to alter the current user profile */
  abstract set userProfile(userProfile: Promise<UserProfile>);

  /** UserProfileService has to provide a function to initiate an update of a users role */
  abstract updateUserRoles$(addRoles?: string[]): void;

  abstract updateUserRolesForCarrier$(): void;
}
