/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { CommonModule } from '@angular/common';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { KeycloakService } from 'keycloak-angular';
import { environment } from '../../../environments/environment';
import { KeycloakLoginPanelComponent } from './components/login-panel/implementations/keycloak-login-panel/keycloak-login-panel.component';
import { LocaldevLoginPanelComponent } from './components/login-panel/implementations/localdev-login-panel/localdev-login-panel.component';
import { LoginPanelComponent } from './components/login-panel/login-panel.component';
import { AuthGuard } from './guards/auth.guard';
import { KeycloakAuthGuard } from './guards/implementations/keycloak/keycloak-auth.guard';
import { LocaldevAuthGuard } from './guards/implementations/localdev/localdev-auth.guard';
import { initializeKeycloak } from './initializers/implementations/keycloak-initializer';
import { KeycloakAuthInterceptor } from './interceptors/implementations/keycloak/keycloak-auth.interceptor';
import { LocaldevAuthInterceptor } from './interceptors/implementations/localdev/localdev-auth.interceptor';
import { AuthService } from './services/auth.service';
import { KeycloakAuthService } from './services/implementations/keycloak/keycloak-auth.service';
import { KeycloakUserProfileService } from './services/implementations/keycloak/keycloak-user-profile.service';
import { LocaldevAuthService } from './services/implementations/localdev/localdev-auth.service';
import { LocaldevUserProfileService } from './services/implementations/localdev/localdev-user-profile.service';
import { UserProfileService } from './services/user-profile.service';

@NgModule({
  imports: [
    CommonModule,
    MatCardModule,
    MatButtonModule,
    MatSelectModule,
    FormsModule,
  ],
  declarations: [
    LoginPanelComponent,
    KeycloakLoginPanelComponent,
    LocaldevLoginPanelComponent,
  ],
  providers: [
    KeycloakService,
    {
      provide: AuthService,
      deps: [KeycloakAuthService, LocaldevAuthService],
      useFactory: (
        keycloak: KeycloakAuthService,
        localdev: LocaldevAuthService
        // TODO Implement others
      ) => {
        switch (environment.userManagement) {
          case 'localdev': {
            return localdev;
          }
          case 'database': {
            throw new Error(
              'UserManagement of type database was not implemented yet'
            );
          }
          case 'keycloak': {
            return keycloak;
          }
          default: {
            throw new Error('No default UserManagement type was set yet');
          }
        }
      },
    },
    {
      provide: AuthGuard,
      deps: [KeycloakAuthGuard, LocaldevAuthGuard],
      useFactory: (
        keycloak: KeycloakAuthGuard,
        localdev: LocaldevAuthGuard
        // TODO: Implement others
      ) => {
        switch (environment.userManagement) {
          case 'localdev': {
            return localdev;
          }
          case 'database': {
            throw new Error(
              'UserManagement of type database was not implemented yet'
            );
          }
          case 'keycloak': {
            return keycloak;
          }
          default: {
            throw new Error('No default UserManagement type was set yet');
          }
        }
      },
    },
    {
      provide: HTTP_INTERCEPTORS,
      deps: [KeycloakService, UserProfileService],
      useFactory: (
        keycloakService: KeycloakService,
        userProfileService: UserProfileService
      ) => {
        switch (environment.userManagement) {
          case 'localdev': {
            return new LocaldevAuthInterceptor(userProfileService);
          }
          case 'database': {
            throw new Error(
              'UserManagement of type database was not implemented yet'
            );
          }
          case 'keycloak': {
            return new KeycloakAuthInterceptor(keycloakService);
          }
          default: {
            throw new Error('No default UserManagement type was set yet');
          }
        }
      },
      multi: true,
    },
    {
      provide: UserProfileService,
      deps: [KeycloakUserProfileService, LocaldevUserProfileService],
      useFactory: (
        keycloak: KeycloakUserProfileService,
        localdev: LocaldevUserProfileService
        // TODO: Implement others
      ) => {
        switch (environment.userManagement) {
          case 'localdev': {
            return localdev;
          }
          case 'database': {
            throw new Error(
              'UserManagement of type database was not implemented yet'
            );
          }
          case 'keycloak': {
            return keycloak;
          }
          default: {
            throw new Error('No default UserManagement type was set yet');
          }
        }
      },
    },
    {
      provide: APP_INITIALIZER,
      deps: [KeycloakService],
      useFactory: (keycloakService: KeycloakService) => {
        if (environment.userManagement === 'keycloak') {
          return initializeKeycloak(keycloakService);
        } else {
          return () => {};
        }
      },
      multi: true,
    },
  ],
  exports: [
    LocaldevLoginPanelComponent,
    KeycloakLoginPanelComponent,
    LoginPanelComponent,
  ],
})
export class UserManagementModule {}
