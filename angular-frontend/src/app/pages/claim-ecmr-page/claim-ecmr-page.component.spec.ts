/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ClaimEcmrPageComponent } from './claim-ecmr-page.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { EcmrUserService } from '../../shared/services/http/ecmr-user.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ecmrUserServiceStub } from '../../shared/mockups/service-stubs';

// TODO: Use DialogHarnessModule for testing, also more specific testing needed
describe('ClaimEcmrPageComponent', () => {
  let component: ClaimEcmrPageComponent;
  let fixture: ComponentFixture<ClaimEcmrPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: { get: () => 'value' } } },
        },
        {
          provide: EcmrUserService,
          useValue: ecmrUserServiceStub(),
        },
        {
          provide: Router,
          useValue: {
            navigate: () => {},
          },
        },
      ],
      imports: [MatDialogModule, BrowserAnimationsModule],
      declarations: [ClaimEcmrPageComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimEcmrPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should call EcmrUserService on consent', done => {
    component.dialogRef.close(true);
    expect(component).toBeTruthy();
    done();
  });

  it('should call EcmrUserService on dissent', done => {
    component.dialogRef.close(false);
    expect(component).toBeTruthy();
    done();
  });
});
