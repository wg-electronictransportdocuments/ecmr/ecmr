/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ConfirmationDialogComponent,
  ConfirmDialogModel,
} from '../../shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { EcmrUserService } from '../../shared/services/http/ecmr-user.service';

@Component({
  selector: 'app-claim-ecmr-page',
  templateUrl: './claim-ecmr-page.component.html',
  styleUrls: ['./claim-ecmr-page.component.scss'],
})
export class ClaimEcmrPageComponent implements OnInit {
  ecmrId: string;
  dialogRef: MatDialogRef<ConfirmationDialogComponent>;

  constructor(
    public matDialog: MatDialog,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public ecmrUserService: EcmrUserService
  ) {
    this.ecmrId = activatedRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.confirmDialog(
      `Do you want to claim eCMR '${this.ecmrId}' for yourself?`,
      'Claim eCMR'
    );
  }

  private confirmDialog(message: string, title: string): void {
    const dialogData = new ConfirmDialogModel(title, message);
    this.dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
      maxWidth: '400px',
      data: dialogData,
    });

    this.dialogRef.afterClosed().subscribe((dialogResult) => {
      if (dialogResult) {
        this.ecmrUserService.submitClaimRequest(this.ecmrId);
      }
    });
  }
}
