/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { EditorPageComponent } from './editor-page.component';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule, UntypedFormArray, UntypedFormControl, UntypedFormGroup, } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RouterTestingModule } from '@angular/router/testing';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { UserService } from '../../shared/services/http/user.service';
import { EcmrService } from '../../shared/services/http/ecmr.service';
import { mockEcmr, mockFieldArray } from '../../shared/mockups/mock-objects';
import { MatDialogModule } from '@angular/material/dialog';
import { EncryptionService } from '../../shared/services/encryption/encryption.service';
import { AuthService } from '../../modules/user-management/services/auth.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { MatButtonModule } from '@angular/material/button';
import { OverlayContainer } from '@angular/cdk/overlay';
import { By } from '@angular/platform-browser';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatExpansionPanelHarness } from '@angular/material/expansion/testing';
import { MatBadgeModule } from '@angular/material/badge';
import { MatButtonHarness } from '@angular/material/button/testing';
import { fieldIds, placeholderEcmrFields, } from '../../shared/models/entities/fields';
import { MatFormFieldHarness } from '@angular/material/form-field/testing';
import { positionFieldIds } from '../../shared/models/entities/position';
import { MatInputHarness } from '@angular/material/input/testing';
import { DebugElement } from '@angular/core';
import {
    authServiceStub,
    ecmrServiceStub,
    ecmrServiceStubWithoutEcmr,
    ecmrTemplateServiceStub,
    encryptionServiceStub,
    snackbarServiceStub,
    userProfileServiceStub,
    userServiceStub,
} from '../../shared/mockups/service-stubs';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { UserProfileService } from '../../modules/user-management/services/user-profile.service';
import { EcmrTemplateService } from '../../shared/services/http/ecmr-template.service';

/**
 * TODO: Implement in code first, then in test
 *  - Show error message on not editable eCMR:
 *    - The Page should throw an Error-Message if the ecmrId provided leads to a not editable eCMR
 */

describe('EditEcmrPageComponent', () => {
    let component: EditorPageComponent;
    let fixture: ComponentFixture<EditorPageComponent>;
    let loader: HarnessLoader;
    let overlayContainer: OverlayContainer;
    const dummyFields = JSON.parse(JSON.stringify(placeholderEcmrFields));

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [EditorPageComponent],
            imports: [
                BrowserAnimationsModule,
                FormsModule,
                MatBadgeModule,
                MatButtonModule,
                MatCardModule,
                MatDialogModule,
                MatExpansionModule,
                MatFormFieldModule,
                MatIconModule,
                MatInputModule,
                MatSelectModule,
                MatToolbarModule,
                ReactiveFormsModule,
                RouterModule,
                RouterTestingModule,
            ],
            providers: [
                {provide: AuthService, useValue: authServiceStub()},
                {provide: EcmrService, useValue: ecmrServiceStub()},
                {provide: EcmrTemplateService, useValue: ecmrTemplateServiceStub()},
                {provide: EncryptionService, useValue: encryptionServiceStub({})},
                // { provide: MatDialogRef },
                {provide: SnackbarService, useValue: snackbarServiceStub()},
                {provide: UserProfileService, useValue: userProfileServiceStub()},
                {provide: UserService, useValue: userServiceStub()},
            ],
        }).compileComponents();
    });

    afterEach(async () => {
        // Make sure, that all dialogs are closed before continuing
        const dialogs = await loader.getAllHarnesses(MatDialogHarness);
        await Promise.all(
            dialogs.map(async (d: MatDialogHarness) => await d.close())
        );
        overlayContainer.ngOnDestroy();
    });

    describe('with anything but \'new\' as route parameter and no signature needed', () => {
        // Initialize dialog container, harness loader and route parameter
        beforeEach(() => {
            TestBed.overrideProvider(ActivatedRoute, {
                useValue: {
                    snapshot:
                        {
                            paramMap: {get: () => 'not-new'},
                            url: [{path: '/edit-ecmr/'}]
                        }
                },
            });
            fixture = TestBed.createComponent(EditorPageComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
            loader = TestbedHarnessEnvironment.documentRootLoader(fixture);
            overlayContainer = TestBed.inject(OverlayContainer);
        });

        describe('the eCMR form', () => {
            it('should contain every fieldId that is part of the given API', async () => {
                // check if array length is >= number of IDs minus 3 signaturefields
                const formFields = await loader.getAllHarnesses(MatFormFieldHarness);
                expect(formFields.length).toBeGreaterThanOrEqual(
                    fieldIds(false, false).length - 3
                );
                // then check if every id exists in static FormGroup in typescript
                for (const fieldId of fieldIds()) {
                    expect(component.staticFormGroup.get(fieldId)).not.toBeNull();
                }
                // then check if every id exists at least once in dynamic FormArray in typescript
                for (const fieldId of positionFieldIds) {
                    expect(
                        component.dynamicFormArray.controls[0].get(fieldId)
                    ).not.toBeNull();
                }
            });
            it('should have the eCMR ID and host as it\'s title', () => {
                const formTitle = fixture.debugElement.query(
                    By.css('h1')
                ).nativeElement;
                expect(formTitle.innerHTML).toBe(
                    'Edit eCMR with ID ' + 'not-new' + ' — localhost'
                );
            });
            // TODO: Fix this
            it('should be prefilled with values from provided eCMR', () => {
                /*
                  1.Get all Fields Inputs and connect with ID in Map
                  2.For each Field in present eCMR get Field with id as key Map
                  3.Assert that Inputs are equal
                  */
                // Build Map
                const inputToId: Map<number, DebugElement> = new Map();
                const inputs = fixture.debugElement.queryAll(By.css('input'));
                inputs.forEach(input => {
                    if (input) {
                        inputToId.set(
                            +input.nativeElement.getAttribute('formControlName'),
                            input
                        );
                    }
                });
                //Iterate over fields
                let correctlyPrefilled = true;
                for (let index = 0; index < inputToId.size; index++) {
                    if (inputToId.get(index) && component.presentEcmr.fields[index]) {
                        // console.warn(
                        //   component.presentEcmr.fields[index] +
                        //     ' = ' +
                        //     inputToId.get(index).nativeElement.value
                        // );
                        correctlyPrefilled =
                            component.presentEcmr.fields[index] ===
                            inputToId.get(index).nativeElement.value && correctlyPrefilled;
                    }
                }
                //Assert that true
                expect(correctlyPrefilled).toBeTrue();
            });

            it('should show errors in FormFields only when badly formatted values are put into the forms fields', async () => {
                const senderNameField = await loader.getHarness(
                    MatFormFieldHarness.with({floatingLabelText: /name/i})
                );
                const senderNameInput = await senderNameField.getControl(
                    MatInputHarness
                );
                // Expect correct placeholder value and no errors
                expect(await senderNameInput.getValue()).toEqual(dummyFields['1']);
                expect(await senderNameField.hasErrors()).toBeFalse();
                // Check Sender-Name-Field for correct "too few characters" error-behavior
                await senderNameInput.setValue('A');
                await senderNameInput.blur();

                // Check Sender-Name-Field for correct "required" error-behavior
                await senderNameInput.setValue('');
                await senderNameInput.blur();
                await senderNameInput.setValue('Someone');

                // Check Cash-On-Delivery-Field for correct "not alphanumeric" error-behavior
                const codField = await loader.getHarness(
                    MatFormFieldHarness.with({floatingLabelText: /cash on delivery/i})
                );
                const codInput = await codField.getControl(MatInputHarness);
                await codInput.setValue('abc');
                await codInput.blur();
                expect(await codField.hasErrors()).toBeTrue();
                expect((await codField.getTextErrors())[0]).toContain('Alphanumerics');

                // Check a Place-Field for correct "too many characters" error-behavior
                const tooManyCharString = 'X'.repeat(520);
                const placeField = await loader.getHarness(
                    MatFormFieldHarness.with({floatingLabelText: /place/i})
                );
                const placeInput = await placeField.getControl(MatInputHarness);
                await placeInput.setValue(tooManyCharString);
                await placeInput.blur();
            });
        });

        describe('fields that are', () => {
            it('optional or mandatory should be editable', () => {
                const inputToId: Map<number, DebugElement> = new Map();
                // const matInputs = await loader.getAllHarnesses(MatInputHarness);
                const inputs = fixture.debugElement.queryAll(By.css('input'));
                inputs.forEach(input => {
                    if (input) {
                        inputToId.set(
                            +input.nativeElement.getAttribute('formControlName'),
                            input
                        );
                    }
                });
                //Check
                expect(inputToId.size).toBeGreaterThan(0);
                //Check if inputs are disabled
                let allMandatoryDisabled = false;
                let allOptionalDisabled = false;
                component.mandatoryFieldIds.forEach(mandField => {
                    const inputElement = inputToId.get(+mandField);
                    if (inputElement) {
                        // If one field is disabled the test fails
                        allMandatoryDisabled =
                            inputElement.nativeElement.disabled || allMandatoryDisabled;
                    }
                });
                component.optionalFieldIds.forEach(optField => {
                    const inputElement = inputToId.get(+optField);

                    if (inputElement) {
                        // If one field is disabled the test fails
                        allOptionalDisabled =
                            inputElement.nativeElement.disabled || allOptionalDisabled;
                    }
                });
                expect(allMandatoryDisabled).toBeFalse();
                expect(allOptionalDisabled).toBeFalse();
            });
            // fields that are...
            it('neither optional nor mandatory should not be editable', () => {
                //Arrange
                let allDisabled = true;
                const inputs = fixture.debugElement.queryAll(By.css('input'));
                inputs.forEach(input => {
                    const mandatory = component.mandatoryFieldIds.includes(
                        input.nativeElement.getAttribute('formControlName')
                    );
                    const optional = component.optionalFieldIds.includes(
                        input.nativeElement.getAttribute('formControlName')
                    );
                    if (!mandatory && !optional) {
                        //check if input is disabeld
                        allDisabled =
                            input.nativeElement.hasAttribute('disabled') && allDisabled;
                    }
                });
                //check if input of allInputs not in mandatory List and not in optional List
                expect(allDisabled).toBeTrue();
            });
        });

        describe('a click on save', () => {
            it('should be allowed if all mandatory fields are filled', async () => {
                /*
                1. Get MatInputs and Change input on requiered to null
                2. Click save Button and assert that Dialogs doesnt open
                3. Reset the Form with Reset Button
                4. Click Save Button
                5. Assert that dialog opens -> form is savable
                */
                const matInputs = await loader.getAllHarnesses(MatInputHarness);
                for (const input of matInputs) {
                    if (await input.isRequired()) {
                        await input.setValue('');
                    }
                }
                const saveButton = await loader.getHarness(
                    MatButtonHarness.with({text: /save/i})
                );
                const resetButton = await loader.getHarness(
                    MatButtonHarness.with({text: /reset/i})
                );
                await saveButton.click();
                fixture.detectChanges();
                let dialog = await loader.getAllHarnesses(MatDialogHarness);
                expect(dialog.length).toBe(1);
                fixture.detectChanges();
                await resetButton.click();
                fixture.detectChanges();
                await saveButton.click();
                dialog = await loader.getAllHarnesses(MatDialogHarness);
                expect(dialog.length).toBe(3);
            });
            // it('should result in a valid DTO of described format on permission', () => {
            //   //TODO
            // });
        });

        describe('a click on reset', () => {
            it('should reset all data to data of provided eCMR', async () => {
                /*
                Should reset clear all formControl values
                1. Get all Input Values Compare them with presentEcmr
                2. Assert that they are equal before the Change
                3. Change all Values to placeholder
                4. Assert that values are changed
                5. Click Reset Button
                6. Assert that Values are equal to presentEcmr values again
                */
                const inputToId: Map<number, DebugElement> = new Map();
                const resetButton = await loader.getHarness(
                    MatButtonHarness.with({text: /reset/i})
                );
                // const matInputs = await loader.getAllHarnesses(MatInputHarness);
                const inputs = fixture.debugElement.queryAll(By.css('input'));
                inputs.forEach(input => {
                    if (input) {
                        inputToId.set(
                            +input.nativeElement.getAttribute('formControlName'),
                            input
                        );
                    }
                });
                let beforeChange = true;
                let afterChange = false;
                let afterReset = true;
                for (let i = 0; i < inputToId.size; i++) {
                    if (inputToId.get(i) && component.presentEcmr.fields[i]) {
                        beforeChange =
                            inputToId.get(i).nativeElement.value ===
                            component.presentEcmr.fields[i] && beforeChange;
                        //Change
                        inputToId.get(i).nativeElement.value = 'place';
                        fixture.detectChanges();

                        afterChange =
                            inputToId.get(i).nativeElement.value ===
                            component.presentEcmr.fields[i] || afterChange;
                    }
                }
                //Click Reset Test Dialog
                const dialog = await loader.getAllHarnesses(MatDialogHarness);
                expect(dialog.length).toBe(0);
                await resetButton.click();
                const yesButton = await loader.getHarness(
                    MatButtonHarness.with({text: /yes/i})
                );
                await yesButton.click();

                await loader.getAllHarnesses(MatDialogHarness);
                fixture.detectChanges();
                for (let i = 0; i < inputToId.size; i++) {
                    if (inputToId.get(i) && component.presentEcmr.fields[i]) {
                        afterReset =
                            inputToId.get(i).nativeElement.value ===
                            component.presentEcmr.fields[i] && afterReset;
                    }
                }
                expect(beforeChange).toBeTrue();
                expect(afterChange).toBeFalse();
                expect(afterReset).toBeTrue();
            });
        });

        describe('a click on cancel', () => {
            it('should lead to an assertion step', async () => {
                const cancelButton = await loader.getHarness(
                    MatButtonHarness.with({text: /cancel/i})
                );
                let dialogs = await loader.getAllHarnesses(MatDialogHarness);
                expect(dialogs.length).toBe(0);
                await cancelButton.click();
                dialogs = await loader.getAllHarnesses(MatDialogHarness);
                expect(dialogs.length).toBe(1);
            });
            // it('should try to redirect to ecmrOverview', () => {
            //   //TODO
            // });
        });

        describe('the positions list', () => {
            it('should have a line count that matches the given eCMR\'s positions-field', () => {
                //Get line Count from present eCMR  lineCount= Count of Position Objects
                const presentEcmrLineCount =
                    component.presentEcmr.fields.positions.length;
                //Line Count in template = controls of dynamic form array
                const lineCountInTemplate = component.dynamicFormArray.controls.length;
                //Alt count span of positions
                const lineCountInExpansionPanel = fixture.debugElement.queryAll(
                    By.css('mat-panel-title')
                ).length;
                // There should always be an empty line, even if the template has no entries
                if (presentEcmrLineCount === 0) {
                    expect(lineCountInTemplate).toEqual(1);
                    expect(lineCountInExpansionPanel).toEqual(1);
                } else {
                    expect(lineCountInTemplate).toEqual(presentEcmrLineCount);
                    expect(lineCountInExpansionPanel).toEqual(presentEcmrLineCount);
                }
            });
            it('should be shown as one empty line if given eCMR has no positions', async () => {
                let expansionPanels = await loader.getAllHarnesses(
                    MatExpansionPanelHarness
                );
                expect(expansionPanels.length).toEqual(
                    component.presentEcmr.fields.positions.length
                );
                //Reload position list
                component.dynamicFormArray = new UntypedFormArray([]);
                component.addPositionToFormArray();

                fixture.detectChanges();
                //Act Count position lines
                expansionPanels = await loader.getAllHarnesses(
                    MatExpansionPanelHarness
                );
                //Assert
                expect(expansionPanels.length).toBe(1);
            });
            it('should decrement it\'s line count on press of delete', async () => {
                //Get delete Button and click
                let deleteButtons = await loader.getAllHarnesses(
                    MatButtonHarness.with({ancestor: 'mat-expansion-panel-header'})
                );

                // If only one line is available, add another one:
                if (deleteButtons.length === 1) {
                    const addButton = await loader.getAllHarnesses(
                        MatButtonHarness.with({text: /add/i})
                    );
                    await addButton[0].click();
                }

                // update harness
                deleteButtons = await loader.getAllHarnesses(
                    MatButtonHarness.with({ancestor: 'mat-expansion-panel-header'})
                );
                const oldLineCount = component.dynamicFormArray.controls.length;
                expect(deleteButtons.length).toBe(oldLineCount);

                //Click 1 button and get new Line Count
                await deleteButtons[0].click();

                // Handle Dialog
                const yesButton = await loader.getAllHarnesses(
                    MatButtonHarness.with({text: /yes/i})
                );
                await yesButton[0].click();

                fixture.detectChanges();
                const newLineCount = component.dynamicFormArray.controls.length;
                //Assert new LineCount < old line Count
                if (oldLineCount > 1) {
                    expect(oldLineCount).toBeGreaterThan(newLineCount);
                } else {
                    expect(oldLineCount).toEqual(newLineCount);
                }
            });
            it('should increment it\'s line count on press of add', async () => {
                //Getline count
                //Get add Button and click
                //Assert new LineCount > old line Count
                const oldLineCount = component.dynamicFormArray.controls.length;
                const addButton = await loader.getHarness(
                    MatButtonHarness.with({text: /add/i})
                );
                expect(addButton).toBeDefined();
                //Click 1 button and get new Line Count
                await addButton.click();
                const newLineCount = component.dynamicFormArray.controls.length;
                //Assert
                expect(newLineCount).toBeGreaterThan(oldLineCount);
            });

            it('should only enable add and remove line buttons if it is editable', async () => {
                // 1. 41 is in enabledFields => buttons are enabled
                // 2.Remove 41 and assert that buttons are disabled
                const isInEnabledFields: boolean =
                    component.mandatoryFieldIds.includes('41') ||
                    component.optionalFieldIds.includes('41');
                expect(isInEnabledFields).toBeTrue();

                let removeButtons = await loader.getAllHarnesses(
                    MatButtonHarness.with({text: /delete/i})
                );
                while (removeButtons.length > 1) {
                    await removeButtons[removeButtons.length - 1].click();
                    fixture.detectChanges();
                    const dialogs = await loader.getAllHarnesses(MatDialogHarness);
                    const yesButton = await dialogs[0].getHarness(
                        MatButtonHarness.with({text: /yes/i})
                    );
                    await yesButton.click();
                    // const yesButtons = await loader.getAllHarnesses(MatButtonHarness.with({text: /yes/i}));
                    // await yesButtons[0].click();
                    removeButtons = await loader.getAllHarnesses(
                        MatButtonHarness.with({text: /delete/i})
                    );
                }
                expect(removeButtons.length).toBe(1);

                let removeButtonDisabled = false;
                for (const button of removeButtons) {
                    removeButtonDisabled =
                        (await button.isDisabled()) || removeButtonDisabled;
                }
                expect(removeButtonDisabled).toBeTrue();

                const addButton = await loader.getHarness(
                    MatButtonHarness.with({text: /add/i})
                );
                let addButtonDisabled = false;
                addButtonDisabled = (await addButton.isDisabled()) || addButtonDisabled;
                expect(addButtonDisabled).toBeFalse();

                //Remove 41 => buttons should be disabled
                const indexOf41 = component.optionalFieldIds.indexOf('41');
                component.optionalFieldIds = component.optionalFieldIds.slice(
                    indexOf41,
                    1
                );
                //Click all remvove Buttons but one to disable the last remove Button
                for (let i = removeButtons.length - 1; i > 0; i--) {
                    await removeButtons[i].click();
                    // Second dialog request
                    await (
                        await loader.getAllHarnesses(
                            MatButtonHarness.with({text: /yes/i})
                        )
                    )[0].click();
                }
                removeButtons = await loader.getAllHarnesses(
                    MatButtonHarness.with({text: /delete/i})
                );
                expect(removeButtons.length).toBe(1);

                fixture.detectChanges();

                removeButtonDisabled = false;

                for (const buttons of removeButtons) {
                    removeButtonDisabled =
                        (await buttons.isDisabled()) || removeButtonDisabled;
                }
                expect(removeButtonDisabled).toBeTrue();
                expect(component.isPositionListEnabled()).toBeFalse();
                expect(await addButton.isDisabled()).toBeTrue();
            });
            it('should have a position-shorthand method that turns it\'s values to shorthands as expected', () => {
                /*
                Position state
                Test getPositionShorthand
                1. Build every possible Position State
                2. Assert Position state is correct
                3. Assert Shorthand is correctly build
                */
                //Arrange
                const emptyPosition: UntypedFormGroup = new UntypedFormGroup({
                    41: new UntypedFormControl(''),
                    42: new UntypedFormControl(''),
                    43: new UntypedFormControl(''),
                    45: new UntypedFormControl(''),
                    49: new UntypedFormControl(''),
                    50: new UntypedFormControl(''),
                    51: new UntypedFormControl(''),
                });
                const unspecifiedPosition: UntypedFormGroup = new UntypedFormGroup({
                    41: new UntypedFormControl(''),
                    42: new UntypedFormControl(''),
                    43: new UntypedFormControl('20'),
                    45: new UntypedFormControl(''),
                    49: new UntypedFormControl(''),
                    50: new UntypedFormControl(''),
                    51: new UntypedFormControl(''),
                });
                const quantifiedPosition: UntypedFormGroup = new UntypedFormGroup({
                    41: new UntypedFormControl('DHJ'),
                    42: new UntypedFormControl('124'),
                    43: new UntypedFormControl('20'),
                    45: new UntypedFormControl(''),
                    49: new UntypedFormControl(''),
                    50: new UntypedFormControl(''),
                    51: new UntypedFormControl(''),
                });
                const qualifiedPosition: UntypedFormGroup = new UntypedFormGroup({
                    41: new UntypedFormControl('DHJ'),
                    42: new UntypedFormControl('124'),
                    43: new UntypedFormControl(''),
                    45: new UntypedFormControl('Euro pallet'),
                    49: new UntypedFormControl('Water canisters'),
                    50: new UntypedFormControl('1210'),
                    51: new UntypedFormControl('1.44'),
                });
                //Assert
                expect(component.getPositionState(emptyPosition)).toBe('Empty');
                expect(component.getPositionState(unspecifiedPosition)).toBe(
                    'Unspecified'
                );
                expect(component.getPositionState(quantifiedPosition)).toBe(
                    'Quantified'
                );
                expect(component.getPositionState(qualifiedPosition)).toBe('Qualified');
                expect(component.getPositionShorthand(emptyPosition)).toBe('Empty');
                expect(component.getPositionShorthand(unspecifiedPosition)).toBe(
                    'Unspecified'
                );
                const quantifiedShortHand =
                    quantifiedPosition.get('43').value +
                    ' × ' +
                    (quantifiedPosition.get('41').value
                        ? quantifiedPosition.get('41').value
                        : quantifiedPosition.get('42').value);
                expect(component.getPositionShorthand(quantifiedPosition)).toBe(
                    quantifiedShortHand
                );

                const qualifiedShorthand = qualifiedPosition.get('41').value
                    ? qualifiedPosition.get('41').value
                    : qualifiedPosition.get('42').value;
                expect(component.getPositionShorthand(qualifiedPosition)).toBe(
                    qualifiedShorthand
                );
            });
        });
    });

    describe('with anything but \'new\' as route parameter and an explicit signature needed at field \'69\'', () => {
        beforeEach(() => {
            TestBed.overrideProvider(ActivatedRoute, {
                useValue: {
                    snapshot:
                        {
                            paramMap: {get: () => 'not-new'},
                            url: [{path: '/edit-ecmr/'}]
                        }
                },
            })
                .overrideProvider(UserService, {
                    useValue: userServiceStub({
                        // Manatory Fields from index 0 to 34 plus field '69'
                        //               equals to id 1 to 38 plus field '69'
                        //               equals to Sendername to Successive Carrier Signature plus field 'Signature'
                        mandatoryFields: [...mockFieldArray(59).slice(0, 35), '69'],
                        // Optional Fields from index 35 to 58
                        //               equals to id 39 to 66
                        //               equals to Reservations to Cash On Delivery
                        optionalFields: [...mockFieldArray(59).slice(35)],
                    }),
                })
                .overrideProvider(EcmrService, {
                    useValue: ecmrServiceStub(mockEcmr(152, false)),
                });
            fixture = TestBed.createComponent(EditorPageComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
            loader = TestbedHarnessEnvironment.documentRootLoader(fixture);
            overlayContainer = TestBed.inject(OverlayContainer);
        });
        describe('a click on save', () => {
            it('should only be allowed if all mandatory fields are filled and signature is given', async () => {
                // Initialize button
                const saveButton = await loader.getHarness(
                    MatButtonHarness.with({text: /save/i})
                );
                /* And apply spy to ecmrService's sendEcmrToServer Method
                const sendToServerSpy = spyOn(
                  component.ecmrService,
                  'sendEcmrToServer'
                );
                 */
                // signature field is missing
                // click on save and...
                await saveButton.click();
                // expect a dialog to appear
                let dialogs = await loader.getAllHarnesses(MatDialogHarness);
                expect(dialogs.length).toEqual(1);
                // but expect scroll position to have changed

                // Then press the sign button
                const signButton = await loader.getHarness(
                    MatButtonHarness.with({text: /sign/i})
                );
                await signButton.click();
                // And expect an assurance dialog to appear
                dialogs = await loader.getAllHarnesses(MatDialogHarness);
                expect(dialogs.length).toEqual(1);
                // TODO: Fix swim lane flow and this test shall pass
                // Then click on yes
                // let yesButton = await loader.getHarness(
                //   MatButtonHarness.with({ text: /yes/i })
                // );
                // await yesButton.click();
                // And then click on save
                // await saveButton.click();
                // yesButton = await loader.getHarness(
                //   MatButtonHarness.with({ text: /yes/i })
                // );
                // // and then click on yes in the new dialog
                // await yesButton.click();
                // // and finally expect something to have happened
                // fixture.detectChanges();
                // await fixture.whenStable();
                //
                // expect(sendToServerSpy).toHaveBeenCalled();
            });
        });
    });

    describe('with \'new\' as route parameter (thus no signature needed)', () => {
        const expectEcmrFormToBeEmpty = () => {
            expect(component.staticFormGroup.dirty).toBeFalse();
            expect(
                Object.values(component.staticFormGroup.value).every(
                    val => val === '' || val === null
                )
            ).toBeTrue();
            let arrayIsEmpty = true;
            for (const position of component.dynamicFormArray.value) {
                if (Object.values(position).some(val => val !== '')) {
                    arrayIsEmpty = false;
                    break;
                }
            }
            expect(arrayIsEmpty).toBeTrue();
        };

        beforeEach(() => {
            TestBed.overrideProvider(ActivatedRoute, {
                useValue: {
                    snapshot:
                        {
                            paramMap: {
                                get: (name: string) => {
                                    if (name == 'tan') {
                                        return null;
                                    }
                                    return 'new'
                                }
                            },
                            url: [{path: '/edit-ecmr/'}]
                        }
                },
            });
            TestBed.overrideProvider(EcmrService, {
                useValue: ecmrServiceStubWithoutEcmr()
            });
            fixture = TestBed.createComponent(EditorPageComponent);
            component = fixture.componentInstance;
            fixture.detectChanges();
            loader = TestbedHarnessEnvironment.documentRootLoader(fixture);
            overlayContainer = TestBed.inject(OverlayContainer);
        });

        describe('the eCMR form', () => {
            it('should have the eCMR ID as it\'s title', () => {
                const formTitle = fixture.debugElement.query(
                    By.css('h1')
                ).nativeElement;
                expect(formTitle.innerHTML).toBe('Create a new eCMR');
            });
            it('should be empty by start', () => {
                expectEcmrFormToBeEmpty();
            });
        });

        describe('a click on reset', () => {
            it('should clear all fields', async () => {
                // Set fields to placeholder input
                const fillButton = await loader.getHarness(
                    MatButtonHarness.with({text: /fill/i})
                );
                await fillButton.click();

                // Trigger reset
                const resetButton = await loader.getHarness(
                    MatButtonHarness.with({text: /reset/i})
                );
                await resetButton.click();

                // Click yes in assertion dialog
                const yesButton = await loader.getHarness(
                    MatButtonHarness.with({text: /yes/i})
                );
                await yesButton.click();
                expectEcmrFormToBeEmpty();
            });
        });

        describe('the positions list', () => {
            it('should be shown as one empty line', () => {
                expect(
                    fixture.debugElement.queryAll(By.css('.mat-expansion-panel')).length
                ).toEqual(1);
                expect(component.dynamicFormArray.value.length).toEqual(1);
            });
        });

        it('should work toUppercaseName-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            expect(component.toUppercaseName('string')).toEqual(' String');
            expect(component).toBeTruthy();
        });

        it('should work onSaveClicked-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.onSaveClicked();

            component.networkError$.next(true);
            component.onSaveClicked();

            component.networkError$.next(false);
            component.alreadySavedError$.next(true);
            component.onSaveClicked();
            expect(component).toBeTruthy();
        });

        it('should work onCancelClicked-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.onCancelClicked();
            expect(component).toBeTruthy();
        });

        it('should work onMagnifySOG-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.staticFormGroup.patchValue({
                72: JSON.stringify({
                    type: 'signOnGlass',
                    data: 'asfgsd,ghda',
                    name: 'first last',
                    company: 'compAny',
                }),
            });
            component.onMagnifySOG('72');
            expect(component).toBeTruthy();
        });

        it('should work sign-Function', async () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.setSignable$('72',false);
            await component.sign('72');

            component.setSignable$('72',true);
            await component.sign('72');

            expect(component).toBeTruthy();
        });

        it('should work signOnGlass-Function', async () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            await component.signOnGlass('72', {
                type: 'signOnGlass',
                data: 'asfgsd,ghda',
                name: 'first last',
                company: 'compAny',
            });
            expect(component).toBeTruthy();
        });

        it('should work onSignOnGlassClicked-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.onSignOnGlassClicked('72');

            component.setSignable$('72',true);
            component.onSignOnGlassClicked('72');

            expect(component).toBeTruthy();
        });

        it('should work generateDtoAndSubmit-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.generateDtoAndSubmit();
            expect(component).toBeTruthy();
        });

        it('should work onSignClicked-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.onSignClicked('72');
            expect(component).toBeTruthy();
        });

        it('should work onFillClicked-Function', () => {
            component = fixture.componentInstance;
            fixture.detectChanges();
            component.isSuperUser$.next(true);
            component.onFillClicked();
            expect(component).toBeTruthy();
        });
    });
});
