/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterContentChecked, Component, ElementRef, OnDestroy, OnInit, ViewChild, } from '@angular/core';
import { AbstractControl, UntypedFormArray, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators, } from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, mergeMap, Subject, takeUntil } from 'rxjs';
import { ConfirmationDialogComponent, ConfirmDialogModel, } from 'src/app/shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { SignOnGlassDialogComponent, SignOnGlassDialogModel, } from 'src/app/shared/dialogs/sign-on-glass-dialog/sign-on-glass-dialog.component';
import { en as EN } from 'src/assets/i18n/en';
import { AuthService } from '../../modules/user-management/services/auth.service';
import { UserProfileService } from '../../modules/user-management/services/user-profile.service';
import { SetEcmrDto } from '../../shared/models/dtos/requests/set-ecmr-dto';
import { Ecmr } from '../../shared/models/entities/ecmr';
import { fieldIds, Fields, placeholderEcmrFields, } from '../../shared/models/entities/fields';
import { Position, positionFieldIds, } from '../../shared/models/entities/position';
import { AccordionState } from '../../shared/models/types/accordion-state';
import { PositionState } from '../../shared/models/types/position-state';
import { EncryptionService } from '../../shared/services/encryption/encryption.service';
import { EcmrService } from '../../shared/services/http/ecmr.service';
import { endpoints } from '../../shared/services/http/endpoints';
import { UserService } from '../../shared/services/http/user.service';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { TemplateNameDialogComponent } from '../../shared/dialogs/template-name-dialog/template-name-dialog.component';
import { filter } from 'rxjs/operators';
import { SetTemplateDto } from '../../shared/models/dtos/requests/set-template.dto';
import { LoadFromTemplateDialogComponent } from '../../shared/dialogs/load-from-template-dialog/load-from-template-dialog.component';
import { EcmrTemplateService } from '../../shared/services/http/ecmr-template.service';
import { Template } from '../../shared/models/entities/template';
import { TemplateRoutingService } from '../../shared/services/state/template-routing-service';

@Component({
    selector: 'app-editor-page',
    templateUrl: './editor-page.component.html',
    styleUrls: ['./editor-page.component.scss'],
})
export class EditorPageComponent
    implements OnInit, AfterContentChecked, OnDestroy {
    @ViewChild(MatAccordion) accordion: MatAccordion;
    @ViewChild('signature69') signature69: ElementRef;
    @ViewChild('signature70') signature70: ElementRef;
    @ViewChild('signature72') signature72: ElementRef;

    /* Object that holds field names, descriptions and validation requirements*/
    en = EN;

    mandatoryFieldIds: string[] = [];
    optionalFieldIds: string[] = [];
    allSignatureFieldIds: string[] = [];
    mandatorySignatureFieldIds: string[] = [];
    ecmrId: string;
    tan: string;
    presentEcmr: Ecmr;
    presentTemplate: Template;
    isTemplateCreation: boolean;
    isEditTemplate: boolean;
    loadedFromTemplate: boolean;
    isCopy: boolean = false;
    unsubscribe$ = new Subject<void>();

    // TODO: Integrate into Formbuilder
    /* Default selectors for Box 17 */
    selectedOther;
    selectedCustomsDuty;
    selectedSupplementary;
    selectedCarriage;

    staticFormGroup: UntypedFormGroup;
    dynamicFormArray: UntypedFormArray;
    expansionPanelState: boolean[] = [];
    accordionState: AccordionState;

    dialogRef: MatDialogRef<any>;
    dialogSignOnGlassRef: MatDialogRef<any>;

    // reactive states
    isSuperUser$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    signables$: BehaviorSubject<{ [key: string]: boolean }[]> =
        new BehaviorSubject<{ [key: string]: boolean }[]>([]);
    userRoles: string[];
    savable$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(true);
    signatureStates$: BehaviorSubject<{ [key: string]: boolean }[]> =
        new BehaviorSubject<{ [key: string]: boolean }[]>([]);
    networkError$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    alreadySavedError$: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(
        false
    );

    ecmrOverviewUrlFragment = '/' + endpoints.frontend.ecmrOverview;
    templateOverviewUrlFragment = '/' + endpoints.frontend.ecmrTemplatesOverview;

    disponentSignatureDisabled = true;
    carrierSignatureDisabled = true;
    consignorSignatureDisabled = true;

    /* Constructor using Router and CurrentUserService */
    constructor(
        public activatedRoute: ActivatedRoute,
        public authService: AuthService,
        public ecmrService: EcmrService,
        public templateService: EcmrTemplateService,
        public encryptionService: EncryptionService,
        public formBuilder: UntypedFormBuilder,
        public matDialog: MatDialog,
        public matSignOnGlassDialog: MatDialog,
        public router: Router,
        public snackbarService: SnackbarService,
        public userProfileService: UserProfileService,
        public userService: UserService,
        public templateRoutingService: TemplateRoutingService
    ) {
    }

    // Wrapper Methods for calling from array in template
    onSignWrapper = x => this.onSignClicked(x);
    onSignOnGlassWrapper = x => this.onSignOnGlassClicked(x);

    ngOnDestroy(): void {
        this.unsubscribe$.next();
        this.unsubscribe$.complete();
        this.ecmrService.unsetEcmr();
    }

    ngOnInit(): void {
        this.allSignatureFieldIds = this.encryptionService.getSignatureFieldIds();
        this.signatureStates$.next(
            this.allSignatureFieldIds.map(id => ({[id]: false}))
        );
        this.signables$.next(
            this.allSignatureFieldIds.map(id => ({[id]: false}))
        );

        this.ecmrId = this.activatedRoute.snapshot.paramMap.get('id');
        this.tan = this.activatedRoute.snapshot.paramMap.get('tan');
        this.isCopy = this.activatedRoute.snapshot.url.join('/').includes('copy-ecmr');

        //Check if it's template or eCMR
        this.templateRoutingService.isTemplateCreation$.subscribe(value => {
            this.isTemplateCreation = value;
        });
        this.templateRoutingService.isEditTemplate$.subscribe(value => {
            this.isEditTemplate = value;
        });

        this.dynamicFormArray = this.formBuilder.array([]);

        this.staticFormGroup = this.generateStaticForm();

        this.setEcmrFields();
        if (this.tan) {
            this.userService.updateUserFieldsForCarrier();
            this.userProfileService.updateUserRolesForCarrier$();
        } else {
            this.userService.updateUserFields$();
            this.userProfileService.updateUserRoles$();
        }
        if (this.isEditTemplate) {
            this.loadFromTemplate(this.templateRoutingService.templateId);
        }

    }

    setEcmrFields() {
        if (this.ecmrId !== 'new') {
            this.ecmrService.updateEcmr$(this.ecmrId, this.tan);
        }

        combineLatest([
            this.userService.userFields$,
            this.userProfileService.isSuperuser$,
            this.ecmrService.ecmr$,
            this.userProfileService.userRoles$
        ]).pipe(
            takeUntil(this.unsubscribe$))
            .subscribe(
                ([fields, superuser, ecmr, userRoles]) => {
                    this.networkError$.next(false);
                    this.isSuperUser$.next(superuser as boolean);
                    this.userRoles = userRoles;

                    this.mandatoryFieldIds = this.setMandatoryFieldIdsForRole();
                    this.optionalFieldIds = (
                        fields as {
                            optionalFields: string[];
                        }
                    ).optionalFields;

                    this.mandatorySignatureFieldIds =
                        this.onlyMandatorySignatureFieldIds();

                    this.setRequiredAndOptionalFields(
                        this.mandatoryFieldIds,
                        this.optionalFieldIds
                    );

                    this.disableFieldsForRole();

                    if (ecmr) {
                        if (this.isCopy) {
                            this.presentEcmr = {...ecmr} as Ecmr;
                            this.emptyFieldsForCopy();
                        } else {
                            this.presentEcmr = ecmr as Ecmr;
                        }
                        this.fillFormFromEcmrFields(this.presentEcmr.fields);
                        this.expansionPanelState = [];
                        while (
                            this.expansionPanelState.length <
                            this.presentEcmr.fields.positions?.length
                            ) {
                            this.expansionPanelState.push(false);
                        }
                    } else {
                        this.addPositionToFormArray(true);
                        // this.updateSignatureStates$();
                        this.updateSignables$();
                        // this.updateSavable$();
                    }

                    this.staticFormGroup.valueChanges.subscribe(() => {
                        if (this.staticFormGroup.enabled) {
                            this.updateSignatureStates$();
                            this.updateSignables$();
                            this.updateSavable$();
                        }
                    });
                    this.dynamicFormArray.valueChanges.subscribe(() => {
                        if (this.dynamicFormArray.enabled) {
                            this.updateSignatureStates$();
                            this.updateSignables$();
                            this.updateSavable$();
                        }
                    });
                },
                error => {
                    console.debug('updateEcmr occurs: ' + error);
                    this.networkError$.next(true);
                    this.staticFormGroup?.disable();
                    this.dynamicFormArray?.disable();
                    this.updateSignatureStates$();
                    this.updateSignables$();
                    this.updateSavable$();
                }
            );
    }

    ngAfterContentChecked(): void {
        this.accordionState = this.getAccordionExpansionState(
            this.expansionPanelState
        );
    }

    /* Error message generator*/
    getErrorMessage(
        obj: UntypedFormControl | AbstractControl,
        inputType: string,
        pattern: string
    ) {
        if (obj.hasError('required')) {
            return this.en.error_required;
        }
        if (obj.hasError('pattern')) {
            return this.en.error_invalid + inputType + ': Pattern is ' + pattern;
        }
        if (obj.hasError('maxlength')) {
            return this.en.error_invalid + inputType + ': too many characters';
        }
        if (obj.hasError('minlength')) {
            return this.en.error_invalid + inputType + ': too few characters';
        }
        return '';
    }

    /**
     * Save-button method
     * This method serves three purposes:
     *  - If there was an error a press on save pushes a new status message
     *  - If the form is incomplete (savable == false) a press on save scrolls the faulty formfield into the view
     *  - If everything is correct (savable == true) a DTO is generated and submitted if user consents
     */
    onSaveClicked() {
        if (this.networkError$.getValue()) {
            this.snackbarService.pushNewStatus(
                'An error occurred so it is not possible to enter data or save the document. Try to reload the page.'
            );
            return;
        }
        if (this.alreadySavedError$.getValue()) {
            this.snackbarService.pushNewStatus(
                'This eCMR was already saved with the configured signatures.'
            );
            return;
        }
        if (!this.savable$.getValue()) {
            this.scrollToError();
            return;
        }
        if(this.isTemplateCreation) {
            this.onSaveAsTemplateClicked();
        } else {
            this.confirmDialog('Are you sure?', this.isEditTemplate ? 'Submit changes?' : 'Submit eCMR?');
            this.dialogRef.afterClosed().subscribe(dialogResult => {
                if (dialogResult) {
                    if(this.isCopy) {
                        this.ecmrId = 'new';
                    }
                    this.generateDtoAndSubmit();
                }
            });
        }

    }

    onSaveAsTemplateClicked() {
        if (this.networkError$.getValue()) {
            this.snackbarService.pushNewStatus(
                'An error occurred so it is not possible to enter data or save the document. Try to reload the page.'
            );
            return;
        }
        if (!this.savable$.getValue()) {
            this.scrollToError();
            return;
        }

        let templateName = '';
        this.matDialog.open(TemplateNameDialogComponent, {
            width: '500px',
            disableClose: true,
        }).afterClosed().pipe(
            filter(dialogResult => dialogResult)
        ).subscribe(dialogResult => {
            templateName = dialogResult;
            if (templateName) {
                this.generateTemplateDtoAndSubmit(templateName);
            }
        });
    }

    onLoadFromTemplateClicked() {
        this.matDialog.open(LoadFromTemplateDialogComponent)
            .afterClosed()
            .subscribe(dialogResult => {
                if (dialogResult) {
                    this.loadFromTemplate(dialogResult);
                    this.loadedFromTemplate = true;
                    this.snackbarService.pushNewStatus('Loaded from Template: ' + dialogResult, 4000);
                }
            });
    }

    loadFromTemplate(templateId: number) {
        this.templateService.updateComplete$.pipe(
            mergeMap(() => this.templateService.template$)
        ).subscribe({
            next: template => {
                this.presentTemplate = template;
                this.ecmrId = this.presentTemplate.ecmr.ecmrId;

                this.setEcmrFields();
            },
            error: () => {
                this.snackbarService.pushNewStatus('Could not fetch Template for id: ' + templateId.toString(), 4000);
            }
        });

        this.templateService.updateTemplate$(templateId);
    }

    loadFromTemplateEnabled(): boolean {
        let loadFromTemplateEnabled = false;
        if (this.ecmrId === 'new' || this.isTemplateCreation || this.loadedFromTemplate) {
            loadFromTemplateEnabled = true;
        }

        return loadFromTemplateEnabled;
    }

    /**
     * Sign-button method
     * This method serves two purposes:
     *  - If the form is incomplete (signable == false) a press on sign scrolls the faulty formfield into the view
     *  - If everything is correct (signable == true) a signature is generated and pushed into the form if user consents
     */
    onSignClicked(id: string): void {
        if (!this.readStateArray(this.signables$.getValue(), id)) {
            this.scrollToError();
            return;
        }

        this.confirmDialog(
            'You hereby confirm that the information made is correct.\n' +
            'Set personal signature?',
            'Sign eCMR'
        );
        this.dialogRef.afterClosed().subscribe(async dialogResult => {
            if (dialogResult) {
                await this.sign(id);
                this.updateSignables$();
            }
        });
    }

    /**
     * SignOnGlass-button method
     * This method serves two purposes:
     *  - If the form is incomplete (signable == false) a press on sign scrolls the faulty formfield into the view
     *  - If everything is correct (signable == true) a signature is generated and pushed into the form if user consents
     */
    onSignOnGlassClicked(id: string): void {
        if (!this.readStateArray(this.signables$.getValue(), id)) {
            this.scrollToError();
            return;
        }

        this.dialogSignOnGlassRef = this.matSignOnGlassDialog.open(
            SignOnGlassDialogComponent,
            {
                minWidth: '100%',
                height: '100%',
                width: '100%',
                panelClass: 'sign-on-glass-dialog',
                data: new SignOnGlassDialogModel(null, null, null, false),
            }
        );

        this.dialogSignOnGlassRef.backdropClick().subscribe(() => {
            this.dialogSignOnGlassRef.close();
        });

        this.dialogSignOnGlassRef
            .afterClosed()
            .subscribe(async (dialogResult: any) => {
                if (dialogResult) {
                    await this.signOnGlass(id, dialogResult);
                    this.updateSignables$();
                }
            });
    }

    /* Cancel Editing */
    onCancelClicked() {
        this.confirmDialog('Cancel and back to overview?', 'Cancel?');
        this.dialogRef.afterClosed().subscribe(dialogResult => {
            if (dialogResult) {
                if (this.isEditTemplate || this.isTemplateCreation) {
                    this.router.navigate([this.templateOverviewUrlFragment]);
                } else {
                    this.router.navigate([this.ecmrOverviewUrlFragment]);
                }
            }
        });
    }

    /* Clear all FormControls */
    onResetClicked() {
        this.confirmDialog('Do you want to reset all data?', 'Reset?');
        this.dialogRef.afterClosed().subscribe(dialogResult => {
            if (dialogResult) {
                this.resetForm();
            }
        });
    }

    onFillClicked() {
        this.generatePlaceholderInput();
    }

    async sign(id: string) {
        if (!this.signables$.getValue()) {
            return;
        }
        const comp = this.userService.userCompany;
        const profile = await this.userProfileService.userProfile;
        const signature = {
            name: profile.firstName + ' ' + profile.lastName,
            userCompany: comp?.companyName,
            timestamp: new Date().getTime(),
        };
        this.staticFormGroup.get(id).setValue(JSON.stringify(signature));

        if (!this.isSuperUser$.getValue()) {
            this.staticFormGroup.disable();
            this.dynamicFormArray.disable();
        }
    }

    async signOnGlass(id: string, dataUrl: any) {
        if (!this.signables$.getValue()) {
            return;
        }
        if (dataUrl.data) {
            const signature = {
                type: 'signOnGlass',
                data: dataUrl.data.split(',')[1],
                name: dataUrl.name,
                userCompany: dataUrl.company,
                timestamp: new Date().getTime(),
            };
            this.staticFormGroup.get(id).setValue(JSON.stringify(signature));

            if (!this.isSuperUser$.getValue()) {
                this.staticFormGroup.disable();
                this.dynamicFormArray.disable();
            }
        }
    }

    readStateArray(objArray: { [p: string]: boolean }[], key: string): boolean {
        const tempObject = objArray.find(obj => Object.keys(obj)[0] === key);
        return tempObject[Object.keys(tempObject)[0]];
    }

    /**
     * Prepares DTO and submits it to http-service.
     * Valid information is assumed.
     */
    generateDtoAndSubmit(): void {
        const dto: SetEcmrDto = {
            ecmrId: '',
            fields: {positions: []},
            signature: [],
        };
        // concat mandatory and optional fields and...
        [...this.mandatoryFieldIds, ...this.optionalFieldIds]
            // ...iterate over Ids to generate dto
            // Sorting is not needed, since the dto is an object and not an array
            .forEach(fieldId => {
                if (positionFieldIds.find(r => r === fieldId)) {
                    this.dynamicFormArray.controls.forEach((positionGroup, index) => {
                        if (positionGroup instanceof UntypedFormGroup) {
                            while (dto.fields.positions.length < index + 1) {
                                dto.fields.positions.push({});
                            }
                            dto.fields.positions[index][fieldId] =
                                positionGroup.getRawValue()[fieldId];
                        }
                    });
                } else {
                    dto.fields[fieldId] = this.staticFormGroup.getRawValue()[fieldId];
                }
            });

        dto.fields.positions = dto.fields.positions.filter(
            pos => this.getPositionState(pos) !== 'Empty'
        );

        this.encryptionService
            .signMsg(this.encryptionService.mapToString(dto.fields))
            .then(sig => {
                dto.signature = sig;
                const isEcmr = !this.isEditTemplate && !this.isTemplateCreation;
                if (this.presentTemplate && !this.isEditTemplate) {
                    dto.ecmrId = '0';
                } else {
                    dto.ecmrId = this.ecmrId === 'new' ? '0' : this.ecmrId;
                }

                this.ecmrService.sendEcmrToServer(dto, isEcmr).catch(reason => {
                    if (reason === 'retry') {
                        this.generateDtoAndSubmit();
                    }
                });
            })
            .catch(() => {
                this.snackbarService.pushNewStatus(
                    'An error occurred while generation a signature. Please log in once again.',
                    5000
                );
            });
    }

    generateTemplateDtoAndSubmit(templateName: string): void {
        const dto: SetTemplateDto = {
            ecmrId: '',
            fields: {positions: []},
            signature: [],
            templateName: templateName
        };
        // concat mandatory and optional fields and...
        [...this.mandatoryFieldIds, ...this.optionalFieldIds]
            // ...iterate over Ids to generate dto
            // Sorting is not needed, since the dto is an object and not an array
            .forEach(fieldId => {
                if (positionFieldIds.find(r => r === fieldId)) {
                    this.dynamicFormArray.controls.forEach((positionGroup, index) => {
                        if (positionGroup instanceof UntypedFormGroup) {
                            while (dto.fields.positions.length < index + 1) {
                                dto.fields.positions.push({});
                            }
                            dto.fields.positions[index][fieldId] =
                                positionGroup.getRawValue()[fieldId];
                        }
                    });
                } else {
                    dto.fields[fieldId] = this.staticFormGroup.getRawValue()[fieldId];
                }
            });

        dto.fields.positions = dto.fields.positions.filter(
            pos => this.getPositionState(pos) !== 'Empty'
        );

        this.encryptionService
            .signMsg(this.encryptionService.mapToString(dto.fields))
            .then(sig => {
                dto.signature = sig;
                dto.ecmrId = this.ecmrId === 'new' ? '0' : this.ecmrId;
                this.ecmrService.sendEcmrToServer(dto, false).catch(reason => {
                    if (reason === 'retry') {
                        this.generateTemplateDtoAndSubmit(templateName);
                    }
                });
            })
            .catch(() => {
                this.snackbarService.pushNewStatus(
                    'An error occurred while generation a signature. Please log in once again.',
                    5000
                );
            });
    }

    resetForm(): void {
        // Empty static group
        this.staticFormGroup.reset();
        this.staticFormGroup.enable();
        this.setRequiredAndOptionalFields(
            this.mandatoryFieldIds,
            this.optionalFieldIds
        );
        // Remove all dynamic groups but one and empty content
        while (this.dynamicFormArray.length > 0) {
            this.dynamicFormArray.removeAt(this.dynamicFormArray.length - 1);
            this.expansionPanelState.pop();
        }
        this.addPositionToFormArray();
        if (this.presentEcmr) {
            this.fillFormFromEcmrFields(this.presentEcmr.fields);
        }
    }

    getMandatory(fieldId: string): boolean {
        return this.mandatoryFieldIds.find(r => r === fieldId) !== undefined;
    }

    isPositionListEnabled(): boolean {
        const enabledFieldsIds: string[] = [
            ...this.mandatoryFieldIds,
            ...this.optionalFieldIds,
        ].sort((a, b) => parseInt(a, 10) - parseInt(b, 10));
        return !!enabledFieldsIds.find(r => r === positionFieldIds[0]);
    }

    generateSignatureTemplate(id: string): string {
        const idString = this.staticFormGroup.get(id).value;
        if (idString) {
            try {
                const identity = JSON.parse(idString);
                const timestamp = new Date(identity.timestamp).toISOString();
                if (identity.type === 'signOnGlass') {
                    return `
              <p class="header-line">Digitally signed by</p>
              <div class="sign-on-glass-container">
              <center>
              <img class="sign-on-glass-image" src='${
                        'data:image/png;base64,' + identity.data
                    }' draggable="false"/>
              </center>
              <div class="timestamp-line">${identity.name}, ${
                        identity.userCompany
                    }, ${timestamp}</div>
              </div>`;
                } else {
                    return `
              <p class="header-line">Digitally signed by</p>
              <p class="name-line">${this.toUppercaseName(identity.name)}</p>
              <p class="company-line">${identity.userCompany}</p>
              <p class="timestamp-line">${timestamp}</p>`;
                }
            } catch (e) {
                return idString + ' [deprecated format]';
            }
        }
        return ' ';
    }

    // Magnify sign on glass signature
    public onMagnifySOG(id: string) {
        const idString = this.staticFormGroup.get(id).value;
        if (idString) {
            try {
                const identity = JSON.parse(idString);
                if (identity.type === 'signOnGlass') {
                    // open SOG dialog
                    this.dialogSignOnGlassRef = this.matSignOnGlassDialog.open(
                        SignOnGlassDialogComponent,
                        {
                            minWidth: '100%',
                            height: '100%',
                            width: '100%',
                            panelClass: 'sign-on-glass-dialog',
                            data: new SignOnGlassDialogModel('', '', '', false),
                        }
                    );
                    this.dialogSignOnGlassRef
                        .afterClosed()
                        .subscribe(async (dialogResult: any) => {
                            if (dialogResult) {
                                await this.signOnGlass(id, dialogResult);
                                this.updateSignables$();
                            }
                        });

                    this.dialogSignOnGlassRef.backdropClick().subscribe(() => {
                        this.dialogSignOnGlassRef.close();
                    });
                }
            } catch (e) {
            }
        }
    }

    addPositionToFormArray(expanded = true): void {
        this.expansionPanelState.push(expanded);
        this.dynamicFormArray.push(this.generateDynamicFormGroup());
    }

    toUppercaseName(name: string): string {
        const parts = name.split(' ');
        let newName = '';
        for (const part of parts) {
            newName = newName.concat(
                ' ' + part.charAt(0).toUpperCase() + part.slice(1)
            );
        }
        return newName;
    }

    removePositionFromFormArray(i: number) {
        this.confirmDialog(
            'Are you sure to delete this item?',
            'Delete Item ' + (i + 1) + '?'
        );
        this.dialogRef.afterClosed().subscribe(dialogResult => {
            if (dialogResult) {
                this.expansionPanelState.splice(i, 1);
                this.dynamicFormArray.removeAt(i);
            }
        });
    }

    /**
     * Cases:
     *  - Empty
     *  - Unspecified (Neither Marking Text nor Barcode, but not empty)
     *  - Qualified: Specified without item count (Marking Text or Barcode but no Number)
     *  - Quantified: Specified with item count (Marking Text or Barcode and a Number)
     */
    getPositionState(position: UntypedFormGroup | Position): PositionState {
        // Convert function parameter to object if needed:
        const positionFromFormGroup: Position = {};
        let isEmpty = true;
        positionFieldIds.forEach(fieldId => {
            if (position instanceof UntypedFormGroup) {
                positionFromFormGroup[fieldId] = position.get(fieldId).value;
                if (positionFromFormGroup[fieldId] !== '') {
                    isEmpty = false;
                }
            } else {
                if (position[fieldId] !== '') {
                    isEmpty = false;
                }
            }
        });
        position = positionFromFormGroup;

        if (isEmpty) {
            return 'Empty';
        } else if (!(position['41'] || position['42'])) {
            return 'Unspecified';
        } else if (position['43']) {
            return 'Quantified';
        } else {
            return 'Qualified';
        }
    }

    // Turns FormGroup Values to shorthand using getPositionState method
    getPositionShorthand(formGroup: UntypedFormGroup): string {
        const positionState = this.getPositionState(formGroup);
        if (positionState === 'Empty' || positionState === 'Unspecified') {
            return positionState;
        } else if (positionState === 'Qualified') {
            return formGroup.get('41').value
                ? formGroup.get('41').value
                : formGroup.get('42').value;
        } else {
            return (
                formGroup.get('43').value +
                ' × ' +
                (formGroup.get('41').value
                    ? formGroup.get('41').value
                    : formGroup.get('42').value)
            );
        }
    }

    getAccordionExpansionState(expansionStates: boolean[]): AccordionState {
        if (expansionStates.length !== 0) {
            const expItemNumber = expansionStates.filter(r => r).length;
            if (expItemNumber === expansionStates.length) {
                return 'Expanded';
            } else if (expItemNumber === 0) {
                return 'Collapsed';
            } else {
                return 'Mixed';
            }
        } else {
            return undefined;
        }
    }

    openDocumentation() {
        window.open(
            'https://git.openlogisticsfoundation.org/wg-electronictransportdocuments/ecmr/ecmr/-/tree/main/documentation/app',
            '_blank'
        );
    }

  scrollToElement() {
    const element = document.getElementById('ecmr-signature-scroll');
    element?.scrollIntoView({behavior: 'smooth'});
  }

  // Not private static to enhance readability of formbuilder
  // noinspection JSMethodCanBeStatic
  private getVals(min: number, max: number, pat: string): Validators[] {
    return [
      Validators.minLength(min),
      Validators.maxLength(max),
      Validators.pattern(pat.toString()),
    ];
  }

    // noinspection JSMethodCanBeStatic
    private getPat(identifier: 'any' | 'an' | 'n'): string {
        if (identifier === 'an') {
            return '^.+';
        } else if (identifier === 'n') {
            return '^[1-9]\\d*(\\.\\d+)?$';
        } else {
            return '';
        }
    }

    private updateFormEnableState() {
        // If no SU
        if (
            !this.isSuperUser$.getValue() &&
            this.mandatorySignatureFieldIds.length > 0
        ) {
            // check if all mandatory signatures are already set and if so disable forms
            let allSet = true;
            this.mandatorySignatureFieldIds.forEach(id => {
                if (!this.readStateArray(this.signatureStates$.getValue(), id)) {
                    allSet = false;
                }
            });
            if (allSet) {
                this.dynamicFormArray.disable();
                this.staticFormGroup.disable();
            }
        }
    }

    private updateAlreadySavedError$() {
        if (!this.isSuperUser$.getValue()) {
            this.mandatorySignatureFieldIds.forEach(id => {
                if (
                    this.readStateArray(this.signatureStates$.getValue(), id) &&
                    !!this.presentEcmr.fields[id]
                ) {
                    this.alreadySavedError$.next(true);
                    return;
                }
            });
        }
    }

    private updateSignatureStates$(): void {
        // Update Array
        this.signatureStates$.next(
            this.signatureStates$.getValue().map(id => ({
                [Object.keys(id)[0]]: !!this.staticFormGroup.get(Object.keys(id)[0])
                    .value,
            }))
        );
    }

    private setAllSignables$(val: boolean): void {
        this.signables$.next(
            this.signables$
                .getValue()
                .map(signable => ({[Object.keys(signable)[0]]: val}))
        );
    }

    public setSignable$(key: string, val: boolean): void {
        const currentSignables = this.signables$.getValue();
        const index = currentSignables.findIndex(signable => signable.hasOwnProperty(key));

        if (index !== -1) {
            currentSignables[index][key] = val;
        }

        this.signables$.next(currentSignables);
    }

    private updateSignables$(): void {
        // Never signable if error flag is set
        if (this.networkError$.getValue()) {
            this.setAllSignables$(false);
            return;
        }
        // Only signable if one or more signature fields are mandatory
        if (this.mandatorySignatureFieldIds.length > 0) {
            // Never signable if any FormGroup is invalid or disabled
            if (
                this.staticFormGroup.invalid ||
                this.staticFormGroup.disabled ||
                this.dynamicFormArray.invalid ||
                this.dynamicFormArray.disabled
            ) {
                this.setAllSignables$(false);
                return;
            } else {
                if (this.userRoles.includes('disponent')) {
                    this.setSignable$('69', true);
                    this.setSignable$('70', false);
                    this.setSignable$('72',false);
                    this.disponentSignatureDisabled = false;
                }

                if (this.userRoles.includes('fahrer')) {
                    this.setSignable$('70', true);
                    this.setSignable$('69', false);
                    this.setSignable$('72',false);
                    this.carrierSignatureDisabled = false;
                }

                if(this.userRoles.includes('empfaenger')) {
                    this.setSignable$('72',true);
                    this.setSignable$('69', false);
                    this.setSignable$('70', false);
                    this.consignorSignatureDisabled = false;
                }
            }


            // Only signable if signature fields aren't already signed
            const currentSignables = this.signables$.getValue();
            const modifiedSignables: { [key: string]: boolean }[] = [];
            const signatureStates = this.signatureStates$.getValue();
            currentSignables.forEach(currentSignable => {
                const signableKey = Object.keys(currentSignable)[0];
                signatureStates.forEach(signatureState => {
                    const stateKey = Object.keys(signatureState)[0];
                    if (stateKey === signableKey) {
                        if (this.mandatorySignatureFieldIds.includes(stateKey)) {
                            currentSignable[signableKey] = !signatureState[stateKey];
                        } else {
                            currentSignable[signableKey] = false;
                        }
                        modifiedSignables.push(currentSignable);
                    }
                });
            });
            this.signables$.next(modifiedSignables);
        } else {
            this.setAllSignables$(false);
        }
    }

    private updateSavable$(): void {
        // Assert error flag is not set
        if (this.networkError$.getValue()) {
            this.savable$.next(false);
            return;
        }
        // always savable, if user is SU
        else if (this.isSuperUser$.getValue()) {
            this.savable$.next(true);
            return;
        }
        this.savable$.next(true);
    }

    private scrollToError(): void {
        if (this.staticFormGroup.invalid) {
            let invalidControls = [];
            for (const control in this.staticFormGroup.controls) {
                if (this.staticFormGroup.controls[control].invalid) {
                    this.staticFormGroup.controls[control].markAsTouched();
                    invalidControls.push(control);
                }
            }

            if (invalidControls.length > 0) {
                const element = document.querySelector(`[formControlName="${invalidControls[0]}"]`) as HTMLElement;
                element.focus();
                this.snackbarService.pushNewStatus('Please fill in all mandatory fields.');
            }
        } else if (this.dynamicFormArray.invalid) {
            this.snackbarService.pushNewStatus('Please fill in all mandatory fields');
        } else {
            // TODO
        }
    }

    /*
      private updateSavableSignable(
        signOnly: boolean = false,
        shouldScrollToForm: boolean = true
      ): boolean {
        if (this.isSuperUser$) {
          this.savable$ = true;
          this.signables$ = true;
          return true;
        }
        // iterate over all keys of static and dynamic form group/array in numeric order to
        for (const fieldId of fieldIds(false)) {
          // check if iteration reached the ID of the initial form in the dynamic array
          if (fieldId === positionFieldIds[0]) {
            // if it belongs to the array
            for (const posFormGroup of this.dynamicFormArray.controls) {
              const posIndex = this.dynamicFormArray.controls.indexOf(posFormGroup);
              if (posFormGroup instanceof FormGroup) {
                for (const positionFieldId of positionFieldIds) {
                  if (posFormGroup.get(positionFieldId).invalid) {
                    if (shouldScrollToForm) {
                      this.expansionPanelState[posIndex] = true;
                      const invalidControl =
                        this.elementRef.nativeElement.querySelector(
                          '#pos' + positionFieldId + posIndex
                        );
                      invalidControl.scrollIntoView({
                        block: 'center',
                        behavior: 'smooth',
                      });
                      // Allow Animation to finish by setting timeout before focusing
                      setTimeout(() => {
                        invalidControl.focus({ preventScroll: true });
                      }, 50);
                    }
                    this.signables$ = false;
                    return false;
                  }
                }
              }
            }
          } else {
            // check if current form group is invalid, and if so...
            if (this.staticFormGroup.controls[fieldId].invalid) {
              // assure that fieldId does not represent signature, and if so...
              if (fieldId !== this.mandatoryFieldsContainSignature()) {
                // check if controller should scroll invalid form into view, and do so if requested
                if (shouldScrollToForm) {
                  const invalidControl =
                    this.elementRef.nativeElement.querySelector(
                      '[formControlName="' + fieldId + '"]'
                    );
                  invalidControl.scrollIntoView({
                    block: 'center',
                    behavior: 'smooth',
                  });
                  invalidControl.focus({ preventScroll: true });
                }
                this.signables$ = false;
                return false;
              } // if the fieldId represents a signature, then...
              else {
                // check if method should only check for signature-readiness or also scroll to invalid form
                if (!signOnly) {
                  if (shouldScrollToForm) {
                    this['signature' + fieldId].nativeElement.scrollIntoView({
                      block: 'center',
                      behavior: 'smooth',
                    });
                    this['signature' + fieldId].nativeElement.focus({
                      preventScroll: true,
                    });
                  }
                }
                return signOnly;
              }
            }
          }
        }
        this.signables$ = true;
        return true;
      }
      */

    private setRequiredAndOptionalFields(
        mandatoryFieldIds: string[],
        optionalFieldIds: string[] = []
    ): void {
        const enabledFieldsIds: string[] = [
            ...mandatoryFieldIds,
            ...optionalFieldIds,
        ];
        fieldIds().forEach(staticFieldId => {
            if (!enabledFieldsIds.find(r => r === staticFieldId)) {
                this.staticFormGroup.get(staticFieldId).disable();
            } else if (mandatoryFieldIds.find(r => r === staticFieldId)) {
                this.staticFormGroup
                    .get(staticFieldId)
                    .addValidators(Validators.required);
            }
        });
    }

    private fillFormFromEcmrFields(fields: Fields) {
        // Fill static fields
        fieldIds().forEach(fieldId => {
            if (fields[fieldId]) {
                this.staticFormGroup.get(fieldId).setValue(fields[fieldId]);
                // Handle copy
                if(this.isCopy) {
                    if(fieldId == "75") {
                        this.staticFormGroup.get(fieldId).setValue("copy_"+fields[fieldId]);
                    }
                }
            }
        });
        // Fill array
        fields.positions.forEach((pos, index) => {
            if (index + 1 > this.dynamicFormArray.length) {
                this.addPositionToFormArray();
            }
            positionFieldIds.forEach(fieldId => {
                this.dynamicFormArray.at(index).get(fieldId).setValue(pos[fieldId]);
            });
        });

        this.updateSignatureStates$();
        this.updateSignables$();
        this.updateSavable$();
        this.updateFormEnableState();
        this.updateAlreadySavedError$();
    }

    private confirmDialog(message: string, title: string): void {
        const dialogData = new ConfirmDialogModel(title, message);
        this.dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
            maxWidth: '400px',
            data: dialogData,
        });
    }

    private generateStaticForm(): UntypedFormGroup {
        return this.formBuilder.group({
            // Group 1:
            /*name*/ 1: ['', this.getVals(2, 256, this.getPat('an'))],
            /*personName*/ 2: ['', this.getVals(2, 256, this.getPat('an'))],
            /*streetName*/ 3: ['', this.getVals(2, 256, this.getPat('an'))],
            /*postcode*/ 4: ['', this.getVals(2, 17, this.getPat('an'))],
            /*cityName*/ 5: ['', this.getVals(2, 256, this.getPat('an'))],
            /*countryName*/ 6: ['', this.getVals(2, 256, this.getPat('an'))],
            // Group 2:
            /*name*/ 7: ['', this.getVals(2, 256, this.getPat('an'))],
            /*personName*/ 8: ['', this.getVals(2, 256, this.getPat('an'))],
            /*postcode*/ 9: ['', this.getVals(2, 17, this.getPat('an'))],
            /*streetName*/ 10: ['', this.getVals(2, 256, this.getPat('an'))],
            /*cityName*/ 11: ['', this.getVals(2, 256, this.getPat('an'))],
            // /*countryCode*/ 12: ['', this.getVals(2, 3, this.getPat('an'))],
            /*countryName*/ 13: ['', this.getVals(2, 256, this.getPat('an'))],
            // Group 3:
            /*name*/ 14: ['', this.getVals(2, 256, this.getPat('an'))],
            /*toA*/ 15: ['', this.getVals(2, 19, this.getPat('an'))],
            /*toD*/ 16: ['', this.getVals(2, 19, this.getPat('an'))],
            // Group 4:
            /*name*/ 17: ['', this.getVals(2, 256, this.getPat('an'))],
            /*openingHours*/ 18: ['', this.getVals(2, 512, this.getPat('an'))],
            // Group 5:
            /*description*/ 19: ['', this.getVals(2, 256, this.getPat('an'))],
            // /*descriptionCode*/ 20: ['', this.getVals(2, 3, this.getPat('an'))],
            // Group 6:
            /*name*/ 21: ['', this.getVals(2, 256, this.getPat('an'))],
            /*personName*/ 22: ['', this.getVals(2, 256, this.getPat('an'))],
            /*postcode*/ 23: ['', this.getVals(2, 17, this.getPat('an'))],
            /*streetName*/ 24: ['', this.getVals(2, 256, this.getPat('an'))],
            /*cityName*/ 25: ['', this.getVals(2, 256, this.getPat('an'))],
            /*countryCode*/ 26: ['', this.getVals(2, 3, this.getPat('an'))],
            /*countryName*/ 27: ['', this.getVals(2, 256, this.getPat('an'))],
            /*countrySubDivName*/ 28: ['', this.getVals(2, 256, this.getPat('an'))],
            /*licensePlate*/ 29: ['', this.getVals(2, 256, this.getPat('an'))],
            // Group 7:
            /*name*/ 30: ['', this.getVals(2, 256, this.getPat('an'))],
            /*personName*/ 31: ['', this.getVals(2, 256, this.getPat('an'))],
            /*postcode*/ 32: ['', this.getVals(2, 17, this.getPat('an'))],
            /*streetName*/ 33: ['', this.getVals(2, 256, this.getPat('an'))],
            /*cityName*/ 34: ['', this.getVals(2, 256, this.getPat('an'))],
            /*countryCode*/ 35: ['', this.getVals(2, 3, this.getPat('an'))],
            /*countryName*/ 36: ['', this.getVals(2, 256, this.getPat('an'))],
            /*date*/ 37: ['', this.getVals(2, 19, this.getPat('an'))],
            /*signature*/ 38: ['', this.getVals(2, 45, this.getPat('an'))],
            // Group 8:
            /*reservations*/ 39: ['', this.getVals(2, 512, this.getPat('any'))],
            // Group 9:
            /*remarks*/ 40: ['', this.getVals(2, 512, this.getPat('any'))],

            // ...moved to dynamicFormArray

            // Group 16:
            /*specialAgreement*/ 52: ['', this.getVals(2, 256, this.getPat('any'))],
            // Group 17:
            /*carriageCharges*/ 53: ['', this.getVals(1, 18, this.getPat('n'))],
            /*ccCurrency*/ 54: ['', this.getVals(2, 26, this.getPat('an'))],
            // /*ccPayer*/ 55: ['', this.valset()(2,256, this.pat()('a'))],
            /*ccPayer*/ 55: [''],
            /*supplementaryCharges*/ 56: ['', this.getVals(1, 18, this.getPat('n'))],
            /*scCurrency*/ 57: ['', this.getVals(2, 26, this.getPat('an'))],
            // /*scPayer*/ 58: ['', this.valset()(2,256, this.pat()('a'))],
            /*scPayer*/ 58: [''],
            /*customsDuties*/ 59: ['', this.getVals(1, 18, this.getPat('n'))],
            /*cdCurrency*/ 60: ['', this.getVals(2, 26, this.getPat('an'))],
            // /*cdPayer*/ 61: ['', this.valset()(2,256, this.pat()('a'))],
            /*cdPayer*/ 61: [''],
            /*otherCharges*/ 62: ['', this.getVals(1, 26, this.getPat('n'))],
            /*ocCurrency*/ 63: ['', this.getVals(2, 26, this.getPat('an'))],
            // /*ocPayer*/ 64: ['', this.valset()(2,256, this.pat()('a'))],
            /*ocPayer*/ 64: [''],
            // Group 18:
            /*particulars*/ 65: ['', this.getVals(2, 512, this.getPat('any'))],
            // Group 19:
            /*cashOnDelivery*/ 66: ['', this.getVals(1, 18, this.getPat('n'))],
            // Group 21:
            /*establishedIn*/ 67: ['', this.getVals(2, 256, this.getPat('an'))],
            /*establishedOn*/ 68: ['', this.getVals(2, 19, this.getPat('an'))],
            // Group 22:
            /*senderSignature*/ 69: ['', this.getVals(2, 45, this.getPat('an'))],
            // Group 23:
            /*carrierSignature*/ 70: ['', this.getVals(2, 45, this.getPat('an'))],
            // Group 24:
            /*goodsReceivedDate*/ 71: ['', this.getVals(2, 19, this.getPat('an'))],
            /*goodsReceivedSign*/ 72: ['', this.getVals(2, 45, this.getPat('an'))],
            // Group 25:
            /*1*/ 73: ['', this.getVals(2, 512, this.getPat('an'))],
            // Group 25:
            /*1*/ 74: ['', this.getVals(2, 256, this.getPat('an'))],
            // Group 26
            /*1*/ 75: ['', this.getVals(2, 256, this.getPat('an'))],
        });
    }

    private generateDynamicFormGroup(): UntypedFormGroup {
        const dis = !this.isPositionListEnabled();
        return this.formBuilder.group({
            // Group 10:
            /*markingText*/ 41: [
                {value: '', disabled: dis},
                this.getVals(2, 512, this.getPat('an')),
            ],
            /*barcodeNumber*/ 42: [
                {value: '', disabled: dis},
                this.getVals(2, 35, this.getPat('an')),
            ],
            // Group 11:
            /*itemQuantity*/ 43: [
                {value: '', disabled: dis},
                this.getVals(1, 15, this.getPat('n')),
            ],
            // Group 12:
            // /*typeCode*/ 44: [{ value: '', disabled: dis }, this.getVals(2, 17, this.getPat('an'))],
            /*typeText*/ 45: [
                {value: '', disabled: dis},
                this.getVals(2, 35, this.getPat('an')),
            ],
            // Group 13:
            // /*sequenceNumber*/ 46: [{ value: '', disabled: dis }, this.getVals(2, 5, this.getPat('an'))],
            // /*commodityCode*/ 47: [{ value: '', disabled: dis }, this.getVals(2, 35, this.getPat('an'))],
            // /*invoiceAmount*/ 48: [{ value: '', disabled: dis }, this.getVals(1, 18, this.getPat('n'))],
            /*identificationText*/ 49: [
                {value: '', disabled: dis},
                this.getVals(2, 512, this.getPat('an')),
            ],
            // Group 14:
            /*grossWeight*/ 50: [
                {value: '', disabled: dis},
                this.getVals(1, 16, this.getPat('n')),
            ],
            // Group 15:
            /*grossVolume*/ 51: [
                {value: '', disabled: dis},
                this.getVals(1, 9, this.getPat('n')),
            ],
        });
    }

    private generatePlaceholderInput(): void {
        // Fill mandatory fields from static form group
        if (this.isSuperUser$.getValue()) {
            fieldIds().forEach(id => {
                if (
                    this.optionalFieldIds.find(r => r.toString() === id) !== undefined
                ) {
                    this.staticFormGroup.get(id).markAsTouched();
                    if (placeholderEcmrFields[id]) {
                        this.staticFormGroup.get(id).setValue(placeholderEcmrFields[id]);
                    }
                }
            });
            // Fill dynamic form groups
            // Check if first id of positions array is mandatory
            if (
                this.optionalFieldIds.find(
                    optionalFieldId => positionFieldIds[0] === optionalFieldId
                )
            ) {
                // If so, add sufficient empty slots to array
                while (
                    this.dynamicFormArray.length < placeholderEcmrFields.positions.length
                    ) {
                    this.addPositionToFormArray();
                }
                // And fill them from the placeholder Array
                this.dynamicFormArray.controls.forEach((positionFormGroup, index) => {
                    positionFieldIds.forEach(positionFieldId => {
                        positionFormGroup
                            .get(positionFieldId)
                            .setValue(
                                placeholderEcmrFields.positions[index][positionFieldId]
                            );
                    });
                });
            }
        } else {
            fieldIds().forEach(id => {
                if (
                    this.mandatoryFieldIds.find(r => r.toString() === id) !== undefined
                ) {
                    if (
                        this.staticFormGroup.get(id).invalid &&
                        this.mandatoryFieldsContainSignature() !== id
                    ) {
                        this.staticFormGroup.get(id).markAsTouched();
                        if (placeholderEcmrFields[id]) {
                            this.staticFormGroup.get(id).setValue(placeholderEcmrFields[id]);
                        } else {
                            this.staticFormGroup.get(id).setValue('Placeholder');
                        }
                        if (this.staticFormGroup.get(id).invalid) {
                            this.staticFormGroup.get(id).setValue('GER');
                        }
                        if (this.staticFormGroup.get(id).invalid) {
                            this.staticFormGroup.get(id).setValue('100');
                        }
                    }
                }
            });

            // Fill dynamic form groups
            // Check if first id of positions array is mandatory
            if (
                this.mandatoryFieldIds.find(
                    mandatoryFieldId => positionFieldIds[0] === mandatoryFieldId
                )
            ) {
                // If so, add sufficient empty slots to array
                while (
                    this.dynamicFormArray.length < placeholderEcmrFields.positions.length
                    ) {
                    this.addPositionToFormArray();
                }
                // And fill them from the placeholder Array
                this.dynamicFormArray.controls.forEach((positionFormGroup, index) => {
                    positionFieldIds.forEach(positionFieldId => {
                        positionFormGroup
                            .get(positionFieldId)
                            .setValue(
                                placeholderEcmrFields.positions[index][positionFieldId]
                            );
                    });
                });
            }
        }
    }

    private mandatoryFieldsContainSignature(): string {
        for (const signatureFieldId of this.encryptionService.getSignatureFieldIds()) {
            const sigId = signatureFieldId;
            if (this.mandatoryFieldIds.includes(sigId)) {
                return sigId;
            }
        }
        return undefined;
    }

    private onlyMandatorySignatureFieldIds(): string[] {
        const sigIds: string[] = [];
        for (const signatureFieldId of this.allSignatureFieldIds) {
            // Use every field if User is SU
            if (
                this.mandatoryFieldIds.includes(signatureFieldId) ||
                this.isSuperUser$.getValue()
            ) {
                sigIds.push(signatureFieldId);
            }

            if(signatureFieldId == '69' && this.userRoles.includes('disponent')) {
                sigIds.push(signatureFieldId);
            }

            if(signatureFieldId == '70' && this.userRoles.includes('fahrer')) {
                sigIds.push(signatureFieldId);
            }

            if(signatureFieldId == '72' && this.userRoles.includes('empfaenger')) {
                sigIds.push(signatureFieldId);
            }
        }
        return sigIds;
    }

    private emptyFieldsForCopy() {
        //Field 8
        this.presentEcmr.fields['39'] = null;

        //All Signature fields
        this.presentEcmr.fields['69'] = null;
        this.presentEcmr.fields['70'] = null;
        this.presentEcmr.fields['71'] = null;
        this.presentEcmr.fields['72'] = null;
    }

    private setMandatoryFieldIdsForRole(): string[] {
        let mandatoryFieldIds: string[] = [];
        //Sender Role
        if (this.userRoles.includes('disponent')) {
            mandatoryFieldIds = [...this.disponentFields()];
        }
        return mandatoryFieldIds;
    }

    private disableFieldsForRole() {
        if (this.userRoles.includes('disponent')) {
            this.staticFormGroup.controls['39'].disable();
            this.disponentSignatureDisabled = false;
        }

        if (this.userRoles.includes('fahrer') || this.userRoles.includes('empfaenger')) {
            fieldIds().forEach(field => {
                this.staticFormGroup.controls[field].disable();
            })

            if (this.userRoles.includes('fahrer')) {
                if (this.staticFormGroup.controls['69'].value) {
                    this.carrierSignatureDisabled = false;
                    this.staticFormGroup.controls['70'].enable();
                }
                this.staticFormGroup.controls['39'].enable();
            } else if (this.userRoles.includes('empfaenger')) {
                if(this.staticFormGroup.controls['70'].value) {
                    this.consignorSignatureDisabled = false;
                }
                this.staticFormGroup.controls['71'].enable();
                this.staticFormGroup.controls['74'].enable();
            }
        }
    }

    private disponentFields(): string[] {
        let disponentFields: string[] = [];
        //1 Sender Area
        disponentFields.push('1', '2', '3', '4', '5', '6');
        //2 Consignee Area
        disponentFields.push('7', '8', '9', '10', '11', '13');
        //3 Taking over the goods Area
        disponentFields.push('14', '15', '16');
        //4 Delivery of the goods Area
        disponentFields.push('17');
        //6 Carrier Area
        disponentFields.push('21', '22', '23', '24', '25', '26', '27', '29');
        //10 Marks and Nos Area
        disponentFields.push('41');
        //11 Number of packages Area
        disponentFields.push('43');
        //12 Method of packing Area
        disponentFields.push('45');
        //13 Nature of the goods
        disponentFields.push('49');
        //14 Gross weight in kg
        disponentFields.push('50');
        //15 Volume in m³
        disponentFields.push('51');
        //21 Established
        disponentFields.push('67', '68');
        //75 Reference-ID
        disponentFields.push('75');

        return disponentFields;
    }
}
