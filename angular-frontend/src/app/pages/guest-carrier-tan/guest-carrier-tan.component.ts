import { Component } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { EcmrUserService } from '../../shared/services/http/ecmr-user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { MatButton } from '@angular/material/button';
import { MatFormField, MatHint, MatLabel } from '@angular/material/form-field';
import { MatInput } from '@angular/material/input';
import { endpoints } from '../../shared/services/http/endpoints';

@Component({
    selector: 'app-guest-carrier-tan',
    standalone: true,
    imports: [
        FormsModule,
        MatButton,
        MatFormField,
        MatHint,
        MatInput,
        MatLabel,
        ReactiveFormsModule
    ],
    templateUrl: './guest-carrier-tan.component.html',
    styleUrl: './guest-carrier-tan.component.scss'
})
export class GuestCarrierTanComponent {

    readonly ecmrId: string;

    form: FormGroup = new FormGroup({
        tan: new FormControl<string>(null, [Validators.required]),
    })

    constructor(private ecmrUserService: EcmrUserService, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
                private router: Router) {
        this.ecmrId = this.activatedRoute.snapshot.paramMap.get('id');
    }

    onSubmit() {
        if (!this.form.valid) {
            return;
        }
        this.ecmrUserService.checkGuestCarrierTan(this.ecmrId, this.form.controls.tan.value).subscribe(isCorrect => {
            if (isCorrect) {
                this.router.navigateByUrl(endpoints.frontend.editEcmr + this.ecmrId + "/tan/" + this.form.controls.tan.value);
            } else {
                this.snackbarService.pushNewStatus('Wrong tan', 5000);
            }
        })
    }
}
