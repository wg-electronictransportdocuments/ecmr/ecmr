import { Component } from '@angular/core';
import { FormControl, FormGroup, FormsModule, ReactiveFormsModule, Validators } from '@angular/forms';
import { MatFormField, MatLabel, MatSuffix } from '@angular/material/form-field';
import { MatIcon } from '@angular/material/icon';
import { MatButton, MatIconButton } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { NgIf } from '@angular/common';
import { GuestCarrierRequest } from '../../shared/models/dtos/requests/guest-carrier-request';
import { EcmrUserService } from '../../shared/services/http/ecmr-user.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { endpoints } from '../../shared/services/http/endpoints';

@Component({
    selector: 'app-guest-carrier',
    standalone: true,
    imports: [
        FormsModule,
        MatFormField,
        MatIcon,
        MatIconButton,
        MatInputModule,
        MatLabel,
        MatSuffix,
        NgIf,
        MatButton,
        ReactiveFormsModule,
    ],
    templateUrl: './guest-carrier.component.html',
    styleUrl: './guest-carrier.component.scss'
})
export class GuestCarrierComponent {

    readonly ecmrId: string;

    constructor(private ecmrUserService: EcmrUserService, private activatedRoute: ActivatedRoute, private snackbarService: SnackbarService,
                private router: Router) {
        this.ecmrId = this.activatedRoute.snapshot.paramMap.get('id');
    }

    form: FormGroup = new FormGroup({
        firstName: new FormControl<string>(null, [Validators.required]),
        lastName: new FormControl<string>(null, [Validators.required]),
        company: new FormControl<string>(null, []),
        phoneNumber: new FormControl<string>(null, [Validators.required]),
    })

    onSendTanClick() {
        this.form.markAsTouched();
        if (!this.form.valid) {
            return;
        }
        const guestCarrierRequest: GuestCarrierRequest = {
            firstName: this.form.controls.firstName.value,
            lastName: this.form.controls.lastName.value,
            company: this.form.controls.company.value,
            phoneNumber: this.form.controls.phoneNumber.value,
        }

        this.ecmrUserService.requestEcmrGuestCarrier(this.ecmrId, guestCarrierRequest).subscribe({
            next: () => {
                this.snackbarService.pushNewStatus(
                    `You will receive an SMS with your access code in a few moments`,
                    15000
                );
                this.router.navigateByUrl(endpoints.frontend.guestCarrierTan + this.ecmrId);
            }
        });
    }
}
