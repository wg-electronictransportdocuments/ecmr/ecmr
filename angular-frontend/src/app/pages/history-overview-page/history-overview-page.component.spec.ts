/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryOverviewPageComponent } from './history-overview-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { TableStateService } from '../../shared/services/state/table-state.service';
import { EcmrHistoricalListService } from '../../shared/services/http/ecmr-historical-list.service';
import { Subject } from 'rxjs';
import { EcmrListEntry } from '../../shared/models/entities/ecmr-list-entry';
import { mockEcmrList } from '../../shared/mockups/mock-objects';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDividerModule } from '@angular/material/divider';
import { MatInputModule } from '@angular/material/input';
import { ReactiveFormsModule } from '@angular/forms';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import {
  mockBreakpointObserver,
  resize,
} from '../../shared/mockups/mock-breakpoint-observer';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatPaginatorHarness } from '@angular/material/paginator/testing';
import { MatSortHarness } from '@angular/material/sort/testing';
import { MatTableHarness } from '@angular/material/table/testing';
import { DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('EcmrHistoryOverviewPageComponent', () => {
  let component: HistoryOverviewPageComponent;
  let fixture: ComponentFixture<HistoryOverviewPageComponent>;

  let loader: HarnessLoader;

  const mockListSubject: Subject<EcmrListEntry[]> = new Subject<
    EcmrListEntry[]
  >();

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [HistoryOverviewPageComponent],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        MatPaginatorModule,
        MatSortModule,
        MatTableModule,
        MatIconModule,
        MatFormFieldModule,
        MatDividerModule,
        MatInputModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: BreakpointObserver, useValue: mockBreakpointObserver },
        {
          provide: EcmrHistoricalListService,
          useValue: {
            getEcmr$: () => mockListSubject.asObservable(),
            updateEcmr$: () => {
              mockListSubject.next(mockEcmrList(20, false));
            },
          },
        },
        {
          provide: TableStateService,
          useValue: {
            getSortState: () => ({
              sortName: 'allContacts',
              active: '',
              direction: '',
            }),
            getPageState: () => ({
              pageName: 'allContacts',
              pageIndex: 0,
              pageSize: 25,
            }),
            setPageStateFromEvent: () => {},
            setSortState: () => {},
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryOverviewPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);
  });

  it('should provide a working filter for its table', async () => {
    await fixture.whenStable();
    const filterInputHarness = await loader.getHarness(
      MatInputHarness.with({ placeholder: 'input to filter' })
    );
    await filterInputHarness.setValue('abc');
    expect(component.dataSource.filter).toEqual('abc');
    const clearButtonHarness = await loader.getHarness(
      MatButtonHarness.with({ text: /close/i })
    );
    await clearButtonHarness.click();
    expect(component.dataSource.filter).toEqual('');
    await filterInputHarness.setValue('Testing');
    expect(component.dataSource.filter).toEqual('testing');
  });

  it('should provide a working pagination for its table', async () => {
    fixture.detectChanges();
    const paginator = await loader.getHarness(MatPaginatorHarness);
    fixture.detectChanges();
    await fixture.whenStable();
    await paginator.setPageSize(5);
    await paginator.goToNextPage();
    await paginator.goToPreviousPage();
    expect(component).toBeTruthy();
  });

  it('should provide a working sorting for its table', async () => {
    await fixture.whenStable();
    resize(Breakpoints.Medium);
    fixture.detectChanges();
    const sort = await loader.getHarness(MatSortHarness);
    fixture.detectChanges();
    const tableHeaders = await sort.getSortHeaders();
    for (const header of tableHeaders) {
      await header.click();
    }
    expect(component).toBeTruthy();
  });

  it('should select a version on click', async () => {
    const navigationSpy = spyOn(component.router, 'navigate');
    await fixture.whenStable();
    await loader.getHarness(MatButtonHarness.with({ text: /assignment/i }));
    const table = await loader.getHarness(MatTableHarness);
    const tableRows = await table.getRows();
    const firstRow = tableRows[0];
    const someCell = (await firstRow.getCells())[0];
    await (await someCell.host()).click();
    expect(component.selectedVersion).not.toBeNull();
    await (await someCell.host()).click();
    expect(component.selectedVersion).toBeNull();

    const doubleClickEl: DebugElement[] = fixture.debugElement.queryAll(
      By.css('tr')
    );
    doubleClickEl[1].triggerEventHandler(
      'dblclick',
      new MouseEvent('dblclick')
    );
    await fixture.whenStable();
    expect(navigationSpy).toHaveBeenCalled();
  });
});
