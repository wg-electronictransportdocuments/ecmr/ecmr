/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ActivatedRoute, Router } from '@angular/router';
import { endpoints } from '../../shared/services/http/endpoints';
import {
  AvailableTables,
  TableStateService,
} from '../../shared/services/state/table-state.service';
import {
  EcmrListEntry,
  ecmrListHeaderObjects,
} from '../../shared/models/entities/ecmr-list-entry';
import { EcmrHistoricalListService } from '../../shared/services/http/ecmr-historical-list.service';

@Component({
  selector: 'app-history-overview-page',
  templateUrl: './history-overview-page.component.html',
  styleUrls: ['./history-overview-page.component.scss'],
})
export class HistoryOverviewPageComponent implements OnInit, AfterViewInit {
  @ViewChild('filterInput', { static: false }) filterInput: ElementRef;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  ecmrHistoryUrlFragment = '/' + endpoints.frontend.ecmrHistory;

  dataSource: MatTableDataSource<EcmrListEntry>;

  tableName: AvailableTables = 'ecmrHistoryOverview';
  headers = ecmrListHeaderObjects;
  ecmrId: string;

  columnsToDisplay = [];
  keysToDisplay = [];
  columnsToDisplayMobile = [
    this.headers.version,
    this.headers.senderName,
    this.headers.status,
  ];
  columnsToDisplayDesktop = [
    this.headers.version,
    this.headers.senderName,
    this.headers.consigneeName,
    this.headers.lastUsername,
    this.headers.status,
    this.headers.lastUpdated,
  ];

  bpStateGtSm;

  selectedVersion: number;
  numberOfVersions: number;

  constructor(
    private ecmrHistoricalListService: EcmrHistoricalListService,
    private activatedRoute: ActivatedRoute,
    public breakpointObserver: BreakpointObserver,
    public tableStateService: TableStateService,
    public router: Router
  ) {
    this.ecmrId = this.activatedRoute.snapshot.paramMap.get('id');
    this.dataSource = new MatTableDataSource<EcmrListEntry>();
  }

  ngOnInit(): void {
    this.ecmrHistoricalListService.getEcmr$().subscribe((list) => {
      this.dataSource.data = list;
      this.numberOfVersions = list.length;
    });

    this.ecmrHistoricalListService.updateEcmr$(this.ecmrId);

    this.breakpointObserver
      .observe([Breakpoints.Small, Breakpoints.XSmall])
      .subscribe((result) => {
        this.bpStateGtSm = !result.matches;
        this.columnsToDisplay = result.matches
          ? this.columnsToDisplayMobile
          : this.columnsToDisplayDesktop;
        this.keysToDisplay = this.columnsToDisplay.map((r) => r.id);
      });
  }

  ngAfterViewInit() {
    // Sorting
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = this.ecmrSortingAccessor;

    //Pagination
    this.dataSource.paginator = this.paginator;
  }

  // Filter ecmr overview table for specific filter strings
  filter(filterEvent: Event): void {
    const filterValue = (filterEvent.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // Clear filter input
  clearFilters(): void {
    this.filterInput.nativeElement.value = '';
    this.dataSource.filter = '';
  }

  // Communicate sort-state-changes to respective service
  onSortData(sortEvent: Sort): void {
    this.tableStateService.setSortState({
      active: sortEvent.active,
      direction: sortEvent.direction,
      sortName: 'ecmrHistoryOverview',
    });
  }

  ecmrSortingAccessor = (data, sortHeaderId) => {
    let returnValue: number;
    switch (sortHeaderId) {
      case ecmrListHeaderObjects.senderName.id:
        return data[sortHeaderId].toLowerCase();
      case ecmrListHeaderObjects.consigneeName.id:
        return data[sortHeaderId].toLowerCase();
      case ecmrListHeaderObjects.lastUsername.id:
        return data[sortHeaderId].toLowerCase();
      case ecmrListHeaderObjects.status.id:
        switch (data[sortHeaderId]) {
          case 'GENERATED':
            returnValue = 0;
            break;
          case 'NEW':
            returnValue = 1;
            break;
          case 'LOADING':
            returnValue = 2;
            break;
          case 'IN TRANSPORT':
            returnValue = 3;
            break;
          case 'ARRIVED AT DESTINATION':
            returnValue = 4;
            break;
          case 'TRANSPORT COMPLETED':
            returnValue = 5;
            break;
          default:
            returnValue = 10;
        }
        return returnValue;
      case ecmrListHeaderObjects.lastUpdated.id:
        return new Date(data[sortHeaderId])?.getTime();
      default:
        return data[sortHeaderId];
    }
  };

  selectVersion(element, force = false) {
    if (!force) {
      this.selectedVersion =
        this.selectedVersion === element.version ? null : element.version;
    } else {
      this.selectedVersion = element.version;
    }
  }
}
