/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EcmrService} from '../../shared/services/http/ecmr.service';
import {Ecmr} from '../../shared/models/entities/ecmr';
import {endpoints} from '../../shared/services/http/endpoints';

@Component({
  selector: 'app-show-historical-ecmr-page',
  templateUrl: './history-viewer-page.component.html',
  styleUrls: ['./history-viewer-page.component.scss'],
})
export class HistoryViewerPageComponent implements OnInit, OnDestroy {
  ecmrOverviewUrlFragment = '/' + endpoints.frontend.ecmrOverview;
  ecmrHistoryUrlFragment = '/' + endpoints.frontend.ecmrHistory;

  ecmrId: string;
  version: number;
  nextVersion: number;
  previousVersion: number;
  numberOfVersions: number;
  ecmr: Ecmr = {
    ecmrId: '0',
    fields: {},
    lastUpdated: '',
    created: '',
    lastUserId: '',
    lastUsername: '',
    status: '',
    version: '',
    host: '',
  };
  nextEcmr: Ecmr = {
    ecmrId: '0',
    fields: {},
    lastUpdated: '',
    created: '',
    lastUserId: '',
    lastUsername: '',
    status: '',
    version: '',
    host: '',
  };

  constructor(
    public activatedRoute: ActivatedRoute,
    public ecmrService: EcmrService,
    private router: Router
  ) {
    this.ecmrId = this.activatedRoute.snapshot.paramMap.get('id');
    this.numberOfVersions = Number.parseInt(
      this.activatedRoute.snapshot.paramMap.get('numberOfVersions'),
      10
    );
    this.version = this.getVersion(
      Number.parseInt(this.activatedRoute.snapshot.paramMap.get('version'), 10),
      this.numberOfVersions
    );
    this.nextVersion = this.getNextVersion(this.version, this.numberOfVersions);
    this.previousVersion = this.getPreviousVersion(this.version);
    this.ecmrService.updateHistoricalEcmr$(
      this.ecmrId,
      this.version.toString()
    ).subscribe({
      next: response => {
        console.debug('updateHistoricalEcmr response is: ' + response);
      },
      error: error => {
        console.debug('updateHistoricalEcmr occurs: ' + error);
        this.router.navigate([endpoints.frontend.ecmrHistory + '/' + this.ecmrId]);
      }
    });
    this.ecmrService.historicalEcmr$.subscribe(r => {
      this.ecmr = r;
    });

    this.ecmrService.updateNextHistoricalEcmr$(
      this.ecmrId,
      this.nextVersion.toString()
    ).subscribe({
      next: value => {
        this.nextEcmr = value;
      }
    });
  }

  ngOnDestroy(): void {
    this.ecmrService.unsetEcmr();
  }

  ngOnInit() {
    this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  getVersion(requestedVersion: number, numberOfVersions: number) {
    if (requestedVersion < 1) {
      requestedVersion = 1;
    }
    if (requestedVersion > numberOfVersions) {
      requestedVersion = numberOfVersions;
    }
    return requestedVersion;
  }

  getPreviousVersion(version: number) {
    let previousVersion = 1;
    if (version < 2) {
      previousVersion = 1;
    } else {
      previousVersion = version - 1;
    }
    return previousVersion;
  }

  getNextVersion(version: number, numberOfVersions: number) {
    let nextVersion = 1;
    if (version >= numberOfVersions - 1) {
      nextVersion = numberOfVersions;
    } else {
      nextVersion = version + 1;
    }
    return nextVersion;
  }

  getEditedFieldKeys(): string[] {
    const updatedFieldKeys: string[] = [];
    for (const fieldsKey in this.ecmr.fields) {
      if (this.ecmr.fields[fieldsKey] != this.nextEcmr.fields[fieldsKey]) {
        updatedFieldKeys.push(fieldsKey);
      }
    }
    return updatedFieldKeys;
  }
}
