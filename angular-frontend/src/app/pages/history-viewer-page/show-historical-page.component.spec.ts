/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { EcmrService } from '../../shared/services/http/ecmr.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { HistoryViewerPageComponent } from './history-viewer-page.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ecmrServiceStub } from '../../shared/mockups/service-stubs';
import { MatIconModule } from '@angular/material/icon';
import { NO_ERRORS_SCHEMA } from '@angular/core';

// TODO: More specific testing needed
describe('ShowHistoricalEcmrPageComponent', () => {
  let component: HistoryViewerPageComponent;
  let fixture: ComponentFixture<HistoryViewerPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: { get: () => 'value' } } },
        },
        { provide: EcmrService, useValue: ecmrServiceStub() },
      ],
      declarations: [HistoryViewerPageComponent],
      imports: [MatToolbarModule, RouterTestingModule, MatIconModule],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryViewerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('should calculate the correct version correctly', () => {
    expect(component.getVersion(0, 5)).toBe(1);
    expect(component.getVersion(6, 5)).toBe(5);
  });
  it('should calculate next version correctly', () => {
    expect(component.getNextVersion(1, 5)).toBe(2);
    expect(component.getNextVersion(5, 5)).toBe(5);
    expect(component.getNextVersion(6, 5)).toBe(5);
  });
  it('should calculate previous version correctly', () => {
    expect(component.getPreviousVersion(1)).toBe(1);
    expect(component.getPreviousVersion(-2)).toBe(1);
  });
});
