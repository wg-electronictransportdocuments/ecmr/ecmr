/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { LoginPageComponent } from './login-page.component';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormBuilder,
} from '@angular/forms';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatInputModule } from '@angular/material/input';
import { AuthService } from '../../modules/user-management/services/auth.service';
import { EncryptionService } from '../../shared/services/encryption/encryption.service';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import {
  authServiceStub,
  encryptionServiceStub,
} from '../../shared/mockups/service-stubs';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        BrowserAnimationsModule,
        FormsModule,
        MatCardModule,
        MatFormFieldModule,
        MatInputModule,
        MatCheckboxModule,
        MatDialogModule,
        ReactiveFormsModule,
      ],
      declarations: [LoginPageComponent],
      providers: [
        MatDialog,
        { provide: UntypedFormBuilder },
        {
          provide: AuthService,
          useValue: authServiceStub(),
        },
        {
          provide: EncryptionService,
          useValue: encryptionServiceStub({}),
        },
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should work imprint() and privacy()', () => {
    component.openImprint();
    component.dialog.closeAll();
    component.openPrivacy();
    component.dialog.closeAll();
    expect(component).toBeTruthy();
  });
});
