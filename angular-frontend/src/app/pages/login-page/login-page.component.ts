/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, TemplateRef, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent {
  @ViewChild('imprint') imprintTemplate: TemplateRef<any>;
  @ViewChild('privacy') privacyTemplate: TemplateRef<any>;

  constructor(
    public dialog: MatDialog
  ) {
    // TODO: Make use of promise by logging out on failure
  }

  openImprint() {
    this.dialog.open(this.imprintTemplate);
  }

  openPrivacy() {
    this.dialog.open(this.privacyTemplate);
  }
}
