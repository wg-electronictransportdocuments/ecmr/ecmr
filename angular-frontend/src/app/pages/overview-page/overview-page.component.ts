/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterViewInit, Component, ElementRef, Input, OnInit, ViewChild, } from '@angular/core';
import { animate, state, style, transition, trigger, } from '@angular/animations';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserProfileService } from '../../modules/user-management/services/user-profile.service';
import { ShareEcmrService } from '../../shared/services/state/share-ecmr.service';
import { EcmrListService } from '../../shared/services/http/ecmr-list.service';
import { EcmrListEntry, ecmrListHeaderObjects, ecmrStatuses } from '../../shared/models/entities/ecmr-list-entry';
import { TableStateService } from 'src/app/shared/services/state/table-state.service';
import { endpoints } from '../../shared/services/http/endpoints';
import { MatPaginator } from '@angular/material/paginator';
import { EcmrDeleteService } from '../../shared/services/http/ecmr-delete.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ConfirmationDialogComponent, ConfirmDialogModel, } from 'src/app/shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { UserService } from '../../shared/services/http/user.service';
import { first, take } from 'rxjs/operators';
import { ImportDialogComponent } from 'src/app/shared/dialogs/import-dialog/import-dialog.component';
import { ExportObject } from '../../shared/models/entities/export-object';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { EcmrExportImportService } from '../../shared/services/http/ecmr-export-import.service';
import { TemplateRoutingService } from '../../shared/services/state/template-routing-service';
import { EcmrFilterService } from '../../shared/services/http/ecmr-filter.service';
import { EcmrFilter } from '../../shared/models/entities/ecmr-filter';
import { ShareGuestCarrierDialogComponent } from './share-guest-carrier-dialog/share-guest-carrier-dialog.component';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.scss'],
  animations: [
    trigger('detailExpand', [
      state('collapsed, void', style({height: '0px'})),
      state('expanded', style({height: '*'})),
      transition(
        'expanded <=> collapsed',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
      transition(
        'expanded <=> void',
        animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
      ),
    ]),
  ],
})
export class OverviewPageComponent implements OnInit, AfterViewInit {
  @ViewChild('filterInput', {static: false}) filterInput: ElementRef;
  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  @Input() isArchive = false;
  dataSource: MatTableDataSource<EcmrListEntry>;

  userIsDisponent = false;
  userIsSuperuser = false;

  referenceFilter = '';
  destinationFilter = '';
  dateOfCreationFilter = '';
  statusFilter = '';
  consigneeFilter = '';
  licensePlateFilter = '';
  universalFilter = '';
  carrierNameFilter = '';

  columnsToDisplay = [];
  keysToDisplay = [];
  columnsToDisplayMobile = [
    ecmrListHeaderObjects.refId,
    ecmrListHeaderObjects.status,
    ecmrListHeaderObjects.consigneeName,
    ecmrListHeaderObjects.edit,
    ecmrListHeaderObjects.share
  ];
  hiddenColumnsMobile = [];
  columnsToDisplayDesktop = [
    ecmrListHeaderObjects.ecmrId,
    ecmrListHeaderObjects.refId,
    ecmrListHeaderObjects.senderName,
    ecmrListHeaderObjects.consigneeName,
    ecmrListHeaderObjects.nationalInternationalTransport,
    ecmrListHeaderObjects.lastUsername,
    ecmrListHeaderObjects.status,
    ecmrListHeaderObjects.lastUpdated,
    ecmrListHeaderObjects.created,
  ];
  expandedElement: EcmrListEntry | null;

  showEcmrUrlFragment = '/' + endpoints.frontend.showEcmr;
  editEcmrUrlFragment = '/' + endpoints.frontend.editEcmr;
  copyEcmrUrlFragment = '/' + endpoints.frontend.copyEcmr;
  ecmrHistoryUrlFragment = '/' + endpoints.frontend.ecmrHistory;
  shareUrlFragment = '/' + endpoints.frontend.shareEcmr;

  bpStateGtSm;

  dialogRef: MatDialogRef<any>;

  protected readonly ecmrStatuses = ecmrStatuses;

  constructor(
    public breakpointObserver: BreakpointObserver,
    public ecmrDeleteService: EcmrDeleteService,
    public ecmrExportImportService: EcmrExportImportService,
    public ecmrListService: EcmrListService,
    public matDialog: MatDialog,
    public router: Router,
    public shareEcmrService: ShareEcmrService,
    public snackbarService: SnackbarService,
    public tableStateService: TableStateService,
    public userProfileService: UserProfileService,
    public userService: UserService,
    public templateRoutingService: TemplateRoutingService,
    public filterService: EcmrFilterService

  ) {
    this.dataSource = new MatTableDataSource<EcmrListEntry>([]);
    this.hiddenColumnsMobile = this.columnsToDisplayDesktop.filter(
      item => this.columnsToDisplayMobile.indexOf(item) < 0
    );
  }

  ngOnInit(): void {
    // On incoming change update dataSource for Table
    this.ecmrListService.getEcmrList$().subscribe(data => {
      this.dataSource.data = data;
    });
    this.ecmrListService.updateEcmrList$(this.isArchive);

    if(this.isArchive){
      this.setArchiveFilter();
    } else {
      this.setOverviewFilter();
    }

    this.userProfileService.isDisponent$.pipe(take(1)).subscribe(r => {
      this.userIsDisponent = r;
    });
    this.userProfileService.isSuperuser$.pipe(take(1)).subscribe(r => {
      this.userIsSuperuser = r;
    });

    this.userProfileService.updateUserRoles$();

    // Breakpoint Observing
    this.breakpointObserver
      .observe([Breakpoints.Small, Breakpoints.XSmall])
      .subscribe(result => {
        this.bpStateGtSm = !result.matches;
        this.columnsToDisplay = result.matches
          ? this.columnsToDisplayMobile
          : this.columnsToDisplayDesktop;
        this.keysToDisplay = this.columnsToDisplay.map(r => r.id);
      });
  }

  ngAfterViewInit(): void {
    // Sorting
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = this.ecmrSortingAccessor;

    //Pagination
    this.dataSource.paginator = this.paginator;
  }

  stopPropagation(event: Event): void {
    event.stopPropagation();
  }

  // Filter ecmr overview table for specific filter strings
  filter(filterEvent: Event): void {
    const filterValue = (filterEvent.target as HTMLInputElement).value;
    this.dataSource.filterPredicate = (data, filter) => Object.values(data).some(value => value.toString().toLowerCase().includes(filter));
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  // Filter ecmr overview table for multiple specific filter strings
  applyFilter() {
    const filterValues: EcmrFilter = {
      carrierNameColumn: this.carrierNameFilter.toLowerCase(),
      consigneeColumn: this.consigneeFilter.toLowerCase(),
      dateOfCreationColumn: this.dateOfCreationFilter,
      destinationColumn: this.destinationFilter.toLowerCase(),
      licensePlateColumn: this.licensePlateFilter.toLowerCase(),
      referenceColumn: this.referenceFilter.toLowerCase(),
      statusColumn: this.statusFilter
    };

    this.dataSource.filterPredicate = (data, filter) => {
      const dataDate = new Date(data.created).toISOString().split('T')[0];
      const searchTerm = JSON.parse(filter);
      return (searchTerm.carrierNameColumn ? data.carrierName.toLowerCase().includes(searchTerm.carrierNameColumn) : true) &&
          (searchTerm.consigneeColumn ? data.consigneeName.toLowerCase().includes(searchTerm.consigneeColumn) : true) &&
          (searchTerm.dateOfCreationColumn ? dataDate === searchTerm.dateOfCreationColumn : true) &&
          (searchTerm.destinationColumn ? data.consigneeAddress.toLowerCase().includes(searchTerm.destinationColumn) : true) &&
          (searchTerm.licensePlateColumn ? data.licensePlate.toLowerCase().includes(searchTerm.licensePlateColumn) : true) &&
          (searchTerm.referenceColumn ? data.referenceNr.toLowerCase().includes(searchTerm.referenceColumn) : true) &&
          (searchTerm.statusColumn ? data.status === searchTerm.statusColumn : true);
    };
    this.dataSource.filter = JSON.stringify(filterValues);
    this.isArchive ? this.filterService.setArchiveFilter(filterValues) : this.filterService.setOverviewFilter(filterValues);
  }

  setOverviewFilter(){
    this.filterService.overviewFilter$.pipe(first()).subscribe(filter => {
      if(filter){
        this.carrierNameFilter = filter.carrierNameColumn;
        this.consigneeFilter = filter.consigneeColumn;
        this.dateOfCreationFilter = filter.dateOfCreationColumn;
        this.destinationFilter = filter.destinationColumn;
        this.licensePlateFilter = filter.licensePlateColumn;
        this.referenceFilter = filter.referenceColumn;
        this.statusFilter = filter.statusColumn;
        this.applyFilter();
      }
    });
  }

  setArchiveFilter(){
    this.filterService.archiveFilter$.pipe(first()).subscribe(filter => {
      if(filter){
        this.carrierNameFilter = filter.carrierNameColumn;
        this.consigneeFilter = filter.consigneeColumn;
        this.dateOfCreationFilter = filter.dateOfCreationColumn;
        this.destinationFilter = filter.destinationColumn;
        this.licensePlateFilter = filter.licensePlateColumn;
        this.referenceFilter = filter.referenceColumn;
        this.statusFilter = filter.statusColumn;
        this.applyFilter();
      }
    });
  }

  // Clear filter input
  clearUniversalFilter(): void {
    this.universalFilter = '';
    this.dataSource.filter = '';
  }

  // Clear all filter
  clearAllFilter(): void {
    this.statusFilter = '';
    this.referenceFilter = '';
    this.dateOfCreationFilter = '';
    this.destinationFilter = '';
    this.consigneeFilter = '';
    this.licensePlateFilter = '';
    this.carrierNameFilter = '';
    this.universalFilter = '';
    this.dataSource.filter = '';
    this.isArchive ? this.filterService.resetArchiveFilter() : this.filterService.resetOverviewFilter();
  }

  // Communicate sort-state-changes to respective service
  onSortData(sortEvent: Sort): void {
    this.tableStateService.setSortState({
      active: sortEvent.active,
      direction: sortEvent.direction,
      sortName: 'ecmrOverview',
    });
  }

  // Custom sorting accessor
  ecmrSortingAccessor = (data, sortHeaderId) => {
    let returnValue: number;
    switch (sortHeaderId) {
      case ecmrListHeaderObjects.ecmrId.id:
        return data[sortHeaderId].toLowerCase();
      case ecmrListHeaderObjects.senderName.id:
        return data[sortHeaderId].toLowerCase();
      case ecmrListHeaderObjects.consigneeName.id:
        return data[sortHeaderId].toLowerCase();
      case ecmrListHeaderObjects.lastUsername.id:
        return data[sortHeaderId].toLowerCase();
      case ecmrListHeaderObjects.status.id:
        switch (data[sortHeaderId]) {
          case 'GENERATED':
            returnValue = 0;
            break;
          case 'NEW':
            returnValue = 1;
            break;
          case 'LOADING':
            returnValue = 2;
            break;
          case 'IN TRANSPORT':
            returnValue = 3;
            break;
          case 'ARRIVED AT DESTINATION':
            returnValue = 4;
            break;
          case 'TRANSPORT COMPLETED':
            returnValue = 5;
            break;
          default:
            returnValue = 10;
        }
        return returnValue;
      case ecmrListHeaderObjects.lastUpdated.id:
        return new Date(data[sortHeaderId])?.getTime();
      case ecmrListHeaderObjects.created.id:
        return new Date(data[sortHeaderId])?.getTime();
      default:
        return data[sortHeaderId];
    }
  };

  // Hits the delete-service and updates the list upon resolving of request
  requestDeleteAndUpdateTable(ecmrId: string): void {
    this.ecmrDeleteService.deleteEcmr(ecmrId).subscribe(() => {
      this.ecmrListService.updateEcmrList$(this.isArchive);
    });
  }

  // Handle delete eCMR button pressed
  onDeleteButtonPressed(ecmrId: string): void {
    this.confirmDialog(
      'Do you really want to delete the eCMR?',
      'Delete eCMR ' + ecmrId + '?'
    );
    this.dialogRef.afterClosed().subscribe(dialogResult => {
      if (dialogResult) {
        this.requestDeleteAndUpdateTable(ecmrId);
      }
    });
  }

  onShareQrForCarrierClick(ecmrId: string, referenceNumber: string): void {
    this.dialogRef = this.matDialog.open(ShareGuestCarrierDialogComponent, {
      maxHeight: '100vh',
      maxWidth: '100vw',
      data: {
        ecmrId: ecmrId,
        referenceNumber: referenceNumber
      }
    });
  }

  scanQrToken(): void {
    this.dialogRef = this.matDialog.open(ImportDialogComponent, {
      maxHeight: '100vh',
      maxWidth: '100vw',
    });
    this.dialogRef.afterClosed().subscribe(res => {
      if (res) {
        try {
          const obj: ExportObject = res as ExportObject;
          this.ecmrExportImportService
            .getImport(obj)
            .pipe(take(1))
            .subscribe({
              next: () => {
                this.ecmrListService.updateEcmrList$(this.isArchive);
              },
            });
        } catch (e) {
          this.snackbarService.pushNewStatus(
            'QR Code is corrupted. Try again.'
          );
        }
      }
    });
  }

  // Confirmation dialog
  confirmDialog(message: string, title: string): void {
    const dialogData = new ConfirmDialogModel(title, message);
    this.dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
      maxWidth: '400px',
      data: dialogData,
    });
  }

    openEditEcmrPage(ecmr: EcmrListEntry, action: string) {
    if(ecmr) {
      this.templateRoutingService.setTemplateCreation(false);
      this.templateRoutingService.setEditTemplate(false, null);
      if(action == "edit") {
        this.router.navigateByUrl(this.editEcmrUrlFragment + ecmr.ecmrId);
      } else if(action == "copy") {
        this.router.navigateByUrl(this.copyEcmrUrlFragment + ecmr.ecmrId);
      }
    } else {
      this.templateRoutingService.setTemplateCreation(false);
      this.templateRoutingService.setEditTemplate(false, null);
      this.router.navigateByUrl(this.editEcmrUrlFragment + 'new');
    }
    }
}
