import { Component, Inject } from '@angular/core';
import { QRCodeModule } from 'angularx-qrcode';
import { MatTooltip } from '@angular/material/tooltip';
import { environment } from '../../../../environments/environment';
import { MAT_DIALOG_DATA, MatDialogActions, MatDialogClose, MatDialogContent, MatDialogTitle } from '@angular/material/dialog';
import { ConfirmDialogModel } from '../../../shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { QRCodeDecoderErrorCorrectionLevel } from '@zxing/library';
import ErrorCorrectionLevel from '@zxing/library/es2015/core/qrcode/decoder/ErrorCorrectionLevel';
import { MatButton } from '@angular/material/button';
import { Clipboard } from '@angular/cdk/clipboard';
import { SnackbarService } from '../../../shared/services/state/snackbar.service';

@Component({
  selector: 'app-share-guest-carrier-dialog',
  standalone: true,
  imports: [
    QRCodeModule,
    MatTooltip,
    MatDialogActions,
    MatDialogContent,
    MatDialogTitle,
    MatButton,
    MatDialogClose
  ],
  templateUrl: './share-guest-carrier-dialog.component.html',
  styleUrl: './share-guest-carrier-dialog.component.scss'
})
export class ShareGuestCarrierDialogComponent {

  readonly qrCodeUrl: string;
  readonly referenceNumber: string;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private clipboard: Clipboard, private snackbarService: SnackbarService) {
    this.qrCodeUrl = environment.frontendUrl + '/guest-carrier/' + data.ecmrId;
    this.referenceNumber = data.referenceNumber;
    console.log(this.referenceNumber);
  }


  onQrCodeClick(): void {
    this.clipboard.copy(this.qrCodeUrl);
    this.snackbarService.pushNewStatus(
        `Carrier guest access url saved to clipboard`,
        5000
    );
  }
}
