/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { SharePageComponent } from './share-page.component';
import { BehaviorSubject, of } from 'rxjs';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactListEntry } from '../../shared/models/entities/contact-list-entry';
import { EcmrUserService } from '../../shared/services/http/ecmr-user.service';
import { HarnessLoader } from '@angular/cdk/testing';
import { MatButtonHarness } from '@angular/material/button/testing';
import { MatDividerModule } from '@angular/material/divider';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputHarness } from '@angular/material/input/testing';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorHarness } from '@angular/material/paginator/testing';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortHarness } from '@angular/material/sort/testing';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import {
  mockBreakpointObserver,
  resize,
} from '../../shared/mockups/mock-breakpoint-observer';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { ShareEcmrService } from '../../shared/services/state/share-ecmr.service';
import { TableStateService } from '../../shared/services/state/table-state.service';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { UserService } from '../../shared/services/http/user.service';
import { contactListHeaderObjects } from '../../shared/models/entities/contact';
import { mockContacts } from '../../shared/mockups/mock-objects';
import { MatDialogModule } from '@angular/material/dialog';
import {
  shareEcmrServiceStub,
  snackbarServiceStub,
  userProfileServiceStub,
} from '../../shared/mockups/service-stubs';
import { UserProfileService } from 'src/app/modules/user-management/services/user-profile.service';
import { SnackbarService } from 'src/app/shared/services/state/snackbar.service';

describe('SharePageComponent', () => {
  let component: SharePageComponent;
  let fixture: ComponentFixture<SharePageComponent>;

  let loader: HarnessLoader;

  const mockContactArray = mockContacts(10, 99);
  const mockAssignResponse: BehaviorSubject<unknown> =
    new BehaviorSubject<unknown>('');

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [SharePageComponent],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule,
        MatPaginatorModule,
        MatDialogModule,
        MatSortModule,
        MatTableModule,
        MatIconModule,
        MatFormFieldModule,
        MatDividerModule,
        MatInputModule,
        ReactiveFormsModule,
      ],
      providers: [
        { provide: BreakpointObserver, useValue: mockBreakpointObserver },
        {
          provide: UserService,
          useValue: {
            userContacts$: of(mockContactArray),
            updateUserContacts$: () => {},
          },
        },
        {
          provide: EcmrUserService,
          useValue: {
            assignEcmrToUser: () => mockAssignResponse.asObservable(),
          },
        },
        {
          provide: TableStateService,
          useValue: {
            getSortState: () => ({
              sortName: 'allContacts',
              active: '',
              direction: '',
            }),
            getPageState: () => ({
              pageName: 'allContacts',
              pageIndex: 0,
              pageSize: 25,
            }),
            setPageStateFromEvent: () => {},
            setSortState: () => {},
          },
        },
        {
          provide: ShareEcmrService,
          useValue: shareEcmrServiceStub(),
        },
        {
          provide: Router,
          useValue: {
            navigate: () => new Promise<void>(() => {}),
          },
        },
        {
          provide: UserProfileService,
          useValue: userProfileServiceStub(),
        },
        {
          provide: SnackbarService,
          useValue: snackbarServiceStub(),
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SharePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    loader = TestbedHarnessEnvironment.loader(fixture);
    //window.onbeforeunload = () => '';
  });

  it('should provide a working filter for its table', async () => {
    await fixture.whenStable();
    const filterInputHarness = await loader.getHarness(
      MatInputHarness.with({ placeholder: 'input to filter' })
    );
    await filterInputHarness.setValue('abc');
    expect(component.dataSource.filter).toEqual('abc');
    const clearButtonHarness = await loader.getHarness(
      MatButtonHarness.with({ text: /close/i })
    );
    await clearButtonHarness.click();
    expect(component.dataSource.filter).toEqual('');
    await filterInputHarness.setValue('Testing');
    expect(component.dataSource.filter).toEqual('testing');
  });

  it('should provide a working pagination for its table', async () => {
    fixture.detectChanges();
    const paginator = await loader.getHarness(MatPaginatorHarness);
    fixture.detectChanges();
    await fixture.whenStable();
    await paginator.setPageSize(5);
    await paginator.goToNextPage();
    await paginator.goToPreviousPage();
    expect(component).toBeTruthy();
  });

  it('should provide a working sorting for its table', async () => {
    await fixture.whenStable();
    resize(Breakpoints.Medium);
    fixture.detectChanges();
    const sort = await loader.getHarness(MatSortHarness);
    fixture.detectChanges();
    const tableHeaders = await sort.getSortHeaders();
    for (const header of tableHeaders) {
      await header.click();
    }
    expect(component).toBeTruthy();
  });

  it('should provide a working sorting accessor', async () => {
    const mockListEntry: ContactListEntry = {
      amountOfContacts: 12,
      contactEmail: 'myEmail.123@asdf.ghi',
      contactId: 'SomeId',
      contactName: 'My Name',
      contactType: 'My Type',
      lastContact: '2021-11-10T15:06:41.897+00:00',
    };

    expect(
      component.contactSortingAccessor(
        mockListEntry,
        contactListHeaderObjects.lastContact.id
      )
    ).toBe(new Date(mockListEntry.lastContact).getTime());

    expect(
      component.contactSortingAccessor(
        mockListEntry,
        contactListHeaderObjects.contactType.id
      )
    ).toBe('my type');

    expect(
      component.contactSortingAccessor(
        mockListEntry,
        contactListHeaderObjects.contactName.id
      )
    ).toBe('my name');

    expect(
      component.contactSortingAccessor(
        mockListEntry,
        contactListHeaderObjects.contactEmail.id
      )
    ).toBe('myemail.123@asdf.ghi');

    expect(
      component.contactSortingAccessor(
        mockListEntry,
        contactListHeaderObjects.amountOfContacts.id
      )
    ).toBe(12);
  });

  it('should work shareViaEmail-Function', async () => {
    component = fixture.componentInstance;
    spyOn(component, 'shareViaEmail').and.callFake(async () => {});
    fixture.detectChanges();
    component.shareViaEmail();
    expect(component).toBeTruthy();
  });

  it('should work requestAssignment-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.requestAssignment();
    expect(component).toBeTruthy();
  });
});
