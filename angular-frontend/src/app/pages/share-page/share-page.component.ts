/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MatSort, Sort } from '@angular/material/sort';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { UserService } from '../../shared/services/http/user.service';
import { checkedContactListHeaderObjects, ContactListEntry } from '../../shared/models/entities/contact-list-entry';
import { EcmrUserService } from '../../shared/services/http/ecmr-user.service';
import { Router } from '@angular/router';
import { endpoints } from '../../shared/services/http/endpoints';
import {
  AvailableTables,
  TableStateService,
} from '../../shared/services/state/table-state.service';
import { ShareEcmrService } from '../../shared/services/state/share-ecmr.service';
import { take } from 'rxjs/operators';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ShareQrDialogComponent } from '../../shared/dialogs/share-qr-dialog/share-qr-dialog.component';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { ExportObject } from '../../shared/models/entities/export-object';
import { UserProfileService } from 'src/app/modules/user-management/services/user-profile.service';

@Component({
  selector: 'app-share-page',
  templateUrl: './share-page.component.html',
  styleUrls: ['./share-page.component.scss'],
})
export class SharePageComponent implements OnInit, AfterViewInit {
  @ViewChild('filterInput', { static: false }) filterInput: ElementRef;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  dialogRef: MatDialogRef<any>;

  ecmrOverviewUrlFragment = '/' + endpoints.frontend.ecmrOverview;

  dataSource: MatTableDataSource<ContactListEntry>;
  exportObject: ExportObject;

  tableName: AvailableTables = 'allContacts';
  headers = checkedContactListHeaderObjects;

  columnsToDisplay = [];
  keysToDisplay = [];
  columnsToDisplayMobile = [
    this.headers.contactName,
    this.headers.contactEmail,
    this.headers.lastContact,
  ];
  columnsToDisplayDesktop = [
    this.headers.contactName,
    this.headers.contactType,
    this.headers.contactEmail,
    this.headers.lastContact,
    this.headers.amountOfContacts,
  ];

  bpStateGtSm;

  selectedContact: ContactListEntry;

  constructor(
    public breakpointObserver: BreakpointObserver,
    public userService: UserService,
    public userProfileService: UserProfileService,
    public ecmrUserService: EcmrUserService,
    public tableStateService: TableStateService,
    public shareEcmrService: ShareEcmrService,
    public matDialog: MatDialog,
    public router: Router,
    public snackbarService: SnackbarService
  ) {
    this.dataSource = new MatTableDataSource<ContactListEntry>();
    this.exportObject = shareEcmrService.getExportObjectToShare();
    if (!this.exportObject) {
      this.matDialog.closeAll();
      this.router.navigate([this.ecmrOverviewUrlFragment]);
      this.snackbarService.pushNewStatus(
        'Your request to access the share-page was invalid. The eCMR was already shared or' +
          ' there was no eCMR selected in the first place. Heading back to overview.',
        10000
      );
    }
  }

  ngOnInit(): void {
    this.userService.userContacts$.pipe(take(1)).subscribe(r => {
      this.dataSource.data = r;
    });
    this.userService.updateUserContacts$();
    this.breakpointObserver
      .observe([Breakpoints.Small, Breakpoints.XSmall])
      .subscribe(result => {
        this.bpStateGtSm = !result.matches;
        this.columnsToDisplay = result.matches
          ? this.columnsToDisplayMobile
          : this.columnsToDisplayDesktop;
        this.keysToDisplay = this.columnsToDisplay.map(r => r.id);
      });
  }

  ngAfterViewInit() {
    // Sorting
    this.dataSource.sort = this.sort;
    this.dataSource.sortingDataAccessor = this.contactSortingAccessor;

    //Pagination
    this.dataSource.paginator = this.paginator;
  }

  // Filter ecmr overview table for specific filter strings
  filter(filterEvent: Event): void {
    const filterValue = (filterEvent.target as HTMLInputElement).value;
    if (this.dataSource.filter !== undefined) {
      this.dataSource.filter = filterValue.trim().toLowerCase();
    }
  }

  // Clear filter input
  clearFilters(): void {
    this.filterInput.nativeElement.value = '';
    this.dataSource.filter = '';
  }

  // Communicate sort-state-changes to respective service
  onSortData(sortEvent: Sort): void {
    this.tableStateService.setSortState({
      active: sortEvent.active,
      direction: sortEvent.direction,
      sortName: 'allContacts',
    });
  }

  contactSortingAccessor = (data, sortHeaderId) => {
    switch (sortHeaderId) {
      case this.headers.lastContact.id:
        return new Date(data[sortHeaderId]).getTime();
      case this.headers.contactType.id:
        return data[sortHeaderId].toLowerCase();
      case this.headers.contactName.id:
        return data[sortHeaderId].toLowerCase();
      case this.headers.contactEmail.id:
        return data[sortHeaderId].toLowerCase();
      default:
        return data[sortHeaderId];
    }
  };

  // Share actual ecmr via email
  async shareViaEmail(): Promise<void> {
    const profile = await this.userProfileService.userProfile;
    const payload = {host: this.shareEcmrService.getExportObjectToShare().host, ecmrId: this.shareEcmrService.getExportObjectToShare().ecmrId};
    const token = btoa(JSON.stringify(payload));
    const mailto = '<user@email.com>';
    const subject = 'I would like to share an eCMR with you';
    let body = '';
    body += 'Dear <Partner>,';
    body += '\n\n';
    body += 'I would like to share an eCMR with you!';
    body += '\n\n';
    body += 'Please paste the following token in the import dialog of your eCMR application:';
    body += '\n\n';
    body += 'Token: \"' + token + '\"';
    body += '\n\n';
    body += 'Best Regards!';
    body += '\n';
    body += profile.firstName + ' ' + profile.lastName;
    window.location.href = 'mailto:' + mailto + '?subject=' + subject + '&body=' + encodeURIComponent(body);
    this.snackbarService.pushNewStatus(
      'Your default email program should have opened up. Please adjust the preset' +
        ' data to your needs.',
      10000
    );
  }

  // Hits the assign-service and redirects to success page if resolving of request is successful
  requestAssignment(): void {
    const users: string[] = [];
    for (const el of this.dataSource.data) {
      if (el.checked) {
        users.push(el.contactId);
      }
    }
    if (users.length > 0) {
      this.ecmrUserService
        .assignEcmrToUsers(
          this.shareEcmrService.getExportObjectToShare().ecmrId,
          users)
        .pipe(take(1))
        .subscribe(() => {
          for (const el of this.dataSource.data) {
            if (el.checked) {
              el.checked = false;
            }
          }
          this.snackbarService.pushNewStatus(
            'The eCMR has been shared with the selected contact(s).',
            5000
          );
        });
    }
  }

  selectContact(element) {
    element.checked = element.checked ? false : true;
  }

  onQrCode() {
    this.dialogRef = this.matDialog.open(ShareQrDialogComponent, {
      disableClose: true,
      maxHeight: '95vh',
      maxWidth: '95vw',
    });
  }
}
