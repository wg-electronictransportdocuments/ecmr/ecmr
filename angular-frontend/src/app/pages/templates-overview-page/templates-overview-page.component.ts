/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
    AfterViewInit,
    Component,
    ElementRef,
    OnInit,
    ViewChild,
} from '@angular/core';
import {
    animate,
    state,
    style,
    transition,
    trigger,
} from '@angular/animations';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { MatSort, Sort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { UserProfileService } from '../../modules/user-management/services/user-profile.service';
import { TableStateService } from 'src/app/shared/services/state/table-state.service';
import { endpoints } from '../../shared/services/http/endpoints';
import { MatPaginator } from '@angular/material/paginator';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import {
    ConfirmationDialogComponent,
    ConfirmDialogModel,
} from 'src/app/shared/dialogs/confirmation-dialog/confirmation-dialog.component';
import { first, take } from 'rxjs/operators';
import { SnackbarService } from '../../shared/services/state/snackbar.service';
import { EcmrTemplateListEntry, ecmrTemplateListHeaderObjects } from '../../shared/models/entities/ecmr-template-list-entry';
import { EcmrTemplateListService } from '../../shared/services/http/ecmr-template-list.service';
import { EcmrTemplateService } from '../../shared/services/http/ecmr-template.service';
import { TemplateRoutingService } from '../../shared/services/state/template-routing-service';
import { ecmrStatuses } from '../../shared/models/entities/ecmr-list-entry';
import { EcmrFilterService } from '../../shared/services/http/ecmr-filter.service';

@Component({
    selector: 'app-templates-overview-page',
    templateUrl: './templates-overview-page.component.html',
    styleUrls: ['./templates-overview-page.component.scss'],
    animations: [
        trigger('detailExpand', [
            state('collapsed, void', style({ height: '0px' })),
            state('expanded', style({ height: '*' })),
            transition(
                'expanded <=> collapsed',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
            ),
            transition(
                'expanded <=> void',
                animate('225ms cubic-bezier(0.4, 0.0, 0.2, 1)')
            ),
        ]),
    ],
})
export class TemplatesOverviewPageComponent implements OnInit, AfterViewInit {
    @ViewChild('filterInput', { static: false }) filterInput: ElementRef;
    @ViewChild(MatSort, { static: false }) sort: MatSort;
    @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

    dataSource = new MatTableDataSource<EcmrTemplateListEntry>([]);

    nameFilter = '';
    senderFilter = '';
    consigneeFilter = '';
    universalFilter = '';

    userIsDisponent = false;
    userIsSuperuser = false;

    columnsToDisplay = [];
    keysToDisplay = [];
    columnsToDisplayMobile = [
        ecmrTemplateListHeaderObjects.templateId,
        ecmrTemplateListHeaderObjects.name,
        ecmrTemplateListHeaderObjects.created,
    ];
    hiddenColumnsMobile = [];
    columnsToDisplayDesktop = [
        ecmrTemplateListHeaderObjects.templateId,
        ecmrTemplateListHeaderObjects.name,
        ecmrTemplateListHeaderObjects.refId,
        ecmrTemplateListHeaderObjects.senderName,
        ecmrTemplateListHeaderObjects.consigneeName,
        ecmrTemplateListHeaderObjects.lastUsername,
        ecmrTemplateListHeaderObjects.status,
        ecmrTemplateListHeaderObjects.lastUpdated,
        ecmrTemplateListHeaderObjects.created,
    ];
    expandedElement: EcmrTemplateListEntry | null;

    showEcmrUrlFragment = '/' + endpoints.frontend.showEcmr;
    editEcmrUrlFragment = '/' + endpoints.frontend.editEcmr;
    ecmrHistoryUrlFragment = '/' + endpoints.frontend.ecmrHistory;
    shareUrlFragment = '/' + endpoints.frontend.shareEcmr;

    bpStateGtSm;

    dialogRef: MatDialogRef<any>;

    constructor(
        public breakpointObserver: BreakpointObserver,
        public ecmrTemplateListService: EcmrTemplateListService,
        public templateService: EcmrTemplateService,
        public matDialog: MatDialog,
        public router: Router,
        public snackbarService: SnackbarService,
        public tableStateService: TableStateService,
        public userProfileService: UserProfileService,
        public templateRoutingService: TemplateRoutingService,
        public filterService: EcmrFilterService

    ) {
        this.hiddenColumnsMobile = this.columnsToDisplayDesktop.filter(
            item => this.columnsToDisplayMobile.indexOf(item) < 0
        );
    }

    ngOnInit(): void {
        // On incoming change update dataSource for Table
        this.ecmrTemplateListService.getEcmrTemplateList$().subscribe(data => {
            this.dataSource.data = data;
        });
        this.ecmrTemplateListService.updateEcmrTemplateList$();

        this.setTemplateFilter();

        this.userProfileService.isDisponent$.pipe(take(1)).subscribe(r => {
            this.userIsDisponent = r;
        });
        this.userProfileService.isSuperuser$.pipe(take(1)).subscribe(r => {
            this.userIsSuperuser = r;
        });

        this.userProfileService.updateUserRoles$();

        // Breakpoint Observing
        this.breakpointObserver
            .observe([Breakpoints.Small, Breakpoints.XSmall])
            .subscribe(result => {
                this.bpStateGtSm = !result.matches;
                this.columnsToDisplay = result.matches
                    ? this.columnsToDisplayMobile
                    : this.columnsToDisplayDesktop;
                this.keysToDisplay = this.columnsToDisplay.map(r => r.id);
            });
    }

    ngAfterViewInit(): void {
        // Sorting
        this.dataSource.sort = this.sort;
        this.dataSource.sortingDataAccessor = this.templateSortingAccessor;

        //Pagination
        this.dataSource.paginator = this.paginator;
    }

    // Filter ecmr overview table for specific filter strings
    filter(filterEvent: Event): void {
        const filterValue = (filterEvent.target as HTMLInputElement).value;
        this.dataSource.filterPredicate = (data, filter) => Object.values(data).some(value => value.toString().toLowerCase().includes(filter));
        this.dataSource.filter = filterValue.trim().toLowerCase();
    }

    // Filter ecmr overview table for multiple specific filter strings
    applyFilter() {
        const filterValues = {
            templateNameColumn: this.nameFilter.toLowerCase(),
            senderNameColumn: this.senderFilter.toLowerCase(),
            consigneeColumn: this.consigneeFilter.toLowerCase()
        };

        this.dataSource.filterPredicate = (data, filter) => {
            const searchTerm = JSON.parse(filter);
            return (
                (searchTerm.templateNameColumn ? data.name.toLowerCase().includes(searchTerm.templateNameColumn) : true) &&
                (searchTerm.senderNameColumn ? data.senderName.toLowerCase().includes(searchTerm.senderNameColumn) : true) &&
                (searchTerm.consigneeColumn ? data.consigneeName.toLowerCase().includes(searchTerm.consigneeColumn) : true));
        };
        this.dataSource.filter = JSON.stringify(filterValues);
        this.filterService.setTemplateFilter(filterValues);
    }

    setTemplateFilter(){
        this.filterService.templateFilter$.pipe(first()).subscribe(filter => {
            if(filter){
                this.consigneeFilter = filter.consigneeColumn;
                this.senderFilter = filter.senderNameColumn;
                this.nameFilter = filter.templateNameColumn;
                this.applyFilter();
            }
        });
    }

    // Clear filter input
    clearUniversalFilter(): void {
        this.universalFilter = '';
        this.dataSource.filter = '';
    }

    // Clear all filter
    clearAllFilter(): void {
        this.nameFilter = '';
        this.senderFilter = '';
        this.consigneeFilter = '';
        this.dataSource.filter = '';
        this.filterService.resetTemplateFilter();
    }

    // Communicate sort-state-changes to respective service
    onSortData(sortEvent: Sort): void {
        this.tableStateService.setSortState({
            active: sortEvent.active,
            direction: sortEvent.direction,
            sortName: 'ecmrTemplatesOverview',
        });
    }

    // Custom sorting accessor
    templateSortingAccessor = (data, sortHeaderId) => {
        let returnValue: number;
        switch (sortHeaderId) {
            case ecmrTemplateListHeaderObjects.templateId.id:
                return data[sortHeaderId].toLowerCase();
            case ecmrTemplateListHeaderObjects.senderName.id:
                return data[sortHeaderId].toLowerCase();
            case ecmrTemplateListHeaderObjects.consigneeName.id:
                return data[sortHeaderId].toLowerCase();
            case ecmrTemplateListHeaderObjects.lastUsername.id:
                return data[sortHeaderId].toLowerCase();
            case ecmrTemplateListHeaderObjects.status.id:
                switch (data[sortHeaderId]) {
                    case 'GENERATED':
                        returnValue = 0;
                        break;
                    case 'NEW':
                        returnValue = 1;
                        break;
                    case 'LOADING':
                        returnValue = 2;
                        break;
                    case 'IN TRANSPORT':
                        returnValue = 3;
                        break;
                    case 'ARRIVED AT DESTINATION':
                        returnValue = 4;
                        break;
                    case 'TRANSPORT COMPLETED':
                        returnValue = 5;
                        break;
                    default:
                        returnValue = 10;
                }
                return returnValue;
            case ecmrTemplateListHeaderObjects.lastUpdated.id:
                return new Date(data[sortHeaderId])?.getTime();
            case ecmrTemplateListHeaderObjects.created.id:
                return new Date(data[sortHeaderId])?.getTime();
            default:
                return data[sortHeaderId];
        }
    };

    requestDeleteAndUpdateTable(templateId: number, templateName: string): void {
        this.templateService.deleteTemplate$(templateId, templateName).subscribe(() => {
            this.ecmrTemplateListService.updateEcmrTemplateList$();
        });
    }

    onDeleteButtonPressed(templateId: number, templateName: string): void {
        this.confirmDialog(
            'Do you really want to delete this Template? This is permanent and cannot be undone.',
            'Delete Template - ' + templateName + '?'
        );
        this.dialogRef.afterClosed().subscribe(dialogResult => {
            if (dialogResult) {
                this.requestDeleteAndUpdateTable(templateId, templateName);
            }
        });
    }

    stopPropagation(event: Event): void {
        event.stopPropagation();
    }

    confirmDialog(message: string, title: string): void {
        const dialogData = new ConfirmDialogModel(title, message);
        this.dialogRef = this.matDialog.open(ConfirmationDialogComponent, {
            data: dialogData,
        });
    }

    openAddTemplate() {
        this.templateRoutingService.setEditTemplate(false,null);
        this.templateRoutingService.setTemplateCreation(true);
        this.router.navigateByUrl(this.editEcmrUrlFragment + 'new');
    }

    openEditTemplate(template: EcmrTemplateListEntry) {
        this.templateRoutingService.setEditTemplate(true, template.templateId);
        this.templateRoutingService.setTemplateCreation(false);
        this.router.navigateByUrl(this.editEcmrUrlFragment + template.ecmrId);
    }

    protected readonly ecmrStatuses = ecmrStatuses;
}
