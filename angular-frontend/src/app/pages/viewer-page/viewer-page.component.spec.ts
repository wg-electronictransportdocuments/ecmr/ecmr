/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ViewerPageComponent } from './viewer-page.component';
import { ActivatedRoute } from '@angular/router';
import { EcmrService } from '../../shared/services/http/ecmr.service';
import { mockEcmr } from '../../shared/mockups/mock-objects';
import { RouterTestingModule } from '@angular/router/testing';
import { OverviewPageComponent } from '../overview-page/overview-page.component';
import { EditorPageComponent } from '../editor-page/editor-page.component';
import { ShareEcmrService } from 'src/app/shared/services/state/share-ecmr.service';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatDialogModule } from '@angular/material/dialog';
import { MatButtonModule } from '@angular/material/button';
import { HarnessLoader } from '@angular/cdk/testing';
import { TestbedHarnessEnvironment } from '@angular/cdk/testing/testbed';
import { MatDialogHarness } from '@angular/material/dialog/testing';
import { OverlayContainer } from '@angular/cdk/overlay';
import { MatButtonHarness } from '@angular/material/button/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { SharePageComponent } from '../share-page/share-page.component';
import {
  ecmrServiceStub,
  shareEcmrServiceStub,
} from '../../shared/mockups/service-stubs';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { PrintPdfDialogComponent } from '../../shared/dialogs/print-pdf-dialog/print-pdf-dialog.component';

describe('ShowEcmrPageComponent', () => {
  let component: ViewerPageComponent;
  let fixture: ComponentFixture<ViewerPageComponent>;
  let loader: HarnessLoader;
  let overlayContainer: OverlayContainer;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: { get: () => 'value' } } },
        },
        { provide: EcmrService, useValue: ecmrServiceStub(mockEcmr(59)) },
        { provide: ShareEcmrService, useValue: shareEcmrServiceStub() },
      ],
      declarations: [ViewerPageComponent, PrintPdfDialogComponent],
      imports: [
        NoopAnimationsModule,
        RouterTestingModule.withRoutes([
          { path: 'ecmr-overview', component: OverviewPageComponent },
          { path: 'edit-ecmr', component: EditorPageComponent },
          { path: 'share', component: SharePageComponent },
        ]),
        MatToolbarModule,
        MatButtonModule,
        MatDialogModule,
      ],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create here', () => {
    expect(component).toBeTruthy();
  });

  describe('To PDF Button', () => {
    beforeEach(() => {
      fixture = TestBed.createComponent(ViewerPageComponent);
      component = fixture.componentInstance;

      fixture.detectChanges();
      loader = TestbedHarnessEnvironment.documentRootLoader(fixture);
      overlayContainer = TestBed.inject(OverlayContainer);
    });

    afterEach(async () => {
      // Make sure, that all dialogs are closed before continuing
      const dialogs = await loader.getAllHarnesses(MatDialogHarness);
      await Promise.all(
        dialogs.map(async (d: MatDialogHarness) => await d.close())
      );
      overlayContainer.ngOnDestroy();
      TestBed.resetTestingModule();
    });

    it('should open a dialog on pdf button press', async () => {
      let dialogs = await loader.getAllHarnesses(MatDialogHarness);

      const pdfButtonHarness = await loader.getHarness(
        MatButtonHarness.with({ text: /pdf/i })
      );
      expect(dialogs.length).toEqual(0);

      await pdfButtonHarness.click();
      dialogs = await loader.getAllHarnesses(MatDialogHarness);
      expect(dialogs.length).toEqual(1);
    });
  });
});
