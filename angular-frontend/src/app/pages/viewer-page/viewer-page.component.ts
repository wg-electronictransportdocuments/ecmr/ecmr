/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EcmrService } from '../../shared/services/http/ecmr.service';
import { Ecmr } from '../../shared/models/entities/ecmr';
import { ShareEcmrService } from 'src/app/shared/services/state/share-ecmr.service';
import { endpoints } from '../../shared/services/http/endpoints';
import { MatDialog } from '@angular/material/dialog';
import {
  PrintPdfDialogComponent,
} from 'src/app/shared/dialogs/print-pdf-dialog/print-pdf-dialog.component';

@Component({
  selector: 'app-viewer-page',
  templateUrl: './viewer-page.component.html',
  styleUrls: ['./viewer-page.component.scss'],
})
export class ViewerPageComponent implements OnDestroy {
  ecmrOverviewUrlFragment = '/' + endpoints.frontend.ecmrOverview;
  editEcmrUrlFragment = '/' + endpoints.frontend.editEcmr;
  shareUrlFragment = '/' + endpoints.frontend.shareEcmr;
  ecmrId = '';
  ecmr: Ecmr = {
    ecmrId: '0',
    fields: {},
    lastUpdated: '',
    created: '',
    lastUserId: '',
    lastUsername: '',
    status: '',
    version: '',
    host: '',
  };
  constructor(
    public matDialog: MatDialog,
    public activatedRoute: ActivatedRoute,
    public ecmrService: EcmrService,
    public shareEcmrService: ShareEcmrService
  ) {
    this.ecmrId = this.activatedRoute.snapshot.paramMap.get('id');
    this.ecmrService.updateEcmr$(this.ecmrId);
    this.ecmrService.ecmr$.subscribe((r) => {
      this.ecmr = r;
    });
  }

  ngOnDestroy(): void {
    this.ecmrService.unsetEcmr();
  }

  onToPDF(): void {
    this.matDialog.open(PrintPdfDialogComponent, {
      data: {ecmrId: this.ecmrId},
    });
  }
}
