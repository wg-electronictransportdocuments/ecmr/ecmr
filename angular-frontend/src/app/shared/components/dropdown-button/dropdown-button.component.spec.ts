/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DropdownButtonComponent } from './dropdown-button.component';
import { MatMenuModule } from '@angular/material/menu';
import { SignatureChoiceService } from '../../services/state/signature-choice.service';
import { signatureChoiceServiceStub } from '../../mockups/service-stubs';
import { MatIconModule } from '@angular/material/icon';

describe('DropdownButtonComponent', () => {
  let component: DropdownButtonComponent;
  let fixture: ComponentFixture<DropdownButtonComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        {
          provide: SignatureChoiceService,
          useValue: signatureChoiceServiceStub(),
        },
      ],
      imports: [MatMenuModule, MatIconModule],
      declarations: [DropdownButtonComponent],
    }).compileComponents();

    fixture = TestBed.createComponent(DropdownButtonComponent);
    component = fixture.componentInstance;
    component.choices = ['A', 'B'];
    component.clickable = true;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
