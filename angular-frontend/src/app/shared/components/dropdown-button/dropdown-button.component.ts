/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component, EventEmitter, Input, Output } from '@angular/core';
import { SignatureChoiceService } from '../../services/state/signature-choice.service';

@Component({
  selector: 'app-dropdown-button',
  templateUrl: './dropdown-button.component.html',
  styleUrls: ['./dropdown-button.component.scss'],
})
export class DropdownButtonComponent {
  @Input() choices: string[];
  @Input() clickable: boolean;
  @Output() functionIndex: EventEmitter<number> = new EventEmitter();
  selected = 0;

  constructor(public signatureChoiceService: SignatureChoiceService) {
    this.selected = this.signatureChoiceService.getSignatureChoice();
  }

  setSelected(x): void {
    this.signatureChoiceService.setSignatureChoice(x);
    this.selected = x;
  }
}
