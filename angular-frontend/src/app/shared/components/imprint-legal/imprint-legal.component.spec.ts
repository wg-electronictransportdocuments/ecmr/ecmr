/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ImprintLegalComponent } from './imprint-legal.component';

describe('ImprintLegalPageComponents', () => {
  let component: ImprintLegalComponent;
  let fixture: ComponentFixture<ImprintLegalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ImprintLegalComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImprintLegalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
