/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';

@Component({
  selector: 'app-imprint-legal',
  templateUrl: './imprint-legal.component.html',
  styleUrls: ['./imprint-legal.component.scss'],
})
export class ImprintLegalComponent {}
