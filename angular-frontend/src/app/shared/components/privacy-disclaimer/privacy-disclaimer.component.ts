/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy-disclaimer.component.html',
  styleUrls: ['./privacy-disclaimer.component.scss'],
})
export class PrivacyDisclaimerComponent {}
