/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { ViewerComponent } from './viewer.component';
import {
  FormsModule,
  ReactiveFormsModule,
  UntypedFormBuilder,
} from '@angular/forms';
import { Ecmr } from '../../models/entities/ecmr';
import { mockEcmr } from '../../mockups/mock-objects';
import { MatCardModule } from '@angular/material/card';
import { Position, positionFieldIds } from '../../models/entities/position';
import { placeholderEcmrFields } from '../../models/entities/fields';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';

describe('EcmrTemplateComponent', () => {
  let component: ViewerComponent;
  let fixture: ComponentFixture<ViewerComponent>;
  const dummyFields = JSON.parse(JSON.stringify(placeholderEcmrFields));
  const mockDoc: Ecmr = mockEcmr(5);

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ViewerComponent],
      imports: [MatCardModule, MatDialogModule, FormsModule, ReactiveFormsModule],

      providers: [
        MatDialog,
        { provide: UntypedFormBuilder },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewerComponent);
    component = fixture.componentInstance;
    component.ecmr = mockDoc;
    fixture.detectChanges();
  });

  it('should have working methods', () => {
    component.parseDate('something');

    const emptyPosition: Position = {};
    positionFieldIds.forEach(id => {
      emptyPosition[id] = '';
    });
    const unspecifiedPosition: Position = dummyFields.positions[0];
    unspecifiedPosition['41'] = '';
    unspecifiedPosition['42'] = '';
    unspecifiedPosition['43'] = '1';

    const barcodeQualifiedPosition: Position = dummyFields.positions[0];
    barcodeQualifiedPosition['41'] = '';
    barcodeQualifiedPosition['42'] = '987654321';
    barcodeQualifiedPosition['43'] = '';

    const textQualifiedPosition: Position = dummyFields.positions[0];
    textQualifiedPosition['41'] = 'marking text';
    textQualifiedPosition['42'] = '';
    textQualifiedPosition['43'] = '';

    const barcodeQuantifiedPosition: Position = dummyFields.positions[0];
    barcodeQuantifiedPosition['41'] = '';
    barcodeQuantifiedPosition['42'] = '';
    barcodeQuantifiedPosition['43'] = '';

    const textQuantifiedPosition: Position = dummyFields.positions[0];
    textQuantifiedPosition['41'] = '';
    textQuantifiedPosition['42'] = '';
    textQuantifiedPosition['43'] = '';

    // eCMR with deprecated Signature format:
    component.ecmr = {
      host: 'localhost:8080',
      fields: { 69: 'Signature' },
      lastUserId: '',
      status: '',
      ecmrId: '1',
      lastUpdated: '0',
      created: '0',
      lastUsername: 'mockLastUserName',
      version: '',
    };
    component.ngOnChanges();
    component.ngAfterContentChecked();
    // Standard placeholder eCMR
    component.ecmr = mockEcmr(123);
    component.ngOnChanges();
    component.ngAfterContentChecked();
    // eCMR with new Signature Format
    component.ecmr.fields['69'] = '{"timestamp": "123", "name": "someone"}';
    component.ngOnChanges();
    component.ngAfterContentChecked();
    // eCMR with empty Positions Array
    component.ecmr.fields.positions = [
      emptyPosition,
      unspecifiedPosition,
      barcodeQualifiedPosition,
      textQualifiedPosition,
      barcodeQuantifiedPosition,
    ];
    component.ngOnChanges();
    component.ngAfterContentChecked();

    // eCMR with empty Positions Array
    fixture.detectChanges();
    component.onMagnifySOG('72');

    component.ecmr.fields['72'] = '{"type": "signOnGlass", "data": "asdhkj", "userCompany": "compAny", "name": "someone"}';
    component.getSignatureTemplate('72');

    component.ecmr.fields['72'] = '{"type": "signOnGlass", "data": "asdhkj", "userCompany": "compAny", "name": "someone"}';
    component.onMagnifySOG('72');

    expect(component).toBeTruthy();

  });
});
