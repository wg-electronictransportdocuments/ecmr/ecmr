/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {
  AfterContentChecked,
  Component,
  Input,
  OnChanges,
  ViewChild,
} from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { MatAccordion } from '@angular/material/expansion';
import { en as EN } from '../../../../assets/i18n/en';
import { SignOnGlassDialogComponent, SignOnGlassDialogModel } from '../../dialogs/sign-on-glass-dialog/sign-on-glass-dialog.component';
import { Ecmr } from '../../models/entities/ecmr';
import { Position, positionFieldIds } from '../../models/entities/position';
import { AccordionState } from '../../models/types/accordion-state';
import { ThemeService } from '../../services/state/theme.service';

@Component({
  selector: 'app-show-ecmr',
  templateUrl: './viewer.component.html',
  styleUrls: ['./viewer.component.scss'],
})
export class ViewerComponent implements OnChanges, AfterContentChecked {
  @Input() ecmr: Ecmr;
  @Input() updatedFieldKeys: string[];
  @Input() isHistoricalVersion: boolean;

  @ViewChild(MatAccordion) accordion: MatAccordion;

  expansionPanelState: boolean[] = [];
  accordionState: AccordionState;

  dialogSignOnGlassRef: MatDialogRef<any>;

  /* Object that holds field names, descriptions and validation requirements*/
  en = EN;

  constructor(public matSignOnGlassDialog: MatDialog,
              public themeService: ThemeService) {}

  ngOnChanges(): void {
    while (
        this.expansionPanelState.length < this.ecmr?.fields?.positions?.length
        ) {
      this.expansionPanelState.push(false);
      if (this.ecmr.fields.positions?.length > 0) {
        this.expansionPanelState[0] = true;
      }
    }
  }

  ngAfterContentChecked(): void {
    this.accordionState = this.getAccordionExpansionState(
        this.expansionPanelState
    );
  }

  getAccordionExpansionState(expansionStates: boolean[]): AccordionState {
    if (expansionStates.length !== 0) {
      const expItemNumber = expansionStates.filter(r => r).length;
      if (expItemNumber === expansionStates.length) {
        return 'Expanded';
      } else if (expItemNumber === 0) {
        return 'Collapsed';
      } else {
        return 'Mixed';
      }
    } else {
      return undefined;
    }
  }

  parseDate(dateString: string): Date {
    return new Date(dateString);
  }

  getSignatureTemplate(id: string): string {
    const idString = this.ecmr.fields[id];
    if (idString) {
      try {
        const identity = JSON.parse(idString);
        const timestamp = new Date(identity.timestamp).toISOString();
        if (identity.type === 'signOnGlass') {
          return `
              <p class="header-line">Digitally signed by</p>
              <div class="sign-on-glass-container">
              <center>
              <img class="sign-on-glass-image" src='${
              'data:image/png;base64,' + identity.data
          }'/>
              <div class="timestamp-line">${identity.name}, ${
              identity.userCompany
          }, ${timestamp}</div>
              </center>
              </div>`;
        } else {
          return `
              <p class="header-line">Digitally signed by</p>
              <p class="name-line">${this.toUppercaseName(identity.name)}</p>
              <p class="company-line">${identity.userCompany}</p>
              <p class="timestamp-line">${timestamp}</p>`;
        }
      } catch (e) {
        return idString + ' [deprecated format]';
      }
    }
    return '';
  }

  // Magnify sign on glass signature
  public onMagnifySOG(id: string) {
    const idString = this.ecmr.fields[id];
    if (idString) {
      try {
        const identity = JSON.parse(idString);
        if (identity.type === 'signOnGlass') {
          this.dialogSignOnGlassRef = this.matSignOnGlassDialog.open(
              SignOnGlassDialogComponent,
              {
                minWidth: '100%',
                height: '100%',
                width: '100%',
                panelClass: 'sign-on-glass-dialog',
                data: new SignOnGlassDialogModel(
                    identity.data,
                    identity.name,
                    identity.userCompany,
                    true
                ),
              }
          );

          this.dialogSignOnGlassRef.backdropClick().subscribe(() => {
            this.dialogSignOnGlassRef.close();
          });
        }
      } catch (e) {
      }
    }
  }

  toUppercaseName(name: string): string {
    const parts = name.split(' ');
    let newName = '';
    for (const part of parts) {
      newName = newName.concat(
          ' ' + part.charAt(0).toUpperCase() + part.slice(1)
      );
    }
    return newName;
  }

  // Turns FormGroup Values to shorthand
  getPositionShorthand(pos: Position): string {
    // Cases:
    //  - Empty
    //  - Specified without item count (Marking Text or Barcode but no Number)
    //  - Specified with item count (Marking Text or Barcode and a Number)
    //  - Unspecified (Neither Marking Text nor Barcode)
    let isEmpty = true;
    positionFieldIds.forEach(fieldId => {
      if (pos[fieldId] !== '') {
        isEmpty = false;
      }
    });
    if (isEmpty) {
      return 'Empty';
    } else if (!pos['41'] && !pos['42']) {
      return 'Unspecified';
    } else if (pos['43']) {
      // return specified ('count' times either marking text (preferred) or barcode)
      return pos['43'] + ' × ' + (pos['41'] ? pos['41'] : pos['42']);
    } else {
      // return qualified = specified without item count (either marking text (preferred) or barcode)
      return pos['41'] ? pos['41'] : pos['42'];
    }
  }

  openDocumentation() {
    window.open(
        'https://git.openlogisticsfoundation.org/wg-electronictransportdocuments/ecmr/ecmr/-/tree/main/documentation/app',
        '_blank'
    );
  }

  checkFieldHistoricVersion(fieldKey: string): string {
    let highlightColor: string;
    this.themeService.getModeName$().subscribe(value => {
      (value.toString() == 'dark-theme') ? highlightColor = '#62a46f' : highlightColor = '#D8E5E3';
    });
    return this.isHistoricalVersion && this.updatedFieldKeys.includes(fieldKey) ? highlightColor : 'inherit';

  }
}
