/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import {
  ConfirmationDialogComponent,
  ConfirmDialogModel,
} from './confirmation-dialog.component';

describe('ConfirmationDialogComponent', () => {
  let component: ConfirmationDialogComponent;
  let fixture: ComponentFixture<ConfirmationDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ConfirmationDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            open: () => {},
            close: () => {},
          },
        },
        { provide: MAT_DIALOG_DATA, useValue: [] },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConfirmationDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('dialog should be closed after onConfirm', () => {
    const spy = spyOn(component.matDialogRef, 'close').and.callThrough();
    component.onConfirm();
    expect(spy).toHaveBeenCalled();
  });

  it('dialog should be closed after onDismissed', () => {
    const spy = spyOn(component.matDialogRef, 'close').and.callThrough();
    component.onDismiss();
    expect(spy).toHaveBeenCalled();
  });

  it('should create Model', () => {
    const dialogData = new ConfirmDialogModel('title', 'message');
    expect(dialogData).toBeTruthy();
    expect(dialogData.title).toEqual('title');
    expect(dialogData.message).toEqual('message');
  });
});
