/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ImportDialogComponent} from './import-dialog.component';
import {EcmrExportImportService} from '../../services/http/ecmr-export-import.service';
import {ecmrExportImportServiceStub, snackbarServiceStub,} from '../../mockups/service-stubs';
import {CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {SnackbarService} from '../../services/state/snackbar.service';
import {MatDialogModule, MatDialogRef} from '@angular/material/dialog';
import {ZXingScannerModule} from '@zxing/ngx-scanner';
import {MatIconModule} from '@angular/material/icon';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';

describe('ImportDialogComponent', () => {
  let component: ImportDialogComponent;
  let fixture: ComponentFixture<ImportDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
        ZXingScannerModule,
        MatIconModule,
        BrowserAnimationsModule,
        MatFormFieldModule,
        MatInputModule,
      ],
      declarations: [ImportDialogComponent],
      providers: [
        {
          provide: EcmrExportImportService,
          useValue: ecmrExportImportServiceStub(),
        },
        {provide: SnackbarService, useValue: snackbarServiceStub()},
        {
          provide: MatDialogRef,
          useValue: {
            open: () => {
            },
            close: () => {
            },
          },
        },
        {
          provide: Clipboard,
          useValue: {
            copy: () => {
            },
          },
        },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should work onCamera-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onCamera();
    expect(component).toBeTruthy();
  });

  it('should work onSubmit-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onSubmit();
    expect(component).toBeTruthy();
  });

  it('should work onScan-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onScan('');
    expect(component).toBeTruthy();
  });

  it('should work onPaste-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onPaste();
    expect(component).toBeTruthy();
  });
});
