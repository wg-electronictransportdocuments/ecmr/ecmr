/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { SnackbarService } from '../../services/state/snackbar.service';
import { BarcodeFormat } from '@zxing/library';

import {Qrcode} from '../../models/qrcode';

@Component({
  selector: 'app-import-dialog',
  templateUrl: './import-dialog.component.html',
  styleUrls: ['./import-dialog.component.scss'],
})
export class ImportDialogComponent {
  allowedFormats = [BarcodeFormat.QR_CODE];
  showScanner = false;
  token ='';

   data = new Qrcode('','');

  constructor(
    public matDialogRef: MatDialogRef<ImportDialogComponent>,
    public snackbarService: SnackbarService
  ) { }

  onScan(event: string) {
    try {
      this.data = JSON.parse(atob(event));

      console.log('Valid QR Code: '+ JSON.stringify(this.data));

      this.token = event;
      this.showScanner = false;

    } catch (e) {
      console.error('During QR Code scanning an error occurs', e);
      this.snackbarService.pushNewStatus(
        'QR Code is not a valid eCMR code.');
    }
  }

  onSubmit() {
    this.matDialogRef.close(this.data);
  }

  onCamera() {
    this.showScanner = !this.showScanner;
  }

  onChange($event: any) {

    try {

      this.token = $event.target.value;
      this.data = JSON.parse(atob($event.target.value));

      const json = this.data;

      // Verify, if the qrcode contains valid ecmr information (ecmrId && host)
      if(json.ecmrId == null || json.host == null) {
        console.error('Invalid QR Code: '+ json);
      }

    } catch (e) {
      console.error('An error occurs during QR Code content parsing', e);

      this.snackbarService.pushNewStatus(
        'QR Code is not a valid eCMR code.');
    }
  }

  onPaste() {
    navigator.clipboard.readText().then(clipText => {
      try {
        this.token = clipText;
        this.data =  JSON.parse(atob(clipText));
      } catch (e) {
        this.snackbarService.pushNewStatus(
          'Contents from clipboard do not match import token structure.'
        );
      }
    });
  }
}
