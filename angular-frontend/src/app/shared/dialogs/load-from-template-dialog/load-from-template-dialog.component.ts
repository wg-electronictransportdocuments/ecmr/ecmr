/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';
import { EcmrTemplateListEntry, ecmrTemplateListHeaderObjects } from '../../models/entities/ecmr-template-list-entry';
import { EcmrTemplateListService } from '../../services/http/ecmr-template-list.service';
import { MatSort, Sort } from '@angular/material/sort';
import { TableStateService } from '../../services/state/table-state.service';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { take } from 'rxjs/operators';
import { UserProfileService } from '../../../modules/user-management/services/user-profile.service';

@Component({
    selector: 'app-load-from-template-dialog',
    templateUrl: './load-from-template-dialog.component.html',
    styleUrls: ['./load-from-template-dialog.component.scss']
})
export class LoadFromTemplateDialogComponent implements OnInit, AfterViewInit{
    @ViewChild(MatSort, { static: false }) sort: MatSort;

    columnsToDisplay = [];
    keysToDisplay = [];
    columnsToDisplayMobile = [
        ecmrTemplateListHeaderObjects.templateId,
        ecmrTemplateListHeaderObjects.name
    ];
    hiddenColumnsMobile = [];
    columnsToDisplayDesktop = [
        ecmrTemplateListHeaderObjects.templateId,
        ecmrTemplateListHeaderObjects.name,
        ecmrTemplateListHeaderObjects.senderName,
        ecmrTemplateListHeaderObjects.consigneeName
    ];
    dataSource = new MatTableDataSource<EcmrTemplateListEntry>();
    selectedTemplate: EcmrTemplateListEntry;

    bpStateGtSm;

    userIsDisponent = false;
    userIsSuperuser = false;


    constructor(public dialogRef: MatDialogRef<LoadFromTemplateDialogComponent>,
                public ecmrTemplateListService: EcmrTemplateListService,
                public tableStateService: TableStateService,
                public breakpointObserver: BreakpointObserver,
                public userProfileService: UserProfileService) {
        this.hiddenColumnsMobile = this.columnsToDisplayDesktop.filter(
            item => this.columnsToDisplayMobile.indexOf(item) < 0
        );
    }

    ngOnInit() {
        this.ecmrTemplateListService.getEcmrTemplateList$().subscribe(data => {
            this.dataSource.data = data;
        });
        this.ecmrTemplateListService.updateEcmrTemplateList$();

        this.userProfileService.isDisponent$.pipe(take(1)).subscribe(r => {
            this.userIsDisponent = r;
        });
        this.userProfileService.isSuperuser$.pipe(take(1)).subscribe(r => {
            this.userIsSuperuser = r;
        });

        this.userProfileService.updateUserRoles$();

        this.breakpointObserver
            .observe([Breakpoints.Small, Breakpoints.XSmall])
            .subscribe(result => {
                this.bpStateGtSm = !result.matches;
                this.columnsToDisplay = result.matches
                    ? this.columnsToDisplayMobile
                    : this.columnsToDisplayDesktop;
                this.keysToDisplay = this.columnsToDisplay.map(r => r.id);
            });
    }

    ngAfterViewInit(): void {
        // Sorting
        this.dataSource.sort = this.sort;
    }

    onSortData(sortEvent: Sort): void {
        this.tableStateService.setSortState({
            active: sortEvent.active,
            direction: sortEvent.direction,
            sortName: 'ecmrLoadFromTemplateDialog',
        });
    }

    closeDialog() {
        this.dialogRef.close();
    }

    onRowClick(event: any, element: EcmrTemplateListEntry): void {
        const previousSelectedRow = document.querySelector('.highlight');
        if (previousSelectedRow) {
            previousSelectedRow.classList.remove('highlight');
        }

        const clickedRow = event.target.closest('tr');
        clickedRow.classList.add('highlight');

        this.selectedTemplate = element;
    }

    loadSelectedTemplate() {
        this.dialogRef.close(this.selectedTemplate.templateId);
    }
}