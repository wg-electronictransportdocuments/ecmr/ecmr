/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { en as EN } from '../../../../assets/i18n/en';
import { LoadingStateService } from '../../services/state/loading-state.service';
import { EcmrService } from '../../services/http/ecmr.service';

@Component({
  selector: 'app-print-pdf-dialog',
  templateUrl: './print-pdf-dialog.component.html',
  styleUrls: ['./print-pdf-dialog.component.scss'],
})
export class PrintPdfDialogComponent {
  ecmrId: string;
  en = EN;


  constructor(
      public loadingStateService: LoadingStateService,
      public ecmrService: EcmrService,
      public matDialogRef: MatDialogRef<PrintPdfDialogComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any) {
    this.ecmrId = data.ecmrId;
  }

  onDownloadClick() {
     this.ecmrService.exportAsPDF(this.ecmrId).subscribe(
        blob => {
          this.downLoadFile(blob,'application/pdf',this.ecmrId + '.pdf');
        }
    );
  }

  private downLoadFile(data: Blob, type: string, fileName: string) {
      let blob = new Blob([data], {type: type});
      let url = window.URL.createObjectURL(blob);
      let anchor = document.createElement('a');
      anchor.download = fileName;
      anchor.href = url;
      anchor.click();
  }
}
