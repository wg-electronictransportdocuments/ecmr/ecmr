/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {ComponentFixture, TestBed} from '@angular/core/testing';
import {ShareQrDialogComponent} from './share-qr-dialog.component';
import {RouterTestingModule} from '@angular/router/testing';
import {OverviewPageComponent} from '../../../pages/overview-page/overview-page.component';
import {MatCardModule} from '@angular/material/card';
import {ShareEcmrService} from '../../services/state/share-ecmr.service';
import {Router} from '@angular/router';
import {SnackbarService} from '../../services/state/snackbar.service';
import {Clipboard} from '@angular/cdk/clipboard';
import {shareEcmrServiceStub} from '../../mockups/service-stubs';
import {MatDialogModule} from "@angular/material/dialog";

describe('InviteQrPageComponent', () => {
  let component: ShareQrDialogComponent;
  let fixture: ComponentFixture<ShareQrDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: 'ecmr-overview', component: OverviewPageComponent},
        ]),
        MatCardModule,
        MatDialogModule,
      ],
      declarations: [ShareQrDialogComponent],
      providers: [
        {
          provide: ShareEcmrService,
          useValue: shareEcmrServiceStub(),
        },
        {
          provide: Router,
          useValue: {
            navigate: () => {
            },
          },
        },
        {
          provide: SnackbarService,
          useValue: {
            pushNewStatus: () => {
            },
          },
        },
        {
          provide: Clipboard,
          useValue: {
            copy: () => {
            },
          },
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShareQrDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should create with ecmrId of undefined', () => {
    component.exportObject = undefined;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });

  it('should work copyToClipboard-Function', () => {
    fixture = TestBed.createComponent(ShareQrDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.copyToClipboard();
    expect(component).toBeTruthy();
  });

  it('should work onClickGoToOverview-Function', () => {
    fixture = TestBed.createComponent(ShareQrDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onClickGoToOverview();
    expect(component).toBeTruthy();
  });
});
