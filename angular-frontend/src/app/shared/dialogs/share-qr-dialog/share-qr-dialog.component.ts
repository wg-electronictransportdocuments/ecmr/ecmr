/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {AfterViewInit, Component} from '@angular/core';
import { ShareEcmrService } from '../../services/state/share-ecmr.service';
import { environment as env } from '../../../../environments/environment';
import { Router } from '@angular/router';
import { SnackbarService } from '../../services/state/snackbar.service';
import { Clipboard } from '@angular/cdk/clipboard';
import { endpoints } from '../../services/http/endpoints';
import { ExportObject } from '../../models/entities/export-object';

@Component({
  selector: 'app-share-qr-dialog',
  templateUrl: './share-qr-dialog.component.html',
  styleUrls: ['./share-qr-dialog.component.scss'],
})
export class ShareQrDialogComponent{
  ecmrOverviewUrlFragment = '/' + endpoints.frontend.ecmrOverview;
  showEcmrUrl: string;
  qrCodeString: string;
  exportObject: ExportObject;

  constructor(
    public shareEcmrService: ShareEcmrService,
    public router: Router,
    public snackbarService: SnackbarService,
    public clipboard: Clipboard
  ) {
    this.exportObject = shareEcmrService.getExportObjectToShare();
    if (!this.exportObject) {
      router.navigate([this.ecmrOverviewUrlFragment]);
      snackbarService.pushNewStatus(
        'Your request to access the share-page was invalid. The eCMR was already shared or' +
          ' there was no eCMR selected in the first place. Heading back to overview.',
        10000
      );
    } else {
      this.showEcmrUrl =
        env.frontendUrl +
        endpoints.frontend.showEcmr +
        this.exportObject.ecmrId;
      this.qrCodeString = btoa(JSON.stringify(this.exportObject));
    }
  }

  onClickGoToOverview() {
    this.router.navigate([this.ecmrOverviewUrlFragment]);
    this.shareEcmrService.unsetExportObjectToShare();
  }

  copyToClipboard(): void {
    this.clipboard.copy(this.qrCodeString);
    this.snackbarService.pushNewStatus(
      'Link to claim eCMR has been copied to clipboard!',
      6000
    );
  }
}
