/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SignOnGlassDialogComponent, SignOnGlassDialogModel } from './sign-on-glass-dialog.component';
import {
  MAT_DIALOG_DATA,
  MatDialogModule,
  MatDialogRef,
} from '@angular/material/dialog';
import {
  UntypedFormBuilder,
} from '@angular/forms';
import { of } from 'rxjs';
import { SignaturePadModule } from 'angular-signature-pad-v2';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

describe('SignOnGlassDialogComponent', () => {
  let component: SignOnGlassDialogComponent;
  let fixture: ComponentFixture<SignOnGlassDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MatDialogModule, SignaturePadModule ],
      declarations: [SignOnGlassDialogComponent],
      providers: [
        {
          provide: MatDialogRef,
          useValue: {
            open: () => {},
            close: () => {},
            beforeClosed: () => of(true),
          },
        },
        { provide: MAT_DIALOG_DATA, useValue: [] },
        { provide: UntypedFormBuilder },
      ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    }).compileComponents();

    fixture = TestBed.createComponent(SignOnGlassDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should work onDismiss-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onDismiss();
    expect(component).toBeTruthy();
  });

  it('should work onConfirm-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onConfirm();
    expect(component).toBeTruthy();
  });

  it('should work onClear-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.onClear();
    expect(component).toBeTruthy();
  });

  it('should work resetForm-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    component.resetForm();
    expect(component).toBeTruthy();
  });

  it('should work getPat-Function', () => {
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component.getPat('n')).toEqual('^[1-9]\\d*(\\.\\d+)?$');
    expect(component.getPat('an')).toEqual('^.+');
    expect(component.getPat('any')).toEqual('');
    expect(component).toBeTruthy();
  });

  it('should create Model', () => {
    const dialogData = new SignOnGlassDialogModel('asfdnlasgf', 'firstname lastname', 'company', true);
    expect(dialogData).toBeTruthy();
    expect(dialogData.data).toEqual('asfdnlasgf');
    expect(dialogData.name).toEqual('firstname lastname');
    expect(dialogData.userCompany).toEqual('company');
    expect(dialogData.viewOnly).toEqual(true);
  });

  it('should mock window resize', () => {
    spyOnProperty(window, 'innerWidth').and.returnValue(560);
    window.dispatchEvent(new Event('resize'));
  });
});
