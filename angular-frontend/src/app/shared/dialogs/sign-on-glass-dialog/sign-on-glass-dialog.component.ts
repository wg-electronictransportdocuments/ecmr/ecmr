/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Component, ElementRef, HostListener, Inject, OnInit, ViewChild,} from '@angular/core';
import {AbstractControl, UntypedFormBuilder, UntypedFormControl, UntypedFormGroup, Validators,} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

import {en as EN} from 'src/assets/i18n/en';
import {SignaturePad} from "angular-signature-pad-v2";

@Component({
  selector: 'app-sign-on-glass-dialog',
  templateUrl: './sign-on-glass-dialog.component.html',
  styleUrls: ['./sign-on-glass-dialog.component.scss'],
})
export class SignOnGlassDialogComponent implements OnInit {

  @ViewChild(SignaturePad) signaturePad: SignaturePad;
  @ViewChild('signature') signatureDiv: ElementRef;

  title = 'no title';

  signatureData: any = null;

  name = '';
  company = '';
  staticFormGroup: UntypedFormGroup;
  en = EN;
  viewOnly = false;

  isSigned: boolean;

  options = {
    backgroundColor: '#FFFFFF',
    canvasWidth: 800,
    canvasHeight: 800,
    offsetWidth: 800,
    css: {
      'border-radius': '16px',
    },
  };

  constructor(
    public formBuilder: UntypedFormBuilder,
    public matDialogRef: MatDialogRef<SignOnGlassDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SignOnGlassDialogModel
  ) {
    this.isSigned = false;
    matDialogRef.beforeClosed().subscribe(() => {
      // check if the eCMR is finally signed and if the sign-on-glass dialog can therefore be closed.
      if (this.isSigned) {
        this.isSigned = false; // since the dialog will be closed, the toggle needs to be switched off again.
        matDialogRef.close(this.signatureData ?? false);
      }
    });
    this.title = data.data;
    if (data.viewOnly) {
      this.signatureData = {
        data: data.data,
        name: data.name,
        company: data.userCompany,
      };
    } else {
      this.signatureData = {
        data: data.data ?? '',
        name: data.name ?? '',
        company: data.userCompany ?? '',
      };
    }
    this.viewOnly = data.viewOnly;
    const vw = window.innerWidth - 150;
    const vh = window.innerHeight - 150;
    this.options.canvasWidth = vw > 3 * vh ? 3 * vh : vw;
    this.options.canvasHeight = vw > 3 * vh ? vh : vw / 3;
  }

  ngOnInit(): void {
    this.staticFormGroup = this.generateStaticForm();
    if (this.viewOnly) {
      this.name = this.signatureData.name;
      this.company = this.signatureData.company;
    } else {
      this.signatureData = {
        name: this.signatureData.name ?? '',
        company: this.signatureData.company ?? '',
      };
      this.staticFormGroup.get('name').setValue(this.signatureData.name);
      this.staticFormGroup.get('company').setValue(this.signatureData.company);
    }
  }


  @HostListener('window:resize', ['$event'])
  onResize(event) {
    const canvas = document.querySelector('canvas');
    const tempWidth = String(event.target.innerWidth - 150);
    const tempHeight = String(event.target.innerHeight - 150);
    const widthCanvas = tempWidth.concat('px');
    const heightCanvas = tempHeight.concat('px');
    canvas.setAttribute('width', widthCanvas);
    canvas.setAttribute('height', heightCanvas);
    canvas.style.background = 'white';

  }

  /**
   * Confirm the signature given on the sign-on-glass pad.
   */
  onConfirm(): void {
    if (!this.staticFormGroup.invalid) {
      this.signatureData = {
        data: this.signaturePad.toDataURL(),
        name: this.staticFormGroup.get('name').value,
        company: this.staticFormGroup.get('company').value,
      };
      this.isSigned = true; // since the signature is saved, the eCMR is signed.
      this.matDialogRef.close(true);
    } else {
    }
  }

  /**
   * Dismiss the signature given on the sign-on-glass pad.
   */
  onDismiss(): void {
    this.matDialogRef.close(false);
  }

  /**
   * Clear the signature given on the sign-on-glass pad to allow signing again.
   */
  onClear(): void {
    this.signaturePad.clear();
    this.resetForm();
  }

  private generateStaticForm(): UntypedFormGroup {
    return this.formBuilder.group({
      name: ['', this.getVals(2, 35, this.getPat('an'))],
      company: ['', this.getVals(2, 35, this.getPat('an'))],
    });
  }

  resetForm(): void {
    // Empty static group
    this.staticFormGroup.reset();
    this.staticFormGroup.enable();
  }

  private getVals(min: number, max: number, pat: string): Validators[] {
    return [
      Validators.minLength(min),
      Validators.maxLength(max),
      Validators.pattern(pat.toString()),
    ];
  }

  // noinspection JSMethodCanBeStatic
  getPat(identifier: 'any' | 'an' | 'n'): string {
    if (identifier === 'an') {
      return '^.+';
    } else if (identifier === 'n') {
      return '^[1-9]\\d*(\\.\\d+)?$';
    } else {
      return '';
    }
  }

  getErrorMessage(
    _obj: UntypedFormControl | AbstractControl,
    _inputType: string,
    _pattern: string
  ) {
    if (_obj.hasError('required')) {
      return this.en.error_required;
    }
    if (_obj.hasError('pattern')) {
      return this.en.error_invalid + _inputType + ': Pattern is ' + _pattern;
    }
    if (_obj.hasError('maxlength')) {
      return this.en.error_invalid + _inputType + ': too many characters';
    }
    if (_obj.hasError('minlength')) {
      return this.en.error_invalid + _inputType + ': too few characters';
    }
    return '';
  }
}

export class SignOnGlassDialogModel {
  constructor(
    public data: string,
    public name: string,
    public userCompany: string,
    public viewOnly: boolean
  ) {
  }
}
