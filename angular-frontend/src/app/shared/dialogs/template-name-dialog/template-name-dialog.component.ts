/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';

@Component({
    selector: 'app-template-name-dialog',
    templateUrl: './template-name-dialog.component.html',
    styleUrls: ['./template-name-dialog.component.scss']
})
export class TemplateNameDialogComponent {

    templateNameDialogFormGroup = new FormGroup({
        templateName: new FormControl<string>('', Validators.required)
    });



    constructor(public dialogRef: MatDialogRef<TemplateNameDialogComponent>) {
    }

    saveTemplateName() {
        if(this.templateNameDialogFormGroup.controls.templateName.value) {
            this.dialogRef.close(this.templateNameDialogFormGroup.controls.templateName.value);
        }
    }

    closeDialog() {
        this.dialogRef.close();
    }
}