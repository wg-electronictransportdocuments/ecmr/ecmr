/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { FocusBlurDirective } from './focus-blur.directive';
import { ElementRef, Injectable } from '@angular/core';
import { TestBed } from '@angular/core/testing';

@Injectable()
export class MockElementRef {
  nativeElement = {
    blur: () => {},
  };
}

describe('FocusBlurDirective', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [{ provide: ElementRef, useClass: MockElementRef }],
    }).compileComponents();
  });

  it('should create an instance', () => {
    const directive = new FocusBlurDirective(new MockElementRef());
    directive.onClick();
    expect(directive).toBeTruthy();
  });
});
