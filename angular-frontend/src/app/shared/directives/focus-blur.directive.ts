/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  // eslint-disable-next-line @angular-eslint/directive-selector
  selector: 'a, button',
})
export class FocusBlurDirective {
  constructor(private elementRef: ElementRef) {}

  @HostListener('click')
  onClick() {
    this.elementRef.nativeElement.blur();
  }
}
