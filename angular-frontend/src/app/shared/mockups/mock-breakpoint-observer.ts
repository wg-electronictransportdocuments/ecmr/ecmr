/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// Breakpoint Observer Stub
import { Breakpoints, BreakpointState } from '@angular/cdk/layout';
import { from, Observable } from 'rxjs';
import { filter, map } from 'rxjs/operators';

const matchObj = [
  { matchStr: Breakpoints.Small, result: false },
  { matchStr: Breakpoints.XSmall, result: false },
];
const fakeObserve = (s: string[]): Observable<BreakpointState> =>
  from(matchObj).pipe(
    filter(match => match.matchStr === s[0]),
    map(match => ({ matches: match.result, breakpoints: {} }))
  );
export const mockBreakpointObserver = jasmine.createSpyObj(
  'BreakpointObserver',
  ['observe']
);
mockBreakpointObserver.observe.and.callFake(fakeObserve);
export const resize = (breakpointState: string): void => {
  matchObj[0].result = breakpointState === Breakpoints.Small;
  matchObj[1].result = breakpointState === Breakpoints.XSmall;
};
