/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Ecmr } from '../models/entities/ecmr';
import {
  EcmrListEntry,
  ecmrStatuses,
} from '../models/entities/ecmr-list-entry';
import { SetEcmrDto } from '../models/dtos/requests/set-ecmr-dto';
import { Right } from '../models/types/right';
import { User } from '../models/entities/user';
import { Contact } from '../models/entities/contact';
import {
  fieldIds,
  Fields,
  placeholderEcmrFields,
} from '../models/entities/fields';
import { UserProfile } from '../models/entities/user-profile';
import { UserCompanyDto } from '../models/dtos/requests/get-user-dto';
import { ExportObject } from '../models/entities/export-object';
import { Template } from '../models/entities/template';

export const mockUser = (
  seed: number,
  noOfContacts: number,
  rights: Right[]
): User => {
  const contactList: Contact[] = [];
  for (let i = 1; i < noOfContacts; i++) {
    contactList.push(mockContact(i));
  }
  return {
    userId: seed.toString(),
    userName: 'UserName ' + seed,
    contacts: contactList,
    mandatoryFieldIds: [
      seed.toString(),
      (seed + 1).toString(),
      (seed + 2).toString(),
    ],
    rights,
  } as User;
};

export const mockUserProfile = (seed: number) =>
  ({
    id: '' + seed,
    companyId: 'CompanyId' + seed,
    createdTimestamp: 0,
    email: 'someone' + seed + 'abc.com',
    emailVerified: true,
    enabled: true,
    firstName: 'Name ' + seed,
    lastName: 'Lastname ' + seed,
    username: 'user' + seed,
  } as UserProfile);

export const mockContact = (seed: number) =>
  ({
    contactId: seed.toString(),
    contactType: 'type ' + seed,
    contactName: 'contactname ' + seed,
    contactEmail: 'contact' + seed + '@address.com',
    amountOfContacts: seed,
    lastContact: seed.toString(),
  } as Contact);

export const mockContacts = (no = 5, seed = 1) => {
  const retContacts: Contact[] = [];
  for (let i = 0; i < no; i++) {
    retContacts.push(mockContact(Math.round(Math.sqrt(seed * 12 + 9 * i))));
  }
  return retContacts;
};

export const mockUserCompanyDto = seed =>
  ({
    companyId: seed,
    companyLocation: 'location' + seed,
    companyName: 'name' + seed,
  } as UserCompanyDto);

export const mockEcmr = (seed: number, includeSignatureField = true) => {
  const fields = placeholderEcmrFields;
  fields['70'] = '';
  fields['72'] = '';
  if (!includeSignatureField) {
    fields['69'] = '';
  }
  return {
    ecmrId: 'mockId' + seed,
    status: 'mockStatus',
    lastUserId: seed.toString(),
    lastUpdated: '2021-08-30T08:18:22.200+00:00',
    created: '2021-08-30T08:18:22.200+00:00',
    lastUsername: 'mockLastUserName',
    version: 'mockVersion',
    host: 'localhost',
    fields,
  } as Ecmr;
};

export const mockTemplate = (ecmr: Ecmr) => {
    return {
        ecmr: ecmr,
        templateId: '1',
        name: 'TemplateName'
    } as Template;
};

// TODO: Randomize drawn fields for tests
export const mockFieldsObject = (no: number) => {
  const fields: Fields = {};
  for (const val of fieldIds(false, false)) {
    const index = fieldIds(false, false).indexOf(val);
    if (index === no) {
      break;
    }
    fields[val] = 'data';
  }
  return fields;
};

export const mockFieldArray = (no: number) => {
  const fieldArray: string[] = [];
  for (const field of fieldIds(false, false)) {
    fieldArray.push(field);
    if (fieldArray.length === no) {
      break;
    }
  }
  return fieldArray;
};

export const mockEcmrListEntry = (seed: number, excludeFaultyStatus = true) =>
  ({
    consigneeName: 'Consigneename ' + seed,
    created: '2021-08-30T08:18:22.200+00:00',
    deletable: true,
    ecmrId: 'ID' + seed,
    editable: true,
    host: 'localhost:8080',
    lastUpdated: '2021-08-30T08:18:22.200+00:00',
    lastUsername: 'last user name ' + seed,
    senderName: 'Sendername ' + seed,
    shareable: true,
    status: randomStatus(excludeFaultyStatus),
    version: seed,
  } as EcmrListEntry);

export const mockEcmrList = (
  noOfEntries: number,
  excludeFaultyStatus = true
) => {
  const list: EcmrListEntry[] = [];
  for (let i = 0; i < noOfEntries; i++) {
    list.push(mockEcmrListEntry(i, excludeFaultyStatus));
  }
  return list;
};

export const mockSetEcmrDto = (seed: number) =>
  ({
    ecmrId: seed.toString(),
    fields: mockFieldsObject(seed),
  } as SetEcmrDto);

const randomStatus = (excludeFaultyStatus = true) =>
  excludeFaultyStatus
    ? ecmrStatuses[Math.floor(Math.random() * ecmrStatuses.length)]
    : ['faulty status', ...ecmrStatuses][
        Math.floor(Math.random() * (ecmrStatuses.length + 1))
      ];

// TODO:
export const mockExportToken = '';

export const mockExportObject = (seed: string) => {
  const mockObj: ExportObject = {
    host: `localhost:${seed}`,
    ecmrId: `ID${seed}`,
  };
  return mockObj;
};
