/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { BehaviorSubject, of, Subject } from 'rxjs';
import { ThemeState } from '../models/states/theme-state';
import {
  mockContact,
  mockContacts,
  mockEcmr,
  mockEcmrList,
  mockExportObject,
  mockExportToken,
  mockFieldArray, mockTemplate,
  mockUserCompanyDto,
  mockUserProfile,
} from './mock-objects';
import { UserProfile } from '../models/entities/user-profile';

// Stubs:
export const authServiceStub = (b = true) => ({
  authStatus: new Boolean(() => b),
  logout: () => {},
});

export const cookieServiceStub = (b = true) => ({
  getCookieConsent: () => of(b),
  setCookieConsent: () => ({}),
});

export const ecmrDeleteServiceStub = (
  responseSubject: Subject<void> = new Subject<void>()
) => ({
  deleteEcmr: () => responseSubject.asObservable(),
});

export const ecmrServiceStub = (
  ecmr = mockEcmr(151),
  historicalEcmr = (ecmr = mockEcmr(151)),
  nextHistoricalEcmr = (ecmr = mockEcmr(151)),
) => ({
  ecmr$: of(ecmr),
  historicalEcmr$: of(historicalEcmr),
  updateEcmr$: () => {},
  unsetEcmr: () => {},
  updateHistoricalEcmr$: () => of(historicalEcmr),
  sendEcmrToServer: () => {},
  updateNextHistoricalEcmr$: () => of(nextHistoricalEcmr)
});

export const ecmrServiceStubWithoutEcmr = (
    ecmr = undefined,
    historicalEcmr = (ecmr = undefined)
) => ({
  ecmr$: of(ecmr),
  historicalEcmr$: of(historicalEcmr),
  updateEcmr$: () => {},
  unsetEcmr: () => {},
  updateHistoricalEcmr$: () => of(historicalEcmr),
  sendEcmrToServer: () => {},
});

export const ecmrTemplateServiceStub = (template = mockTemplate(mockEcmr(152))) => ({
  template$: of(template),
  updateComplete$: () => {},
  updateTemplate$: () => {},
  deleteTemplate$: () => {},
});

export const ecmrUserServiceStub = () => ({
  submitAssignRequest: () => of('1'),
  submitClaimRequest: () => of('1'),
});

export const ecmrExportImportServiceStub = () => ({
  getImport: () => {},
  getExportToken: () => of(mockExportToken),
});

export const ecmrListServiceStub = () => ({
  updateEcmrList$: () => ({}),
  getEcmrList$: () => of(mockEcmrList(5)),
});

export const encryptionServiceStub = ({
  signatureSuccessful = true,
  signatureFields = ['69', '70', '72'],
  pubKeyString = 'abc123',
  generateKeysSuccess = true,
}: EncryptionServiceStubParams) => ({
  signMsg: () =>
    new Promise<void>((resolve, reject) => {
      if (signatureSuccessful) {
        resolve();
      } else {
        reject();
      }
    }),
  mapToString: () => 'abc',
  getSignatureFieldIds: () => signatureFields,
  getPublicKeyAsString: () => pubKeyString,
  generateKeys: () =>
    new Promise<void>((resolve, reject) => {
      if (generateKeysSuccess) {
        resolve();
      } else {
        reject();
      }
    }),
});

interface EncryptionServiceStubParams {
  signatureSuccessful?: boolean;
  signatureFields?: string[];
  pubKeyString?: string;
  generateKeysSuccess?: boolean;
}

export const loadingServiceStub = (b = false) => ({
  getLoadingState$: () => of(b),
});

export const shareEcmrServiceStub = (currentEcmrId = '123') => ({
  getExportObjectToShare: () => mockExportObject(currentEcmrId),
  setExportObjectToShare: () => '',
  unsetExportObjectToShare: () => {},
  getCurrentContact: () => mockContact(1),
  getCurrentEcmrHash: () => currentEcmrId,
  setShareFields: () => {},
  unsetCurrentEcmrId: () => {},
});

export const sidenavServiceStub = (b = true) => ({
  getSidenavState: () => of(b),
  toggleSidenavState: () => ({}),
});

export const snackbarServiceStub = (msg = 'some message', dur = 10) => ({
  getStatusMessage$: () => of(msg),
  getNextDuration: () => dur,
  pushNewStatus: () => {},
});

export const tableStateServiceStub = (
  pageSize = 10,
  pageIndex = 0,
  sortDirection = '',
  sortActive = ''
) => ({
  getSortState: () => ({
    active: sortActive,
    direction: sortDirection,
  }),
  getPageState: () => ({ pageIndex, pageSize }),
  setSortState: () => {},
  setPageState: () => {},
  setPageStateFromEvent: () => {},
});

export const themeServiceStub = () => ({
  cycleMode: () => ({}),
  getModeName$: () =>
    of(
      { name: 'dark-mode', icon: 'dark_mode' },
      { name: 'light-mode', icon: 'light_mode' }
    ),
  getModeName: () =>
    ({ name: 'dark-mode', icon: 'dark_mode' } as ThemeState),
});

export const userProfileServiceStub = (
  isDisponent = false,
  isSuperuser = false,
  userProfile = mockUserProfile(123),
  userRoles: string[] = ['ecmr-disponent','ecmr-fahrer','ecmr-empfaenger','ecmr-superuser','ecmr-user']
) => ({
  userProfile: new Promise<UserProfile>(resolve => {
    resolve(userProfile);
  }),
  isDisponent$: of(isDisponent),
  isSuperuser$: of(isSuperuser),
  userRoles$: new BehaviorSubject<string[]>(userRoles).asObservable(),
  updateUserRoles$: () => {},
  updateUserRolesForCarrier$: () => {}
});

export const userServiceStub = (
  userFields = {
    // Manatory Fields from index 0 to 34
    //               equals to id 1 to 38
    //               equals to Sendername to Successive Carrier Signature
    mandatoryFields: [...mockFieldArray(59).slice(0, 35)],
    // Optional Fields from index 35 to 58
    //               equals to id 39 to 66
    //               equals to Reservations to Cash On Delivery
    optionalFields: [...mockFieldArray(59).slice(35)],
  }
) => ({
  userContacts$: of(mockContacts),
  userCompany$: of(mockUserCompanyDto(49)),
  userCompany: mockUserCompanyDto(49),
  userFields$: of(userFields),
  updateUserContacts$: () => {},
  updateUserFields$: () => {},
  updateUserFieldsForCarrier: () => {},
  updateUserCompany$: () => {},
});

export const userKeyServiceStub = () => ({
  postUserPubkey: () => of(true),
});

export const signatureChoiceServiceStub = (choice: 0 | 1 = 0) => ({
  getSignatureChoice: () => choice,
  setSignatureChoice: () => {},
});
