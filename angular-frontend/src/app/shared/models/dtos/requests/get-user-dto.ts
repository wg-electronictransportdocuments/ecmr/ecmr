/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface GetUserDto {
  id: string;
  userName: string;
  email: string;
  userRoles: string;
  userCompany: UserCompanyDto;
  attributes: string;
}

export interface UserCompanyDto {
  companyId: number;
  companyLocation: string;
  companyName: string;
}
