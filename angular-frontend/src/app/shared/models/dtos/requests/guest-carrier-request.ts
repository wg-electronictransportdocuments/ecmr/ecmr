export interface GuestCarrierRequest {
    firstName: string;
    lastName: string;
    company: string;
    phoneNumber: string;
}
