/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Contact } from './contact';

export type ContactListEntry = Contact & {
    checked?: boolean;
  };

export const checkedContactListHeaderObjects = {
    checked: { id: 'checked', label: 'Select' },
    contactId: { id: 'contactId', label: 'ID' },
    contactType: { id: 'contactType', label: 'Role' },
    contactName: { id: 'contactName', label: 'Name' },
    contactEmail: { id: 'contactEmail', label: 'E-Mail' },
    amountOfContacts: { id: 'amountOfContacts', label: 'No. of Interactions' },
    lastContact: { id: 'lastContact', label: 'Last Contacted' },
};
