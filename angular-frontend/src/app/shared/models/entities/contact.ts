/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface Contact {
  contactId: string;
  contactType: string;
  contactName: string;
  contactEmail: string;
  amountOfContacts: number;
  lastContact: string;
}

export const contactListHeaderObjects = {
  contactId: { id: 'contactId', label: 'ID' },
  contactType: { id: 'contactType', label: 'Role' },
  contactName: { id: 'contactName', label: 'Name' },
  contactEmail: { id: 'contactEmail', label: 'E-Mail' },
  amountOfContacts: { id: 'amountOfContacts', label: 'No. of Interactions' },
  lastContact: { id: 'lastContact', label: 'Last Contacted' },
};
