/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface EcmrFilter{
    carrierNameColumn: string,
    consigneeColumn: string,
    dateOfCreationColumn: string,
    destinationColumn: string,
    licensePlateColumn: string,
    referenceColumn: string,
    statusColumn: string,
}

export interface EcmrTemplateFilter{
    consigneeColumn: string,
    senderNameColumn: string,
    templateNameColumn: string,
}
