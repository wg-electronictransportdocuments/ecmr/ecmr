/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface EcmrListEntry {
  carrierName: string;
  consigneeAddress: string;
  consigneeName: string;
  created: string;
  deletable: boolean;
  ecmrId: string;
  editable: boolean;
  host: string;
  lastUpdated: string;
  lastUsername: string;
  licensePlate: string;
  referenceNr: string;
  senderName: string;
  nationalInternationalTransport: string;
  shareable: boolean;
  status: string;
  version?: number;
}

export const ecmrListHeaderObjects = {
  carrierName: { id: 'carrierName', label: 'Carrier Name'},
  consigneeAddress: { id: 'consigneeAddress', label: 'Consignee Adress'},
  consigneeName: { id: 'consigneeName', label: 'To' },
  created: { id: 'created', label: 'Creation Date' },
  ecmrId: { id: 'ecmrId', label: 'ID' },
  host: { id: 'host', label: 'Host Address' },
  lastUpdated: { id: 'lastUpdated', label: 'Last Edit Date' },
  lastUsername: { id: 'lastUsername', label: 'Last Editor' },
  licensePlate: { id: 'licensePlate', label: 'License Plate'},
  refId: { id: 'referenceNr', label: 'Ref.-ID' },
  senderName: { id: 'senderName', label: 'From' },
  nationalInternationalTransport: { id: 'nationalInternationalTransport', label: 'Transport Type'},
  status: { id: 'status', label: 'Status' },
  version: { id: 'version', label: 'Version No.' },
  edit: { id: 'edit', label: 'Edit'},
  share: { id: 'share', label: 'Share'},
};

export const ecmrStatuses = [
  'GENERATED',
  'NEW',
  'LOADING',
  'IN TRANSPORT',
  'ARRIVED AT DESTINATION',
  'TRANSPORT COMPLETED',
];
