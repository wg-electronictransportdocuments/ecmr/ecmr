/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface EcmrTemplateListEntry {
    consigneeName: string;
    created: string;
    deletable: boolean;
    templateId: number;
    ecmrId: string;
    name: string;
    editable: boolean;
    host: string;
    lastUpdated: string;
    lastUsername: string;
    senderName: string;
    shareable: boolean;
    status: string;
    version?: number;
}

export const ecmrTemplateListHeaderObjects = {
    consigneeName: { id: 'consigneeName', label: 'To' },
    created: { id: 'created', label: 'Creation Date' },
    templateId: { id: 'templateId', label: 'ID' },
    name: { id: 'name', label: 'Name'},
    host: { id: 'host', label: 'Host Address' },
    lastUpdated: { id: 'lastUpdated', label: 'Last Edit Date' },
    lastUsername: { id: 'lastUsername', label: 'Last Editor' },
    refId: { id: 'referenceNr', label: 'Ref.-ID' },
    senderName: { id: 'senderName', label: 'From' },
    status: { id: 'status', label: 'Status' },
    version: { id: 'version', label: 'Version No.' },
};

export const ecmrStatuses = [
    'GENERATED',
    'NEW',
    'LOADING',
    'IN TRANSPORT',
    'ARRIVED AT DESTINATION',
    'TRANSPORT COMPLETED',
];
