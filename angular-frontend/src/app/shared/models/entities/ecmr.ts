/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Fields } from './fields';

export interface Ecmr {
  ecmrId: string;
  status: string;
  lastUserId: string;
  lastUsername: string;
  lastUpdated: string;
  created: string;
  fields: Fields;
  version: string;
  host: string;
}
