/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface ExportObject {
  ecmrId: string;
  host: string;
}

export const exportObjectHeaders = {
  ecmrId: { id: 'ecmrId', label: 'Ecmr ID' },
  host: { id: 'host', label: 'Host Address' },
};
