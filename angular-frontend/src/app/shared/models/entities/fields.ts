/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Position, positionFieldIds } from './position';

export interface Fields {
  1?: string;
  2?: string;
  3?: string;
  4?: string;
  5?: string;
  6?: string;
  7?: string;
  8?: string;
  // Note: Changed Order
  10?: string;
  9?: string;
  11?: string;
  // 12?: string;
  13?: string;
  14?: string;
  15?: string;
  16?: string;
  17?: string;
  18?: string;
  19?: string;
  // 20?: string;
  21?: string;
  22?: string;
  // Note: Changed Order
  24?: string;
  23?: string;
  25?: string;
  26?: string;
  27?: string;
  // 28?: string;
  29?: string;
  30?: string;
  // Note: Changed Order
  32?: string;
  31?: string;
  33?: string;
  34?: string;
  35?: string;
  36?: string;
  37?: string;
  38?: string;
  39?: string;
  40?: string;
  positions?: Position[];
  52?: string;
  53?: string;
  54?: string;
  55?: string;
  56?: string;
  57?: string;
  58?: string;
  59?: string;
  60?: string;
  61?: string;
  62?: string;
  63?: string;
  64?: string;
  65?: string;
  66?: string;
  67?: string;
  68?: string;
  69?: string;
  70?: string;
  71?: string;
  // Note: Changed Order
  74?: string;
  72?: string;
  73?: string;
  75?: string;
}

export const fieldIds = (
  onlyStaticFields = true,
  onlyFirstPositionField = true
): string[] => [
  '1',
  '2',
  '3',
  '4',
  '5',
  '6',
  '7',
  '8',
  '10',
  '9',
  '11',
  // '12',
  '13',
  '14',
  '15',
  '16',
  '17',
  '18',
  '19',
  // '20',
  '21',
  '22',
  '24',
  '23',
  '25',
  '26',
  '27',
  // '28',
  '29',
  '30',
  '32',
  '31',
  '33',
  '34',
  '35',
  '36',
  '37',
  '38',
  '39',
  ...(onlyStaticFields
    ? ['40']
    : onlyFirstPositionField
    ? ['40', positionFieldIds[0]]
    : ['40', ...positionFieldIds]),
  '52',
  '53',
  '54',
  '55',
  '56',
  '57',
  '58',
  '59',
  '60',
  '61',
  '62',
  '63',
  '64',
  '65',
  '66',
  '67',
  '68',
  '69',
  '70',
  '71',
  '74',
  '72',
  '73',
  '75',
];

export const placeholderEcmrFields: Fields = {
  // 1 Sender
  1: 'Fraunhofer Institute (IML)',
  2: 'Mr. Mueller',
  3: 'Joseph-von-Fraunhofer-Straße 2-4',
  4: '44227',
  5: 'Dortmund',
  6: 'Germany',

  // 2 Consignee
  7: 'Fraunhofer-Gesellschaft',
  8: 'Mr. Fischer',
  9: '80686',
  10: 'Hansastraße 27c',
  11: 'Munich',
  // 12: 'DE',
  13: 'Germany',

  // 3 Taking over the goods
  14: 'Dortmund, Germany', // Mr. Fischer
  15: '14.03.2024', // 13/09/2021
  16: '14.03.2024', // 10/09/2021

  // 4 Delivery of the goods
  17: 'Munich, Germany', // Warehouse
  18: 'Mon: 9am - 5pm Tue: 9am - 5pm Wed:9am - 5pm Thu:9am - 5pm',

  // 5 Sender's instructions
  // 19: '',

  // 20: '560',

  // 9 Documents handed to the carrier by the sender (Remarks)
  40: '',

  // 16 Special agreements between the sender and the carrier
  52: '',

  // 6 Carrier
  21: 'Spedition Weber',
  22: 'Mr. Wagner',
  23: '45139',
  24: 'Leopoldstraße 55',
  25: 'Essen',
  26: 'DE',
  27: 'Germany',
  // 28: 'NRW',
  29: 'E-GH-1053',

  // 7 Successive Carriers
  30: '',
  31: '',
  32: '',
  33: '',
  34: '',
  35: '',
  36: '',
  37: '',
  38: '',
  39: '',

  // 18 Other useful particulars
  65: '',

  // 19 Cash on delivery
  66: '',

  // 24 Goods Received
  71: '', // '21/07/2022'
  74: '', // 'Port of Columbia'

  // 17 To be paid by
  53: '',
  54: 'EUR',
  55: 'sender',
  56: '',
  57: 'EUR',
  58: 'consignee',
  59: '',
  60: 'EUR',
  61: 'consignee',
  62: '',
  63: 'EUR',
  64: 'sender',

  // 21 Established
  67: 'Dortmund',
  68: '14.03.2024',

  // Non-contractual part reserved for the carrier
  73: '',

  // Reference Number
  75: 'FhG-IML-001',

  positions: [
    {
      41: 'Water',
      42: '156742',
      43: '20',
      // 44: '254236',
      45: 'Euro pallet',
      // 46: '165',
      // 47: '125434',
      // 48: '40',
      49: 'Water canisters',
      50: '1',
      51: '1',
    },
  ],
};
