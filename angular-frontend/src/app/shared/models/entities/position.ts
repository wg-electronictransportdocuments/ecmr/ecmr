/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export interface Position {
  41?: string;
  42?: string;
  43?: string;
  // 44?: string;
  45?: string;
  // 46?: string;
  // 47?: string;
  // 48?: string;
  49?: string;
  50?: string;
  51?: string;
}

export const positionFieldIds: string[] = [
  '41',
  '42',
  '43',
  // '44',
  '45',
  // '46',
  // '47',
  // '48',
  '49',
  '50',
  '51',
];
