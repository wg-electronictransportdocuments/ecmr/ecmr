/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Contact } from './contact';
import { Right } from '../types/right';

export interface User {
  userId: string;
  userName: string;
  userCompany: string;
  contacts: Contact[];
  mandatoryFieldIds: string[];
  optionalFieldIds?: string[];
  rights: Right[];
}
