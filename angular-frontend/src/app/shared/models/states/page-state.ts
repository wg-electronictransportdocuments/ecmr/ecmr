/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { AvailableTables } from '../../services/state/table-state.service';

export interface PageState {
  pageName: AvailableTables;
  pageSize: number;
  pageIndex: number;
}
