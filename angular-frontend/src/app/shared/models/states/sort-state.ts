/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Sort } from '@angular/material/sort';
import { AvailableTables } from '../../services/state/table-state.service';

export interface SortState extends Sort {
  sortName: AvailableTables;
}
