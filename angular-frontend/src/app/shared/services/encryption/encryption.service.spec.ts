/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { EncryptionService } from './encryption.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserKeyService } from '../http/user-key.service';
import { userKeyServiceStub } from '../../mockups/service-stubs';

describe('EncryptionService', () => {
  let service: EncryptionService;

  describe('has working methods so', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
        providers: [
          { provide: UserKeyService, useValue: userKeyServiceStub() },
        ],
      });
      service = TestBed.inject(EncryptionService);
    });
    it('the application does not crash without ssl setting', async () => {
      // eslint-disable-next-line @typescript-eslint/dot-notation
      service['useSsl'] = false;
      await service.generateKeys();
      const sig = await service.signMsg('test');
      expect(sig).not.toBeNull();
      await service.loadLocalStorage();
      service.unsetKeys();
      expect(service).toBeTruthy();
    });
    it('the application does not crash with ssl setting', async () => {
      // eslint-disable-next-line @typescript-eslint/dot-notation
      service['useSsl'] = true;
      await service.generateKeys();
      const sig = await service.signMsg('test');
      expect(sig).not.toBeNull();
      await service.loadLocalStorage();
      service.unsetKeys();
      expect(service).toBeTruthy();
    });
  });

  describe('without prefilled local storage', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      service = TestBed.inject(EncryptionService);
    });

    it('should not crash on construction', () => {
      expect(service).toBeTruthy();
    });
  });

  describe('with prefilled local storage', () => {
    it('should catch errors on faulty localStorage', done => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule],
      });
      localStorage.setItem('private-key', '123');
      localStorage.setItem('public-key', '321');
      service = TestBed.inject(EncryptionService);
      expect(service).toBeTruthy();
      done();
    });
  });
});
