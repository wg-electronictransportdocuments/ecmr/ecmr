/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { LoadingStateService } from '../state/loading-state.service';
import { UserKeyService } from '../http/user-key.service';
import { environment } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';

@Injectable({
  providedIn: 'root',
})
export class EncryptionService {
  // not readonly to simplify testing
  private useSsl: boolean;

  private storageKeyPri = 'private-key';
  private storageKeyPub = 'public-key';

  private priKey: CryptoKey;
  private pubKey: CryptoKey;
  private pubKeyStr: string;

  private signatureFieldIds: string[] = ['69', '70', '72'];

  constructor(
    public loadingStateService: LoadingStateService,
    public userKeyService: UserKeyService,
    public snackbarService: SnackbarService
  ) {
    this.useSsl = environment.instanceSsl === 'true';
  }

  /**
   * Converts signature from Uint8Array format to DER-Array format ("Distinguished Encoding Rules")
   */
  private static sigToDerArray(signature: Uint8Array): number[] {
    const derArray = [];
    // Header
    derArray.push(48);
    // Length of Data
    derArray.push(68);
    // int indicator
    derArray.push(2);
    // Length in bit
    derArray.push(32);
    // R value
    for (let i = 0; i < 32; i++) {
      derArray.push(signature[i]);
    }
    // int indicator
    derArray.push(2);
    // length in bit
    derArray.push(32);
    // S value
    for (let i = 32; i < signature.length; i++) {
      derArray.push(signature[i]);
    }
    return derArray;
  }

  /**
   * Convert ArrayBuffer to PEM-formatted String
   */
  private static spkiToPEM(keydata: ArrayBuffer) {
    const keydataS = EncryptionService.arrayBufferToString(keydata);
    // transform to base64
    const keydataB64 = window.btoa(keydataS);
    return EncryptionService.formatAsPem(keydataB64);
  }

  private static arrayBufferToString(buffer: ArrayBuffer) {
    let binary = '';
    const bytes = new Uint8Array(buffer);
    const len = bytes.byteLength;
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i]);
    }
    return binary;
  }

  /**
   *
   */
  private static formatAsPem(str: string) {
    //set header
    let finalString = '-----BEGIN PUBLIC KEY-----\n';

    // encode public key
    while (str.length > 0) {
      finalString += str.substring(0, 64) + '\n';
      str = str.substring(64);
    }
    // set footer
    finalString = finalString + '-----END PUBLIC KEY-----';

    return finalString;
  }

  public mapToString(map: any) {
    let result = '';
    for (const key in map) {
      if (Object.prototype.hasOwnProperty.call(map, key)) {
        const value = map[key];
        // If field is a signature itself, pass this iteration
        if (this.signatureFieldIds.includes(key)) {
          continue;
        }
        if (key === 'positions') {
          for (const pos of value) {
            for (const posKey in pos) {
              if (pos.hasOwnProperty(posKey)) {
                result += posKey + pos[posKey];
              }
            }
          }
        } else {
          result += key + value;
        }
      }
    }
    return result;
  }

  public unsetKeys(): void {
    this.priKey = undefined;
    this.pubKey = undefined;
    this.pubKeyStr = undefined;
    this.deleteLocalStorage();
  }

  public deleteLocalStorage(): void {
    localStorage.removeItem(this.storageKeyPub);
    localStorage.removeItem(this.storageKeyPri);
  }

  /**
   * Generates a signature for any message-string using private key
   *
   * Returns promise object which resolves to an array of numbers
   * Makes use of 'useSsl' environment variable to filter non-supported unsecure web-crypto requests and returns empty
   * array instead
   */
  public async signMsg(msg: string): Promise<number[]> {
    // Sign message
    if (!this.useSsl) {
      return Promise.resolve([]);
    }
    return (
      window.crypto.subtle
        .sign(
          { name: 'ECDSA', hash: { name: 'SHA-256' } },
          this.priKey,
          new TextEncoder().encode(msg)
        )
        .then(result => EncryptionService.sigToDerArray(new Uint8Array(result)))
        // TODO: Catch error
        .catch(() => undefined)
    );
  }

  public getSignatureFieldIds(): string[] {
    return this.signatureFieldIds;
  }

  /**
   * Generates and publishes new keypair to server
   */
  public async generateKeys(): Promise<void> {
    if (!this.useSsl) {
      this.priKey = undefined;
      this.pubKey = undefined;
      this.pubKeyStr = 'dummy-key';
      return Promise.resolve();
    }

    this.loadingStateService.setLoadingState$(true);
    return new Promise<void>(async (resolve, reject) => {
      // Generate Keys
      try {
        await window.crypto.subtle
          .generateKey(
            {
              name: 'ECDSA',
              namedCurve: 'P-256',
            },
            true,
            ['sign', 'verify']
          )
          .then(async keypair => {
            this.priKey = keypair.privateKey;
            this.pubKey = keypair.publicKey;
            await this.exportCryptoKey(this.pubKey).then(async r => {
              this.pubKeyStr = r;
              await this.setLocalStorage();
            });
          });
      } catch (err) {
        this.snackbarService.pushNewStatus(
          `The instance was hosted in a non-secure context, so the application can not generate keys to sign
            documents. Please contact the administrator. Error-Code: ${err}`,
          100000
        );
        // TODO: catch rejects
        resolve();
      }

      // Reach out to server
      this.userKeyService.postUserPubkey(this.pubKeyStr).subscribe({
        next: () => {
          resolve();
        },
        error: () => {
          reject();
        },
        complete: () => {
          this.loadingStateService.setLoadingState$(false);
        },
      });
    });
  }

  public async loadLocalStorage(): Promise<void> {
    if (!this.useSsl) {
      this.unsetKeys();
      return Promise.resolve();
    }
    this.loadingStateService.setLoadingState$(true);
    const pubKeyStr = localStorage.getItem(this.storageKeyPub);
    const priKeyStr = localStorage.getItem(this.storageKeyPri);
    try {
      if (pubKeyStr && priKeyStr) {
        // Assert that pubKey ain't corrupt
        let jwkKey: JsonWebKey = JSON.parse(pubKeyStr);
        await window.crypto.subtle
          .importKey(
            'jwk',
            jwkKey,
            { name: 'ECDSA', namedCurve: 'P-256' },
            true,
            ['verify']
          )
          .then(res => (this.pubKey = res))
          .catch(async () => {
            this.unsetKeys();
            await this.generateKeys();
          });
        // Assert that privKey ain't corrupt
        jwkKey = JSON.parse(priKeyStr);
        await window.crypto.subtle
          .importKey(
            'jwk',
            jwkKey,
            { name: 'ECDSA', namedCurve: 'P-256' },
            true,
            ['sign']
          )
          .then(res => {
            this.priKey = res;
          })
          .catch(async () => {
            this.unsetKeys();
            await this.generateKeys();
          });
      } else {
        this.unsetKeys();
        await this.generateKeys();
      }
    } catch (e) {
      this.unsetKeys();
      await this.generateKeys();
    }
    this.loadingStateService.setLoadingState$(false);
  }

  /**
   * Export key to from CryptopKey format to PEM formatted string
   *
   * Only works in secure contexts, else 'dummy-key' is returned
   */
  private async exportCryptoKey(key: CryptoKey): Promise<string> {
    // Check if secure context should be available
    if (!this.useSsl) {
      return 'dummy-key';
    }
    // Rewrite key as SPKI Key
    const exported = await window.crypto.subtle.exportKey('spki', key);
    // add PEM formatting to SPKI Key
    return EncryptionService.spkiToPEM(exported);
  }

  private async setLocalStorage(): Promise<void> {
    await window.crypto.subtle.exportKey('jwk', this.priKey).then(res => {
      localStorage.setItem(this.storageKeyPri, this.mapToString(res));
    });
    await window.crypto.subtle.exportKey('jwk', this.pubKey).then(res => {
      localStorage.setItem(this.storageKeyPub, this.mapToString(res));
    });
  }

}
