/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { EcmrDeleteService } from './ecmr-delete.service';
import { LoadingStateService } from '../state/loading-state.service';
import { SnackbarService } from '../state/snackbar.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment as env } from '../../../../environments/environment';
import { endpoints } from './endpoints';

describe('EcmrDeleteService', () => {
  let service: EcmrDeleteService;
  let controller: HttpTestingController;

  const expectedDeleteUrl =
    env.backendUrl + endpoints.backend.deleteEcmrById.replace('{id}', 'someId');

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: LoadingStateService,
          useValue: {
            setLoadingState$: () => {},
          },
        },
        {
          provide: SnackbarService,
          useValue: {
            pushNewStatus: () => {},
          },
        },
      ],
    });
    service = TestBed.inject(EcmrDeleteService);
    controller = TestBed.inject(HttpTestingController);
    spyOn(service.snackbarService, 'pushNewStatus');
    spyOn(service.loadingStateService, 'setLoadingState$');
  });

  describe('deleteEcmr Function', () => {
    beforeEach(() => {});

    it('should return observable which resolves w/o error and push status message to snackbar on success', () => {
      let result: boolean;
      service.deleteEcmr('someId').subscribe(
        () => (result = true),
        () => (result = false)
      );
      controller.expectOne(expectedDeleteUrl).flush({});
      expect(result).toBeTrue();
      expect(service.snackbarService.pushNewStatus).toHaveBeenCalled();
      expect(service.loadingStateService.setLoadingState$).toHaveBeenCalledWith(
        false
      );
    });

    it('should return observable which returns error-case and push status message to snackbar on error', () => {
      let result: boolean;
      const errorEvent = new ErrorEvent('some error');
      service.deleteEcmr('someId').subscribe(
        () => (result = true),
        () => (result = false)
      );
      controller.expectOne(expectedDeleteUrl).error(errorEvent);
      expect(result).toBeFalse();
      expect(service.snackbarService.pushNewStatus).toHaveBeenCalled();
      expect(service.loadingStateService.setLoadingState$).toHaveBeenCalledWith(
        false
      );
    });

    it('should handle network errors accordingly', () => {
      let result: boolean;
      const errorEvent = new ErrorEvent('some error');
      service.deleteEcmr('someId').subscribe(
        () => (result = true),
        () => (result = false)
      );
      controller
        .expectOne(expectedDeleteUrl)
        .error(errorEvent, { status: 400 });
      expect(result).toBeFalse();
      expect(service.snackbarService.pushNewStatus).toHaveBeenCalled();
      expect(service.loadingStateService.setLoadingState$).toHaveBeenCalledWith(
        false
      );
    });
  });
});
