/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { LoadingStateService } from '../state/loading-state.service';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';
import { endpoints } from './endpoints';
import { SnackbarService } from '../state/snackbar.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class EcmrDeleteService {
  constructor(
    public loadingStateService: LoadingStateService,
    public snackbarService: SnackbarService,
    public httpClient: HttpClient
  ) {}

  public deleteEcmr(ecmrId: string): Observable<unknown> {
    this.loadingStateService.setLoadingState$(true);
    return this.httpClient
      .delete(
        env.backendUrl +
          endpoints.backend.deleteEcmrById.replace('{id}', ecmrId)
      )
      .pipe(
        tap({
          next: () => {
            this.snackbarService.pushNewStatus(
              `Successfully deleted eCMR ${ecmrId} from server.`
            );
            this.loadingStateService.setLoadingState$(false);
          },
          error: error => {
            if (error.status !== 0) {
              this.snackbarService.pushNewStatus(
                `Cannot delete eCMR ${ecmrId} from server. ${error.error.response}`,
                10000
              );
            } else {
              this.snackbarService.pushNewStatus(
                `There seems to be a network connection issue or the eCMR Server is offline.
                      Please check your connection and/or try again later.`,
                10000
              );
            }
            this.loadingStateService.setLoadingState$(false);
          },
        })
      );
  }
}
