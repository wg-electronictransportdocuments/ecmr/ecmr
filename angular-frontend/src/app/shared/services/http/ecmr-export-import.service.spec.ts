/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { EcmrExportImportService } from './ecmr-export-import.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { LoadingStateService } from '../state/loading-state.service';
import { SnackbarService } from '../state/snackbar.service';

describe('EcmrExportImportService', () => {
  let service: EcmrExportImportService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        {
          provide: LoadingStateService,
          useValue: {
            setLoadingState$: () => {},
          },
        },
        {
          provide: SnackbarService,
          useValue: {
            pushNewStatues: () => {},
          },
        },
      ],
    });
    service = TestBed.inject(EcmrExportImportService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
