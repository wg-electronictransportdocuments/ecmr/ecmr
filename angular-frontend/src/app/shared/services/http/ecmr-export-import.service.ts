/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env } from '../../../../environments/environment';
import { endpoints } from './endpoints';
import { tap } from 'rxjs/operators';
import { LoadingStateService } from '../state/loading-state.service';
import { HttpClient } from '@angular/common/http';
import { SnackbarService } from '../state/snackbar.service';
import { ExportObject } from '../../models/entities/export-object';

@Injectable({
  providedIn: 'root',
})
export class EcmrExportImportService {
  constructor(
    public loadingStateService: LoadingStateService,
    public httpClient: HttpClient,
    public snackbarService: SnackbarService
  ) {}

  public getImport(eto: ExportObject): Observable<any> {
    this.loadingStateService.setLoadingState$(true);
    return this.httpClient
      .post(env.backendUrl + endpoints.backend.postImportEcmr, eto)
      .pipe(
        tap({
          next: () => {
            this.snackbarService.pushNewStatus(
              'Import was successful'
            );
            this.loadingStateService.setLoadingState$(false);
          },
          error: error => {
            if (error.status !== 0) {
              this.snackbarService.pushNewStatus(
                `Import was unsuccessful. ${error.error.response}`,
                10000
              );
            } else {
              this.snackbarService.pushNewStatus(
                `There seems to be a network connection issue or the eCMR Server is offline.
                      Please check your connection and/or try again later.`,
                10000
              );
            }
            this.loadingStateService.setLoadingState$(false);
          },
        })
      );
  }

  public getExportToken(ecmrId: string): Observable<any> {
    this.loadingStateService.setLoadingState$(true);
    return this.httpClient
      .get<string>(
        env.backendUrl +
          endpoints.backend.getExportToken.replace('{id}', ecmrId)
      )
      .pipe(
        tap(
          () => {
            // this.snackbarService.pushNewStatus(
            //   `Successfully retrieved eCMR-Export Token ${ecmrId} from server.`
            // );
            this.loadingStateService.setLoadingState$(false);
          },
          error => {
            if (error.status !== 0) {
              this.snackbarService.pushNewStatus(
                `Cannot retrieve eCMR-Export Token ${ecmrId} from server. ${error.error.response}`,
                10000
              );
            } else {
              this.snackbarService.pushNewStatus(
                `There seems to be a network connection issue or the eCMR Server is offline.
                      Please check your connection and/or try again later.`,
                10000
              );
            }
            this.loadingStateService.setLoadingState$(false);
          }
        )
      );
  }
}
