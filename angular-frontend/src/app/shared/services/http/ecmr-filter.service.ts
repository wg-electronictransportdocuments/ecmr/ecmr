/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { EcmrFilter, EcmrTemplateFilter } from '../../models/entities/ecmr-filter';
import { BehaviorSubject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EcmrFilterService {

  private overviewFilter: BehaviorSubject<EcmrFilter> = new BehaviorSubject<EcmrFilter>(null);
  public overviewFilter$: Observable<EcmrFilter> = this.overviewFilter.asObservable();

  private archiveFilter: BehaviorSubject<EcmrFilter> = new BehaviorSubject<EcmrFilter>(null);
  public archiveFilter$: Observable<EcmrFilter> = this.archiveFilter.asObservable();

  private templateFilter: BehaviorSubject<EcmrTemplateFilter> = new BehaviorSubject<EcmrTemplateFilter>(null);
  public templateFilter$: Observable<EcmrTemplateFilter> = this.templateFilter.asObservable();

  public setOverviewFilter(ecmrFilter: EcmrFilter){
    this.overviewFilter.next(ecmrFilter);
  }

  public resetOverviewFilter(){
    this.overviewFilter.next(null);
  }

  public setArchiveFilter(ecmrFilter: EcmrFilter){
    this.archiveFilter.next(ecmrFilter);
  }

  public resetArchiveFilter(){
    this.archiveFilter.next(null);
  }

  public setTemplateFilter(ecmrTemplateFilter: EcmrTemplateFilter){
    this.templateFilter.next(ecmrTemplateFilter);
  }

  public resetTemplateFilter(){
    this.templateFilter.next(null);
  }

}
