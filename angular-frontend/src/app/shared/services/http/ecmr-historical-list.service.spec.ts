/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { fakeAsync, flush, TestBed } from '@angular/core/testing';
import { AuthService } from '../../../modules/user-management/services/auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment as env } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';
import { RouterTestingModule } from '@angular/router/testing';
import { OverviewPageComponent } from '../../../pages/overview-page/overview-page.component';
import { EcmrHistoricalListService } from './ecmr-historical-list.service';
import { endpoints } from './endpoints';

describe('EcmrHistoricalListService', () => {
  let service: EcmrHistoricalListService;
  let controller: HttpTestingController;

  const mockDoc: any[] = [
    {
      version: '1',
      status: 'new',
      lastUserName: 'ernie',
      lastUpdated: '2021-11-04T12:57:45.442+00:00',
    },
    {
      version: '2',
      status: 'loading',
      lastUserName: 'ernie',
      lastUpdated: '2021-11-04T12:57:45.446+00:00',
    },
  ];
  const firstExpectedId = '0';
  const secondExpectedId = '123';
  const firstExpectedUrl =
    env.backendUrl +
    endpoints.backend.getEcmrHistoryById.replace('{id}', firstExpectedId);
  const secondExpectedUrl =
    env.backendUrl +
    endpoints.backend.getEcmrHistoryById.replace('{id}', secondExpectedId);
  const errorEvent = new ErrorEvent('some error');

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: SnackbarService, useValue: { pushNewStatus: () => ({}) } },
        {
          provide: AuthService,
          useValue: {
            getJwtToken: () => 'some-token',
            removeAuth: () => {},
          },
        },
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          { path: 'ecmr-overview', component: OverviewPageComponent },
        ]),
      ],
    });
    service = TestBed.inject(EcmrHistoricalListService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('updateEcmr$ should update the current Document stream', fakeAsync(() => {
    service.updateEcmr$('123');
    controller.expectOne(secondExpectedUrl).flush(mockDoc);
    service.getEcmr$().subscribe(r => {
      expect(r).toBe(mockDoc);
    });
    flush();
  }));

  it('updateEcmr$ should post an Errormessage to the snackbar on http-error', done => {
    const snackSpy = spyOn(service.snackbarService, 'pushNewStatus');
    service.updateEcmr$('0');
    controller.expectOne(firstExpectedUrl).error(errorEvent);
    expect(snackSpy).toHaveBeenCalled();
    done();
  });
});
