/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { EcmrListEntry } from '../../models/entities/ecmr-list-entry';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';
import { LoadingStateService } from '../state/loading-state.service';
import { endpoints } from './endpoints';

@Injectable({
  providedIn: 'root',
})
export class EcmrHistoricalListService {
  ecmrList$: ReplaySubject<EcmrListEntry[]> = new ReplaySubject<
    EcmrListEntry[]
  >();

  constructor(
    public httpClient: HttpClient,
    public snackbarService: SnackbarService,
    public loadingStateService: LoadingStateService
  ) {}

  public getEcmr$(): Observable<EcmrListEntry[]> {
    return this.ecmrList$.asObservable();
  }

  public updateEcmr$(ecmrId: string): void {
    this.loadingStateService.setLoadingState$(true);
    this.httpClient
      .get<any>(
        env.backendUrl +
          endpoints.backend.getEcmrHistoryById.replace('{id}', ecmrId)
      )
      .subscribe(
        response => {
          this.ecmrList$.next(response);
          this.loadingStateService.setLoadingState$(false);
        },
        error => {
          if (error.status !== 0) {
            this.loadingStateService.setLoadingState$(false);
            this.snackbarService.pushNewStatus(
              `eCMR Backend sent an error: ${error.status}. Please try again later.`,
              10000
            );
          } else {
            this.loadingStateService.setLoadingState$(false);
            this.snackbarService.pushNewStatus(
              'There seems to be a network connection issue or the eCMR Server is' +
                ' offline. Please check your connection and/or try again later.',
              10000
            );
          }
        }
      );
  }
}
