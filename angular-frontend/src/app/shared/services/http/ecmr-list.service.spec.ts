/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { EcmrListService } from './ecmr-list.service';
import { AuthService } from '../../../modules/user-management/services/auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { mockEcmrList } from '../../mockups/mock-objects';
import { environment as env } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';
import { endpoints } from './endpoints';

describe('EcmrListService', () => {
  let service: EcmrListService;
  let controller: HttpTestingController;

  const mockList = mockEcmrList(12);
  const url = env.backendUrl + endpoints.backend.getEcmrs;
  const errorEvent = new ErrorEvent('some error');

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: SnackbarService, useValue: { pushNewStatus: () => ({}) } },
        {
          provide: AuthService,
          useValue: {
            getJwtToken: () => 'some-token',
            removeAuth: () => ({}),
          },
        },
      ],
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(EcmrListService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('updateEcmrList$ should update the list stream', done => {
    service.updateEcmrList$(false);
    controller.expectOne(url + '?isArchive=false').flush(mockList);
    service.getEcmrList$().subscribe(r => {
      expect(r).toBe(mockList);
      done();
    });
  });

  it('updateEcmrList$ should post an Errormessage to the snackbar on http-error', done => {
    const snackSpy = spyOn(service.snackbarService, 'pushNewStatus');
    service.updateEcmrList$(false);
    controller.expectOne(url + '?isArchive=false').error(errorEvent);
    expect(snackSpy).toHaveBeenCalled();
    done();
  });
});
