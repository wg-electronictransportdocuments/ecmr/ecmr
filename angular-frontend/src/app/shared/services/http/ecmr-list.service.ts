/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { EcmrListEntry } from '../../models/entities/ecmr-list-entry';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';
import { LoadingStateService } from '../state/loading-state.service';
import { endpoints } from './endpoints';

@Injectable({
  providedIn: 'root',
})
export class EcmrListService {
  ecmrList$: ReplaySubject<EcmrListEntry[]> = new ReplaySubject<
    EcmrListEntry[]
  >();

  constructor(
    public httpClient: HttpClient,
    public snackbarService: SnackbarService,
    public loadingStateService: LoadingStateService
  ) {}

  public updateEcmrList$(isArchive: boolean): void {
    this.loadingStateService.setLoadingState$(true);
    const queryParams = new HttpParams().set('isArchive', isArchive);
    this.httpClient
      .get<EcmrListEntry[]>(env.backendUrl + endpoints.backend.getEcmrs, {params: queryParams})
      .subscribe({
        next: response => {
          this.ecmrList$.next(response);
          this.loadingStateService.setLoadingState$(false);
        },
        error: error => {
          if (error.status !== 0) {
            this.snackbarService.pushNewStatus(
              `eCMR Backend sent an error: ${error.status}. Please try again later.`,
              10000
            );
          } else {
            this.snackbarService.pushNewStatus(
              `There seems to be a network connection issue or the eCMR Server is offline.
              Please check your connection and/or try again later.`,
              10000
            );
          }
          this.loadingStateService.setLoadingState$(false);
        },
      });
  }

  public getEcmrList$(): Observable<EcmrListEntry[]> {
    return this.ecmrList$.asObservable();
  }
}
