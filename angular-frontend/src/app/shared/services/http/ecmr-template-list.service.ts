/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';
import { EcmrTemplateListEntry } from '../../models/entities/ecmr-template-list-entry';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';
import { LoadingStateService } from '../state/loading-state.service';
import { endpoints } from './endpoints';

@Injectable({
    providedIn: 'root',
})
export class EcmrTemplateListService {
    ecmrTemplateList$: ReplaySubject<EcmrTemplateListEntry[]> = new ReplaySubject<
        EcmrTemplateListEntry[]
    >();

    constructor(
        public httpClient: HttpClient,
        public snackbarService: SnackbarService,
        public loadingStateService: LoadingStateService
    ) {}

    public updateEcmrTemplateList$(): void {
        this.loadingStateService.setLoadingState$(true);
        this.httpClient
            .get<EcmrTemplateListEntry[]>(env.backendUrl + endpoints.backend.getTemplates)
            .subscribe({
                next: response => {
                    this.ecmrTemplateList$.next(response);
                    this.loadingStateService.setLoadingState$(false);
                },
                error: error => {
                    if (error.status !== 0) {
                        this.snackbarService.pushNewStatus(
                            `eCMR Backend sent an error: ${error.status}. Please try again later.`,
                            10000
                        );
                    } else {
                        this.snackbarService.pushNewStatus(
                            `There seems to be a network connection issue or the eCMR Server is offline.
              Please check your connection and/or try again later.`,
                            10000
                        );
                    }
                    this.loadingStateService.setLoadingState$(false);
                },
            });
    }

    public getEcmrTemplateList$(): Observable<EcmrTemplateListEntry[]> {
        return this.ecmrTemplateList$.asObservable();
    }
}
