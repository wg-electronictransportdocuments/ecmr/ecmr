/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { Template } from '../../models/entities/template';
import { HttpClient } from '@angular/common/http';
import { SnackbarService } from '../state/snackbar.service';
import { Router } from '@angular/router';
import { LoadingStateService } from '../state/loading-state.service';
import { environment as env } from '../../../../environments/environment';
import { endpoints } from './endpoints';
import { tap } from 'rxjs/operators';

@Injectable({
    providedIn: 'root',
})
export class EcmrTemplateService {
    private template: BehaviorSubject<Template> = new BehaviorSubject<Template>(undefined);
    private updateCompleteSubject: Subject<void> = new Subject<void>();

    constructor(
        public httpClient: HttpClient,
        public snackbarService: SnackbarService,
        public router: Router,
        public loadingStateService: LoadingStateService
    ) {}

    public get template$(): Observable<Template> {
        return this.template.asObservable();
    }

    public get updateComplete$(): Observable<void> {
        return this.updateCompleteSubject.asObservable();
    }

    public updateTemplate$(templateId: number): void {
        this.loadingStateService.setLoadingState$(true);
        this.httpClient
            .get<Template>(env.backendUrl + endpoints.backend.getTemplateById.replace('{id}', templateId.toString()))
            .subscribe({
                next: response => {
                    this.template.next(response);
                    this.loadingStateService.setLoadingState$(false);
                    this.updateCompleteSubject.next();
                },
                error: error => {
                    // Handle errors
                    this.updateCompleteSubject.next();
                    if (error.status === 401) {
                        this.template.error(error);
                        this.snackbarService.pushNewStatus(`Authentication failed. Please sign in once again.
            Error-Code: ${error.error.text}`, 10000);
                    } else if (error.status !== 0) {
                        this.template.error(error);
                        this.snackbarService.pushNewStatus(`Error-Message: ${error.error.response}. If you're sure this wasn't a mistake try again.`, 10000);
                        this.router.navigate(['error']);
                    } else {
                        this.template.error(error);
                        this.snackbarService.pushNewStatus(`There seems to be a network connection issue or the eCMR Server is offline.
            Please check your connection and/or try again later.`, 10000);
                    }
                    this.loadingStateService.setLoadingState$(false);
                },
            });
    }

    public deleteTemplate$(templateId: number, templateName: string): Observable<unknown> {
        this.loadingStateService.setLoadingState$(true);
        return this.httpClient
            .delete(
                env.backendUrl +
                endpoints.backend.deleteTemplateById.replace('{id}', templateId.toString())
            )
            .pipe(
                tap({
                    next: () => {
                        this.snackbarService.pushNewStatus(
                            `Successfully deleted Template: \'${templateName}\' from server.`
                        );
                        this.loadingStateService.setLoadingState$(false);
                    },
                    error: error => {
                        if (error.status !== 0) {
                            this.snackbarService.pushNewStatus(
                                `Cannot delete Template: \'${templateName}\' from server. ${error.error.response}`,
                                10000
                            );
                        } else {
                            this.snackbarService.pushNewStatus(
                                `There seems to be a network connection issue or the eCMR Server is offline.
                      Please check your connection and/or try again later.`,
                                10000
                            );
                        }
                        this.loadingStateService.setLoadingState$(false);
                    },
                })
            );
    }
}
