/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { fakeAsync, flush, TestBed } from '@angular/core/testing';
import { EcmrUserService } from './ecmr-user.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment as env } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';
import { ShareEcmrService } from '../state/share-ecmr.service';
import { Router } from '@angular/router';
import { endpoints } from './endpoints';
import {
  shareEcmrServiceStub,
  snackbarServiceStub,
} from '../../mockups/service-stubs';

describe('EcmrUserService', () => {
  let service: EcmrUserService;
  let controller: HttpTestingController;

  const expectedUserId = '1';
  const expectedEcmrId = '123';
  const expectedAssignUrl =
    env.backendUrl +
    endpoints.backend.putUserAtEcmrByIds
      .replace('{userId}', expectedUserId)
      .replace('{ecmrId}', expectedEcmrId);
  const expectedClaimUrl =
    env.backendUrl +
    endpoints.backend.putCurrentuserAtEcmrById.replace('{id}', expectedEcmrId);
  const errorEvent = new ErrorEvent('some error');

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: SnackbarService, useValue: snackbarServiceStub() },
        {
          provide: ShareEcmrService,
          useValue: shareEcmrServiceStub(expectedEcmrId),
        },
        {
          provide: Router,
          useValue: {
            navigate: () => {},
          },
        },
      ],
      imports: [HttpClientTestingModule],
    });
    service = TestBed.inject(EcmrUserService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('submitAssignRequest should work on success with userId', fakeAsync(() => {
    service.submitAssignRequest(expectedEcmrId, expectedUserId);
    controller.expectOne(expectedAssignUrl).flush({});
    flush();
  }));

  it('submitAssignRequest should work on error with userId', fakeAsync(() => {
    service.submitAssignRequest(expectedEcmrId, expectedUserId);
    controller.expectOne(expectedAssignUrl).error(errorEvent);
    flush();
  }));

  it('submitClaimRequest should work on success', fakeAsync(() => {
    service.submitClaimRequest(expectedEcmrId);
    controller.expectOne(expectedClaimUrl).flush({});
    flush();
  }));

  it('submitClaimRequest should not crash on error', fakeAsync(() => {
    service.submitClaimRequest(expectedEcmrId);
    controller.expectOne(expectedClaimUrl).error(errorEvent);
    flush();
  }));
});
