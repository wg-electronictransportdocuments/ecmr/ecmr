/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { SetEcmrUserRequest } from '../../models/dtos/requests/set-ecmr-user-request';
import { SetEcmrUsersRequest } from '../../models/dtos/requests/set-ecmr-users-request';
import { SnackbarService } from '../state/snackbar.service';
import { HttpClient } from '@angular/common/http';
import { ShareEcmrService } from '../state/share-ecmr.service';
import { LoadingStateService } from '../state/loading-state.service';
import { Router } from '@angular/router';
import { environment as env } from '../../../../environments/environment';
import { endpoints } from './endpoints';
import { catchError, Observable, of, switchMap } from 'rxjs';
import { tap } from 'rxjs/operators';
import { GuestCarrierRequest } from '../../models/dtos/requests/guest-carrier-request';

@Injectable({
  providedIn: 'root',
})
export class EcmrUserService {
  shareContactUrlFragment = '/' + endpoints.frontend.shareContact;
  showEcmrUrlFragment = '/' + endpoints.frontend.showEcmr;

  constructor(
    public snackbarService: SnackbarService,
    public httpClient: HttpClient,
    public shareEcmrService: ShareEcmrService,
    public loadingStateService: LoadingStateService,
    public router: Router
  ) {}

  public submitClaimRequest(ecmrId: string): void {
    this.loadingStateService.setLoadingState$(true);
    const reqBody: SetEcmrUserRequest = { ecmrId };
    const reqUrl =
      env.backendUrl +
      endpoints.backend.putCurrentuserAtEcmrById.replace(
        '{id}',
        reqBody.ecmrId
      );
    this.httpClient.put(reqUrl, reqBody, { responseType: 'text' }).subscribe(
      () => {
        this.loadingStateService.setLoadingState$(false);
        this.router.navigate([this.showEcmrUrlFragment + ecmrId]);
        this.snackbarService.pushNewStatus(
          `eCMR with ID ${ecmrId} has been successfully assigned to you.`,
          10000
        );
      },
      () => {
        this.loadingStateService.setLoadingState$(false);
        this.snackbarService.pushNewStatus(
          `eCMR with ID ${ecmrId} couldn't be found or wasn't permitted to be claimed by you.`,
          15000
        );
      }
    );
  }

  // Old version, still used
  public submitAssignRequest(ecmrId: string, userId: string): void {
    this.loadingStateService.setLoadingState$(true);
    const reqBody: SetEcmrUserRequest = {
      ecmrId,
      userId,
    };
    const reqUrl =
      env.backendUrl +
      endpoints.backend.putUserAtEcmrByIds
        .replace('{ecmrId}', reqBody.ecmrId)
        .replace('{userId}', reqBody.userId);
    this.httpClient.put(reqUrl, reqBody, { responseType: 'text' }).subscribe(
      () => {
        this.loadingStateService.setLoadingState$(false);
        this.router.navigate([this.shareContactUrlFragment]);
        this.snackbarService.pushNewStatus(
          `eCMR with ID ${ecmrId} has been successfully assigned to user with ID ${userId}`,
          10000
        );
      },
      () => {
        this.loadingStateService.setLoadingState$(false);
        this.snackbarService.pushNewStatus(
          `eCMR with ID ${ecmrId} couldn't be found or wasn't permitted to be assigned to User ${userId} by you.`,
          15000
        );
      }
    );
  }

  // New version
  public assignEcmrToUser(ecmrId: string, userId: string): Observable<unknown> {
    this.loadingStateService.setLoadingState$(true);
    const reqBody: SetEcmrUserRequest = {
      ecmrId,
      userId,
    };
    const reqUrl =
      env.backendUrl +
      endpoints.backend.putUserAtEcmrByIds
        .replace('{ecmrId}', reqBody.ecmrId)
        .replace('{userId}', reqBody.userId);
    return this.httpClient.put(reqUrl, reqBody, { responseType: 'text' }).pipe(
      tap(
        () => {
          this.loadingStateService.setLoadingState$(false);
        },
        () => {
          this.loadingStateService.setLoadingState$(false);
          this.snackbarService.pushNewStatus(
            `eCMR with ID ${ecmrId} couldn't be found or wasn't permitted to be assigned to User ${userId} by you.`,
            15000
          );
        }
      )
    );
  }

  public requestEcmrGuestCarrier(ecmrId: string, model: GuestCarrierRequest): Observable<void> {
    const reqUrl =
        env.backendUrl +
        endpoints.backend.postEcmrGuestCarrier
            .replace('{ecmrId}', ecmrId);
    this.loadingStateService.setLoadingState$(true);
    return this.httpClient.post<void>(reqUrl, model).pipe(
        tap({
            next: () => {
                this.loadingStateService.setLoadingState$(false);
            },
            error: () => {
                this.loadingStateService.setLoadingState$(false);
                this.snackbarService.pushNewStatus(
                    `An error occured`,
                    15000
                );
            }
        })
    );
  }

    public checkGuestCarrierTan(ecmrId: string, tan: string): Observable<boolean> {
        const reqUrl =
            env.backendUrl +
            endpoints.backend.postEcmrGuestCarrierCheckTan
                .replace('{ecmrId}', ecmrId)
                .replace('{tan}', tan);
        this.loadingStateService.setLoadingState$(true);
        return this.httpClient.post<boolean>(reqUrl, null).pipe(
            tap({
                next: () => {
                    this.loadingStateService.setLoadingState$(false);
                },
                error: () => {
                    this.loadingStateService.setLoadingState$(false);
                    this.snackbarService.pushNewStatus(
                        `An error occured`,
                        15000
                    );
                }
            })
        );
    }

  // New version to assign to multiple contacs
  public assignEcmrToUsers(ecmrId: string, userIds: string[]): Observable<unknown> {
    this.loadingStateService.setLoadingState$(true);
    const users = [];
    for (const userId of userIds) {
      users.push({ userId});
    }
    const reqBody: SetEcmrUsersRequest = {
      ecmrId,
      users,
    };
    const reqUrl =
      env.backendUrl +
      endpoints.backend.putUsersAtEcmrByIds
        .replace('{ecmrId}', reqBody.ecmrId);
    return this.httpClient.put(reqUrl, reqBody, { responseType: 'text' }).pipe(
      tap(
        () => {
          this.loadingStateService.setLoadingState$(false);
        },
        () => {
          this.loadingStateService.setLoadingState$(false);
          this.snackbarService.pushNewStatus(
            `eCMR with ID ${ecmrId} couldn't be found or wasn't permitted to be assigned to the seleted users by you.`,
            15000
          );
        }
      )
    );
  }
}
