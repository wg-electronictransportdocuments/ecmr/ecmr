/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { fakeAsync, flush, TestBed } from '@angular/core/testing';

import { EcmrService } from './ecmr.service';
import { AuthService } from '../../../modules/user-management/services/auth.service';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { environment as env } from '../../../../environments/environment';
import { mockEcmr, mockSetEcmrDto } from '../../mockups/mock-objects';
import { SnackbarService } from '../state/snackbar.service';
import { RouterTestingModule } from '@angular/router/testing';
import { OverviewPageComponent } from '../../../pages/overview-page/overview-page.component';
import { ErrorPageComponent } from '../../../pages/error-page/error-page.component';
import { endpoints } from './endpoints';
import { EncryptionService } from '../encryption/encryption.service';
import {
  authServiceStub,
  encryptionServiceStub,
} from '../../mockups/service-stubs';

describe('EcmrService', () => {
  let service: EcmrService;
  let controller: HttpTestingController;

  const mockDoc = mockEcmr(9);

  const firstExpectedId = '0';
  const secondExpectedId = '123';

  const firstExpectedGetUrl =
    env.backendUrl +
    endpoints.backend.getEcmrById.replace('{id}', firstExpectedId);
  const secondExpectedGetUrl =
    env.backendUrl +
    endpoints.backend.getEcmrById.replace('{id}', secondExpectedId);

  const expectedPostUrl = env.backendUrl + endpoints.backend.postEcmr;
  const expectedPutUrl =
    env.backendUrl +
    endpoints.backend.putEcmrById.replace('{id}', secondExpectedId) + '/true';
  const errorEvent = new ErrorEvent('some error');

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: SnackbarService, useValue: { pushNewStatus: () => ({}) } },
        {
          provide: AuthService,
          useValue: authServiceStub(),
        },
        {
          provide: EncryptionService,
          useValue: encryptionServiceStub({}),
        },
      ],
      imports: [
        HttpClientTestingModule,
        RouterTestingModule.withRoutes([
          { path: 'ecmr-overview', component: OverviewPageComponent },
          { path: 'error', component: ErrorPageComponent },
        ]),
      ],
    });
    service = TestBed.inject(EcmrService);
    controller = TestBed.inject(HttpTestingController);
  });

  it('updateEcmr$ should update the current Document stream', fakeAsync(() => {
    service.updateEcmr$(secondExpectedId);
    controller.expectOne(secondExpectedGetUrl).flush(mockDoc);
    service.ecmr$.subscribe(r => {
      expect(r).toBe(mockDoc);
    });
    flush();
  }));

  it('updateEcmr$ should post an Errormessage to the snackbar on http-error', done => {
    const snackSpy = spyOn(service.snackbarService, 'pushNewStatus');
    service.updateEcmr$(firstExpectedId);
    controller.expectOne(firstExpectedGetUrl).error(errorEvent);
    expect(snackSpy).toHaveBeenCalled();
    done();
  });

  it('updateEcmr$ should post an Errormessage to the snackbar on http-error with status != 0', done => {
    const expectedFunction = spyOn(service.snackbarService, 'pushNewStatus');
    service.updateEcmr$(firstExpectedId);
    controller
      .expectOne(firstExpectedGetUrl)
      .error(errorEvent, { status: 400 });
    expect(expectedFunction).toHaveBeenCalled();
    done();
  });

  it(
    'sendEcmrToServer should push a POST message to the correct url without a server error' +
      'and return its submission-status when the eCMR ID is 0 (new eCMR)',
    done => {
      const snackSpy = spyOn(service.snackbarService, 'pushNewStatus');
      const mockDto = mockSetEcmrDto(parseInt(firstExpectedId, 10));
      service.sendEcmrToServer(mockDto,true);
      controller
        .match({ url: expectedPostUrl, method: 'POST' })
        .forEach(req => req.flush({}));
      expect(snackSpy).toHaveBeenCalled();
      done();
    }
  );

  it(
    'sendEcmrToServer should push a PUT message to the correct url without a server error' +
      'and return its submission-status when the eCMR ID is NOT 0 (new eCMR)',
    done => {
      const snackSpy = spyOn(service.snackbarService, 'pushNewStatus');
      service.sendEcmrToServer(mockSetEcmrDto(parseInt(secondExpectedId, 10)),true);
      controller
        .match({ url: expectedPutUrl, method: 'PUT' })
        .forEach(req => req.flush({}));
      expect(snackSpy).toHaveBeenCalled();
      done();
    }
  );
});
