/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject, tap } from 'rxjs';
import { Ecmr } from '../../models/entities/ecmr';
import { HttpClient } from '@angular/common/http';
import { environment as env } from '../../../../environments/environment';
import { SnackbarService } from '../state/snackbar.service';
import { SetEcmrDto } from '../../models/dtos/requests/set-ecmr-dto';
import { Router } from '@angular/router';
import { LoadingStateService } from '../state/loading-state.service';
import { endpoints } from './endpoints';
import { EncryptionService } from '../encryption/encryption.service';
import { SetTemplateDto } from '../../models/dtos/requests/set-template.dto';

@Injectable({
  providedIn: 'root',
})
export class EcmrService {
  private ecmr: BehaviorSubject<Ecmr> = new BehaviorSubject<Ecmr>(undefined);
  private historicalEcmr: Subject<Ecmr> = new Subject<Ecmr>();
  private nextHistoricalEcmr: Subject<Ecmr> = new Subject<Ecmr>();
  private ecmrOverviewUrlFragment = '/' + endpoints.frontend.ecmrOverview;
  private templateOverviewUrlFragment = '/' + endpoints.frontend.ecmrTemplatesOverview;
  private tryCounter = 0;

  constructor(
    public httpClient: HttpClient,
    public snackbarService: SnackbarService,
    public router: Router,
    public loadingStateService: LoadingStateService,
    public encryptionService: EncryptionService
  ) {}

  public get ecmr$(): Observable<Ecmr> {
    return this.ecmr.asObservable();
  }

  public get historicalEcmr$(): Observable<Ecmr> {
    return this.historicalEcmr.asObservable();
  }

  public updateEcmr$(ecmrId: string, tan?: string | null): void {
    this.loadingStateService.setLoadingState$(true);
    const url = tan == null ?
        env.backendUrl + endpoints.backend.getEcmrById.replace('{id}', ecmrId) :
        env.backendUrl + endpoints.backend.getEcmrByIdForCarrier.replace('{id}', ecmrId).replace('{tan}', tan);
    this.httpClient
      .get<Ecmr>(url)
      .subscribe({
        next: response => {
          this.ecmr.next(response);
          this.loadingStateService.setLoadingState$(false);
        },
        error: error => {
          if (error.status === 401) {
            this.ecmr.error(error);
            this.snackbarService.pushNewStatus(
              `Authentication failed. Please sign in once again.
          Error-Code: ${error.error.text}`,
              10000
            );
          } else if (error.status !== 0) {
            this.ecmr.error(error);
            this.snackbarService.pushNewStatus(
              `Error-Message: ${error.error.response}. If you're sure this wasn't a mistake try again.`,
              10000
            );
            this.router.navigate(['error']);
          } else {
            this.ecmr.error(error);
            this.snackbarService.pushNewStatus(
              `There seems to be a network connection issue or the eCMR Server is offline.
              Please check your connection and/or try again later.`,
              10000
            );
          }
          this.loadingStateService.setLoadingState$(false);
        },
      });
  }

  public unsetEcmr(): void {
    this.ecmr.next(undefined);
  }

  public updateHistoricalEcmr$(ecmrId: string, version: string): Observable<Ecmr> {
    this.loadingStateService.setLoadingState$(true);

    return this.httpClient
      .get<Ecmr>(
        env.backendUrl +
          endpoints.backend.getEcmrById.replace('{id}', ecmrId) +
          '?version=' +
          version
      )
      .pipe( tap({
        next: response => {
          this.historicalEcmr.next(response);
        },
        error: error => {
          if (error.status !== 0) {
            this.snackbarService.pushNewStatus(
              `Cannot retrieve eCMR ${ecmrId} from server. Error-Code: ${error.error.text}`,
              10000
            );
          } else {
            this.snackbarService.pushNewStatus(
              `There seems to be a network connection issue or the eCMR Server is offline.
              Please check your connection and/or try again later.`,
              10000
            );
          }
          this.loadingStateService.setLoadingState$(false);
        },
        complete: () => {
          this.loadingStateService.setLoadingState$(false);
        },
      }));
  }

  public updateNextHistoricalEcmr$(ecmrId: string, version: string): Observable<Ecmr> {
    this.loadingStateService.setLoadingState$(true);

    return this.httpClient
        .get<Ecmr>(
            env.backendUrl +
            endpoints.backend.getEcmrById.replace('{id}', ecmrId) +
            '?version=' +
            version
        )
        .pipe( tap({
          next: response => {
            this.nextHistoricalEcmr.next(response);
          },
          error: error => {
            if (error.status !== 0) {
              this.snackbarService.pushNewStatus(
                  `Cannot retrieve eCMR ${ecmrId} from server. Error-Code: ${error.error.text}`,
                  10000
              );
            } else {
              this.snackbarService.pushNewStatus(
                  `There seems to be a network connection issue or the eCMR Server is offline.
              Please check your connection and/or try again later.`,
                  10000
              );
            }
            this.loadingStateService.setLoadingState$(false);
          },
          complete: () => {
            this.loadingStateService.setLoadingState$(false);
          },
        }));
  }

  isEcmr(obj: SetEcmrDto | SetTemplateDto): obj is SetTemplateDto {
    return (obj as SetTemplateDto).templateName === undefined;
  }

  async sendEcmrToServer(ecmrDto: SetEcmrDto | SetTemplateDto, isEcmr: boolean): Promise<void> {
    this.loadingStateService.setLoadingState$(true);
    let request: Observable<string>;
    const isEcmrDto = this.isEcmr(ecmrDto);

    return new Promise<void>((resolve, reject) => {
      // Generate request
      if (ecmrDto.ecmrId === '0') {
        if(isEcmrDto) {
          request = this.httpClient.post(
              env.backendUrl + endpoints.backend.postEcmr,
              ecmrDto,
              { responseType: 'text' }
          );
        } else {
          request = this.httpClient.post(
              env.backendUrl + endpoints.backend.postTemplate,
              ecmrDto,
              { responseType: 'text' }
          );
        }
      } else {
        request = this.httpClient.put(
          env.backendUrl +
            endpoints.backend.putEcmrById.replace('{id}', ecmrDto.ecmrId) + '/' + isEcmr,
          ecmrDto,
          { responseType: 'text' }
        );
      }

      // Execute request
      request.subscribe({
        next: () => {
          this.snackbarService.pushNewStatus(
              isEcmrDto ? 'eCMR was successfully sent to server.' : 'Template was successfully sent to server.',
            4000
          );
          if(isEcmrDto) {
            if(isEcmr) {
              this.router.navigate([this.ecmrOverviewUrlFragment]);
            } else {
              this.router.navigate([this.templateOverviewUrlFragment]);
            }
          } else {
            this.router.navigate([this.templateOverviewUrlFragment]);
          }

          this.loadingStateService.setLoadingState$(false);
          this.tryCounter = 0;
          resolve();
        },
        error: async error => {
          if ((error.status = 401)) {
            let errorCode: string;
            try {
              errorCode = JSON.parse(error.error).response;
            } catch (e) {
              errorCode = 'Status 401 - ' + e;
            }
            if (errorCode === 'Corrupted Signature' && this.tryCounter <= 1) {
              this.encryptionService.unsetKeys();
              await this.encryptionService.generateKeys().then(() => {
                this.loadingStateService.setLoadingState$(false);
                reject('retry');
              });
            } else {
              this.loadingStateService.setLoadingState$(false);
              this.snackbarService.pushNewStatus(
                `Authentication failed. Please sign in once again. Error-Code: ${errorCode}`,
                10000
              );
              reject('signature');
            }
          } else if (error.status !== 0) {
            let errorCode: string;
            try {
              errorCode = JSON.parse(error.error).response;
            } catch (e) {
              errorCode = `Status ${error.status}: Corrupt response - ${e}`;
            }
            this.loadingStateService.setLoadingState$(false);
            this.snackbarService.pushNewStatus(
              isEcmrDto ? `Could not post eCMR to server. Error-Code: ${errorCode}` : `Could not post Template to server. Error-Code: ${errorCode}`,
              10000
            );
            reject();
          } else {
            this.loadingStateService.setLoadingState$(false);
            this.snackbarService.pushNewStatus(
              `There seems to be a network connection issue or the eCMR Server is offline.
            Please check your connection and/or try again later.`,
              10000
            );
            reject();
          }
          this.tryCounter += 1;
        },
      });
    });
  }
  exportAsPDF(ecmrId: string): Observable<Blob> {
    return this.httpClient.get(env.backendUrl + endpoints.backend.getEcmrPdf.replace('{id}', ecmrId), {responseType: 'blob'});
  }
}
