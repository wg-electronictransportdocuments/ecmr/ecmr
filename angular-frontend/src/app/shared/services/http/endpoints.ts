/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

/** Base Paths */
const bps = {
  userBp: 'api/v1/usm',
  ecmrBp: 'api/v1',
  tokenBp: 'api/v1/auth',
};

export const endpoints = {
  backend: {
    getUserById: bps.userBp + '/users/{id}',
    getUsersContacts: bps.userBp + '/users/contacts',
    getUsersFields: bps.userBp + '/users/fields',
    postUserPubkey: bps.userBp + '/connection/key',

    getEcmrs: bps.ecmrBp + '/ecmrs',
    getTemplates: bps.ecmrBp + '/templates',
    postEcmr: bps.ecmrBp + '/ecmrs',
    postTemplate: bps.ecmrBp + '/templates',
    getEcmrById: bps.ecmrBp + '/ecmrs/id/{id}',
    getEcmrByIdForCarrier: bps.ecmrBp + '/ecmrs-carrier/id/{id}/tan/{tan}',
    getTemplateById: bps.ecmrBp + '/templates/id/{id}',
    putEcmrById: bps.ecmrBp + '/ecmrs/id/{id}',
    deleteEcmrById: bps.ecmrBp + '/ecmrs/id/{id}',
    deleteTemplateById: bps.ecmrBp + '/templates/id/{id}',
    getEcmrHistoryById: bps.ecmrBp + '/ecmrs/id/{id}/history',
    getEcmrPdf: bps.ecmrBp + '/ecmrs/{id}/pdf',
    putUserAtEcmrByIds:
      bps.ecmrBp + '/ecmrs/id/{ecmrId}/userid/{userId}/assign',
    postEcmrGuestCarrier:
      bps.ecmrBp + '/ecmrs-carrier/id/{ecmrId}',
    postEcmrGuestCarrierCheckTan:
        bps.ecmrBp + "/ecmrs-carrier/id/{ecmrId}/check-tan/{tan}",
    putUsersAtEcmrByIds:
        bps.ecmrBp + '/ecmrs/id/{ecmrId}/assignMulti',
    putCurrentuserAtEcmrById: bps.ecmrBp + '/ecmrs/id/{id}/claim',
    getFields: bps.ecmrBp + '/ecmrs/fields',
    getFieldById: bps.ecmrBp + '/ecmrs/fields/{id}',
    postAutogenerateEcmr: bps.ecmrBp + '/external',

    getExportToken: bps.ecmrBp + '/ecmrs/id/{id}/export/token',
    getExportEcmr: bps.ecmrBp + '/ecmrs/id/{id}/export',
    postImportEcmr: bps.ecmrBp + '/ecmrs/import/',

    deleteToken: bps.tokenBp + '/session',
    putToken: bps.tokenBp + '/session',
    postToken: bps.tokenBp + '/session',
  },
  frontend: {
    ecmrOverview: 'ecmr-overview',
    ecmrTemplatesOverview: 'ecmr-templates-overview',
    ecmrHistory: 'ecmr-history/',
    editEcmr: 'edit-ecmr/',
    copyEcmr: 'copy-ecmr/',
    claimEcmr: 'claim-ecmr/',
    showEcmr: 'show-ecmr/',
    exportEcmr: 'export-ecmr',
    shareQr: 'share-qr',
    shareEmail: 'share-email',
    shareOverview: 'share-overview',
    shareContact: 'share-contact',
    shareEcmr: 'share-ecmr',
    login: 'login',
    settings: 'settings',
    privacy: 'privacy',
    imprintLegal: 'imprint-legal',
    archive: 'archive',
    guestCarrier: 'guest-carrier/',
    guestCarrierTan: 'guest-carrier-tan/'
  },
};
