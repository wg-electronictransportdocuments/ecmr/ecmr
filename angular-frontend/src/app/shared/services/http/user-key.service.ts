/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment as env } from '../../../../environments/environment';
import { endpoints } from './endpoints';
import { tap } from 'rxjs/operators';
import { LoadingStateService } from '../state/loading-state.service';
import { HttpClient } from '@angular/common/http';
import { SnackbarService } from '../state/snackbar.service';

@Injectable({
  providedIn: 'root',
})
export class UserKeyService {
  constructor(
    public loadingStateService: LoadingStateService,
    public httpClient: HttpClient,
    public snackbarService: SnackbarService
  ) {}

  public postUserPubkey(pubKey: string): Observable<any> {
    this.loadingStateService.setLoadingState$(true);
    return this.httpClient
      .post<any>(env.backendUrl + endpoints.backend.postUserPubkey, { pubKey })
      .pipe(
        tap({
          next: () => {
            this.loadingStateService.setLoadingState$(false);
          },
          error: error => {
            this.loadingStateService.setLoadingState$(false);
            this.snackbarService.pushNewStatus(
              `Posting public key to server was rejected. Error-Message: ${error.error.response}. Please log in once again.`,
              10000
            );
          },
        })
      );
  }
}
