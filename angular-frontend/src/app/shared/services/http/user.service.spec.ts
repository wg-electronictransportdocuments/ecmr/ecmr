/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { inject, TestBed } from '@angular/core/testing';
import { UserService } from './user.service';
import { SnackbarService } from '../state/snackbar.service';
import { LoadingStateService } from '../state/loading-state.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import {
  loadingServiceStub,
  snackbarServiceStub,
} from '../../mockups/service-stubs';

describe('UserService', () => {
  let service: UserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        { provide: SnackbarService, useValue: snackbarServiceStub() },
        { provide: LoadingStateService, useValue: loadingServiceStub() },
      ],
    });
    service = TestBed.inject(UserService);
  });

  it('should be created', inject([UserService], () => {
    expect(service).toBeTruthy();
  }));
});
