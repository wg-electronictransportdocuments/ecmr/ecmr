/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import {
  GetUserDto,
  UserCompanyDto,
} from '../../models/dtos/requests/get-user-dto';
import { SnackbarService } from '../state/snackbar.service';
import { Contact } from '../../models/entities/contact';
import { HttpClient } from '@angular/common/http';
import { Observable, ReplaySubject, shareReplay, Subject } from 'rxjs';
import { LoadingStateService } from '../state/loading-state.service';
import { environment as env } from '../../../../environments/environment';
import { endpoints } from './endpoints';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private userCompanySubject: Subject<UserCompanyDto> =
    new Subject<UserCompanyDto>();
  private userContacts: Subject<Contact[]> = new Subject<Contact[]>();
  private userFields: Subject<{
    mandatoryFields: string[];
    optionalFields: string[];
  }> = new Subject<{
    mandatoryFields: string[];
    optionalFields: string[];
  }>();
  private userCompanyValue: UserCompanyDto;

  constructor(
    private snackbarService: SnackbarService,
    private httpClient: HttpClient,
    private loadingStateService: LoadingStateService
  ) {}

  public get userContacts$(): Observable<Contact[]> {
    return this.userContacts.asObservable();
  }

  public get userCompany$(): Observable<UserCompanyDto> {
    return this.userCompanySubject.asObservable();
  }

  public get userCompany(): UserCompanyDto {
    return this.userCompanyValue;
  }

  public userFields$ = this.userFields.asObservable().pipe(shareReplay(1));

  // HTTP Accessors and Methods
  public updateUserContacts$(): void {
    this.loadingStateService.setLoadingState$(true);
    this.httpClient
      .get<Contact[]>(env.backendUrl + endpoints.backend.getUsersContacts)
      .subscribe(
        response => {
          this.userContacts.next(response);
          this.loadingStateService.setLoadingState$(false);
        },
        () => {
          this.snackbarService.pushNewStatus('An Error occurred.');
          this.loadingStateService.setLoadingState$(false);
        }
      );
  }

  public updateUserFields$(): void {
    this.loadingStateService.setLoadingState$(true);
    this.httpClient
      .get<{
        mandatoryFields: string[];
        optionalFields: string[];
      }>(env.backendUrl + endpoints.backend.getUsersFields)
      .subscribe(
        response => {
          this.userFields.next(response);
          this.loadingStateService.setLoadingState$(false);
        },
        error => {
          this.snackbarService.pushNewStatus('An Error occurred.');
          this.userFields.error(error);
          this.loadingStateService.setLoadingState$(false);
        }
      );
  }

  public updateUserFieldsForCarrier(): void {
    this.userFields.next({
      mandatoryFields: [],
      optionalFields: [
        "59",
        "55",
        "71",
        "33",
        "39",
        "30",
        "23",
        "62",
        "28",
        "10",
        "50",
        "63",
        "49",
        "35",
        "26",
        "41",
        "4",
        "51",
        "5",
        "68",
        "13",
        "18",
        "37",
        "27",
        "69",
        "72",
        "65",
        "16",
        "19",
        "14",
        "60",
        "15",
        "66",
        "24",
        "75",
        "21",
        "34",
        "32",
        "1",
        "64",
        "73",
        "3",
        "57",
        "67",
        "22",
        "25",
        "7",
        "70",
        "9",
        "31",
        "43",
        "2",
        "29",
        "42",
        "53",
        "6",
        "11",
        "58",
        "40",
        "56",
        "38",
        "8",
        "17",
        "74",
        "45",
        "54",
        "36",
        "61",
        "52"
      ]
    });
  }

  public updateUserCompany$(uid: string): void {
    const uri =
      env.backendUrl + endpoints.backend.getUserById.replace('{id}', uid);
    this.httpClient.get<GetUserDto>(uri).subscribe(
      response => {
        this.userCompanySubject.next(response.userCompany);
        this.userCompanyValue = response.userCompany;
      },
      error => {
        this.userCompanySubject.error(error);
      }
    );
  }
}
