/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { CookieConsentService } from './cookie-consent.service';

// TODO: More specific testing needed
describe('CookieConsentService', () => {
  let service: CookieConsentService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CookieConsentService);
  });

  it('should work n functions', () => {
    service.setCookieConsent(true);
    service.setCookieConsent(false);
    service.getCookieConsent();
    localStorage.clear();
    expect(service).toBeTruthy();
  });
});
