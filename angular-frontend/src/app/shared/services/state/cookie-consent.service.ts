/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, ReplaySubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class CookieConsentService {
  private storageKey = 'cookie-consent';
  private cookieConsent$: ReplaySubject<boolean> = new ReplaySubject<boolean>();

  constructor() {
    this.loadState();
  }

  getCookieConsent(): Observable<boolean> {
    return this.cookieConsent$.asObservable();
  }

  setCookieConsent(value: boolean): void {
    this.cookieConsent$.next(value);
    if (value === true) {
      localStorage.setItem(this.storageKey, 'true');
    }
  }

  private loadState(): void {
    const local = localStorage.getItem(this.storageKey);
    if (local !== null) {
      this.cookieConsent$.next(local === 'true');
    } else {
      this.cookieConsent$.next(false);
    }
  }
}
