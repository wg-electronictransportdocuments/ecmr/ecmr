/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class LoadingStateService {
  private loadingStateLevel: number;
  private loadingState$: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(false);

  constructor() {
    this.loadingStateLevel = 0;
    this.loadingState$.next(false);
  }

  public getLoadingState$(): Observable<boolean> {
    return this.loadingState$.asObservable();
  }

  public setLoadingState$(state: boolean): void {
    if (state) {
      if (this.loadingStateLevel === 0) {
        this.loadingStateLevel += 1;
        this.loadingState$.next(state);
      } else {
        this.loadingStateLevel += 1;
      }
    } else {
      this.loadingStateLevel -= 1;
      if (this.loadingStateLevel === 0) {
        this.loadingState$.next(state);
      } else if (this.loadingStateLevel < 0) {
        this.loadingState$.error(
          'Corrupt Loading State detected.'
        );
      }
    }
  }
}
