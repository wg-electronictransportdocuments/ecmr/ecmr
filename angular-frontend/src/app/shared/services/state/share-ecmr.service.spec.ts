/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { ShareEcmrService } from './share-ecmr.service';
import { mockExportObject } from '../../mockups/mock-objects';
import {SnackbarService} from './snackbar.service';
import {snackbarServiceStub} from '../../mockups/service-stubs';

// TODO: More specific testing needed, also rework testing strategy
describe('ShareService', () => {
  let service: ShareEcmrService;



  beforeEach(async () => {
    await TestBed.configureTestingModule({
      providers: [
        { provide: SnackbarService, useValue: snackbarServiceStub() }
      ],
    }).compileComponents();
    service = TestBed.inject(ShareEcmrService);
  });

  it('should be created and have working methods', () => {
    service.getCurrentContact();
    service.getExportObjectToShare();
    service.setExportObjectToShare(mockExportObject('123'));
    service.setShareFields('mail');
    service.setShareFields('mail', 'mailaddress@web.de');
    service.unsetShareState();
    service.unsetExportObjectToShare();
    localStorage.setItem('shareState', '{a:b, c:d, e:1}');
    expect(service).toBeTruthy();
  });
});
