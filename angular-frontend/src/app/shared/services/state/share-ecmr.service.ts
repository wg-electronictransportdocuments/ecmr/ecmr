/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { SnackbarService } from './snackbar.service';
import { ShareType } from '../../models/types/share-type';
import { ShareState } from '../../models/states/share-state';
import { Contact } from '../../models/entities/contact';
import { ExportObject } from '../../models/entities/export-object';

@Injectable({
  providedIn: 'root',
})
export class ShareEcmrService {
  private readonly stateKey = 'shareState';
  private readonly exportObjectKey = 'exportObject';
  private shareState: ShareState = {
    currentShareType: undefined,
    currentContact: undefined,
    currentEmail: undefined,
  };
  private exportObjectToShare: ExportObject;

  constructor(private snackbarService: SnackbarService) {
    this.readStateFromLocalStorage();
  }

  getExportObjectToShare(): ExportObject {
    return this.exportObjectToShare;
  }

  setExportObjectToShare(obj: ExportObject) {
    this.exportObjectToShare = obj;
    this.saveStateToLocalStorage();
  }

  getCurrentContact(): Contact {
    return this.shareState.currentContact;
  }

  setShareFields(type: ShareType, mail?: string, contact?: Contact): void {
    this.shareState.currentShareType = type;
    this.shareState.currentContact = undefined;
    this.shareState.currentEmail = undefined;
    if (type === 'mail') {
      this.shareState.currentEmail = mail;
    } else if (type === 'contact') {
      this.shareState.currentContact = contact;
    }
    this.saveStateToLocalStorage();
  }

  unsetShareState() {
    this.shareState = {
      currentContact: undefined,
      currentEmail: undefined,
      currentShareType: undefined,
    };
    localStorage.removeItem(this.stateKey);
  }

  unsetExportObjectToShare() {
    this.exportObjectToShare = undefined;
    localStorage.removeItem(this.exportObjectKey);
  }

  private readStateFromLocalStorage() {
    if (localStorage.getItem(this.stateKey) !== null) {
      try {
        const state = JSON.parse(localStorage.getItem(this.stateKey));
        this.shareState = state as ShareState;
      } catch (err) {
        this.snackbarService.pushNewStatus(
          'Saved state in local storage is corrupted. State has been reset.'
        );
        this.unsetShareState();
      }
    }
    if (localStorage.getItem(this.exportObjectKey) !== null) {
      try {
        const obj = JSON.parse(localStorage.getItem(this.exportObjectKey));
        this.exportObjectToShare = obj as ExportObject;
      } catch (err) {
        this.snackbarService.pushNewStatus(
          'Saved state in local storage is corrupted. State has been reset.'
        );
        this.unsetShareState();
      }
    }
  }

  private saveStateToLocalStorage() {
    localStorage.setItem(this.stateKey, JSON.stringify(this.shareState));
    localStorage.setItem(
      this.exportObjectKey,
      JSON.stringify(this.exportObjectToShare)
    );
  }
}
