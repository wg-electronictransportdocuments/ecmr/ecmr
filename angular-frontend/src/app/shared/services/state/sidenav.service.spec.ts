/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';
import { SidenavService } from './sidenav.service';

describe('SidenavService', () => {
  let service: SidenavService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SidenavService);
  });

  it('should work n functions', () => {
    service.getSidenavState();
    // eslint-disable-next-line @typescript-eslint/dot-notation
    service['cookieConsent'] = true;
    service.toggleSidenavState();
    // eslint-disable-next-line @typescript-eslint/dot-notation
    service['cookieConsent'] = false;
    service.toggleSidenavState();
    expect(service).toBeTruthy();
  });
});
