/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CookieConsentService } from './cookie-consent.service';
import { take, takeWhile } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SidenavService {
  private storageKey = 'sidenav-state';

  private sidenavState$: BehaviorSubject<boolean> =
    new BehaviorSubject<boolean>(true);
  private cookieConsent: boolean = undefined;

  constructor(private cookieConsentService: CookieConsentService) {
    cookieConsentService
      .getCookieConsent()
      .pipe(takeWhile((v) => v !== undefined))
      .pipe(take(1))
      .subscribe((r) => {
        this.cookieConsent = r;
        this.loadState();
      });
  }

  getSidenavState(): Observable<boolean> {
    return this.sidenavState$.asObservable();
  }

  toggleSidenavState(): void {
    const next = !this.sidenavState$.getValue();
    this.sidenavState$.next(next);
    if (this.cookieConsent) {
      localStorage.setItem(this.storageKey, String(next));
    }
  }

  // Initialize sidenav state from local storage
  private loadState(): void {
    const local = localStorage.getItem(this.storageKey);
    if (local !== null) {
      this.sidenavState$.next(local === 'true');
    }
  }
}
