/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { TestBed } from '@angular/core/testing';

import { SignatureChoiceService } from './signature-choice.service';

describe('SignatureChoiceService', () => {
  let service: SignatureChoiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SignatureChoiceService);
  });

  it('should be created and not crash when methods are used', () => {
    expect(service).toBeTruthy();
    service.setSignatureChoice(1);
    expect(service.getSignatureChoice()).toBe(1);
    service.unsetSignatureChoice();
    expect(service.getSignatureChoice()).toBe(0);
  });
});
