/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class SignatureChoiceService {
  signatureChoice: 0 | 1 = 0;

  private readonly storageKey = 'signatureChoice';

  constructor() {
    this.signatureChoice = this.readStateFromLocalStorage();
  }

  getSignatureChoice(): 0 | 1 {
    return this.signatureChoice;
  }

  setSignatureChoice(choice: 0 | 1): void {
    this.signatureChoice = choice;
    this.saveStateToLocalStorage(choice);
  }

  unsetSignatureChoice() {
    this.signatureChoice = 0;
    localStorage.setItem(this.storageKey, JSON.stringify(0));
  }

  private readStateFromLocalStorage(): 0 | 1 {
    if (localStorage.getItem(this.storageKey) !== null) {
      try {
        return JSON.parse(localStorage.getItem(this.storageKey));
      } catch (err) {
        this.setSignatureChoice(0);
      }
    } else {
      this.setSignatureChoice(0);
      return 0;
    }
  }

  private saveStateToLocalStorage(choice: 0 | 1) {
    localStorage.setItem(this.storageKey, JSON.stringify(choice));
  }
}
