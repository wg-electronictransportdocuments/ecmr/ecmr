/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class SnackbarService {
  private statusMessage$: Subject<string> = new Subject<string>();
  private nextDuration: number;

  getStatusMessage$(): Observable<string> {
    return this.statusMessage$.asObservable();
  }

  pushNewStatus(message: string, duration = 4000) {
    this.nextDuration = duration;
    this.statusMessage$.next(message);
  }

  getNextDuration() {
    return this.nextDuration;
  }
}
