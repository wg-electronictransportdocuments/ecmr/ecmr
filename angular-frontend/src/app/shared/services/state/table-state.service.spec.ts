/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';
import {TableStateService} from './table-state.service';
import {SortState} from '../../models/states/sort-state';
import {SnackbarService} from './snackbar.service';
import {of} from 'rxjs';
import {PageState} from '../../models/states/page-state';

describe('TableStateService', () => {
  let service: TableStateService;

  const snackbarServiceMock = (msg = 'some message', dur = 2000) => ({
    getStatusMessage$: () => of(msg),
    getNextDuration: () => dur,
    pushNewStatus: () => {},
  });

  describe('with no relevant localstorage', () => {
    beforeEach(async () => {
      await TestBed.configureTestingModule({
        providers: [
          { provide: SnackbarService, useValue: snackbarServiceMock }
        ],
      }).compileComponents();
      service = TestBed.inject(TableStateService);
    });

    it('should have working functions for the sort state', () => {
      service.getSortState();
      service.setSortState({
        sortName: 'ecmrOverview',
        direction: 'asc',
        active: 'ecmrid',
      });
      expect(service).toBeTruthy();
    });

    it('should have working functions for the pagesize state', () => {
      service.getPageState();
      service.setPageState({
        pageName: 'ecmrHistoryOverview',
        pageIndex: 0,
        pageSize: 10,
      });
      service.getPageState();
      service.setPageState({
        pageName: 'ecmrHistoryOverview',
        pageIndex: 1,
        pageSize: 100,
      });
      expect(service).toBeTruthy();
    });
  });

  describe('with relevant localstorage', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({});
    });

    it('should have working functions for the sort state', () => {
      const sortStateFromLS: SortState = {
        sortName: 'ecmrHistoryOverview',
        active: '',
        direction: 'asc',
      };
      const sortStateToBeSet: SortState = {
        sortName: 'allContacts',
        active: 'sometype',
        direction: 'desc',
      };
      spyOn(localStorage, 'getItem').and.returnValue(
        JSON.stringify([
          {
            sortName: 'ecmrOverview',
            active: '',
            direction: 'asc',
          },
          sortStateFromLS,
          {
            sortName: 'allContacts',
            active: '',
            direction: 'asc',
          },
        ])
      );
      service = TestBed.inject(TableStateService);
      expect(service.getSortState('ecmrHistoryOverview')).toEqual(
        sortStateFromLS
      );
      service.setSortState(sortStateToBeSet);
      expect(service.getSortState('allContacts')).toEqual(sortStateToBeSet);
    });

    it('should have working functions for the pageState', () => {
      spyOn(localStorage, 'getItem').and.returnValue(
        JSON.stringify([
          { pageName: 'ecmrOverview', pageIndex: 0, pageSize: 99 },
          { pageName: 'ecmrHistoryOverview', pageIndex: 0, pageSize: 99 },
          { pageName: 'allContacts', pageIndex: 0, pageSize: 99 },
        ])
      );
      service = TestBed.inject(TableStateService);
      expect(service.getPageState('ecmrOverview')).toEqual({
        pageName: 'ecmrOverview',
        pageIndex: 0,
        pageSize: 99,
      });
      service.setPageState({
        pageName: 'ecmrOverview',
        pageIndex: 0,
        pageSize: 99,
      });
      expect(service.getPageState('ecmrOverview').pageSize).toBe(99);
    });
  });

  describe('with corrupted localstorage', () => {
    beforeEach(() => {
      TestBed.configureTestingModule({});
      spyOn(localStorage, 'getItem').and.returnValue(
        JSON.stringify({ a: 0, bad: 'good' })
      );
    });

    it('should have working functions for the sort state', () => {
      const sortStateToBeSet: SortState = {
        sortName: 'ecmrOverview',
        active: 'someType',
        direction: 'desc',
      };

      service = TestBed.inject(TableStateService);

      const sortState = service.getSortState();
      // eslint-disable-next-line @typescript-eslint/dot-notation
      const defaultState = service['sortStatesDefault'][0];

      expect(sortState).toEqual(defaultState);

      service.setSortState(sortStateToBeSet);
      expect(service.getSortState()).toEqual(sortStateToBeSet);
    });

    it('should have working functions for the page size state', () => {
      const pageStateToBeSet: PageState = {
        pageName: 'ecmrOverview',
        pageIndex: 10,
        pageSize: 100,
      };

      service = TestBed.inject(TableStateService);

      const pageState = service.getPageState();
      // eslint-disable-next-line @typescript-eslint/dot-notation
      const defaultState = service['pageStatesDefault'][0];

      expect(pageState).toEqual(defaultState);

      service.setPageState(pageStateToBeSet);
      expect(service.getPageState()).toEqual(pageStateToBeSet);
    });
  });
});
