/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { SnackbarService } from './snackbar.service';
import { Sort } from '@angular/material/sort';
import { PageState } from '../../models/states/page-state';
import { PageEvent } from '@angular/material/paginator';
import { SortState } from '../../models/states/sort-state';

export type AvailableTables =
  | 'ecmrOverview'
  | 'allContacts'
  | 'ecmrHistoryOverview'
  | 'ecmrTemplatesOverview'
  | 'ecmrLoadFromTemplateDialog';
export const availableTables: AvailableTables[] = [
  'ecmrOverview',
  'ecmrHistoryOverview',
  'allContacts',
  'ecmrTemplatesOverview',
  'ecmrLoadFromTemplateDialog'
];

@Injectable({
  providedIn: 'root',
})
export class TableStateService {
  private readonly storageKeySort = 'sortState';
  private readonly storageKeyPage = 'pageState';
  private sortStates: SortState[];
  private pageStates: PageState[];
  private sortStatesDefault: SortState[] = [];
  private pageStatesDefault: PageState[] = [];

  constructor(private snackbarService: SnackbarService) {
    availableTables.forEach((table) => {
      this.sortStatesDefault.push({
        sortName: table,
        active: '',
        direction: 'asc',
      });
      this.pageStatesDefault.push({
        pageName: table,
        pageIndex: 0,
        pageSize: 99,
      });
    });
    this.sortStates = this.getSortStatesFromLocalStorage();
    this.pageStates = this.getPageStatesFromLocalStorage();
  }

  getSortState(table: AvailableTables = 'ecmrOverview'): Sort {
    return this.sortStates.find((state) => state.sortName === table);
  }

  getPageState(table: AvailableTables = 'ecmrOverview'): PageState {
    return this.pageStates.find((state) => state.pageName === table);
  }

  setSortState(sortState: SortState): void {
    this.sortStates = this.sortStates.map((state) =>
      state.sortName !== sortState.sortName
        ? state
        : {
            sortName: state.sortName,
            direction: sortState.direction,
            active: sortState.active,
          }
    );
    this.saveSortStatesToLocalStorage(this.sortStates);
  }

  setPageStateFromEvent(
    pageEvent: PageEvent,
    table: AvailableTables = 'ecmrOverview'
  ): void {
    this.pageStates = this.pageStates.map((state) =>
      state.pageName !== table
        ? state
        : {
            pageName: state.pageName,
            pageIndex: pageEvent.pageIndex,
            pageSize: pageEvent.pageSize,
          }
    );
    this.savePageStatesToLocalStorage(this.pageStates);
  }

  setPageState(
    pageState: PageState,
    table: AvailableTables = 'ecmrOverview'
  ): void {
    this.pageStates = this.pageStates.map((state) =>
      state.pageName !== table
        ? state
        : {
            pageName: state.pageName,
            pageIndex: pageState.pageIndex,
            pageSize: pageState.pageSize,
          }
    );
    this.savePageStatesToLocalStorage(this.pageStates);
  }

  private getPageStatesFromLocalStorage(): PageState[] {
    // Prepare returned Array: Copy defaults
    let retStates = JSON.parse(JSON.stringify(this.pageStatesDefault));
    // Assume that value exists in LS
    if (localStorage.getItem(this.storageKeyPage) !== null) {
      // t/c because of parsing and indexer-access methods
      try {
        // Get State from LS
        const lsStates: PageState[] = JSON.parse(
          localStorage.getItem(this.storageKeyPage)
        );
        // Iterate and evaluate Defaults-Array
        this.pageStatesDefault.forEach((defaultState, index) => {
          // Check if entry is valid by...
          if (
            // Checking if pageIndex is defined
            lsStates[index].pageIndex !== undefined &&
            // Checking if pageSize is defined
            lsStates[index].pageSize !== undefined &&
            // Checking if pageName from LS entry @ index equals the one from the default
            lsStates[index].pageName === defaultState.pageName &&
            availableTables.find((table) => table === lsStates[index].pageName)
          ) {
            // And in case it fits adding values to the returned array
            retStates[index].pageIndex = lsStates[index].pageIndex;
            retStates[index].pageSize = lsStates[index].pageSize;
            retStates[index].pageName = lsStates[index].pageName;
          }
        });
      } catch (err) {
        // If parsing or indexer-access fails at any point reset array to defaults again
        retStates = JSON.parse(JSON.stringify(this.pageStatesDefault));
        this.snackbarService.pushNewStatus(
          'Saved table state in local storage is corrupted. Pagination has been reset.'
        );
      }
    }
    this.savePageStatesToLocalStorage(retStates);
    return retStates;
  }

  private getSortStatesFromLocalStorage(): SortState[] {
    let retStates = JSON.parse(JSON.stringify(this.sortStatesDefault));
    if (localStorage.getItem(this.storageKeySort) !== null) {
      try {
        const lsStates: SortState[] = JSON.parse(
          localStorage.getItem(this.storageKeySort)
        );
        this.sortStatesDefault.forEach((defaultState, index) => {
          if (
            lsStates[index].active !== undefined &&
            lsStates[index].direction !== undefined &&
            lsStates[index].sortName === defaultState.sortName &&
            availableTables.find((table) => table === lsStates[index].sortName)
          ) {
            retStates[index].active = lsStates[index].active;
            retStates[index].direction = lsStates[index].direction;
            retStates[index].sortName = lsStates[index].sortName;
          }
        });
      } catch (err) {
        retStates = JSON.parse(JSON.stringify(this.sortStatesDefault));
        this.snackbarService.pushNewStatus(
          'Saved table state in local storage is corrupted. Sorting has been reset.'
        );
      }
    }
    this.saveSortStatesToLocalStorage(retStates);
    return retStates;
  }

  private saveSortStatesToLocalStorage(sortStates: SortState[]) {
    localStorage.setItem(this.storageKeySort, JSON.stringify(sortStates));
  }

  private savePageStatesToLocalStorage(pageStates: PageState[]) {
    localStorage.setItem(this.storageKeyPage, JSON.stringify(pageStates));
  }
}
