/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

/**
 * Handles the routing for templates and ecmrs. Saving the state to local Storage to persist through refresh of page.
 */
@Injectable({
    providedIn: 'root',
})
export class TemplateRoutingService {
    private isTemplateCreationSource = new BehaviorSubject<boolean>(false);
    private isEditTemplateSource = new BehaviorSubject<boolean>(false);
    isTemplateCreation$ = this.isTemplateCreationSource.asObservable();
    isEditTemplate$ = this.isEditTemplateSource.asObservable();
    templateId: number;

    constructor() {
        this.isTemplateCreationSource.next(
            localStorage.getItem('isTemplateCreation') === 'true'
        );
        this.isEditTemplateSource.next(
            localStorage.getItem('isEditTemplate') === 'true'
        );
        this.templateId = +localStorage.getItem('templateId');
    }

    setTemplateCreation(value: boolean) {
        this.isTemplateCreationSource.next(value);

        if (value) {
            this.isEditTemplateSource.next(false);
            localStorage.removeItem('isEditTemplate');
            localStorage.removeItem('templateId');
        }

        localStorage.setItem('isTemplateCreation', value.toString());
    }

    setEditTemplate(value: boolean, templateId: number | null) {
        this.isEditTemplateSource.next(value);

        if (value) {
            this.isTemplateCreationSource.next(false);
            localStorage.removeItem('isTemplateCreation');
        }

        if (templateId !== null) {
            localStorage.setItem('templateId', templateId.toString());
        }

        localStorage.setItem('isEditTemplate', value.toString());
        this.templateId = templateId;
    }
}