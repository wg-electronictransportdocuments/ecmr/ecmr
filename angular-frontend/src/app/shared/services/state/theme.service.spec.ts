/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {TestBed} from '@angular/core/testing';

import {ThemeService} from './theme.service';

describe('ThemeService', () => {
  let service: ThemeService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ThemeService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should work n functions', () => {
    // eslint-disable-next-line @typescript-eslint/dot-notation
    service['cookieConsent'] = true;
    service.cycleMode();
    // eslint-disable-next-line @typescript-eslint/dot-notation
    service['cookieConsent'] = false;
    service.cycleMode();
  });

});
