/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

import {Injectable} from '@angular/core';
import {Observable, ReplaySubject} from 'rxjs';
import {Mode, ModeIcon, ModeName} from '../../models/states/mode';
import {Theme, ThemeName} from '../../models/states/theme';
import { environment } from '../../../../environments/environment';


@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private readonly ModeStorageKey = 'current-mode';
  private readonly ThemeStorageKey = 'current-theme';
  private readonly Modes: Mode[] = [
    {name: 'dark-theme', icon: 'dark_mode'},
    {name: 'light-theme', icon: 'light_mode'}
  ];
  private readonly Themes: Theme[] = [
    {name: 'lightgreen', label: 'Light Green'},
    {name: 'blue', label: 'Blue'},
    {name: 'green', label: 'Green'},
    {name: 'lightblue', label: 'Light Blue'},
    {name: 'darkblue', label: 'Dark Blue'},
    {name: 'darkgreen', label: 'Dark Green'},
    {name: 'yellow', label: 'Yellow'},
    {name: 'red', label: 'Red'},
    {name: 'custom', label: 'Custom'},
  ];

  private modeName$: ReplaySubject<ModeName> = new ReplaySubject<ModeName>();
  private modeName: ModeName = localStorage.getItem(this.ModeStorageKey) as ModeName;

  private themeName$: ReplaySubject<ThemeName> = new ReplaySubject<ThemeName>();
  private themeName: ThemeName = localStorage.getItem(this.ThemeStorageKey) as ThemeName;

  constructor() {
    this.modeName$.asObservable().subscribe(mode => {
      this.modeName = mode;
      this.setLocalStorageMode(mode);
    });
    this.themeName$.asObservable().subscribe(theme => {
      this.themeName = theme;
      this.setLocalStorageTheme(theme);
    });
    const tmpMode = this.getLocalStorageMode();
    this.modeName$.next(tmpMode ? tmpMode : this.Modes[0].name);

    const tmpTheme = this.getLocalStorageTheme();
    this.themeName$.next(tmpTheme ? tmpTheme : environment.instanceColorTheme as ThemeName);

  }

  getModeName$(): Observable<ModeName> {
    return this.modeName$.asObservable();
  }

  public getModeName(): ModeName {
    return this.modeName;
  }

  getThemeName$(): Observable<ThemeName> {
    return this.themeName$.asObservable();
  }

  public getThemeName(): ThemeName {
    return this.themeName;
  }

  // Returns currently selected mode/theme object from resp. array
  public getModeObj(): Mode {
    return this.Modes.find(x => x.name == this.modeName);
  }

  public getThemeObj(): Theme {
    return this.Themes.find(x => x.name == this.themeName);
  }

  // Returns sequent Mode from Modes-Array
  public getNextModeObj(): Mode {
    const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeName));
    const nextIndex = (currentIndex + 1) % this.Modes.length;
    return this.Modes[nextIndex];
  }

  // Returns sequent icon-string-identifier
  public getNextModeIcon(): ModeIcon {
    const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeName));
    const nextIndex = (currentIndex + 1) % this.Modes.length;
    return this.Modes[nextIndex].icon;
  }

  // Updates mode/theme in service-field and localStorage
  public setMode(name: ModeName): void {
    if (this.Modes.find(x => x.name == name) === undefined) {
      console.warn('Requested mode-name does not match available mode names.');
    } else {
      this.modeName$.next(name);
    }
  }

  public setTheme(name: ThemeName): void {

    if (this.Themes.find(x => x.name == name) === undefined) {
      console.warn('Requested theme-name does not match available theme names.');
    } else {
      this.themeName$.next(name);
    }
  }

  // Cycles modes
  public cycleMode() {
    const currentIndex = this.Modes.indexOf(this.Modes.find(x => x.name == this.modeName));
    const nextIndex = (currentIndex + 1) % this.Modes.length;
    this.modeName$.next(this.Modes[nextIndex].name);
  }

  // Returns all available modes/themes
  public getAvailableThemes(): Theme[] {
    return this.Themes;
  }

  public getClassName(): string {
    return this.themeName + '-' + this.modeName;
  }

  private getLocalStorageMode(): ModeName | null {
    return localStorage.getItem(this.ModeStorageKey) as ModeName;
  }

  private getLocalStorageTheme(): ThemeName | null {
    return localStorage.getItem(this.ThemeStorageKey) as ThemeName;
  }

  private setLocalStorageMode(mode: ModeName): void {
    localStorage.setItem(this.ModeStorageKey, mode);
  }

  private setLocalStorageTheme(theme: ThemeName): void {
    localStorage.setItem(this.ThemeStorageKey, theme);
  }
}
