/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

(function(window){
  window['env'] = window['env'] || {};

  window['env']['BACKEND_URL'] = 'BACKEND_URL';
  window['env']['FRONTEND_URL'] = 'FRONTEND_URL';

  window['env']['INSTANCE_USERMANAGEMENT_TYPE'] = 'INSTANCE_USERMANAGEMENT_TYPE';
  window['env']['INSTANCE_SSL'] = 'INSTANCE_SSL';
  window['env']['INSTANCE_COLOR_THEME'] = 'INSTANCE_COLOR_THEME';
  window['env']['INSTANCE_TITLE'] = 'INSTANCE_TITLE';

  window['env']['KEYCLOAK_URL'] = 'KEYCLOAK_URL';
  window['env']['KEYCLOAK_FRONTEND_RESOURCE'] = 'KEYCLOAK_FRONTEND_RESOURCE';
  window['env']['KEYCLOAK_REALM'] = 'KEYCLOAK_REALM';
}(this));
