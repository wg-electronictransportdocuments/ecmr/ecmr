/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// Suppressing naming convention because standardized strings are used
/* eslint-disable @typescript-eslint/naming-convention */
export const de = {
  // TBD
};
