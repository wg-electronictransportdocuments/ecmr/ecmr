/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

// Suppressing naming convention because standardized strings are used
/* eslint-disable @typescript-eslint/naming-convention */
export const en = {
  error_required: 'You must enter a value',
  error_invalid: 'Not a valid ',
  Trade_Party_Identification_Identifier: 'ID',
  Trade_Party_Name_Text: 'Name',
  Trade_Party_Language_Code: 'Language Code',
  Pattern_AN: ' Chars of Alphanumerics',
  Pattern_N: ' Chars of Numerics',
  Pattern_A: ' Letters',

  Trade_Contact_PersonName_Text: 'Person Name',

  Trade_Address_Postcode_Code: 'Postcode',
  Trade_Address_StreetName_Text: 'Street Name',
  Trade_Address_CityName_Text: 'City Name',
  Trade_Address_Country_Identifier: 'Country Code',
  Trade_Address_CountryName_Text: 'Country Name',
  Trade_Address_CountrySubDivisionName_Text: 'Country Sub-Division Name',

  Logistics_Location_Identification_Identifier: 'ID',
  Logistics_Location_Name_Text: 'Place',
  Logistics_Location_Type_Text: 'Type Code',
  Logistics_Location_OpeningHours_Text: 'Opening Hours',

  Logistics_ShippingMarks_Marking_Text: 'Marking Text',

  Logistics_Package_Type_Code: 'Type Code',
  Logistics_Package_Type_Text: 'Type Text',
  Logistics_Package_Item_Quantity: 'Item Quantity',

  Logistics_TimeOfArrival_DateTime: 'Time of Arrival',
  Logistics_TimeOfDeparture_DateTime: 'Time of Departure',

  SupplyChain_ConsignmentItem_Sequence_Numeric: 'Sequence Number',
  SupplyChain_ConsignmentItem_Type_Code: 'Commodity Code',
  SupplyChain_ConsignmentItem_GrossWeight_Measure: 'Gross Weight',
  SupplyChain_ConsignmentItem_GrossVolume_Measure: 'Gross Volume',
  SupplyChain_ConsignmentItem_Invoice_Amount: 'Invoice Amount',

  Transport_Cargo_Identification_Text: 'Identification Text',

  Custom_LicensePlate_Text: 'License Plate',
  Custom_SignatureDate_DateTime: 'Date',
  Custom_Signature_Text: 'Signature',
  Custom_ReservationsObservations_Text: 'Reservations and Observations',
  Custom_Remarks_Text: 'Remarks',
  Custom_Barcode_Text: 'Barcode Number',
  Custom_SpecialAgreement_Text: 'Special Agreement',
  Custom_CashOnDelivery: 'Cash on Delivery',
  Custom_Particulars_Text: 'Particulars',
  Custom_Dropdown_Sender_Text: 'Sender',
  Custom_Dropdown_Consignee_Text: 'Consignee',
  Custom_Charge_Carriage_Text: 'Carriage Charges',
  Custom_Charge_Supplementary_Text: 'Supplementary Charges',
  Custom_Charge_CustomsDuties_Text: 'Customs Duties',
  Custom_Charge_Other_Text: 'Other Charges',
  Custom_Charge_Currency_Text: 'Currency',
  Custom_Established_In_Text: 'In',
  Custom_Established_DateTime: 'On',

  Transport_Instructions_Description_Text: 'Description',
  Transport_Instructions_Description_Code: 'Description Code',

  Specified_Observation_Identification_Identifier: 'Sequence Number',
  Specified_Observation_Description_Text: 'Description',

  Reference_Field: 'Reference Field',

  Box_01: '1 Sender (name, address, country)',
  Box_02: '2 Consignee (name, address, country)',
  Box_03: '3 Taking over the goods',
  Box_04: '4 Delivery of the goods',
  Box_05: '5 Sender\'s instructions',
  Box_06: '6 Carrier (name, address, country, other references)',
  Box_07: '7 Successive Carriers',
  Box_08: '8 Carrier\'s reservations and observations on taking over the goods',
  Box_09: '9 Documents handed to the carrier by the sender',
  Box_10: '10 Marks and Nos',
  Box_11: '11 Number of packages',
  Box_12: '12 Method of packing',
  Box_13: '13 Nature of the goods',
  Box_14: '14 Gross weight in kg',
  Box_15: '15 Volume in m3',
  Box_16: '16 Special agreements between the sender and the carrier',
  Box_17: '17 To be paid by:',
  Box_18: '18 Other useful particulars',
  Box_19: '19 Cash on delivery',
  Box_20:
    '20 This carriage is subject, notwithstanding any clause to the contrary, to the Convention on the Contract for the international' +
    ' Carriage of Goods by Road (CMR)',
  Box_21: '21 Established',
  Box_22: '22 Signature or stamp of the sender',
  Box_23: '23 Signature or stamp of the carrier',
  Box_24: '24 Goods received',
  Box_25: 'Non-contractual part reserved for the carrier ',
  Printed_Positions_List_Disclaimer:
    '10 - 15 A list of transported items is displayed on the following page(s).',
};
