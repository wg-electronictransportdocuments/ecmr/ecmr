## Third Party Icons which are used as Images

Throughout the Application there are some Material-Icons in svg-format which are used as resizable images. Styling 
is done with the angular-svg-icon-package.

### 'Published with changes'-icon
`published_with_changes.svg`

> https://github.com/google/material-design-icons/blob/6ebe181c634f9ced978b526e13db6d7d5cb1c1ba/src/action/published_with_changes/materialiconsoutlined/24px.svg
---
### 'Contact Mail'-icon
`contact_mail.svg`

> https://github.com/google/material-design-icons/blob/6ebe181c634f9ced978b526e13db6d7d5cb1c1ba/src/communication/contact_mail/materialicons/24px.svg
---
### 'Person'-icon
`person.svg`

> https://github.com/google/material-design-icons/blob/6ebe181c634f9ced978b526e13db6d7d5cb1c1ba/src/social/person/materialicons/24px.svg
---
### 'Groups'-icon
`groups.svg`

> https://github.com/google/material-design-icons/blob/6ebe181c634f9ced978b526e13db6d7d5cb1c1ba/src/social/groups/materialicons/24px.svg
---
### 'QR-Code 2'-icon
`qr_code_2.svg`

> https://github.com/google/material-design-icons/blob/6ebe181c634f9ced978b526e13db6d7d5cb1c1ba/src/communication/qr_code_2/materialicons/24px.svg

