/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const environment = {
  production: true,

  backendUrl: window['env'] && window['env']['BACKEND_URL'] ? window['env']['BACKEND_URL'] : 'http://localhost:8080/',
  frontendUrl: window['env'] && window['env']['FRONTEND_URL'] ? window['env']['FRONTEND_URL'] : 'http://localhost:4200',
  userManagement: window['env'] && window['env']['INSTANCE_USERMANAGEMENT_TYPE'] ? window['env']['INSTANCE_USERMANAGEMENT_TYPE'] : 'localdev',
  instanceSsl: window['env'] && window['env']['INSTANCE_SSL'] ? window['env']['INSTANCE_SSL'] : 'false',
  instanceTitle: window['env'] && window['env']['INSTANCE_TITLE'] ? window['env']['INSTANCE_TITLE'] : 'eCMR Frontend',
  instanceColorTheme: window['env'] && window['env']['INSTANCE_COLOR_THEME'] ? window['env']['INSTANCE_COLOR_THEME'] : 'green',
  keycloakUrl: window['env'] && window['env']['KEYCLOAK_URL'] ? window['env']['KEYCLOAK_URL'] : 'http://localhost:8181/',
  keycloakResource: window['env'] && window['env']['KEYCLOAK_FRONTEND_RESOURCE'] ? window['env']['KEYCLOAK_FRONTEND_RESOURCE'] : 'frontend',
  keycloakRealm: window['env'] && window['env']['KEYCLOAK_REALM'] ? window['env']['KEYCLOAK_REALM'] : 'ecmr-keycloak',
};
