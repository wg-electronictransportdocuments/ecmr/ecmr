/**
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

export const environment = {
  production: false,
  backendUrl: 'http://localhost:8080/',
  frontendUrl: 'http://localhost:4200',
  userManagement: 'localdev',
  instanceSsl: 'false',
  instanceTitle:  'eCMR Frontend',
  instanceColorTheme: 'green',
  keycloakUrl:  'http://localhost:8181/',
  keycloakResource: 'frontend',
  keycloakRealm: 'ecmr-keycloak',
};
