### User guide and documentation overview

#### User manual
* [User manual](./user-manual.pdf)

#### Technical documentation
* [Technical documentation](../index.adoc)

#### Readme files
* [General Project Readme](../../README.md)
* [Frontend Readme](../../angular-frontend/README.md)
* [Backend Readme](../../java-backend/README.md)
* [Testing Deployment with Docker Readme](../../local-deployment/README.md)
