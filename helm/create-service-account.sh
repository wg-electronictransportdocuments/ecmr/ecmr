
#!/bin/bash
if [ $# -ne 1 ];
	then echo "Usage: $0 namespace"
	exit
fi

function quit {
        echo $1
        exit 1
}

NAMESPACE=$1
SERVICE_ACCOUNT_NAME=$NAMESPACE-deployer

# Switch to the project/namespace given as a parameter
oc project $NAMESPACE || quit "ERROR: Please make sure the namespace \"$NAMESPACE\" does exist and you are correctly logged in (oc login)"

# Create the Service Account
oc create sa $SERVICE_ACCOUNT_NAME || quit "ERROR: Could not create Service Account \"$SERVICE_ACCOUNT_NAME\""

# Grant the ServiceAccount admin privilege for the project/namespace given as a parameter
oc policy add-role-to-user admin system:serviceaccount:$NAMESPACE:$SERVICE_ACCOUNT_NAME || quit "ERROR: Could not grant privilege to the created ServiceAccount \"$SERVICE_ACCOUNT_NAME\""

# Print the ServiceAccount's token
echo -e "\nThis is the ServiceAccount's token. Use it as a (masked) variable for your gitlab-ci:\n\n"
oc serviceaccounts get-token $SERVICE_ACCOUNT_NAME
echo -e "\n\n"
