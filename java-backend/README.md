# eCMR Java Backend

This project was generated with [Spring Boot 2.5](https://spring.io/projects/spring-boot). For building the backend [Maven](https://maven.apache.org/)
is used.
For setting up Maven, please refer to their [guide](https://maven.apache.org/guides/getting-started/index.html).

## Licenses of third-party dependencies

The licenses used by this project's third-party dependencies are documented in the `third-party-licenses` directory.
This is done to encourage developers to check the licenses used by the third-party dependencies to ensure they do not conflict with the license of
this project itself.
The directory contains the following files:

* `third-party-licenses.txt` - Contains the licenses used by this project's third-party dependencies.
  The content of this file is/can be generated.
* `third-party-licenses-complementary.txt` - Contains entries for third-party dependencies for which the licenses cannot be determined automatically.
  The content of this file is maintained manually.

## Generating third-party license reports

This project uses the [license-maven-plugin](https://github.com/mojohaus/license-maven-plugin) to generate a file containing the licenses used by the
third-party dependencies.
The content of the `mvn license:add-third-party` Maven goal's output (`target/generated-sources/license/THIRD-PARTY.txt`) can be copied
into `third-party-licenses/third-party-licenses.txt`.

Third-party dependencies for which the licenses cannot be determined automatically by the license-maven-plugin have to be documented manually
in `third-party-licenses/third-party-licenses-complementary.txt`.
In the `third-party-licenses/third-party-licenses.txt` file these third-party dependencies have an "Unknown license" license.

## Development environment

To run the eCMR it is necessary to set the 5 environment variables ECMR_KEYCLOAK_ADMIN_RESOURCE=admin-spring-boot, ECMR_KEYCLOAK_ADMIN_SECRET_KEY, KEYCLOAK_AUTH_SERVER_URL
KEYCLOAK_REALM and KEYCLOAK_RESOURCE. The description of the variables can be found in the table below.
When not datasource configuration is provided, an in memory H2 Database will be used.


For the backend, environment variables are registered under

`./src/resources/application.properties`

where * is replaced by the chosen application profile.

The following list of environment variables can be set to affect the backends behavior:

| Environment Variable             | Type      | Description                                                                                                       | Default Value                     |
|----------------------------------|-----------|-------------------------------------------------------------------------------------------------------------------|-----------------------------------|
| `ECMR_BACKEND_PORT`              | _int_     | Port used by backend                                                                                              | `8080`                            |
| `ECMR_BACKEND_ADDRESS`           | _string_  | Address of backend. Used for communication between multiple instances. Should be reachable from outside           | `http://localhost:8080/`          |
| `ECMR_INSTANCE_SSL`              | _boolean_ | Use SSL for commuincation with frontend. Recommended, but can be turned off for local testing without certificate | `true`                            |
| `KEYCLOAK_AUTH_SERVER_URL`       | _string_  | URL of keycloak instance.                                                                                         | `http://localhost:8181/`          |
| `KEYCLOAK_REALM`                 | _string_  | Name of keycloak realm.                                                                                           | `ecmr-keycloak`                   |
| `KEYCLOAK_RESOURCE`              | _string_  | Resource for keycloak realm.                                                                                      | `backend`                         |
| `ECMR_KEYCLOAK_ADMIN_RESOURCE`   | _string_  | Admin Resource for keycloak realm.                                                                                | `spring-boot-admin`               |
| `ECMR_KEYCLOAK_ADMIN_SECRET_KEY` | _string_  | Password for keycloak admin resource.                                                                             |                                   |
| `DATASOURCE_ADDRESS`             | _string_  | JDBC Connection String                                                                                            | `jdbc:h2:mem:mydb`                |
| `DATASOURCE_USERNAME`            | _string_  | Username for the specified DATASOURCE_ADDRESS                                                                     | `sa`                              |
| `DATASOURCE_PASSWORD`            | _string_  | Password for the specified DATASOURCE_ADDRESS                                                                     | `password`                        |
| `DATASOURCE_DRIVERCLASSNAME`     | _string_  | Class name of JDBC driver                                                                                         | `org.h2.Driver`                   |
| `DATASOURCE_PLATFORM`            | _string_  | Name of database dialect                                                                                          | `org.hibernate.dialect.H2Dialect` |

In addition to the parameters entered via the environment variables, the properties files contain further variables that
influence the behavior of the backend.
In particular, database dialect, connection and initialization can be controlled via further variables (
see [here](https://docs.spring.io/spring-boot/docs/2.0.0.M4/reference/html/boot-features-sql.html)). Likewise the
type of the user management can be steered over the variables. Here one should pay attention to the interaction with
the respective frontend.
The current `keycloak` implementation for authentication connects to a keycloak server (tested with v18.0.2), which has to be defined using environment variables, according to
the previously mentioned scripts.
As configured in the concurrent CI/CD-files (gitlab, helm) it is also possible to link to Kubernetes secrets.
The other parameters can be extracted as Env variables if required.

## Build

Run `mvn package` to build and package the project. The build artifacts will be stored in the `target/` directory.

Note: As the default Maven repository the Silicon Economy Nexus is used. If this is not reachable, the corresponding tag `<repository>` in
the `pom.xml` must be adjusted.

## Build profiles

The executed build profile can be controlled by the `-P` flag. This has especially impact on the underlying data sources.

The `test` profile is recommended for testing and development purposes. If no profile is given, this is treated as the default.
As the underlying data source an [H2-database](https://www.h2database.com/html/main.html) is used. This is built together with the backend. Thus,
there is no need to have a database set up manually beforehand. Initially the H2-DB is filled with sample data.
When terminating the backend, the H2-DB gets terminated and cleaned up as well.
Beside the database, also the address for the corresponding Blockchain is controlled by the profile. In case of the `test` profile, it maps the
address to a local instance of the BC-microservice.

The `prod` profile is recommended for production environments. It expects an already set up [PostgreSQL-database](https://www.postgresql.org/). The
address for this is stored in the file `application.prod-properties`. The login data is expected to be stored in environment variables, e.g. through
Kubernetes secrets.
The BC address in this case corresponds to the cluster address of the BC-microservice.

## Running unit tests

Run `mvn test` to execute the unit tests via [JUnit](https://junit.org/junit5/). For running the tests the default `test` profile is strongly
recommended.

## Run Development server

Run `mvn spring-boot:run` for a dev server. This compiles and executes the source code. The server application can then be accessed
via `http://localhost:8080/`.

## Run Keycloak Server

Please refer to the official tutorial on 'https://www.keycloak.org/getting-started/getting-started-docker' and run a local Keycloak Server.
You need to configure some mandatory settings to run the eCMR with Keycloak locally.

### Roles

    - ecmr-user: Every user needs this role. (The name can be configured in the application.properties)
    - ecmr-disponent, ecmr-verlader, ecmr-fahrer, ecmr-empfaenger, ecmr-verwaltung, ecmr-superuser: These roles represent the participiant in the use cases and swimlanes.

### Clients

You need three clients to run the eCMR Service with a Keycloak server.

1. frontend: Login client
2. backend: Client to secure the REST API of the service.
3. admin-spring-boot: Service request information about users, roles, realms.

### Users

To add a user, please refer to the Keycloak documentation site. A user must have the role ecmr-user and a role related to its role in the workflow (
disponent, driver etc.) Additionally, you can add an attribute 'companyID' that refers to a company, that is added in the mandatory data injection.

## Docker

### Build Docker Image

Alternatively, the backend can be built as a container with Docker. Run `docker build . -t ecmr_backend` in the top level java-backend directory.
The `Dockerfile` aims to build a production-ready container. Thus, the backend is built using the `prod` profile by default. If needed, this can be
controlled by passing Docker the corresponding profile as an argument, e.g. `--build-arg mode=test` for the test profile.

### Run Docker Image

Run `docker run ecmr_backend` to execute the build container.
