# Important note: application-prod.properties has to have the server.port prop set to make the -Pprod flag
# able to run on a dedicated port
export ECMR_BACKEND_PORT="8081";
export ECMR_BACKEND_ADDRESS="http://localhost:8081/"
export ECMR_INSTANCE_SSL="false"

export ECMR_KEYCLOAK_ADMIN_RESOURCE="admin-spring-boot";
export ECMR_KEYCLOAK_ADMIN_SECRET_KEY="only-for-demonstration-purposess";

export KEYCLOAK_AUTH_SERVER_URL="http://localhost:8090/";
export KEYCLOAK_ENABLED="true";
export KEYCLOAK_REALM="ecmr-keycloak-1";
export KEYCLOAK_RESOURCE="backend";


export POSTGRES_DATASOURCE_ADDRESS="jdbc:postgresql://localhost:5432/ecmr1"
export POSTGRES_DATASOURCE_USERNAME="postgres"
export POSTGRES_DATASOURCE_PASSWORD="password"

export BC_MS_URL="1"