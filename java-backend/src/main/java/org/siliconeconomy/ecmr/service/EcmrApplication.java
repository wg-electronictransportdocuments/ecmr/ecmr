/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service;

import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.role.KeycloakRoleService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.status.impl.*;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.usermanagement.impl.KeycloakUserManagement;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

/**
 * Main Application class.
 *
 */

@SpringBootApplication
@ComponentScan("org.siliconeconomy.ecmr")
@EnableScheduling
public class EcmrApplication {

    /**
     * Method which starts the backend
     *
     * @param args default list of String arguments, will be ignored
     */
    public static void main(String[] args) {
        SpringApplication.run(EcmrApplication.class, args);
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean(name = "Usermanagement")
    public UserManagement keycloakUserManagement() {
        return new KeycloakUserManagement();
    }

    @Bean(name = "StatusService")
    public StatusService keycloakStatusService() {return new KeycloakStatusService();}

    @Bean(name = "RoleService")
    public RoleService keycloakRoleService() {return new KeycloakRoleService();}
}

