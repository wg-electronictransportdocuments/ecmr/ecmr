/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.constants;

import org.springframework.beans.factory.annotation.Value;

/**
 * This class hold all static values in the eCMR projekt.
 *
 */

public class EcmrConstants {

    @Value("${ecmr.keycloak.user.role:ecmr-user}")
    public void setRoleStatic(String userRole){
        EcmrConstants.USER_ROLE = userRole;
    }

    /** Constants for token expiration */
    public static final int TOKEN_EXPIRE_TIME = 1800000;
    public static final int REFRESH_TOKEN_EXPIRE_TIME = 2400000;
    public static final String REFRESH_TOKEN_KEY = "refreshToken";


    /**
     * Key constants
     */
    public static final String ECMR_ID_KEY = "ecmrId";
    public static final String USER_ID_KEY = "userId";
    public static final String COMPANY_ID = "companyID";
    public static final String SIGNATURE_KEY = "signature";
    public static final String VERSION_KEY = "version";
    public static final String ECMR_FIELD_KEY = "fields";
    public static final String EXTERNAL_SYSTEM_ID_KEY = "externalSystemId";
    public static final String EXTERNAL_SYSTEM_KEY = "externalSystemKey";
    public static final String CONTACTS_KEY = "contacts";
    public static String USER_ROLE ; //hopefully filled by spring
    /**
     * Key constants
     */
    public static final String EMAIL_KEY = "email";
    public static final String PASSWORD_KEY = "password";
    public static final String PUBKEY_KEY = "pubKey";
    public static final String SECRET_TEST_KEY = "x/A?D(G+KbPeShVmYq3t6v9y$B&E)H@McQfTjWnZr4u7x!z%C*F-JaNdRgUkXp2s5v8y/B?D(G+KbPeShVmYq3t6w9z$C&F)H@Mc" +
            "RgUkXp2s5v8x/A?D(G+KbPeShVmYq3t6w9z$B&E)H@McQfTjWnZr4u7x!A%D*F-JaNdRgUkXp2s5v8y/B?E(H+KbPeShVmYq3t6w";

}
