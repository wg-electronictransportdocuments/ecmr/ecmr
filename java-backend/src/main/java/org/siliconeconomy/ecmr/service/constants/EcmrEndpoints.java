/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.constants;

/**
 * This class hold all static enpoint path of the eCMR projekt.
 *
 */

public class EcmrEndpoints {

    public static final String USER_BASE_PATH = "/api/v1/usm";
    public static final String GET_USER_BY_ID = "/users/{id}";
    public static final String GET_USERS_CONTACTS = "/users/contacts";
    public static final String GET_USERS_FIELDS = "/users/fields";
    public static final String POST_USER_PUBKEY = "/connection/key";

    public static final String ECMR_BASE_PATH = "/api/v1";
    public static final String EXTERNAL_PATH = "/external";
    public static final String GET_ECMRS = "/ecmrs";
    public static final String GET_TEMPLATES = "/templates";
    public static final String POST_ECMR = "/ecmrs";
    public static final String POST_TEMPLATE = "/templates";
    public static final String GET_ECMR_BY_ID = "/ecmrs/id/{id}";
    public static final String GET_ECMR_BY_ID_FOR_CARRIER = "/ecmrs-carrier/id/{id}/tan/{tan}";
    public static final String GET_TEMPLATE_BY_ID = "/templates/id/{id}";
    public static final String PUT_ECMR_BY_ID = "/ecmrs/id/{id}/{isEcmr}";
    public static final String DELETE_ECMR_BY_ID = "/ecmrs/id/{id}";
    public static final String DELETE_TEMPLATE_BY_ID = "/templates/id/{id}";
    public static final String GET_ECMR_HISTORY_BY_ID = "/ecmrs/id/{id}/history";
    public static final String PUT_USER_AT_ECMR_BY_IDS = "/ecmrs/id/{ecmrId}/userid/{userId}/assign";
    public static final String PUT_USERS_AT_ECMR_BY_IDS = "/ecmrs/id/{ecmrId}/assignMulti";
    public static final String POST_GUEST_CARRIER = "/ecmrs-carrier/id/{ecmrId}";
    public static final String POST_GUEST_CARRIER_CHECK_TAN = "/ecmrs-carrier/id/{ecmrId}/check-tan/{tan}";
    public static final String PUT_CURRENTUSER_AT_ECMR_BY_ID = "/ecmrs/id/{id}/claim";
    public static final String GET_FIELDS = "/ecmrs/fields";
    public static final String POST_IMPORT = "/ecmrs/import";
    public static final String GET_FIELD_BY_ID = "/ecmrs/fields/{id}";
    public static final String POST_AUTOGENERATE_ECMR = "/generate";

    public static final String GET_EXPORT_ECMR = "/ecmrs/id/{id}/export";
    public static final String GET_PDF_ECMR = "/ecmrs/{id}/pdf";

    // Note: Only for database-implementation, to be removed
    public static final String TOKEN_BASE_PATH = "/api/v1/auth";
    public static final String POST_TOKEN = "/session";

}
