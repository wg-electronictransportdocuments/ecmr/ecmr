/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.controller;

import org.siliconeconomy.ecmr.service.models.BCBlockData;
import org.siliconeconomy.ecmr.service.models.BCMetaData;
import org.siliconeconomy.ecmr.service.models.EcmrFieldEntryData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * This class contains all relevant methods for interacting with the blockchain.
 * It's not meant to be call from outside the backend, thus doesn't have a REST-interface
 *
 */
@Configuration
@PropertySource("classpath:application.properties")
public class BCController {

    /**
     * Constant subdomain of BC-Microservice
     */
    private static final String SUBDOMAIN = "tokens/";
    /**
     * Object for creating requests to BC-Microservice
     */
    private RestTemplate restTemplate;
    /**
     * load BC Address depending on profile from properties file
     */
    @Value("${ecmr.bc.address}")
    private String bcAddress;


    private static final Logger LOGGER = LoggerFactory.getLogger(BCController.class);


    @PostConstruct
    public void init() {
        restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
    }


    /**
     * Helper function for loading values from properties
     *
     * @return Configurer Object
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigBC() {
        return new PropertySourcesPlaceholderConfigurer();
    }

    /**
     * Writes new Block to BC.
     *
     * @param ecmrId   ID of eCMR
     * @param content  Arraylist containing the relevant changes
     * @param metaData Metadata which is saved to the block
     * @return true if request was successful
     */
    public boolean write(String ecmrId, List<EcmrFieldEntryData> content, BCMetaData metaData) {

        // Transform contentmap to string
        var contentString = SignatureController.arraylistToString(content);
        // hash contentstring
        var hashBytes = SignatureController.hashSHA256(contentString);
        var hashString = new String(hashBytes, StandardCharsets.UTF_8);

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);


        BCBlockData blockData = new BCBlockData(ecmrId, hashString, metaData);

        try {
            // Prepare request
            HttpEntity<String> entity = new HttpEntity<>(blockData.toString(), headers);

            // separate thread for avoiding BC bottleneck
            Thread bcWriteThread = new Thread(() -> {
                ResponseEntity<BCBlockData> responseEntity = null;
                ResponseEntity<String> r = null;
                // Send request
                if(metaData.ecmrVersion == 1) {
                    responseEntity = restTemplate.exchange(bcAddress + SUBDOMAIN, HttpMethod.POST ,entity, BCBlockData.class);
                }
                else
                {
                    r = restTemplate.exchange(bcAddress + SUBDOMAIN + ecmrId+"/",HttpMethod.PATCH ,entity, String.class);
                }
                if( (responseEntity != null && responseEntity.getStatusCode() == HttpStatus.CREATED) ||
                        (r != null && r.getStatusCode() == HttpStatus.OK))
                {
                    LOGGER.info("[BCController] Successful write on BC for eCMR-ID "+ecmrId);

                }
            });
            bcWriteThread.start();
            return true;
        }
        catch (Exception e) {
            LOGGER.error("[BCController] Failed to write on BC for eCMR-ID "+ecmrId + "("+e.getMessage()+")");
            return false;
        }
    }

}
