/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.controller;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.jasperreports.JasperService;
import org.siliconeconomy.ecmr.service.models.EcmrParticipant;
import org.siliconeconomy.ecmr.service.models.EcmrUpdateObject;
import org.siliconeconomy.ecmr.service.models.GuestCarrier;
import org.siliconeconomy.ecmr.service.services.ConnectionService;
import org.siliconeconomy.ecmr.service.services.EcmrManagementService;
import org.siliconeconomy.ecmr.service.services.EcmrUserService;
import org.siliconeconomy.ecmr.service.services.ExternalEcmrService;
import org.siliconeconomy.ecmr.service.services.tan.MessageProviderException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

/**
 * This controller provides the endpoint used to handle eCMR management in the eCMR project.
 *
 */

@RestController
@CrossOrigin(allowedHeaders = "*", origins = "*", maxAge = 3600)
@RequestMapping(EcmrEndpoints.ECMR_BASE_PATH)
public class EcmrManagementController {

    @Autowired
    public ConnectionService connectionService;

    @Autowired
    private EcmrManagementService ecmrManagementService;

    @Autowired
    private ExternalEcmrService externalEcmrService;

    @Autowired
    private JasperService jasperService;

    @Autowired
    private EcmrUserService ecmrUserService;

    /**
     * Returns all top level information (no field values) of all eCMR, that the user is connected to as a
     * list (always the highest version number)
     *
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_ECMRS,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getEcmrs(@RequestParam("isArchive") boolean isArchive) {

        return ecmrManagementService.getEcmrs(isArchive, true);
    }

    /**
     * Returns all top level information (no field values) of all eCMR, that the user is connected to as a
     * list (always the highest version number)
     *
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_TEMPLATES,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTemplates() {
        return ecmrManagementService.getTemplates();
    }

    /**
     * creating a new eCMR.
     *
     * @param jsonBody JSON Request Body containing relevant content for creating eCMR
     * @return Response including code and message
     */
    @PostMapping(value = EcmrEndpoints.POST_ECMR,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> newEcmr(
            @RequestBody Map<String, Object> jsonBody) {

        return ecmrManagementService.newEcmr(jsonBody, true);
    }

    /**
     * creating a new template based of an ecmr.
     *
     * @param jsonBody JSON Request Body containing relevant content for creating eCMR
     * @return Response including code and message
     */
    @PostMapping(value = EcmrEndpoints.POST_TEMPLATE,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> newTemplate(
            @RequestBody Map<String, Object> jsonBody) {

        return ecmrManagementService.newEcmr(jsonBody, false);
    }

    /**
     * Returns all information of a existing eCMR with {id}. If version is not set, the latest version will be returned.
     *
     * @param id      of the eCMR
     * @param version of the eCMR
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_ECMR_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getEcmrById(
            @PathVariable String id, @RequestParam(required = false) String version) {

        if (StringUtils.isNotBlank(version)) {
            return ecmrManagementService.getEcmrbyIdandVersion(id, version);
        } else {
            return ecmrManagementService.getEcmrbyId(id);
        }
    }

    @GetMapping(value = EcmrEndpoints.GET_ECMR_BY_ID_FOR_CARRIER,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getEcmrByIdAndTan(@PathVariable String id, @PathVariable String tan) {
        return ecmrManagementService.getEcmrbyIdAndTan(id, tan);
    }

    /**
     * Returns all information of an existing Template with {id}.
     *
     * @param id      of the Template
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_TEMPLATE_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getTemplateById(
            @PathVariable Long id) {
        return ecmrManagementService.getTemplateById(id);
    }

    /**
     * Update a eCMR for a given {id} (by increasing its version number) and the status
     *
     * @param id       of the eCMR
     * @param isEcmr Determines, if an eCMR or Template should be updated
     * @param jsonBody JSON Request Body containing relevant content for updating eCMR
     * @return Response including code and message
     */
    @PutMapping(value = EcmrEndpoints.PUT_ECMR_BY_ID,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateEcmrById(
            @PathVariable String id, @PathVariable boolean isEcmr, @RequestBody Map<String, Object> jsonBody) {

        return ecmrManagementService.updateEcmrById(id, isEcmr, jsonBody);
    }

    /**
     * Interface for deleting a eCMR by id
     *
     * @param id ID for deleting eCMR
     * @return Response including code and message
     */
    @DeleteMapping(
            value = EcmrEndpoints.DELETE_ECMR_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> deleteEcmrById(@PathVariable String id) {
        return ecmrManagementService.deleteEcmrbyId(id, true);
    }

    /**
     * Delete a Template by id
     *
     * @param id ID of Template to delete
     * @return Response including code and message
     */
    @DeleteMapping(
            value = EcmrEndpoints.DELETE_TEMPLATE_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> deleteTemplateById(@PathVariable String id) {
        return ecmrManagementService.deleteEcmrbyId(id, false);
    }

    /**
     * Returns all top level information (no field values) of all versions of a eCMR that the user is connected to as a list.
     *
     * @param id of the eCMR to read the eCMR history for
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_ECMR_HISTORY_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getEcmrHistory(
            @PathVariable String id) {

        return ecmrManagementService.getEcmrHistoryById(id);
    }

    /**
     * Creates a new connection eCMR↔Users and updates the eCMR status.
     *
     * @param ecmrId id of eCMR
     * @param jsonBody contains the relevant information like userId
     * @return Response including code and message
     */
    @PutMapping(value = EcmrEndpoints.PUT_USERS_AT_ECMR_BY_IDS,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> assignMultiShare(
            @PathVariable String ecmrId, @RequestBody Map<String, Object> jsonBody) {

        return ecmrManagementService.setUsersAtEcmr(ecmrId, jsonBody);
    }

    /**
     * Creates a new connection eCMR↔User and updates the eCMR status.
     *
     * @param userId id of user
     * @param ecmrId id of eCMR
     * @return Response including code and message
     */
    @PutMapping(value = EcmrEndpoints.PUT_USER_AT_ECMR_BY_IDS,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> assign(
            @PathVariable String userId, @PathVariable String ecmrId) {

        return ecmrManagementService.setUserAtEcmr(userId, ecmrId);
    }

    /**
     * Creates a new connection eCMR↔User and updates the eCMR status.
     *
     * @param id id of eCMR
     * @return Response including code and message
     */
    @PutMapping(value = EcmrEndpoints.PUT_CURRENTUSER_AT_ECMR_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> assign(
            @PathVariable String id) {

        return ecmrManagementService.setCurrentUserAtEcmr(id);
    }

    /**
     * Returns all field definitions that are used within a system
     *
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_FIELDS,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getFields() {

        return ecmrManagementService.getFields();
    }

    /**
     * Returns all field definitions for a single eCMR.
     *
     * @param id eCMR id
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_FIELD_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getFieldsById(
            @PathVariable String id) {

        return ecmrManagementService.getFieldsById(id);
    }

    /**
     * Interface for generating a eCMR by external systems e.g. WMS/TMW
     *
     * @param jsonBody json containing relevant informations
     * @return Response including code and message
     */
    @PostMapping(value = EcmrEndpoints.POST_AUTOGENERATE_ECMR,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> newEcmrAutogenerate(
            @RequestBody Map<String, Object> jsonBody) {
        return ecmrManagementService.autogenerateEcmr(jsonBody);
    }

    /**
     * Interface for exporting eCMR
     */
    @PostMapping(value = EcmrEndpoints.GET_EXPORT_ECMR,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE
    )
    @ResponseBody
    public ResponseEntity<Object> exportEcmr(@PathVariable String id, @RequestBody EcmrParticipant ecmrParticipant) {
        return externalEcmrService.exportEcmr(id, ecmrParticipant);
    }

    @PostMapping(value = EcmrEndpoints.POST_IMPORT,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> importEcmr(@RequestBody Map<String, Object> jsonBody) {
        return externalEcmrService.importEcmr(jsonBody);
    }

    @PutMapping(value = EcmrEndpoints.EXTERNAL_PATH + EcmrEndpoints.PUT_ECMR_BY_ID,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity<Object> updateEcmrByExternal(@PathVariable String id,
            @RequestBody EcmrUpdateObject ecmrUpdateObject) throws NoSuchAlgorithmException {
        return externalEcmrService.updateEcmrByExternal(id, ecmrUpdateObject);
    }

    @GetMapping(value = EcmrEndpoints.GET_PDF_ECMR)
    public ResponseEntity<StreamingResponseBody> downloadEcmrPdfFile(@PathVariable("id") String id) {
        try {
            byte[] ecmrReportData = this.jasperService.createJasperReportForEcmr(id);
            StreamingResponseBody streamingResponseBody = outputStream -> {
                try (InputStream inputStream = new ByteArrayInputStream(ecmrReportData)) {
                    inputStream.transferTo(outputStream);
                } catch (Exception e) {
                    throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
                }
            };
            return ResponseEntity.ok()
                    .contentLength(ecmrReportData.length)
                    .contentType(MediaType.APPLICATION_PDF)
                    .body(streamingResponseBody);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @PostMapping(value = EcmrEndpoints.POST_GUEST_CARRIER)
    @ResponseBody
    public ResponseEntity<Object> newEcmrAutogenerate(@PathVariable("ecmrId") String ecmrId,
            @RequestBody GuestCarrier guestCarrierModel) {
        try {
            this.ecmrUserService.addGuestCarrier(ecmrId, guestCarrierModel.getFirstName(), guestCarrierModel.getLastName(), guestCarrierModel.getCompany(), guestCarrierModel.getPhoneNumber());
            return ResponseEntity.ok().build();
        } catch (MessageProviderException e) {
            return ResponseEntity.internalServerError().build();
        }
    }

    @PostMapping(value = EcmrEndpoints.POST_GUEST_CARRIER_CHECK_TAN)
    @ResponseBody
    public ResponseEntity<Boolean> newEcmrAutogenerate(@PathVariable("ecmrId") String ecmrId, @PathVariable("tan") String tan) {
        if (this.ecmrUserService.isTanCorrect(ecmrId, tan)) {
            return ResponseEntity.ok(true);
        } else {
            return ResponseEntity.ok(false);
        }
    }
}
