/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.controller;

import org.bouncycastle.util.io.pem.PemObject;
import org.bouncycastle.util.io.pem.PemWriter;
import org.siliconeconomy.ecmr.service.models.EcmrFieldEntryData;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.io.IOException;
import java.io.StringWriter;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.*;
import java.util.stream.IntStream;

/**
 * This class contains all relevant methods for interacting creating and verifying signatures
 *
 *
 * To be assured that the checksum of an eCMR is the same in the front-end, back-end and blockchain, its calculation
 * must remain constant, especially for the exchange between different eCMR instances.
 *
 * For this purpose, a string is first constructed from the eCMR document. For this, the IDs of the respective field
 * are concatenated with the respective value in turn. A comma is inserted both between and at the end of each key-value
 * pair to separate them. This is applied to all transmitted fields except the signature fields.
 * The calculation of the hash must also be performed in a uniform manner. The asymmetric signature procedure ECDSA
 * with the SHA256 hash function is used for this purpose. For the respective public and private keys, the
 * secp256r1 curve is used for generation. This procedure is already implemented in the default for the eCMR service.
 * Individualizing the procedure on your own can lead to incompatibility between components and instances.
 **
 */
@Configuration
@PropertySource("classpath:application.properties")
public class SignatureController {

  /** FieldIds which require a signature */
  public static final Set<String> SIGNATURE_FIELD_IDS = Set.of("69", "70", "72");

  /**
   * Converts int-based representation of signature to string one
   *
   * @param arr Array with int values representing signature
   * @return Base64 representation of signature
   */
  public static String signatureArrayToSignatureString(List<Integer> arr) {
    return convertToBase64(arr.stream().mapToInt(i -> i).toArray());
  }

  /**
   * Encodes array of ints to Base64
   *
   * @param ints array of ints
   * @return String in Base64 format
   */
  public static String convertToBase64(int[] ints) {
    var buf = ByteBuffer.allocate(ints.length);
    IntStream.of(ints).forEach(i -> buf.put((byte) i));
    return Base64.getEncoder().encodeToString(buf.array());
  }

  /**
   * Creates string representation of ECMR fields, which is used to create signature
   * @param list Arraylist of ECMRfieldentries
   * @return concatenated string
   */
  public static String arraylistToString(List<EcmrFieldEntryData> list) {
    var result = new StringBuilder();
    for (EcmrFieldEntryData entry :list) {
      // if field contains "Signature" don't use
      if (SIGNATURE_FIELD_IDS.contains(String.valueOf(entry.getFieldId()))) continue;
      result.append(entry.getFieldId()).append(",").append(entry.getEntryId()).append(",").append(entry.getValue());
    }
    return result.toString();
  }

  /**
   * Creates string representation of ECMR fields, which is used to create signature
   * @param m Map of entries
   * @return concatenated string
   */
  public static String mapToString(Map<String, Object> m) {
    var result = new StringBuilder();
    for (Map.Entry<String,Object> entry :m.entrySet()) {
      // if field contains "Signature" don't use
      if (SIGNATURE_FIELD_IDS.contains(String.valueOf(entry.getKey()))) continue;
      // iterate over position list
      if(entry.getKey().equals("positions"))
      {
        ArrayList<Map<String, String>> positionsList = (ArrayList<Map<String, String>>) entry.getValue();
        if(positionsList.isEmpty()) continue;
        for(Map<String, String> posMap : positionsList)
        {
          for(Map.Entry<String, String> e:  posMap.entrySet())
          {
            result.append(e.getKey()).append(e.getValue());
          }
        }
      }
      else {
        result.append(entry.getKey()).append((String) m.get(entry.getKey()));
      }
    }
    return result.toString();
  }


  /***
   * Checks whether given signature matches content + public key
   * Assumes EC DSA based on SHA-256 Hashing
   *
   * @param signature signature in DER style
   * @param msg the message as string
   * @param pubKey pem-key as string
   * @return true if valid signature, false otherwise
   */
  public static boolean verify(String signature, String msg, String pubKey) {

    try {

      // cut footer, header and control characters from PEM Key
      pubKey =
              pubKey
                      .replace("-----BEGIN PUBLIC KEY-----", "")
                      .replace("\n", "")
              .replaceAll(System.lineSeparator(), "")
              .replace("-----END PUBLIC KEY-----", "")
              .replace(" ", "+");

      // initialize Signature object
      var ecdsaVerify = Signature.getInstance("SHA256withECDSA");
      // decode
      var decodedPEM = Base64.getDecoder().decode(pubKey);
      var decodedSig = Base64.getDecoder().decode(signature);
      // Define key
      var newPublicKeySpec = new X509EncodedKeySpec(decodedPEM);
      var keyFactory = KeyFactory.getInstance("EC");
      var publicKey = keyFactory.generatePublic(newPublicKeySpec);

      // Verify and return result
      ecdsaVerify.initVerify(publicKey);
      ecdsaVerify.update(msg.getBytes());
      return ecdsaVerify.verify(decodedSig);
    } catch (NoSuchAlgorithmException
        | InvalidKeySpecException
        | InvalidKeyException
        | SignatureException noSuchAlgorithmException) {
      return false;
    }
  }

  /**
   * Generates pair of Elliptic Curve private and public key for signing. Keys are generated on
   * secp256r1 curve.
   *
   * @return Keypair object containing ECDSA-Keypair
   * @throws InvalidAlgorithmParameterException thrown if algorithm is not correctly initialized
   * @throws NoSuchAlgorithmException thrown if algorithm is not found in java security library
   */
  public static KeyPair generateKeys()
      throws InvalidAlgorithmParameterException, NoSuchAlgorithmException {
    var ecSpec = new ECGenParameterSpec("secp256r1");
    var g = KeyPairGenerator.getInstance("EC");
    g.initialize(ecSpec, new SecureRandom());

    return g.generateKeyPair();
  }

  /**
   * Create signature for given input based on private key. For hashing SHA256 is used, for actual
   * signing ECDSA
   *
   * @param kp EC-Keypair
   * @param msg Message which should be signed
   * @return Arraylist representing the signature
   * @throws NoSuchAlgorithmException thrown if algorithm is not found in java security library
   * @throws SignatureException thrown if signing fails
   * @throws InvalidKeyException thrown if key is invalid
   */
  public static List<Integer> sign(KeyPair kp, String msg)
      throws NoSuchAlgorithmException, SignatureException, InvalidKeyException {
    var ecdsaSign = Signature.getInstance("SHA256withECDSA");
    // use private key for signing
    ecdsaSign.initSign(kp.getPrivate());
    // set bytes to signature object
    ecdsaSign.update(msg.getBytes(StandardCharsets.UTF_8));
    var signature = ecdsaSign.sign();
    // Transform to array object
    List<Integer> sig = new ArrayList<>();
    for (byte b : signature) {
      sig.add((int) b);
    }
    return sig;
  }

  /**
   * Converts PublicKey object to string representation in PEM-format.
   *
   * @param pb publickey to be transformed
   * @return PEM-String
   * @throws IOException thrown if encoding fails
   */
  public static String toPem(PublicKey pb) throws IOException {
    var writer = new StringWriter();
    var pemWriter = new PemWriter(writer);
    pemWriter.writeObject(new PemObject("PUBLIC KEY", pb.getEncoded()));
    pemWriter.flush();
    pemWriter.close();
    return writer.toString();
  }

  /**
   * Hashes content with SHA-256
   *
   * @param s String to hash
   * @return byte array representing the hash
   */
  public static byte[] hashSHA256(String s) {
    try {
      var md = MessageDigest.getInstance("SHA-256");
      return md.digest(s.getBytes(StandardCharsets.UTF_8));
    } catch (NoSuchAlgorithmException nsae) {
      return (new byte[0]);
    }
  }

  /**
   * Helper function for loading values from properties
   * @return Configurer Object
   */
  @Bean
  public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigSig() {
    return new PropertySourcesPlaceholderConfigurer();
  }
}
