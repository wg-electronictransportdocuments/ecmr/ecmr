/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.controller;


import org.apache.commons.lang3.StringUtils;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;


/**
 * This controller provides the endpoint used to handle user management in the eCMR projekt.
 *
 */

@RestController
@CrossOrigin(allowedHeaders = "*", origins = "*", maxAge = 3600)
@RequestMapping(EcmrEndpoints.USER_BASE_PATH)
public class UserManagementController {

    @Autowired
    private UserManagementService userManagementService;

    /**
     * Returns the username string for a given user.
     *
     * @param id UserId
     * @return Response including code and message
     */
    @GetMapping(value = EcmrEndpoints.GET_USER_BY_ID,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUser(@PathVariable String id) {
        ResponseEntity<Object> response;
        // Check if UserId was passed
        if (StringUtils.isBlank(id)) {
            response = ExceptionHandling.setBadRequestError();
            return response;
        } else {
            try {
                UserFacade userById = userManagementService.getUserById(id);
                if (userById != null) {
                    response = new ResponseEntity<>(userById, HttpStatus.OK);
                } else {
                    // resource doesn't exist -> 404
                    response = ExceptionHandling.setNotFoundError();
                    return response;
                }
            } catch (Exception e) {
                // Internal error occurred -> 500
                response = ExceptionHandling.setInternalError();
                return response;
            }
        }
        return response;
    }

    /**
     * Returns the contacts of a  user.
     *
     * @return Response including code and message
     */
    @GetMapping(EcmrEndpoints.GET_USERS_CONTACTS)
    public ResponseEntity<Object> getContactsForUser() {

        return userManagementService.getContactsForUser();
    }

    /**
     * Returns all users.
     *
     * @return Response including code and message
     */
    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserFacade>> getAll() {

        return new ResponseEntity<>(userManagementService.getEcmrUsers(), HttpStatus.OK);
    }

    /**
     * Returns the user fields of a user.
     *
     * @return Response including code and message
     */
    @GetMapping(EcmrEndpoints.GET_USERS_FIELDS)
    public ResponseEntity<Object> getFieldsForUser() {

        return userManagementService.getFieldsForUser();
    }

    /**
     * Set pubKey to user connection
     *
     * @return Response including message
     */
    @PostMapping(value = EcmrEndpoints.POST_USER_PUBKEY, consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> setPubKeyToUserConnection(@RequestBody Map<String, Object> jsonBody) {

        return userManagementService.setPubKeyToUserConnection(
                userManagementService.getToken(),
                userManagementService.getUserIdFromToken(),
                jsonBody.get("pubKey").toString()
        );
    }
}
