/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.data;

import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * This class contains instructions for loading initial users into the database through JPA. It's
 * executed automatically when build with test profile.
 *
 */
@Component
@Order(1)
public class EcmrDataInjector implements CommandLineRunner {


    /**
     * relevant repositories to interact with DB
     */

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    UserManagement userManagement;

    @Autowired
    StatusService statusService;

    @Autowired
    RoleService roleService;

    /**
     * Method which inserts initial values. Gets called automatically with ECMRApplication-main()
     *
     * @param args optional parameters, ignored
     */
    @Override
    public void run(String... args) {
        roleService.loadRoles();
        statusService.generateStatus();
        statusService.getSignatureFieldStatus();
        userManagementService.getEcmrUsers();
    }
}
