/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.exception;

import org.siliconeconomy.ecmr.service.models.ResponseData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;

/**
 * Generates responses in case an exception occurs during processing the request of a user.
 *
 */
@RestControllerAdvice
public class ExceptionHandling {

  private static final Logger LOGGER = LoggerFactory.getLogger(ExceptionHandling.class);

  /**
   * Generates 400 BAD_REQUEST response
   *
   * @return response object
   */
  public static ResponseEntity<Object> setBadRequestError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(new ResponseData("Invalid parameters"), HttpStatus.BAD_REQUEST);
    LOGGER.info("Received bad request");
    return resp;
  }

  /**
   * Generates 500 INTERNAL_SERVER_ERROR response
   *
   * @return response object
   */
  public static ResponseEntity<Object> setInternalError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(
                    new ResponseData("Internal error"), HttpStatus.INTERNAL_SERVER_ERROR);
    LOGGER.info("Internal error occured");
    return resp;
  }

  /**
   * Generates 401 UNAUTHORIZED response (for invalid credentials)
   *
   * @return response object
   */
  public static ResponseEntity<Object> setUnauthorizedError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(
                    new ResponseData("Invalid credentials"), HttpStatus.UNAUTHORIZED);
    LOGGER.info("Received login attempt with invalid credentials");
    return resp;
  }

  /**
   * Generates 401 UNAUTHORIZED response (for invalid credentials)
   *
   * @return response object
   */
  public static ResponseEntity<Object> setCorruptedSignatureError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(
                    new ResponseData("Corrupted Signature"), HttpStatus.UNAUTHORIZED);
    LOGGER.info("Received update attempt with invalid signature");
    return resp;
  }

  /**
   * Generates 401 UNAUTHORIZED response (for expired token)
   *
   * @return response object
   */
  public static ResponseEntity<Object> setTokenExpiredError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(new ResponseData("Token expired"), HttpStatus.UNAUTHORIZED);
    LOGGER.info("Received request with expired token");
    return resp;
  }

  /**
   * Generates 404 NOT_FOUND response
   *
   * @return response object
   */
  public static ResponseEntity<Object> setNotFoundError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(
                    new ResponseData("Resource does not exist"), HttpStatus.NOT_FOUND);
    LOGGER.info("Requested resource wasn't found or isn't available to user");
    return resp;
  }

  /**
   * Generates 403 FORBIDDEN response (when user is not allowed to access resource)
   *
   * @return response object
   */
  public static ResponseEntity<Object> setUserNotAllowedError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(new ResponseData("User not allowed"), HttpStatus.FORBIDDEN);
    LOGGER.info("User has no permission to access requested resource");
    return resp;
  }

  /**
   * Generates 403 FORBIDDEN response (when user does not have rights to change ECMR)
   *
   * @return response object
   */
  public static ResponseEntity<Object> setNotAllowedByStatusError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(
                    new ResponseData("Changes prohibited due to ECMR status"), HttpStatus.FORBIDDEN);
    LOGGER.info("Changes prohibited due to ECMR status");
    return resp;
  }

  /**
   * Generates 400 BAD_REQUEST response
   *
   * @return response object
   */
  public static ResponseEntity<Object> setMandatoryMissingError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(
                    new ResponseData("Bad format. Mandatory field is missing"), HttpStatus.BAD_REQUEST);
    LOGGER.info("At least one mandatory ECMR field is missing");
    return resp;
  }

  /**
   * Generates 409 CONFLICT response
   *
   * @return response object
   */
  public static ResponseEntity<Object> setUserConflictError() {
    ResponseEntity<Object> resp =
            new ResponseEntity<>(
                    new ResponseData("User has already been associated"), HttpStatus.CONFLICT);
    LOGGER.info("User has already been associated");
    return resp;
  }
}
