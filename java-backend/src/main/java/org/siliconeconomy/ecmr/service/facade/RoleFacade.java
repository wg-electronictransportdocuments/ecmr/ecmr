/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.facade;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.siliconeconomy.ecmr.service.models.Role;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class represents an encapsulation of a Keycloak role object. It is used to automatically
 * create a JSON-response that is returned from backend service functions.
 *
 */
@NoArgsConstructor
public class RoleFacade {

    @Getter
    @Setter
    private String id;
    @Getter
    @Setter
    private String name;
    @Getter
    @Setter
    private List<String> mandatoryFields;
    @Getter
    @Setter
    private List<String> optionalFields;
    @Getter
    @Setter
    private Boolean canDelete;
    @Getter
    @Setter
    private List<String> canChange;
    @Getter
    @Setter
    private List<String> canShare;
    @Getter
    @Setter
    private Boolean isSuperuser;
    @Getter
    @Setter
    private Boolean canGenerate;

    public RoleFacade(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public RoleFacade(JSONObject role) {
        try {
            this.name = (String) role.get("name");

            List<String> mf = new ArrayList<>();
            List<String> of = new ArrayList<>();
            for (Long m :(List<Long>) role.get("mandatoryFields")){
                mf.add(""+m);
            }
            for (Long o :(List<Long>) role.get("optionalFields")){
                of.add(""+o);
            }

            this.mandatoryFields =  new ArrayList<>(mf);
            this.optionalFields =  new ArrayList<>(of);
            this.canDelete = (Boolean) role.get("canDelete");
            this.canChange =  new ArrayList<>((List<String>) role.get("canChange"));
            this.canShare =  new ArrayList<>((List<String>) role.get("canShare"));
            this.isSuperuser = (Boolean) role.get("isSuperuser");
            this.canGenerate = (Boolean) role.get("canGenerate");
        }catch(Exception e){
            // ignored
        }
    }

    public RoleFacade(Role role) {
        this.id = Long.toString(role.getId());
        this.name = role.getName();
        this.canGenerate = role.getCanGenerate();
        this.canDelete = role.getCanDelete();
        this.canChange = role.getCanChange();
        this.isSuperuser = role.getIsSuperuser();
        this.mandatoryFields = role.getMandatoryFields().stream().map(field -> Long.toString(field.getFieldId())).toList();
        this.optionalFields = role.getOptionalFields().stream().map(field -> Long.toString(field.getFieldId())).toList();
    }
}
