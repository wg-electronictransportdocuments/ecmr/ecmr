/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.facade;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.json.simple.JSONObject;
import org.keycloak.representations.idm.UserRepresentation;
import org.siliconeconomy.ecmr.service.models.Company;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * This class represents an encapsulation of a Keycloak user object.
 *
 */
@NoArgsConstructor
public class UserFacade {

    @Getter
    @Setter
    private String id;

    @Getter
    @Setter
    private String userName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    private Company userCompany;

    @Getter
    @Setter
    private List<String> userRoles;

    @Getter
    @Setter
    private Map<String, List<String>> attributes;

    public UserFacade(UserRepresentation userRepresentation, List<String> userRoles, Company userCompany) {

        this.id = userRepresentation.getId();
        this.userName = userRepresentation.getUsername();
        this.email = userRepresentation.getEmail();
        this.userRoles = userRoles;
        this.userCompany = userCompany;
        this.attributes = userRepresentation.getAttributes();

    }

    public UserFacade(UserFacade userFacade, List<String> userRoles, Company userCompany) {

        this.id = userFacade.getId();
        this.userName = userFacade.getUserName();
        this.email = userFacade.getEmail();
        this.userRoles = userRoles;
        this.userCompany = userCompany;
        this.attributes = userFacade.getAttributes();

    }

    public UserFacade(String id, String userName, String email, List<String> userRoles, Company userCompany, Map<String, List<String>> attributes) {

        this.id = id;
        this.userName = userName;
        this.email = email;
        this.userRoles = userRoles;
        this.userCompany = userCompany;
        this.attributes = attributes;

    }

    public UserFacade(JSONObject user) {
        try {
            this.id = UUID.randomUUID().toString(); // temp id, external has prio
            this.userName = (String) user.get("userName");
            this.email = (String) user.get("email");
            this.userRoles = (List<String>)user.get("roles");
            JSONObject company = (JSONObject) user.get("company");
            this.userCompany = new Company((String)company.get("name"),(String)company.get("location")); //maybe load from repo
            Map<String,Object> attributes = (Map<String,Object>) user.get("attributes");
            Map<String,List<String>> attributeMap = new HashMap<String,List<String>>();
            for(String key : attributes.keySet()){
                attributeMap.put(key, (List<String>) attributes.get(key)); // todo: convert more clever
            }
            this.attributes = attributeMap;
        }catch(Exception e){
            // ignored
        }
    }
}
