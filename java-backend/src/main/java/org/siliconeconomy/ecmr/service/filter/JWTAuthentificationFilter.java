/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.filter;

import io.jsonwebtoken.*;
import io.jsonwebtoken.security.SignatureException;
import org.apache.commons.io.IOUtils;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Filter for checking incoming requests.
 *
 */
public class JWTAuthentificationFilter extends OncePerRequestFilter {

    private static final String HEADER = "Authorization";

    /**
     * Check if request carries valid jwt-token
     * @param request incling request
     * @param response response object
     * @param chain Defined Chain of filtering
     * @throws ServletException
     * @throws IOException
     */
    @Override
    protected void doFilterInternal(
            HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        try {
            if (checkJWTToken(request)) {
                Claims claims = validateToken(request);
                setUpSpringAuthentication(claims);

                // maybe not so nice, but important for the DatabaseUserManagement.class
                SecurityContext sec = SecurityContextHolder.getContext();
                AbstractAuthenticationToken auth = (AbstractAuthenticationToken)sec.getAuthentication();
                HashMap<String, Object> info = new HashMap<String, Object>();
                info.put("body", IOUtils.toString(request.getReader()));
                info.put("jwToken", request.getHeader(HEADER));
                auth.setDetails(info);

            } else {
                SecurityContextHolder.clearContext();
            }
            chain.doFilter(request, response);
        } catch (ExpiredJwtException
                | UnsupportedJwtException
                | MalformedJwtException
                | SignatureException e) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            response.getOutputStream().println(constructInvalidTokenMsg(request));
            response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        }
    }

    private Claims validateToken(HttpServletRequest request) {
        String jwtToken = request.getHeader(HEADER);
        if (jwtToken == null || jwtToken.equals("")) {
            throw new SignatureException("Token is empty");
        }
        return Jwts.parserBuilder()
                .setSigningKey(DatatypeConverter.parseBase64Binary(UserManagementService.SECRETKEY))
                .build()
                .parseClaimsJws(jwtToken)
                .getBody();
    }

    /**
     * Authentication method in Spring flow
     *
     * @param claims
     */
    private void setUpSpringAuthentication(Claims claims) {
        @SuppressWarnings("unchecked")
        List<String> authorities = (List<String>) claims.get("authorities");
        UsernamePasswordAuthenticationToken auth =
                new UsernamePasswordAuthenticationToken(
                        claims.getSubject(),
                        null,
                        authorities.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList()));
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
    private boolean checkJWTToken(HttpServletRequest request) {
        return (request.getHeader(HEADER) != null);
    }

    private String constructInvalidTokenMsg(HttpServletRequest request) {
        String result = "{\n\"timestamp\": \"";
        // timestamp
        result += (java.time.ZonedDateTime.now()).toString() + "\",";
        // status
        result += "\n\"status\": 401,";
        result += "\n\"error\":\"Unauthorized\",";
        result += "\n\"message\": \"Invalid Token\",";
        result += "\n\"path\":\"" + request.getRequestURI() + "\"\n";
        result += "\n}";
        return result;
    }
}
