/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.jasperreports;

import java.util.Map;

import org.siliconeconomy.ecmr.service.models.Ecmr;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EcmrReportData {
    private Ecmr ecmr;
    private Map<String,Object> ecmrFieldEntries;
}
