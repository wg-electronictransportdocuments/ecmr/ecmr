/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.jasperreports;

import java.awt.image.BufferedImage;

import lombok.Data;

@Data
public class GlassSignature {
    private String signatureMetaData;
    private BufferedImage glassSignature;
}
