/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.jasperreports;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ItemFields {
    private String markingText;
    private String itemQuantity;
    private String typeText;
    private String barcodeNumber;
    private String identificationText;
    private String weightKG;
    private String volume;
}
