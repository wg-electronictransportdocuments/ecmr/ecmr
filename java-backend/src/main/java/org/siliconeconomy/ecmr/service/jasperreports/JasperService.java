/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.jasperreports;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.siliconeconomy.ecmr.service.models.Ecmr;
import org.siliconeconomy.ecmr.service.services.EcmrFieldService;
import org.siliconeconomy.ecmr.service.services.EcmrService;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
@RequiredArgsConstructor
@Log4j2
public class JasperService {

    private final EcmrService ecmrService;
    private final EcmrFieldService ecmrFieldService;
    public byte[] createJasperReportForEcmr(String id) {
        try {
            InputStream ecmrReportStream = getClass().getResourceAsStream("/reports/ecmr.jrxml");
            JasperReport jasperReport = JasperCompileManager.compileReport(ecmrReportStream);
            return JasperRunManager.runReportToPdf(jasperReport, setEcmrParameters(id), new JREmptyDataSource());
        } catch (JRException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private HashMap<String, Object> setEcmrParameters(String id) throws IOException {
        EcmrReportData ecmrReportData = getAllEcmrData(id);
        JRBeanCollectionDataSource fields = setItemFieldsData((List<HashMap<String,String>>) ecmrReportData.getEcmrFieldEntries().get("positions"));
        HashMap<String, Object> parameters = new HashMap<>();
        //items
        parameters.put("itemList",fields);
        //ecmr Meta Data
        parameters.put("ecmrID", ecmrReportData.getEcmr().getEcmrId());
        parameters.put("status", ecmrReportData.getEcmr().getStatus());
        parameters.put("lastUser", ecmrReportData.getEcmr().getLastUserId());
        parameters.put("version", String.valueOf(ecmrReportData.getEcmr().getVersion()));
        parameters.put("last_updated", timestampConverter(ecmrReportData.getEcmr().getLastUpdated()));
        parameters.put("created", timestampConverter(ecmrReportData.getEcmr().getCreated()));
        parameters.put("referenceID",ecmrReportData.getEcmrFieldEntries().get("75"));
        //sender data
        parameters.put("senderName", ecmrReportData.getEcmrFieldEntries().get("1"));
        parameters.put("senderPersonName", ecmrReportData.getEcmrFieldEntries().get("2"));
        parameters.put("senderStreetName", ecmrReportData.getEcmrFieldEntries().get("3"));
        parameters.put("senderPostCode", ecmrReportData.getEcmrFieldEntries().get("4"));
        parameters.put("senderCityName", ecmrReportData.getEcmrFieldEntries().get("5"));
        parameters.put("senderCountryName", ecmrReportData.getEcmrFieldEntries().get("6"));
        //consignee Data
        parameters.put("consigneeName", ecmrReportData.getEcmrFieldEntries().get("7"));
        parameters.put("consigneePersonName", ecmrReportData.getEcmrFieldEntries().get("8"));
        parameters.put("consigneeStreetName", ecmrReportData.getEcmrFieldEntries().get("10"));
        parameters.put("consigneePostCode", ecmrReportData.getEcmrFieldEntries().get("9"));
        parameters.put("consigneeCityName", ecmrReportData.getEcmrFieldEntries().get("11"));
        parameters.put("consigneeCountryName", ecmrReportData.getEcmrFieldEntries().get("13"));
        //taking over The goods
        parameters.put("goodsPlace", ecmrReportData.getEcmrFieldEntries().get("14"));
        parameters.put("goodsArrivalTime", ecmrReportData.getEcmrFieldEntries().get("15"));
        parameters.put("goodsDepartureTime", ecmrReportData.getEcmrFieldEntries().get("16"));
        parameters.put("deliveryPlace", ecmrReportData.getEcmrFieldEntries().get("17"));
        parameters.put("deliveryOpenHours", ecmrReportData.getEcmrFieldEntries().get("18"));
        parameters.put("InstructionDescription", ecmrReportData.getEcmrFieldEntries().get("19"));
        //carrier Data
        parameters.put("carrierName", ecmrReportData.getEcmrFieldEntries().get("21"));
        parameters.put("carrierPersonName", ecmrReportData.getEcmrFieldEntries().get("22"));
        parameters.put("carrierPostCode", ecmrReportData.getEcmrFieldEntries().get("23"));
        parameters.put("carrierStreetName", ecmrReportData.getEcmrFieldEntries().get("24"));
        parameters.put("carrierCityName", ecmrReportData.getEcmrFieldEntries().get("25"));
        parameters.put("carrierCountryCode", ecmrReportData.getEcmrFieldEntries().get("26"));
        parameters.put("carrierCountryName", ecmrReportData.getEcmrFieldEntries().get("27"));
        parameters.put("carrierLicensePlate", ecmrReportData.getEcmrFieldEntries().get("29"));
        //successive carrier Data
        parameters.put("successiveName", ecmrReportData.getEcmrFieldEntries().get("30"));
        parameters.put("successivePersonName", ecmrReportData.getEcmrFieldEntries().get("31"));
        parameters.put("successiveStreetName", ecmrReportData.getEcmrFieldEntries().get("33"));
        parameters.put("successivePostCode", ecmrReportData.getEcmrFieldEntries().get("32"));
        parameters.put("successiveCityName", ecmrReportData.getEcmrFieldEntries().get("34"));
        parameters.put("successiveCountryName", ecmrReportData.getEcmrFieldEntries().get("36"));
        parameters.put("successiveCountryCode", ecmrReportData.getEcmrFieldEntries().get("35"));
        parameters.put("successiveDate", ecmrReportData.getEcmrFieldEntries().get("37"));
        parameters.put("successiveSignature", ecmrReportData.getEcmrFieldEntries().get("38"));

        parameters.put("carrierRemarks", ecmrReportData.getEcmrFieldEntries().get("39"));
        parameters.put("carrierReservation", ecmrReportData.getEcmrFieldEntries().get("40"));
        parameters.put("specialAgreement", ecmrReportData.getEcmrFieldEntries().get("52"));
        //charges
        parameters.put("CarriageCharges", ecmrReportData.getEcmrFieldEntries().get("53"));
        parameters.put("CarriageCurrency", ecmrReportData.getEcmrFieldEntries().get("54"));
        parameters.put("CarriagePayer", ecmrReportData.getEcmrFieldEntries().get("55"));
        parameters.put("SupplementaryCharges", ecmrReportData.getEcmrFieldEntries().get("56"));
        parameters.put("SupplementaryCurrency", ecmrReportData.getEcmrFieldEntries().get("57"));
        parameters.put("SupplementaryPayer", ecmrReportData.getEcmrFieldEntries().get("58"));
        parameters.put("CustomDuties", ecmrReportData.getEcmrFieldEntries().get("59"));
        parameters.put("CustomsCurrency", ecmrReportData.getEcmrFieldEntries().get("60"));
        parameters.put("CustomsPayer", ecmrReportData.getEcmrFieldEntries().get("61"));
        parameters.put("OtherCharges", ecmrReportData.getEcmrFieldEntries().get("62"));
        parameters.put("OtherCurrency", ecmrReportData.getEcmrFieldEntries().get("63"));
        parameters.put("OtherPayer", ecmrReportData.getEcmrFieldEntries().get("64"));
        //customs
        parameters.put("usefulParticular", ecmrReportData.getEcmrFieldEntries().get("65"));
        parameters.put("cashOnDelivery", ecmrReportData.getEcmrFieldEntries().get("66"));
        //establishment and signatures
        parameters.put("EstablishedOn", ecmrReportData.getEcmrFieldEntries().get("67"));
        parameters.put("EstablishedIn", ecmrReportData.getEcmrFieldEntries().get("68"));
        if (!ecmrReportData.getEcmrFieldEntries().get("69").toString().isEmpty()) {
            parameters.put("senderSignature", keySignatureStringBuilder((String) ecmrReportData.getEcmrFieldEntries().get("69")));
        }
        if (!ecmrReportData.getEcmrFieldEntries().get("69").toString().isEmpty()) {
            parameters.put("carrierSignature", keySignatureStringBuilder((String) ecmrReportData.getEcmrFieldEntries().get("70")));
        }
        parameters.put("goodPlace", ecmrReportData.getEcmrFieldEntries().get("74"));
        parameters.put("goodsDate", ecmrReportData.getEcmrFieldEntries().get("71"));
        parameters.put("nonContractualReservations", ecmrReportData.getEcmrFieldEntries().get("73"));
        if (!ecmrReportData.getEcmrFieldEntries().get("72").toString().isEmpty()) {
            GlassSignature glassSignature = glassSignatureBuilder((String) ecmrReportData.getEcmrFieldEntries().get("72"));
            parameters.put("signature", glassSignature.getSignatureMetaData());
            if (glassSignature.getGlassSignature() != null) {
                parameters.put("signatureImage", glassSignature.getGlassSignature());
            }
        }
        return parameters;
    }

    private JRBeanCollectionDataSource setItemFieldsData(List<HashMap<String ,String>> items) {
        List<ItemFields> itemFieldsList = new ArrayList<>();
        items.forEach((item)-> {
            itemFieldsList.add(ItemFields.builder().markingText(item.get("41")).itemQuantity(item.get("43"))
                    .typeText(item.get("45")).barcodeNumber(item.get("42")).identificationText(item.get("49")).weightKG(item.get("50")).volume(item.get("51")).build());
        });
        return new JRBeanCollectionDataSource(itemFieldsList);
    }

    private EcmrReportData getAllEcmrData(String id) {
        EcmrReportData ecmrReportData = new EcmrReportData();
        Ecmr ecmr = this.ecmrService.getLatestEcmr(id);
        ecmrReportData.setEcmr(ecmr);
        ecmrReportData.setEcmrFieldEntries(this.ecmrFieldService.getEcmrFields(id, ecmr.getVersion()));
        return ecmrReportData;
    }

    private String timestampConverter(Timestamp timestamp) {
        Date date = new Date(timestamp.getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        return dateFormat.format(date);
    }

    private String keySignatureStringBuilder(String signature) throws IOException {
        StringBuilder signatureString = new StringBuilder();
        ObjectMapper objectMapper = new ObjectMapper();
        SignatureData signatureData = objectMapper.readValue(signature, SignatureData.class);
        signatureString.append("Digitally Signed by: \n").append(signatureData.getUserCompany()).append("\n")
                .append(signatureData.getName()).append("\n").append(timestampConverter(signatureData.getTimestamp()));
        return signatureString.toString();
    }

    private GlassSignature glassSignatureBuilder(String signature) throws IOException {
        StringBuilder signatureString = new StringBuilder();
        ObjectMapper objectMapper = new ObjectMapper();
        SignatureData signatureData = objectMapper.readValue(signature, SignatureData.class);
        signatureString.append("Digitally Signed by: \n").append(signatureData.getUserCompany()).append("\n")
                .append(signatureData.getName()).append("\n").append(timestampConverter(signatureData.getTimestamp()));

        GlassSignature glassSignature = new GlassSignature();
        glassSignature.setSignatureMetaData(signatureString.toString());
        if (signatureData.getType().equals("signOnGlass")) {
            glassSignature.setGlassSignature(createSignatureImage(Base64.getDecoder().decode(signatureData.getData())));
        }
        return glassSignature;
    }

    private  BufferedImage createSignatureImage(byte[] imageData) {
        BufferedImage bufferedImage = null;
        try {
            bufferedImage = ImageIO.read(new ByteArrayInputStream(imageData));
        } catch (IOException e) {
            e.printStackTrace();
        }
        assert bufferedImage != null;
        return bufferedImage;
    }
}
