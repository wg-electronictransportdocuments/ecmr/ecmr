/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.jobs;

import org.siliconeconomy.ecmr.service.services.EcmrService;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import net.javacrumbs.shedlock.spring.annotation.SchedulerLock;

@Log4j2
@Component
@RequiredArgsConstructor
public class archivingJob {

    private final EcmrService ecmrService;

    @Scheduled(cron = "0 0 02 * * *")
    @SchedulerLock(name = "ArchiveEcmr_job", lockAtLeastFor = "PT30S", lockAtMostFor = "PT15M")
    public void startJob() {
        log.info("Started Job {}", archivingJob.class);
        ecmrService.archiveFinishedEcmr();
        log.info("Ended Job {}", archivingJob.class);
    }
}
