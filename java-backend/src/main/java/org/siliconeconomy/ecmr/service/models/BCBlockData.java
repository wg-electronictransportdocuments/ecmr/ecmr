/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.siliconeconomy.ecmr.service.controller.BCController;

import java.util.HashMap;

/**
 * This class represents an information saved in blockchain for one eCMR version. Is not saved in DB.
 *
 */
public class BCBlockData {

  /*
  *
  Information saved in Block chain:
  *
   */

  /** Ecmr ID */
  public String ecmrId;

  /**  Hash value of changes interpreted as String */
  public String hash;

  /** Metadata saved to blockchain */
  public BCMetaData additionalInformation;


  public String creator;

  public String timestamp;


  public BCBlockData() {}


  public BCBlockData(String ecmrId, String hash, BCMetaData bcMetaData)
  {
    this.ecmrId = ecmrId;
    this.hash = hash;
    this.additionalInformation = bcMetaData;
  }

  /**
   * Method which transforms BC Data to JSON
   * @return Meta Data as JSON-String
   */
  public String toString() {
    HashMap<String, String> m = new HashMap<>();
    m.put("id", ecmrId);
    m.put("hash", hash);
    m.put("additionalInformation", additionalInformation.toString());
    try {
      return new ObjectMapper().writeValueAsString(m);
    } catch (JsonProcessingException e) {
      return "Error while transferring to JSON";
    }
  }
}
