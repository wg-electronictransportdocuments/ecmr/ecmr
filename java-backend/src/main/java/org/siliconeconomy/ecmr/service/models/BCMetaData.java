/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.jackson.Jacksonized;

import java.util.HashMap;
import java.util.Map;


/** 
 * Class representing Meta Data in a BC-Block
 *
 */
@NoArgsConstructor
public class BCMetaData {

  /** New eCMR version */
  public int ecmrVersion;

  public String host;

  private ObjectMapper mapper = new ObjectMapper();

  public BCMetaData(int ecmrVersion, String host) {

    this.ecmrVersion = ecmrVersion;
    this.host = host;
  }

  // Transform incoming JSON from BC to object
  public BCMetaData(String json) throws JsonProcessingException {
    Map<String, Object> map = mapper.readValue(json, Map.class);
    this.ecmrVersion = (int) map.get("ecmrVersion");
    this.host = (String) map.get("host");
  }


  /**
   * Method which transforms Meta Data to JSON
   * @return Meta Data as JSON-String
   */
  public String toString() {
    HashMap<String, Object> m = new HashMap<>();
    m.put("ecmrVersion", ecmrVersion);
    m.put("host", host);
    try {
      return new ObjectMapper().writeValueAsString(m);
    } catch (JsonProcessingException e) {
      return "Error while transferring to JSON";
    }
  }
}
