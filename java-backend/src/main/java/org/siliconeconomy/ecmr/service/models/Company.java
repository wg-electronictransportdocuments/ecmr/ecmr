/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents a user's company. The corresponding database-table
 * is COMPANY.
 *
 */
@Entity
@Table(name = "COMPANY")
@EnableAutoConfiguration
@Getter
@NoArgsConstructor
public class Company {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long companyId;

    /** The company name  */
    private String companyName;
    /** The company location */
    private String companyLocation;

    public Company(String companyName, String companyLocation) {
        this.companyName = companyName;
        this.companyLocation=companyLocation;
    }

}


