/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents a user connection to the ECMR services. All information regarding
 * authorization and signatures is stored. A connection identifies the relation between one
 * session token and one public key. The corresponding database-table is AUTH_CONNECTION.
 *
 */
@Entity
@Table(name = "AUTH_CONNECTION")
@Getter
@NoArgsConstructor
public class Connection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long connectionId;

    /**
     * Token that is used to authorize the user-session when accessing the services.
     */
    @Lob
    private String sessionToken;

    /**
     * ECDSA-Public-Key for verifying signatures
     */
    private String pubKey;

    /**
     *
     */
    private String userId;

    /**
     *
     */
    @CreationTimestamp
    private Timestamp creationTimestamp;

    public Connection(
            String sessionToken,
            String pubKey,
            String userId) {
        this.sessionToken = sessionToken;
        this.pubKey = pubKey;
        this.userId = userId;
    }
}
