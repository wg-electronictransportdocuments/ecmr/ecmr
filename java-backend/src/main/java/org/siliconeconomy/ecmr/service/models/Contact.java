/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.*;
import java.sql.Timestamp;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents a contact between two users. It is stored, when and how often user 1 has
 * shared ECMRs with user 2. This is used to fasten ECMR sharing. The corresponding database-table is
 * AUTH_CONTACT.
 *
 */
@Entity
@Table(name = "AUTH_CONTACT")
@Getter
@NoArgsConstructor
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long contactId;


    /**
     * Number of times that two users shared ECMRs
     */
    private int amountOfContacts;

    private Timestamp lastContact;

    private String contactUserID;
    private String userID;

    public Contact(String userID, String contactUserID, int amountOfContacts, Timestamp lastContact) {
        this.userID = userID;
        this.contactUserID = contactUserID;
        this.amountOfContacts = amountOfContacts;
        this.lastContact = lastContact;
    }
}
