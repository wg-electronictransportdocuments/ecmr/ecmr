/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import java.sql.Timestamp;

/**
 * This class represents an encapsulation of a Contact.java object. It is used to automatically
 * create a JSON-response that is returned from backend service functions.
 *
 */
public class CurrentUserContact {

  private String contactId;
  private String contactType;
  private String contactName;
  private String contactEmail;
  private int amountOfContacts;
  private Timestamp lastContact;

  public String getContactId() {
    return contactId;
  }

  public String getContactType() {
    return contactType;
  }

  public String getContactName() {
    return contactName;
  }

  public String getContactEmail() {
    return contactEmail;
  }

  public int getAmountOfContacts() {
    return amountOfContacts;
  }

  public Timestamp getLastContact() {
    return lastContact;
  }

  public void setContactType(String contactType) {
    this.contactType = contactType;
  }

  public void setContactName(String contactName) {
    this.contactName = contactName;
  }

  public void setContactEmail(String contactEmail) {
    this.contactEmail = contactEmail;
  }

  public CurrentUserContact() {}

  public CurrentUserContact(
      String contactid,
      String contacttype,
      String contactname,
      String contactemail,
      int amountofcontacts,
      Timestamp lastcontact) {
    this.contactId = contactid;
    this.contactType = contacttype;
    this.contactName = contactname;
    this.contactEmail = contactemail;
    this.amountOfContacts = amountofcontacts;
    this.lastContact = lastcontact;
  }
}
