/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.models;

import java.util.List;


/**
 * This class represents an encapsulation of several user-connected information that are returned
 * after login: (User.java, Connection.java, CurrentUserContact.java, UserMandatoryField,java,
 * UserOptionalField.java, Userright.java) It is used to automatically create a JSON-reponse that is
 * returned from backend service functions.
 *
 */
public class CurrentUserData {

  private String userId;
  private String userName;
  private String userCompany;

  private String jwt;
  private String refreshJwt;

  private List<CurrentUserContact> contacts;
  private List<String> mandatoryFieldIds;
  private List<String> optionalFieldIds;
  private List<String> rights;

  public String getUserId() {
    return userId;
  }

  public String getUserName() {
    return userName;
  }

  public String getUserCompany() {return userCompany;}

  public String getJwt() {
    return jwt;
  }

  public String getRefreshJwt() {
    return refreshJwt;
  }

  public List<CurrentUserContact> getContacts() {
    return contacts;
  }

  public List<String> getMandatoryFieldIds() {
    return mandatoryFieldIds;
  }

  public List<String> getOptionalFieldIds() {
    return optionalFieldIds;
  }

  public List<String> getRights() {
    return rights;
  }

  public CurrentUserData() {}

  public CurrentUserData(
      String userId,
      String userName,
      String userCompany,
      String jwtToken,
      String jwtRefreshToken,
      List<CurrentUserContact> contacts,
      List<String> mandatoryFieldIds,
      List<String> optionalFieldIds,
      List<String> rights) {
    this.userId = userId;
    this.userName = userName;
    this.userCompany=userCompany;
    this.jwt = jwtToken;
    this.refreshJwt = jwtRefreshToken;
    this.contacts = contacts;
    this.mandatoryFieldIds = mandatoryFieldIds;
    this.optionalFieldIds = optionalFieldIds;
    this.rights = rights;
  }
}
