/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents an encapsulation of a Contact.java object. It is used to automatically
 * create a JSON-response that is returned from backend service functions.
 *
 */
@NoArgsConstructor
public class CurrentUserFields {

    @Getter
    @Setter
    private List<String> mandatoryFields = new ArrayList<>();
    @Getter
    @Setter
    private List<String> optionalFields = new ArrayList<>();

}
