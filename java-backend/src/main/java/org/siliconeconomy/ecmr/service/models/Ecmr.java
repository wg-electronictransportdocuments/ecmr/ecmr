/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.status.StatusService;

import javax.persistence.*;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Calendar;

import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Getter;
import lombok.Setter;

/**
 * This class represents a single eCMR object. Each change to a eCMR increases its version number. All
 * versions are stored. A eCMR is identified with eCMRid+version (that is used for
 * sharing). The corresponding database-table is eCMR_eCMR.
 *
 */
@Entity
@IdClass(EcmrId.class)
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "ecmrId")
@Table(name = "eCMR_ECMR")
public class Ecmr {

    @Id
    @Column(name = "ecmr_id")
    private String ecmrId;
    @Id
    private int version;

    /**
     * status of eCMR (see Status.java or arc42 documentation for details)
     */
    private String status;
    /**
     * Last user that changed the eCMR
     */
    private String lastUserId;

    /**
     * Timestamp of the last eCMR change
     */
    private Timestamp lastUpdated;
    private Timestamp created;
    /*
    A new Boolean Flag to indicate if the Ecmr is archived after it reached the final status of  "arrived at destination"
     */
    private boolean archived;

    /**
     * An eCMR can be a template. Creating a OneToOne Relation between both.
     */
    @OneToOne(mappedBy = "ecmr")
    @Getter
    @Setter
    @JsonManagedReference
    private Template template;


    public Ecmr() {
    }

    /**
     * eCMR-Constructor.
     *
     * <p>This constructor takes the userid and creates a cmr object by combining current datetime and
     * userid into a cmrid The initial version number is 1, the initial status and creation
     * information is set.
     *
     * @param creatingUser, the userid of the creating user
     */
    public Ecmr(UserFacade creatingUser) throws NoSuchAlgorithmException {
        Calendar calendar = Calendar.getInstance();
        long timeMilli = calendar.getTimeInMillis();
        this.ecmrId = UUID.randomUUID().toString();
        this.version = 1;
        this.status = StatusService.statusPrioList.get(0);
        this.lastUserId = creatingUser.getId();
        this.lastUpdated = new Timestamp(timeMilli);
        this.created = new Timestamp(timeMilli);
        this.archived = false;
    }

    public Ecmr(UserFacade creatingUser, String status) throws NoSuchAlgorithmException {
        Calendar calendar = Calendar.getInstance();
        long timeMilli = calendar.getTimeInMillis();
        this.ecmrId = UUID.randomUUID().toString();
        this.version = 1;
        this.status = status;
        this.lastUserId = creatingUser.getId();
        this.lastUpdated = new Timestamp(timeMilli);
        this.created = new Timestamp(timeMilli);
    }

    /**
     * Constructor to create a new eCMR with an attached Template. Connecting itself to the template.
     * @param creatingUser UserID of the creating user
     * @param template Template that the eCMR gets attached to
     */
    public Ecmr(UserFacade creatingUser, Template template) {
        Calendar calendar = Calendar.getInstance();
        long timeMilli = calendar.getTimeInMillis();
        this.ecmrId = UUID.randomUUID().toString();
        this.version = 1;
        this.status = StatusService.statusPrioList.get(0);
        this.lastUserId = creatingUser.getId();
        this.lastUpdated = new Timestamp(timeMilli);
        this.created = new Timestamp(timeMilli);
        this.template = template;
        this.template.setEcmr(this);
    }


    public Ecmr(String ecmrId, int version, String status, String lastUserId, Timestamp lastUpdated, Timestamp created) {
        this.ecmrId = ecmrId;
        this.version = version;
        this.status = status;
        this.lastUserId = lastUserId;
        this.lastUpdated = lastUpdated;
        this.created = created;
    }

    public Ecmr clone() {
        Ecmr newEcmr = new Ecmr();
        newEcmr.ecmrId = this.ecmrId;
        newEcmr.version = this.version;
        newEcmr.status = this.status;
        newEcmr.lastUserId = this.lastUserId;
        newEcmr.lastUpdated = this.lastUpdated;
        newEcmr.created = this.created;
        return newEcmr;
    }

    public String getEcmrId() {
        return ecmrId;
    }

    public int getVersion() {
        return version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLastUserId() {
        return lastUserId;
    }

    public void setLastUserId(String lastUserId) {
        this.lastUserId = lastUserId;
    }

    public Timestamp getLastUpdated() {
        return lastUpdated;
    }

    public Timestamp getCreated() {
        return created;
    }


    public boolean isArchived() { return archived; }

    public void setArchived(boolean archived) { this.archived = archived; }

    /**
     * Updates lastupdated variable.
     *
     * <p>This method updates "lastupdated" with a current Systemtime.
     */
    public void updateLastUpdated() {
        this.lastUpdated = new Timestamp(System.currentTimeMillis());
    }

    /**
     * COunts up the version number.
     *
     * <p>This method increases the version number of a eCMR and creates the corresponding new hash
     * link
     */
    public void countUpVersion() throws NoSuchAlgorithmException {
        this.version = this.version + 1;
    }


    /**
     * Create eCMR from external instance.
     * Note that instead over userID the name is used directly
     * @param ecmrData
     */
    public Ecmr(EcmrData ecmrData)
    {
        this.ecmrId = ecmrData.getEcmrId();
        this.version = ecmrData.getVersion();
        this.status = ecmrData.getStatus();
        this.lastUserId = ecmrData.getLastUsername();
        this.lastUpdated = ecmrData.getLastUpdated();
        this.created = ecmrData.getCreated();
        this.archived = ecmrData.isArchived();
    }
}
