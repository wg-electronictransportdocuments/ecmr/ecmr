/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

/**
 * This class represents an area in a eCMR.
 *
 */
@Entity
@Table(name = "eCMR_AREA")
@EnableAutoConfiguration
@NoArgsConstructor
@AllArgsConstructor
public class EcmrArea {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "area_id")
  private long areaId;

  private String description;

  public EcmrArea(String description) {
    this.description = description;
  }
}
