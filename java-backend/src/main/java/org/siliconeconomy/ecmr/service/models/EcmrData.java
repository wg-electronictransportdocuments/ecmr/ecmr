/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.Setter;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

/**
 * This class represents an encapsulation of a eCMR.java object.
 * It is used to automatically create a JSON-reponse that is returned from backend service functions.
 * <p>
 * Note: Ids that are stored as numeric values in the database will be converted to String.
 *
 */
public class EcmrData {

    @Getter
    @Setter
    private String ecmrId;

    @Getter
    private int version;


    @Getter
    private String status;

    @Getter
    private Timestamp lastUpdated;

    @Getter
    @Setter
    private Timestamp created;

    @Getter
    @Setter
    private String lastUserId;

    @Getter
    @Setter
    private String lastUsername;

    @Setter
    private String host;

    @Getter
    @Setter
    private Map<String, Object> fields;

    @Getter
    @Setter
    private boolean archived;

    public EcmrData(Ecmr e) {
        this.ecmrId = e.getEcmrId();
        this.version = e.getVersion();
        this.status = e.getStatus();
        this.lastUserId = e.getLastUserId();
        this.lastUsername = "";
        this.lastUpdated = e.getLastUpdated();
        this.created = e.getCreated();
        this.fields = new HashMap<>();
        this.host = "";
        this.archived = e.isArchived();
    }

    public EcmrData() {
    }

}
