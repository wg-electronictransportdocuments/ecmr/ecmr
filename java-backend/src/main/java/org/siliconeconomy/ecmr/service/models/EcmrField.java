/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * This class represents a single field object of a eCMR. The corresponding eCMR is referenced. The fieldid is referenced and an abstract "value" String holds the field information
 * The corresponding database-table is eCMR_eCMRFIELD.
 *
 */
@Entity
@Table(name = "eCMR_ECMRFIELD")
public class EcmrField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ecmr_field_id")
    private long ecmrFieldId;

    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "ecmr_id", referencedColumnName = "ecmr_id"),
            @JoinColumn(name = "version", referencedColumnName = "version")
    })
    private Ecmr ecmr;

    @ManyToOne
    @JoinColumn(name = "field_id")
    @Getter
    private Field field;

    /**
     * this is used if a field can have multiple distinct entries (e.g. the intital release field areas 10 to 15)
     */
    @Getter
    private int entryId;

    /**
     * this describes the actual field value
     */
    @Lob
    @Getter
    @Setter
    private String value;

    public EcmrField() {
    }

    public EcmrField(Ecmr ecmr, Field field, int entryId, String value) {
        this.ecmr = ecmr;
        this.field = field;
        this.entryId = entryId;
        this.value = value;
    }

    public String getEcmrId() {
        return ecmr.getEcmrId();
    }

    public int getVersion() {
        return ecmr.getVersion();
    }

    public long getFieldId() {
        return field.getFieldId();
    }

}
