/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import java.util.List;

/**
 * This class represents an encapsulation of a eCMRField.java object. It is used to automatically
 * create a JSON-reponse that is returned from backend service functions.
 *
 * <p>Note: Ids that are stored as numeric values in the database will be converted to String.
 *
 */
public class EcmrFieldData {

  private String fieldId;

  private List<String> values;

  public EcmrFieldData() {}

  public EcmrFieldData(int fieldid, List<String> values) {
    this.fieldId = String.valueOf(fieldid);
    this.values = values;
  }


  public String getFieldId() {
    return fieldId;
  }

  public List<String> getValues() {
    return values;
  }
}
