/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * This class represents a single return object for the different unique field of a single eCMRfield.
 * Since there might be multiple entries for a single eCMRfield, a simple integer id reference is not sufficient.
 *
 */

@Getter
@AllArgsConstructor
public class EcmrFieldEntryData {

	private long fieldId;
	private int entryId;

	@Setter
	private String value;

	public static ArrayList<EcmrFieldEntryData> fromJson(Map<String, Object> fields)
	{
		ArrayList<EcmrFieldEntryData> resultList = new ArrayList<>();
		for (var entry: fields.entrySet())
		{
			if (entry.getKey().startsWith("_comment")) {
				// just a comment -> ignore
			} else if (!entry.getKey().equals("positions")) {
				resultList.add(new EcmrFieldEntryData(Long.parseLong(entry.getKey()), 1, (String) entry.getValue()));
			} else {
				int positionIndex = 1;
				for (Map<String, String> position: (List<Map<String, String>>) entry.getValue()) {
					for (Map.Entry<String, String> positionEntry : position.entrySet()) {
						if(!positionEntry.getKey().startsWith("_comment")) {
							resultList.add(new EcmrFieldEntryData(Long.parseLong(positionEntry.getKey()), positionIndex, positionEntry.getValue()));
						}
					}
					positionIndex++;
				}
			}
		}
		return resultList;
	}

}
