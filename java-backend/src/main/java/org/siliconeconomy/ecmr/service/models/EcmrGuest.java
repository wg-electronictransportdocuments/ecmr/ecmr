/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "eCMR_Guest")
@NoArgsConstructor
@AllArgsConstructor
public class EcmrGuest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long ecmrGuestId;
    @NotNull
    @Getter
    @Setter
    public String ecmrId;
    @NotNull
    @Getter
    @Setter
    public String firstName;
    @NotNull
    @Getter
    @Setter
    public String lastName;
    @Getter
    @Setter
    public String company;
    @Getter
    @Setter
    @NotNull
    public String phoneNumber;
    @Getter
    @NotNull
    @Setter
    public String tan;
    @Getter
    @Setter
    @NotNull
    public Timestamp tanValidForSigningUntil;
    @Getter
    @Setter
    @NotNull
    public Timestamp createdAt;
}
