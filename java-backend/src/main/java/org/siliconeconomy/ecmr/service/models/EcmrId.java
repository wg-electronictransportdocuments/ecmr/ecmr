/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.models;


/** 
 * This class defines a primary key consisting of two indices for eCMR-Objects.
 * eCMR is uniquely identifiable by ecmrId and version
 *
 */
import java.io.Serializable;

public class EcmrId implements Serializable {

  private String ecmrId;

  private int version;

  public EcmrId() {}

  public EcmrId(String ecmrId, int version) {
    this.ecmrId = ecmrId;
    this.version = version;
  }

  public boolean equals(Object other) {
    if (this == other) return true;
    if (!(other instanceof EcmrId)) return false;
    EcmrId castOther = (EcmrId) other;
    return ecmrId == castOther.ecmrId && version == castOther.version;
  }

  public int hashCode() {
    final int prime = 12;
    int hash = 55;
    hash = hash * prime + (ecmrId == null ? 0 : ecmrId.hashCode());
    hash = hash * prime + Integer.hashCode(version);
    return hash;
  }
}
