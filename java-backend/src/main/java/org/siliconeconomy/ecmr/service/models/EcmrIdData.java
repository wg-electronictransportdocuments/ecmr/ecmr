/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.models;

/**
 * This class represents an encapsulation of a part of a eCMR.java object. Only the identifiers ecmrId and version are included.
 * It is used to automatically create a JSON-reponse that is returned from backend service functions.
 * <p>
 * Note: Ids that are stored as numeric values in the database will be converted to String.
 *
 */
public class EcmrIdData {

    private final String ecmrId;
    private final int version;

    public EcmrIdData(String ecmrId, int version) {
        this.ecmrId = ecmrId;
        this.version = version;
    }

    public String getEcmrId() {
        return ecmrId;
    }

    public int getVersion() {
        return version;
    }
}
