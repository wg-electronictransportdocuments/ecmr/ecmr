/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

/**
 * This sub-class represents an encapsulation of a eCMR.java object. It extends GeneralListData to be able to show more info, if needed.
 *
 * <p>Note: Ids that are stored as numeric values in the database will be converted to String.
 *
 */
public class EcmrListData extends GeneralListData{

    public EcmrListData(Ecmr e) {
        super(e);
    }
    public EcmrListData()
    {

    }
}
