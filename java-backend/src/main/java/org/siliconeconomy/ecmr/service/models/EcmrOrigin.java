/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.models;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "eCMR_Origin")
@NoArgsConstructor
public class EcmrOrigin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long originId;

    @Getter
    @Setter
    public String ecmrId;

    @Getter
    @Setter
    public String origin;

    public EcmrOrigin(String ecmrId, String origin)
    {
        this.ecmrId = ecmrId;
        this.origin = origin;
    }

}
