/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Table(name = "eCMR_Participant")
@NoArgsConstructor
public class EcmrParticipant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public long participantId;


    @Getter
    @Setter
    public String participantOrigin;


    @Getter
    @Setter
    public String ecmrId;


    public EcmrParticipant(String participantOrigin, String ecmrId)
    {
        this.ecmrId = ecmrId;
        this.participantOrigin = participantOrigin;
    }
}
