/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Object for Updating existing eCMR on external host instance
 */
public class EcmrUpdateObject {


    @Getter
    @Setter
    EcmrData ecmrData;


    @Getter
    @Setter
    String participant;

    public EcmrUpdateObject(EcmrData ecmrData, String participant)
    {
        this.ecmrData = ecmrData;
        this.participant = participant;
    }

    public EcmrUpdateObject()
    {}
}
