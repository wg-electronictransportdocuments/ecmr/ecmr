/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.*;

import lombok.Getter;

/**
 * This class represents the connection between a user and a eCMR. Users are only allowed to view or
 * change eCMR that they are connected to. The share functionality allows to connect new users to a
 * eCMR. The corresponding database-table is eCMR_ECMRUSER.
 *
 */
@Entity
@Table(name = "eCMR_ECMRUSER")
@Getter
public class EcmrUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long ecmrUserId;

    private String userId;

    /**
     * eCMR Id, not version specific
     */
    private String ecmrId;

    public EcmrUser() {
    }

    public EcmrUser(String ecmrId, String userId) {
        this.ecmrId = ecmrId;
        this.userId = userId;
    }
}
