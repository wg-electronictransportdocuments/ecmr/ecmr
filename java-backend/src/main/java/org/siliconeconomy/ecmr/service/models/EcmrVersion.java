/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Class represents eCMR Version in DB associated with signature by user.
 *
 */
@Entity
@Table(name = "eCMR_ECMRVERSION")
@NoArgsConstructor
public class EcmrVersion {
    /**
     * Primary key for version of a certain eCMR
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long versionId;
    /**
     * Id of eCMR
     */
    @Getter
    @Setter
    private String ecmrId;
    /**
     * Version of eCMR
     */
    @Getter
    @Setter
    private int ecmrVersion;
    /**
     * UserId of user which created this version
     */
    private String userId;
    /**
     * Timestamp of change
     */
    private Timestamp ts;

    @Getter
    @Setter
    private String signature;

    @Getter
    @Setter
    private boolean createdExternally;


    public EcmrVersion(String ecmrId, int ecmrVersion, String userId, String signature, boolean createdExternally) {
        this.ecmrId = ecmrId;
        this.ecmrVersion = ecmrVersion;
        this.userId = userId;
        this.ts = new Timestamp(System.currentTimeMillis());
        this.signature = signature;
        this.createdExternally = createdExternally;
    }
}
