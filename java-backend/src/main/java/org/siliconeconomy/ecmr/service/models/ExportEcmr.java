/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

public class ExportEcmr {

    @Getter
    @Setter
    List<EcmrData> ecmrData;
    @Getter
    @Setter
    EcmrOrigin origin;

    public ExportEcmr()
    {
    }

    public ExportEcmr(List<EcmrData> ecmrData,EcmrOrigin origin)
    {
        this.ecmrData = ecmrData;
        this.origin = origin;
    }

}
