/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents a general field object. The field is described by id, name and description.
 * The field can be associated with a ECMR in ECMRField.java The corresponding database-table is
 * ECMR_FIELD.
 *
 */
@Entity
@Table(name = "eCMR_FIELD")
@Getter
@NoArgsConstructor
public class Field {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "field_id")
  private long fieldId;

  @ManyToOne
  @JoinColumn(name="area_id")
  private EcmrArea area;

  /**
   * name representation of the field (either connects to UN-Standard (e.g.
   * eCMR.UN01004159.UN01004213.UN01004598) or is a custom field
   */
  private String fieldName;
  /** textual description of the field (what is included), e.g. Consignee_Name_Text */
  private String description;

  public Field(EcmrArea areaId, String fieldName, String description) {
    this.area = areaId;
    this.fieldName = fieldName;
    this.description = description;
  }
}
