/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import java.sql.Timestamp;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents the data of an eCMR top level information.
 */
@Getter
@Setter
public class GeneralListData {
    private String ecmrId;

    private String referenceNr;
    private int version;


    private String status;
    private Timestamp lastUpdated;
    private Timestamp created;

    private String lastUserId;
    private String lastUsername;
    private String consigneeName;
    private String senderName;

    private String licensePlate;
    private String carrierName;
    private String consigneeAddress;
    private String nationalInternationalTransport;

    private boolean shareable;
    private boolean editable;
    private boolean deletable;

    private String host;

    public GeneralListData( Ecmr e) {
        this.ecmrId = e.getEcmrId();
        this.version = e.getVersion();
        this.status = e.getStatus();
        this.lastUserId = e.getLastUserId();
        this.lastUsername = "";
        this.lastUpdated = e.getLastUpdated();
        this.created = e.getCreated();
        this.shareable = Boolean.FALSE;
        this.editable = Boolean.FALSE;
        this.deletable = Boolean.FALSE;
        this.referenceNr = ""; // Not available om ecmr getReferenceNr();
        this.host = "";
        this.licensePlate = "";
        this.carrierName = "";
        this.consigneeAddress = "";
    }

    public GeneralListData() {}
}
