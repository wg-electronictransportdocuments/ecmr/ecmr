/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Object for requesting access for a guest carrier
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class GuestCarrier {
    String firstName;
    String lastName;
    String company;
    String phoneNumber;

}
