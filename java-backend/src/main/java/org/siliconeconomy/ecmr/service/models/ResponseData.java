/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

/**
 * This class represents a wrapper for all backend-responses that only include a single string
 * message (e.g. errormessages). ResponseData-Objects are than added to a spring ResponseEntity.
 *
 */
public class ResponseData {

  private String response;

  public String getResponse() {
    return response;
  }

  public void setResponse(String response) {
    this.response = response;
  }

  public ResponseData() {}

  public ResponseData(String response) {
    this.response = response;
  }
}
