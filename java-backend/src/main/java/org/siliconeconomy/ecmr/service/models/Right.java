/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class represents right definitons that allow for a detailed rightmanagement. The connection
 * to a user is done in UserRight.java. In this versions testdata, only a single "All-rights" right
 * is created for all users, because eCMR interaction rights are also managened depending on eCMR
 * status.
 *
 */
@Entity
@Table(name = "AUTH_RIGHT")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Right {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long rightId;

  /***name representation of the right*/
  private String rightName;
  /** textual description of the right (what does it mean) */
  private String description;

  public Right(String rightName, String description) {
    this.rightName = rightName;
    this.description = description;
  }
}
