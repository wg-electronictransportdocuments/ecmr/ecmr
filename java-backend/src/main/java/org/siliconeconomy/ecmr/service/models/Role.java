/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import java.util.List;
import java.util.Set;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.siliconeconomy.ecmr.service.facade.RoleFacade;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * This class represents the connection between fields and roles.
 * The corresponding database-table is AUTH_ROLE.
 *
 */
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "AUTH_ROLE")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private long id;
    @Getter
    @Setter
    private String name;
    @ManyToMany
    @JoinTable(name = "AUTH_ROLE_MANDATORY_FIELD",
            joinColumns = { @JoinColumn(name = "AUTH_ROLE_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "ECMR_FIELD_ID", nullable = false, updatable = false) })
    @Getter
    @Setter
    private Set<Field> mandatoryFields;
    @ManyToMany
    @JoinTable(name = "AUTH_ROLE_OPTIONAL_FIELD",
            joinColumns = { @JoinColumn(name = "AUTH_ROLE_ID", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "ECMR_FIELD_ID", nullable = false, updatable = false) })
    @Getter
    @Setter
    private Set<Field> optionalFields;
    @Getter
    @Setter
    private Boolean canDelete;
    @Getter
    @Setter
    @ElementCollection
    private List<String> canChange;
    @Getter
    @Setter
    @ElementCollection
    private List<String> canShare;
    @Getter
    @Setter
    private Boolean isSuperuser;
    @Getter
    @Setter
    private Boolean canGenerate;
}
