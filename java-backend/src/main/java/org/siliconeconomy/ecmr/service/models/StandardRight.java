/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.*;

import lombok.NoArgsConstructor;

/**
 * This class represents a standardized rights-definition
 *
 */
@Entity
@Table(name = "AUTH_STANDARDRIGHT")
@NoArgsConstructor
public class StandardRight {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long standardRightId;

  /***foreign key to right*/
  @ManyToOne
  @JoinColumn(name = "right_id")
  private Right right;

  /** foreignkey to usertypeid */
  @ManyToOne
  @JoinColumn(name = "user_type_id")
  private Usertype userType;

  public StandardRight(Right right, Usertype userType) {
    this.right = right;
    this.userType = userType;
  }
}
