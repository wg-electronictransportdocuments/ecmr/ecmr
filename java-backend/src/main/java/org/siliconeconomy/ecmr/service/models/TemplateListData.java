/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.Setter;

/**
 * Represents the top level information of a Template with usage of GeneralListData
 */
@Getter
@Setter
public class TemplateListData extends GeneralListData{

    private String name;
    private Long templateId;


    public TemplateListData( Ecmr e) {
        super(e);
        this.name = e.getTemplate().getName();
        this.templateId = e.getTemplate().getTemplateId();
    }
}
