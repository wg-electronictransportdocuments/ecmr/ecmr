/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.siliconeconomy.ecmr.service.facade.UserFacade;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents the user.
 * The corresponding database-table is AUTH_USER.
 *
 */
@Entity
@NoArgsConstructor
@Table(name = "AUTH_USER")
public class User {

    @Id
    @Getter
    @Setter
    private String uuid;

    @Getter
    @Setter
    private String userName;

    @Getter
    @Setter
    private String email;

    @Getter
    @Setter
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "user_company_company_id")
    private Company userCompany;

    @Getter
    @Setter
    @ElementCollection
    @CollectionTable(name = "userRoles")
    @Column(name = "userRoles")
    private List<String> userRoles;

    @ElementCollection
    @MapKeyColumn(name="name")
    @Column(name="value")
    @CollectionTable(name="user_properties", joinColumns=@JoinColumn(name="id"))
    private Map<String, String> attributes;
    // maybe find a nicer way to define Map<String, List<String>> with jpa

    public User(UserFacade user) {
        this.uuid = user.getId();
        this.userName = user.getUserName();
        this.email = user.getEmail();
        this.userCompany = user.getUserCompany();
        this.userRoles = user.getUserRoles();
        setAttributes(user.getAttributes());
    }

    public void setAttributes(Map<String, List<String>> attr){
        this.attributes = new HashMap<>();
        for (String key : attr.keySet()){
            attributes.put(key, StringUtils.join(attr.get(key), ";"));
        }
    }

    public Map<String, List<String>> getAttributes(){
        Map<String, List<String>> niceResult = new HashMap<String, List<String>>();

        for (String key : attributes.keySet()){
            niceResult.put(
                    key, Arrays.asList(
                        StringUtils.split(
                            attributes.get(key),";"
                        )
                    )
            )
            ;
        }

        return niceResult;
    }
}
