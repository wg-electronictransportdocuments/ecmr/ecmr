/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents the connection between users and fields, that defines which fields are a
 * mandatory input for a given user. The corresponding database-table is eCMR_USERMANDATORYFIELD.
 *
 */
@Entity
@Table(name = "eCMR_USERMANDATORYFIELD")
@NoArgsConstructor
public class UserMandatoryField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private long userMandatoryFieldId;

    @ManyToOne
    @JoinColumn(name = "field_id")
    private Field field;

    @ManyToOne
    @JoinColumn(name = "role_id")
    @Getter
    private Role role;

    public UserMandatoryField(Field field, Role role) {
        this.field = field;
        this.role = role;
    }

    public long getFieldId() {
        return field.getFieldId();
    }
}
