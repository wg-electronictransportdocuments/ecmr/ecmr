/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents the connection between users and fields, that defines which fields are a optional input for a given user.
 * The corresponding database-table is eCMR_USEROPTIONALFIELD.
 *
 */
@Entity
@Table(name = "eCMR_USEROPTIONALFIELD")
@NoArgsConstructor
public class UserOptionalField {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private long userOptionalFieldId;

    @ManyToOne
    @JoinColumn(name = "field_id")
    private Field field;

    @ManyToOne
    @JoinColumn(name = "role_id")
    @Getter
    private Role role;

    public UserOptionalField(Field field, Role role) {
        this.field = field;
        this.role = role;
    }

    public long getFieldId() {
        return field.getFieldId();
    }
}
