/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import javax.persistence.*;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents the connection between users and rights, that defines which rights are
 * associated to a given user. In this versions testdata, only a single "All-rights" right is
 * created for all users, because eCMR interaction rights are also managened depending on eCMR status.
 * The corresponding database-table is AUTH_USERRIGHT.
 *
 */
@Entity
@Table(name = "AUTH_USERRIGHT")
@NoArgsConstructor
public class UserRight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Getter
    private long userRightId;
    @ManyToOne(cascade=CascadeType.ALL)
    @JoinColumn(name = "right_id")
    private Right right;

    @ElementCollection
    @Getter
    private List<String> keycloakRoles;

    public UserRight(Right right, List<String> roles) {
        this.right = right;
        this.keycloakRoles = roles;
    }

    public long getRightId() {
        return right.getRightId();
    }
}
