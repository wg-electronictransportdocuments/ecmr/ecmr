/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.models;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * This class represents the different usertypes (persona) that users can be. Each user has only a
 * single usertype.
 *
 */
@Entity
@Table(name = "AUTH_USERTYPE")
@EnableAutoConfiguration
@Getter
@NoArgsConstructor
public class Usertype {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long userTypeId;

  /***name representation of the usertype*/
  private String userTypeName;
  /** textual description of the usertype (what does it mean) */
  private String description;

  public Usertype(String userTypeName, String description) {
    this.userTypeName = userTypeName;
    this.description = description;
  }
}
