/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.repositories;

import java.util.Optional;

import org.siliconeconomy.ecmr.service.models.EcmrGuest;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EcmrGuestRepository extends CrudRepository<EcmrGuest, Long> {
    Optional<EcmrGuest> findByEcmrIdAndTan(String ecmrId, String tan);
}
