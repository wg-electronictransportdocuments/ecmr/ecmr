/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.repositories;

import java.util.List;

import org.siliconeconomy.ecmr.service.models.Ecmr;
import org.siliconeconomy.ecmr.service.models.EcmrId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * This interface defines a Repository for class Ecmr.java
 *
 */
@Repository
public interface EcmrRepository extends CrudRepository<Ecmr, EcmrId> {

    List<Ecmr> getEcmrByStatus(String status);

    List<Ecmr> getEcmrByEcmrIdInAndArchived(List<String> ids, boolean archived);

    List<Ecmr> getAllByEcmrIdIn(List<String> ids);
}
