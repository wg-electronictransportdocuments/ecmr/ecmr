/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.repositories;

import org.siliconeconomy.ecmr.service.models.Template;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * CRUD-Repository of Template.
 */
@Repository
public interface TemplateRepository extends CrudRepository<Template, Long> {
}
