/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.role;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.idm.RoleRepresentation;
import org.siliconeconomy.ecmr.service.facade.RoleFacade;
import org.siliconeconomy.ecmr.service.models.Role;
import org.siliconeconomy.ecmr.service.repositories.RoleRepository;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ResourceLoader;

/**
 * This class defines a service that allows to load roles from a config file
 *
 */
public class KeycloakRoleService implements RoleService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeycloakRoleService.class);

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    Keycloak keycloak;

    @Value("${usermanagement.config.filename}")
    public String configFileName;

    @Value("${keycloak.realm}")
    public String realm;

    @Autowired
    private ResourceLoader resourceLoader;

    /**
     * Gets the roles from a config file.
     */
    // BEGIN-NOSCAN
    @Transactional
    public void loadRoles() {
        RoleService.storedRoles.clear();
        List<String> roleRepresentationsName = new ArrayList<>();
        try {
            List<RoleRepresentation> roleRepresentations = keycloak.realm(realm).roles().list();
            for (RoleRepresentation rr : roleRepresentations) {
                roleRepresentationsName.add(rr.getName());
            }
        } catch (Exception e) {
            // ignore
        }

        LOGGER.info("Start reading config file");
        LOGGER.info(configFileName);

        Iterable<Role> allRoles = this.roleRepository.findAll();

        for (Role role : allRoles) {
            LOGGER.info("Add " + role.getName());
            if (roleRepresentationsName.contains(role.getName())) { // only roles provides in keycloak are added
                RoleService.roleNames.add(role.getName());
                var facade = new RoleFacade(role);
                facade.setId(getIdFromSavedRole(facade.getName()));
                RoleService.storedRoles.add(facade);
            }
        }
    }
    // END-NOSCAN

    public boolean isSuperUser(List<String> userRoles) {
        boolean result = Boolean.FALSE;
        for (String roleName : userRoles) {
            var rf = getRoleByName(roleName);
            if (Boolean.TRUE.equals(rf.getIsSuperuser())) {
                result = Boolean.TRUE;
                break;
            }
        }
        return result;
    }

    @Override
    public boolean isAdminRole(List<String> userRoles) {
        boolean result = Boolean.FALSE;
        for (String roleName : userRoles) {
            //var rf = getRoleByName(roleName);
            if (roleName.contains("admin") || roleName.contains("Admin")) {
                result = Boolean.TRUE;
                break;
            }
        }
        return result;
    }

    private String getIdFromSavedRole(String roleName) {
        for (Role role : roleRepository.findAll()) {
            if (roleName.equals(role.getName())) {
                return "" + role.getId();
            }
        }
        return null;
    }

    @Override
    public RoleFacade getRoleByName(String rolename) {

        RoleFacade result = null;
        for (RoleFacade rf : RoleService.storedRoles) {
            if (rf.getName().equals(rolename)) {
                result = rf;
                break;
            }
        }
        return result;
    }

    @Override
    public List<String> getRoleListFromUser(String id) {
        return null;
    }

    @Override
    public void loadRolesFromStorage() {
     throw new RuntimeException("Method is deprecated. Use loadRoles instead");
    }

}



