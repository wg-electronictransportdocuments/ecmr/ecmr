/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.role;

import org.siliconeconomy.ecmr.service.facade.RoleFacade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * This class defines a service that allows to load roles from a config file
 *
 */
public interface RoleService {

    List<String> roleNames = Collections.synchronizedList(new ArrayList<>());
    List<RoleFacade> storedRoles = Collections.synchronizedList(new ArrayList<>());

    void loadRoles();

    boolean isSuperUser(List<String> userRoles);

    boolean isAdminRole(List<String> userRoles);

    RoleFacade getRoleByName(String roleName);

    List<String> getRoleListFromUser(String id);

    void loadRolesFromStorage();
}



