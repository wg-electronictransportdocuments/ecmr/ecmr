/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.Company;
import org.siliconeconomy.ecmr.service.repositories.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * This class defines a service that allows to connect to a Repository of UserService.java
 *
 */
@Service
public class CompanyService {
    @Autowired
    private CompanyRepository companyRepository;

    /**
     * Gets the company name for a given companyid.
     * <p>
     * This method returns the corresponding companyname to an input companyid.
     *
     * @param companyId, the companyid that is searched for
     * @return String, the corresponding companyname if found; null otherwise
     */
    public String getCompanyname(int companyId) {
        for (Company c : companyRepository.findAll()) {
            if (c.getCompanyId() == companyId) {
                return c.getCompanyName();
            }
        }
        return null;
    }
}
