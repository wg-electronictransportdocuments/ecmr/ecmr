/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.Connection;
import org.siliconeconomy.ecmr.service.repositories.ConnectionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

/**
 * This class defines a service that allows to connect to a Repository of Connection.java
 *
 */
@Service
public class ConnectionService {

  @Autowired private ConnectionRepository connectionRepository;

  /**
   * Adds a connection.
   *
   * This method takes a input Connection object and adds it to the connectionRepository
   *
   * @param t, the connection that should be added
   */
  public void addConnection(Connection t) {
    connectionRepository.save(t);
  }

  /**
   *  Search for associated public key by session token
   *
   * @param token Current JWT-Session-Token
   * @return String PublicKey as String
   */
  public String getCurrentPubKey(String token) {
    String latestPubKey = null;
    Timestamp latestTimestamp = new Timestamp(0);
    for (Connection c : connectionRepository.findAll()) {

      if (c.getSessionToken().equals(token) && c.getCreationTimestamp().after(latestTimestamp)) {
        latestPubKey = c.getPubKey();
        latestTimestamp = c.getCreationTimestamp();
      }
    }
    return latestPubKey;
  }
}

