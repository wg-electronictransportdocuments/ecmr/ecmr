/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.Contact;
import org.siliconeconomy.ecmr.service.models.CurrentUserContact;
import org.siliconeconomy.ecmr.service.repositories.ContactRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/**
 * This class defines a service that allows to connect to a Repository of Contact.java
 */
@Service
public class ContactService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactService.class);

    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    UserManagementService userManagementService;

    @PersistenceContext
    private EntityManager em;

    /**
     * Gets the contacts of a user.
     * <p>
     * This method returns all contacts that are connected to a user.
     *
     * @param userId, the userId for whom the contacts are searched
     * @return List<CurrentUserContact> List that contains an initialized CurrentUserContact object, for each contact found in the repository.
     */
    public List<CurrentUserContact> getCurrentUserContacts(String userId) {
        List<CurrentUserContact> currentUserContacts = new ArrayList<>();

        Query q = em.createQuery("SELECT DISTINCT c from Contact c where c.userID = :userId ");
        q.setParameter("userId", userId);
        List<Contact> relevantContacts = q.getResultList();

        for (Contact contact : relevantContacts) {
            try {
                String id = contact.getContactUserID();
                LOGGER.info("Gather contacts for User {}", id);
                UserFacade userContact = userManagementService.getUserById(id);

                currentUserContacts.add(
                        new CurrentUserContact(
                                contact.getContactUserID(),
                                null == userContact || null == userContact.getUserRoles()
                                        ? "" : userContact.getUserRoles().get(0),
                                null == userContact ? "" : userContact.getUserName(),
                                null == userContact ? "" : userContact.getEmail(),
                                contact.getAmountOfContacts(),
                                contact.getLastContact()));
            } catch (Exception e) {
                LOGGER.info("Could not get contact {}. {}", contact.getContactUserID(), e.getMessage());
            }
        }

        return currentUserContacts.isEmpty() ? null : currentUserContacts;
    }

    /**
     * Updates the amount of contacts +1 when an eCMR is shared to a user
     *
     * @param userId      id of user
     * @param userIdToSet id of user shared
     */
    @Transactional
    public void updateAmountOfContacts(String userId, String userIdToSet) {
        LOGGER.info("[eCMR ContactService] updateAmountOfContacts started");
        try {
            if ((userId == null || userId.isEmpty()) || (userIdToSet == null || userIdToSet.isEmpty())) {
                LOGGER.info("Could not update AmountOfContacts userID: {} and/or userIdToSet: {} is/are empty.", userId, userIdToSet);
            } else {
                Query q = em.createQuery("UPDATE Contact c SET c.amountOfContacts = c.amountOfContacts + 1 WHERE c.userID = :userId AND c.contactUserID = :userIdToSet");
                q.setParameter("userId", userId);
                q.setParameter("userIdToSet", userIdToSet);
                q.executeUpdate();
                LOGGER.info("[eCMR ContactService] updateAmountOfContacts updated for userID: {} and userIdToSet: {}.", userId, userIdToSet);
            }
        } catch (Exception e) {
            LOGGER.info("Could not update AmountOfContacts for userID: {} and userIdToSet: {}. {}", userId, userIdToSet, e.getMessage());
        }
        LOGGER.info("[eCMR ContactService] updateAmountOfContacts finished");
    }
}
