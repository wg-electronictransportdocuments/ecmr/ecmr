/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.LongStream;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.siliconeconomy.ecmr.service.controller.SignatureController;
import org.siliconeconomy.ecmr.service.models.Ecmr;
import org.siliconeconomy.ecmr.service.models.EcmrField;
import org.siliconeconomy.ecmr.service.models.EcmrFieldEntryData;
import org.siliconeconomy.ecmr.service.models.ExportEcmr;
import org.siliconeconomy.ecmr.service.models.Field;
import org.siliconeconomy.ecmr.service.repositories.EcmrFieldRepository;
import org.siliconeconomy.ecmr.service.repositories.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * This class defines a service that allows to connect to a Repository of EcmrField.java
 *
 */
@Service
public class EcmrFieldService {

  @Autowired private EcmrFieldRepository ecmrFieldRepository;
  @Autowired private FieldRepository fieldRepository;

  @PersistenceContext
  private EntityManager em;


  private final List<Long> overviewFieldIds = Arrays.asList(1L,6L,7L,9L,11L,13L,21L,29L,75L);


  public List<EcmrField> prefetchByIds(List<String> ecmrIds)
  {
    Query q = em.createQuery("SELECT DISTINCT f from EcmrField f join fetch f.ecmr where f.ecmr.ecmrId in :id and f.field.fieldId in :fieldIds ");
    q.setParameter("fieldIds", overviewFieldIds);
    q.setParameter("id",ecmrIds);
    return q.getResultList();
  }


  private static final long[] LIST_IDS = new long[]{41L,42L,43L,44L,45L,46L,47L,48L,49L,50L,51L};

  /**
   * Gets the eCMRFieldIds+EntryIds for a single eCMR
   *
   * This method returns all eCMR field ids and corresponding entryids that are connected to a single eCMR.
   * For most fields there is only a single entry but but for different field blocks (e.g. area 10 to 15), there can be multiple.
   * Always returns the eCMRFieldIds+EntryIds for the current eCMR version.
   *
   * @param id, the ecmrId for the searched eCMR
   * @return ArrayList<ecmrFieldEntry> List that contains EcmrFieldEntry objects that contain all ecmrFieldIds+EntryIds.
   */
  public List<EcmrFieldEntryData> getEcmrFieldIds(String id, int version) {
    ArrayList<EcmrFieldEntryData> returnfields = new ArrayList<>();

    Query q = em.createQuery("SELECT distinct f from EcmrField f join fetch f.ecmr where f.ecmr.ecmrId = :id and f.ecmr.version = :version", EcmrField.class);
    q.setParameter("id",id);
    q.setParameter("version",version);
    List<EcmrField> res  = q.getResultList();

    int maxversion = -1;
    for (EcmrField d1 : res) {
      if (d1.getEcmrId().equals(id)
              && d1.getVersion() > maxversion) {
        maxversion = d1.getVersion();
      }
    }
    for (EcmrField d : res) {
      if (d.getEcmrId().equals(id) && d.getVersion() == maxversion) {
        returnfields.add(new EcmrFieldEntryData(d.getFieldId(),d.getEntryId(), null));
      }
    }

    return returnfields;
  }


  /**
   * Gets the EcmrFields for a single eCMR
   *
   * This method returns all eCMR fields that are connected to a single eCMR.
   * Always returns the eCMRFieldIds for the input eCMR version.
   * Returned fields are encapsulated as EcmrFieldData.java.
   *
   * @param ecmrId, the ecmrid for the searched eCMR
   * @param version, the version for the searched eCMR
   * @return ArrayList<EcmrFieldData> List that contains all found ecmrFields
   */
  public Map<String, Object> getEcmrFields(String ecmrId, int version) {
    Map<String, Object> fieldMap = new HashMap<>();
    ArrayList<Map<String, String>> positionsList = new ArrayList<>();
    Query q = em.createQuery("SELECT distinct f from EcmrField f join fetch f.ecmr where f.ecmr.ecmrId = :id and f.ecmr.version = :version", EcmrField.class);
    q.setParameter("id",ecmrId);
    q.setParameter("version",version);
    List<EcmrField> res = new ArrayList<>();
    try {
      res = q.getResultList();
    }
    catch (Exception e)
    {
      e.printStackTrace();
    }

    for (EcmrField d : res) {
      if (d.getEcmrId().equals(ecmrId) && d.getVersion() == version ) {
        // if not a list item
        if(LongStream.of(LIST_IDS).noneMatch(x -> x == d.getFieldId())) {
          fieldMap.put(Long.toString(d.getFieldId()), d.getValue());
        }
        else {
          int entryIndex = d.getEntryId() - 1;
          while (positionsList.size() < entryIndex + 1) {
            positionsList.add(new HashMap<>());
          }
          positionsList.get(entryIndex).put(Long.toString(d.getFieldId()),d.getValue());
        }
      }
    }
    fieldMap.put("positions", positionsList);


    return fieldMap;
  }


  /**
   * Updates the EcmrFields for a eCMR
   *
   * This method updates a set of fields of a eCMR. Only the fields that are passed as input are updated.
   * Only the current version of the eCMR is updated.
   *
   * @param actualFields, the list of all the fields that should be updated
   */
  public void updateEcmrFields(
          Ecmr ecmr, List<EcmrFieldEntryData> actualFields) {


    Query q = em.createQuery("SELECT distinct f from EcmrField f join fetch f.ecmr where f.ecmr.ecmrId = :id and f.ecmr.version = :version", EcmrField.class);
    q.setParameter("id",ecmr.getEcmrId());
    q.setParameter("version",ecmr.getVersion());
    List<EcmrField> fetchedFields = q.getResultList();

    int maxversion = -1;
    for (EcmrField d1 : fetchedFields) {
      if (d1.getEcmrId().equals(ecmr.getEcmrId())
              && d1.getVersion() > maxversion) {
        maxversion = d1.getVersion();
      }
    }
    for (EcmrFieldEntryData field : actualFields) {
      var fieldcontained=false;
      Field existingfield=null;
      var entrycontained=false;
      for (EcmrField d : fetchedFields) {
        if (d.getFieldId() == field.getFieldId() && d.getEcmrId().equals(ecmr.getEcmrId()) && d.getVersion() == maxversion) {
          fieldcontained=true;
          existingfield=d.getField();
          if (d.getEntryId() == field.getEntryId()) {
            entrycontained=true;
            if (field.getValue() != null) {
              d.setValue(field.getValue());
            }
            ecmrFieldRepository.save(d);
          }

        }
      }
      if (fieldcontained && !entrycontained) {
        ecmrFieldRepository.save(new EcmrField(ecmr,existingfield,field.getEntryId(),field.getValue()));
      }
    }
  }

  /**
   * Creates ecmrFields for a new eCMR
   *
   * This method creates a set of fields of a new eCMR. Only the fields that are passed as input are created.
   *
   * @param ecmr, the eCMR that should be created
   * @param actualFields, the list of all the fields that should be created
   */
  public void createEcmrFields(
          Ecmr ecmr, List<EcmrFieldEntryData> actualFields) {
    for (EcmrFieldEntryData fieldid : actualFields) {
      var d = new EcmrField(ecmr, fieldRepository.findById(fieldid.getFieldId()).orElseThrow(()-> new EntityNotFoundException("Field not found")), fieldid.getEntryId(), fieldid.getValue());
      ecmrFieldRepository.save(d);
    }
  }

  /**
   * Gets the Sender and Consignee names of a eCMR.
   *
   * This method returns the sender and consignee names of a eCMR. Only the current version of the eCMR is used.
   *
   * @param id, the eCMRid for the eCMR that is searched
   * @param version, the eCMR version for the eCMR that is searched
   * @param prefetchedFields List of prefetched eCMR-Fields
   * @return ArrayList<String>, sender and consignee names, both added to a list
   */
  public List<String> getSenderAndConsignee(String id, int version, List<EcmrField> prefetchedFields) {
    var c = "";
    var s = "";

    for (EcmrField d : prefetchedFields) {
      if (d.getEcmrId().equals(id) && d.getVersion() == version) {
        if (d.getFieldId() == 7) {
          c = d.getValue();
        }
        if (d.getFieldId() == 1) {
          s = d.getValue();
        }
      }
    }
    List<String> n = new ArrayList<>();
    n.add(s);
    n.add(c);
    return n;
  }


  /**
   * Gets the Sender and Consignee names of a eCMR with highest version.
   *
   * This method returns the sender and consignee names of a eCMR. Only the current version of the eCMR is used.
   *
   * @param id, the eCMRid for the eCMR that is searched
   * @param prefetchedFields Prefetched eCMR-Fields
   * @return ArrayList<String>, sender and consignee names, both added to a list
   */
  public List<String> getSenderAndConsigneeLatest(String id, List<EcmrField> prefetchedFields) {
    int maxversion = -1;
    for (EcmrField d1 : prefetchedFields) {
      if (d1.getEcmrId().equals(id) && d1.getVersion() > maxversion) {
        maxversion = d1.getVersion();
      }
    }
    return getSenderAndConsignee(id, maxversion, prefetchedFields);
  }

  public String getNationalInternationalTransport(String id, List<EcmrField> prefetchedFields) {
    int maxversion = -1;

    String senderCountry = "";
    String consigneeCountry = "";

    for(EcmrField field : prefetchedFields) {
      if(field.getEcmrId().equals(id) && field.getVersion() > maxversion) {
        maxversion = field.getVersion();
      }
    }

    for(EcmrField field : prefetchedFields) {
      if(field.getEcmrId().equals(id) && field.getVersion() == maxversion) {
        if(field.getFieldId() == 6) {
          senderCountry = field.getValue();
        }
        if(field.getFieldId() == 13) {
          consigneeCountry = field.getValue();
        }
      }
    }

    return senderCountry.equals(consigneeCountry) ? "National" : "International" ;
  }


  /***
   * Gets signatures associated with a given eCMR.
   *
   * @param ecmr  New eCMR
   * @return arraylist with indices of given signatures
   */
  public List<Long> getSignatures(Ecmr ecmr)
  {


    Query q = em.createQuery("SELECT distinct f from EcmrField f join fetch f.ecmr where f.ecmr.ecmrId = :id and f.ecmr.version = :version");
    q.setParameter("id",ecmr.getEcmrId());
    q.setParameter("version",ecmr.getVersion());
    List<EcmrField> res  = q.getResultList();



    List<Long> signatures = new ArrayList<>();
    int maxversion = -1;
    for (EcmrField d1 : res) {
      if (d1.getEcmrId().equals(ecmr.getEcmrId())
              && d1.getVersion() > maxversion) {
        maxversion = d1.getVersion();
      }
    }
    for (EcmrField d : res) {
      if (d.getEcmrId().equals(ecmr.getEcmrId())
              && d.getVersion() == maxversion
              && SignatureController.SIGNATURE_FIELD_IDS.contains(Long.toString(d.getFieldId()))
              && !d.getValue().isEmpty()) {
        signatures.add(d.getFieldId());
      }
    }
    return signatures;
  }


  /**
   * Gets the Sender and Consignee names of a eCMR.
   *
   * This method returns reference number of a eCMR. Only the current version of the eCMR is used.
   *
   * @param id, the eCMRid for the eCMR that is searched
   * @param prefetchedFields Prefetched eCMR-Fields
   * @return String Reference-Number of requested eCMR
   */
  public String getReferenceNr(String id, List<EcmrField> prefetchedFields) {
    var refNr = "";

    int maxversion = -1;
    for (EcmrField d1 : prefetchedFields) {
      if (d1.getEcmrId().equals(id)
              && d1.getVersion() > maxversion) {
        maxversion = d1.getVersion();
      }
    }
    for (EcmrField d : prefetchedFields) {
      if (d.getEcmrId().equals(id)
              && d.getVersion() == maxversion
              && d.getFieldId() == 75) {
        refNr = d.getValue();
      }
    }
    return refNr;
  }


  /**
   * Gets the License Plate of a eCMR.
   *
   * This method returns a license plate of a eCMR. Only the current version of the eCMR is used.
   *
   * @param id, the eCMRid for the eCMR that is searched
   * @param prefetchedFields Prefetched eCMR-Fields
   * @return String License Plate of requested eCMR
   */
  public String getLicensePlate(String id, List<EcmrField> prefetchedFields) {

    var licensePlate = "";

    for (EcmrField d : prefetchedFields) {
      if (d.getEcmrId().equals(id)) {
        if (d.getFieldId() == 29) {
          licensePlate = d.getValue();
        }
      }
    }

    return licensePlate;
  }

  /**
   * Gets the Carrier Name of a eCMR.
   *
   * This method returns a Carrier Name of a eCMR. Only the current version of the eCMR is used.
   *
   * @param id, the eCMRid for the eCMR that is searched
   * @param prefetchedFields Prefetched eCMR-Fields
   * @return String Carrier Name of requested eCMR
   */
  public String getCarrierName(String id, List<EcmrField> prefetchedFields) {

    var carrierName = "";

    for (EcmrField d : prefetchedFields) {
      if (d.getEcmrId().equals(id)) {
        if (d.getFieldId() == 21) {
          carrierName = d.getValue();
        }
      }
    }

    return carrierName;
  }

  /**
   * Gets the Consignee Address of a eCMR.
   *
   * This method returns a Consignee Address of a eCMR. Only the current version of the eCMR is used.
   *
   * @param id, the eCMRid for the eCMR that is searched
   * @param prefetchedFields Prefetched eCMR-Fields
   * @return String Consignee Address of requested eCMR
   */
  public String getConsigneeAddress(String id, List<EcmrField> prefetchedFields) {

    StringBuilder consigneeAddress = new StringBuilder();

    for (EcmrField d : prefetchedFields) {
      if (d.getEcmrId().equals(id)) {
        if (d.getFieldId() == 9) {
          consigneeAddress = new StringBuilder(d.getValue() + ", ");
        }
        if (d.getFieldId() == 11) {
          consigneeAddress.append(d.getValue());
        }
      }
    }

    return consigneeAddress.toString();
  }

  /**
   * Duplicates the EcmrFields for a new eCMR version.
   *
   * This method duplicates the eCMR fields of the current version of a eCMR with a now increased versioncounter.
   * @param currentEcmr eCMR object referring to current version
   * @param newEcmr eCMR object referring to incoming version
   */
  public void duplicateEcmrFields(Ecmr currentEcmr, Ecmr newEcmr) {
    Query q = em.createQuery("SELECT f from EcmrField f join fetch f.ecmr where f.ecmr.ecmrId = :id and f.ecmr.version = :version");
    q.setParameter("id",currentEcmr.getEcmrId());
    q.setParameter("version",currentEcmr.getVersion());
    List<EcmrField> res  = q.getResultList();


    ArrayList<EcmrField> duplicates = new ArrayList<>();

    for (EcmrField d : res) {
      if (d.getEcmrId().equals(currentEcmr.getEcmrId()) && d.getVersion() == currentEcmr.getVersion()) {
        duplicates.add(new EcmrField(newEcmr, fieldRepository.findById(d.getFieldId()).orElseThrow(()-> new EntityNotFoundException("Field not found")), d.getEntryId(), d.getValue()));
      }
    }
    ecmrFieldRepository.saveAll(duplicates);
  }

  /**
   * Deletes all field entries to a given eCMR
   * @param ecmrId relevant id of Ecmr
   */
  public void deleteEcmrFields(String ecmrId) {

    Query q = em.createQuery("SELECT f from EcmrField f join fetch f.ecmr where f.ecmr.ecmrId = :id");
    q.setParameter("id",ecmrId);
    List<EcmrField> res  = q.getResultList();

    for (EcmrField d1 : res) {
      if (d1.getEcmrId().equals(ecmrId)) {
        ecmrFieldRepository.delete(d1);
      }
    }
  }

    public void importExternalEcmr(ExportEcmr exportEcmr, List<Ecmr> internalEcmrs)
    {
      for(int i = 0; i < exportEcmr.getEcmrData().size(); i++)
      {
        for(String key: exportEcmr.getEcmrData().get(i).getFields().keySet())
        {
          if(key.equals("positions"))
          {
            ArrayList<Map<String, String>> positionsList = (ArrayList<Map<String, String>>) exportEcmr.getEcmrData().get(i).getFields().get(key);
            for(int j = 0; j < positionsList.size(); j++)
            {
              for(String posKey : positionsList.get(j).keySet())
              {
                Optional<Field> f = fieldRepository.findById(Long.parseLong(posKey));
                if(f.isPresent()) {
                  EcmrField ecmrField = new EcmrField(internalEcmrs.get(i), f.get(), j+1, positionsList.get(j).get(posKey));
                  ecmrFieldRepository.save(ecmrField);
                }              }
            }
          }
          else
          {
            Optional<Field> f = fieldRepository.findById(Long.parseLong(key));
            if(f.isPresent()) {
              EcmrField ecmrField = new EcmrField(internalEcmrs.get(i), f.get(), 1,(String) exportEcmr.getEcmrData().get(i).getFields().get(key));
              ecmrFieldRepository.save(ecmrField);
            }
          }
        }
      }
    }


}
