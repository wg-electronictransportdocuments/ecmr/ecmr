/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.services;

import org.apache.commons.lang3.StringUtils;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.controller.BCController;
import org.siliconeconomy.ecmr.service.controller.SignatureController;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.*;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

import java.security.NoSuchAlgorithmException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * This service is used to handle eCMR management in the eCMR projekt.
 */

@Service
public class EcmrManagementService {

    public static final String E_CMR_SERVICE_USER_IS_NOT_ALLOWED1 = "[eCMR SERVICE] User is not allowed";
    public static final String E_CMR_SERVICE_USER_IS_NOT_ALLOWED = E_CMR_SERVICE_USER_IS_NOT_ALLOWED1;
    private static final Logger LOGGER = LoggerFactory.getLogger(EcmrManagementService.class);
    @Autowired
    private EcmrService ecmrService;

    @Autowired
    private TemplateService templateService;

    @Autowired
    private StatusService statusService;

    @Autowired
    private FieldService fieldService;

    @Autowired
    private EcmrFieldService ecmrFieldService;

    @Autowired
    private EcmrOriginService ecmrOriginService;

    @Autowired
    private UserService userService;

    @Autowired
    private EcmrUserService ecmrUserService;

    @Autowired
    private UserMandatoryFieldService userMandatoryFieldService;

    @Autowired
    private EcmrVersionService ecmrVersionService;

    @Autowired
    private BCController bcController;

    @Autowired
    private ConnectionService connectionService;

    @Autowired
    private UserManagement usermanagement;

    @Autowired
    private RoleService roleService;

    @Autowired
    private ExternalEcmrService externalEcmrService;

    @Autowired
    private ContactService contactService;

    @Value("${ecmr.be.ssl}")
    private String useSsl;

    /**
     * Returns all top level information (no field values) of all eCMR or Templates, that the user is connected to as a
     * list (always the highest version number)
     *
     * @param noTemplates Is used to determine, if it should get the templates or ecmrs. true -> ecmrs | false -> templates.
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> getEcmrs(boolean isArchive, boolean noTemplates) {
        LOGGER.info("[eCMR SERVICE] GetEcmrs started");
        ResponseEntity<Object> response;
        try {
            String userId = usermanagement.getCurrentUserId();

            // read eCMR list
            List<String> allowedIds;
            List<GeneralListData> allowedEcmr = new ArrayList<>();
            List<String> allIds = ecmrUserService.getEcmrList(userId);

            if(noTemplates) {
                allowedIds = ecmrService.filterTemplates(allIds);
            } else {
                allowedIds = ecmrService.filterEcmrs(allIds);
            }

            List<Ecmr> prefetchedEcmrs = ecmrService.prefetchEcmrs(allowedIds, isArchive);
            List<EcmrField> prefetchedFields = ecmrFieldService.prefetchByIds(allowedIds);
            List<EcmrOrigin> prefetchedOrigins = ecmrOriginService.prefetchEcmrVersions(allowedIds);

            for (String id : allowedIds) {
                GeneralListData d;
                if(noTemplates) {
                    d = ecmrService.getEcmrListDataByID(id, prefetchedEcmrs);
                } else {
                    d = ecmrService.getTemplateListDataByID(id, prefetchedEcmrs);
                }

                // Get meta data for displaying in list
                String username;
                if (ecmrVersionService.isCurrentVersionExternal(id)) {
                    // If newest version is external, just use given string
                    d.setLastUsername(d.getLastUserId() + " (external)");

                } else {
                    LOGGER.info("Latest version is internal, set own username");
                    username = userService.getUsername(d.getLastUserId());
                    d.setLastUsername(username);
                }

                List<String> sc = ecmrFieldService.getSenderAndConsigneeLatest(d.getEcmrId(), prefetchedFields);
                d.setSenderName(sc.get(0));
                d.setConsigneeName(sc.get(1));

                d.setReferenceNr(ecmrFieldService.getReferenceNr(d.getEcmrId(), prefetchedFields));

                d.setLicensePlate(ecmrFieldService.getLicensePlate(d.getEcmrId(), prefetchedFields));
                d.setCarrierName(ecmrFieldService.getCarrierName(d.getEcmrId(), prefetchedFields));
                d.setConsigneeAddress(ecmrFieldService.getConsigneeAddress(d.getEcmrId(), prefetchedFields));
                d.setNationalInternationalTransport(ecmrFieldService.getNationalInternationalTransport(d.getEcmrId(),prefetchedFields));

                String status = ecmrService.getEcmrStatus(d.getEcmrId(), prefetchedEcmrs);

                if (status != null) {
                    List<String> userRoles = userService.getUserRoles(userId);
                    d.setShareable(statusService.allowedToShareEcmr(status, userRoles));
                    d.setEditable(statusService.allowedToChangeEcmr(status, userRoles));
                    d.setDeletable(statusService.allowedToDeleteEcmr(status, userRoles));
                    allowedEcmr.add(d);
                    d.setHost(ecmrOriginService.getEcmrOrigins(d.getEcmrId(), prefetchedOrigins).get(0).getOrigin());
                } else {

                    LOGGER.info("[eCMR SERVICE] Got eCMR with invalid status");
                }
            }

            // return List -> 200
            if(noTemplates) {
                response = new ResponseEntity<>(allowedEcmr, HttpStatus.OK);
            } else {
                //return List as TemplateListData
                List<TemplateListData> allTemplates = new ArrayList<>();
                for(GeneralListData generalListData : allowedEcmr) {
                    if(generalListData instanceof TemplateListData) {
                        allTemplates.add((TemplateListData) generalListData);
                    }
                }
                response = new ResponseEntity<>(allTemplates, HttpStatus.OK);
            }

            LOGGER.info("[eCMR SERVICE] GetEcmrs finished");

            // If overview is still empty, check if user is new
            if (allowedIds.isEmpty() && userService.getById(userId) == null) {
                LOGGER.info("[eCMR SERVICE] New user with id " + userId + " detected, initialize contacts");
                // if so, add initial contacts
                usermanagement.initNewUser(userId);
            }

        } catch (Exception e) {
            LOGGER.info(e.getMessage());
            // Internal error occured -> 500
            response = ExceptionHandling.setInternalError();
            return response;
        }

        return response;
    }

    /**
     * Uses getEcmrs as they are logically the same, but with noTemplates set to false.
     * @return Top level information of all Templates
     */
    @Transactional
    public ResponseEntity<Object> getTemplates() {
        LOGGER.info("[eCMR SERVICE] Getting Templates via getEcmrs");
        return getEcmrs(false, false);
    }

    /**
     * creating a new eCMR or template.
     *
     * @param jsonBody JSON Request Body containing relevant content for creating eCMR
     * @param isECMR Is used to determine, if it's a new eCMR or a new template. true -> Creates a new eCMR | false -> Creates a new Template
     * @return Response including code and message
     */
    public ResponseEntity<Object> newEcmr(Map<String, Object> jsonBody, boolean isECMR) {
        LOGGER.info("[eCMR SERVICE] NewEcmr started");
        ResponseEntity<Object> response;
        // JSON must contain Signature
        if (jsonBody == null
                || !jsonBody.containsKey(EcmrConstants.SIGNATURE_KEY)
                || (jsonBody.containsKey(EcmrConstants.SIGNATURE_KEY) && jsonBody.get(EcmrConstants.SIGNATURE_KEY) == null)) {
            response = ExceptionHandling.setBadRequestError();
            return response;
        } else {
            try {
                var writingUser = userService.getById(usermanagement.getCurrentUserId());
                ArrayList<EcmrFieldEntryData> passedFields = EcmrFieldEntryData.fromJson(
                        (Map<String, Object>) jsonBody.get(EcmrConstants.ECMR_FIELD_KEY));

                var signature = "";
                if (useSsl.equals("true") && isECMR) {
                    signature = getSignature(usermanagement.getCurrentToken(), jsonBody);
                    if (signature == null) {
                        response = ExceptionHandling.setCorruptedSignatureError();
                        LOGGER.info("[eCMR SERVICE] Failed signature check by user: " + writingUser);
                        return response;
                    }
                }

                // set initial status
                var userRoles = userService.getUserRoles(writingUser.getId());

                List<Long> explicitSignatureList = new ArrayList<>();
                for (EcmrFieldEntryData passedField : passedFields) {
                    if (SignatureController.SIGNATURE_FIELD_IDS.contains(Long.toString(passedField.getFieldId())) && !passedField.getValue()
                            .isEmpty()) {
                        explicitSignatureList.add(passedField.getFieldId());
                    }
                }

                var status = statusService.getNextStatus(StatusService.statusPrioList.get(0), explicitSignatureList);

                if (!statusService.allowedToCreateEcmr(userRoles)) {
                    LOGGER.info(E_CMR_SERVICE_USER_IS_NOT_ALLOWED);
                    // Not allowed -> 403
                    response = ExceptionHandling.setUserNotAllowedError();
                    return response;
                }

                if(isECMR) {
                    if (statusService.allowedToChangeEcmr(status, userRoles)) {
                        var newEcmr = ecmrService.createEcmr(writingUser);
                        ecmrService.setEcmrStatus(newEcmr.getEcmrId(), status);
                        ecmrOriginService.addNewOrigin(newEcmr.getEcmrId());
                        response = writeEcmr(passedFields, writingUser, newEcmr, userRoles, signature);
                    } else {
                        LOGGER.info(E_CMR_SERVICE_USER_IS_NOT_ALLOWED);
                        // Not allowed -> 403
                        response = ExceptionHandling.setUserNotAllowedError();
                        return response;
                    }
                } else {
                    if (statusService.allowedToChangeEcmr(status, userRoles)) {
                        LOGGER.info("[eCMR SERVICE] Creating Template");
                        Ecmr newTemplate = ecmrService.createTemplate(writingUser, getNewTemplate(jsonBody));
                        ecmrService.setEcmrStatus(newTemplate.getEcmrId(), status);
                        ecmrOriginService.addNewOrigin(newTemplate.getEcmrId());
                        response = writeEcmr(passedFields, writingUser, newTemplate, userRoles, signature);
                    } else {
                        LOGGER.info(E_CMR_SERVICE_USER_IS_NOT_ALLOWED);
                        // Not allowed -> 403
                        response = ExceptionHandling.setUserNotAllowedError();
                        return response;
                    }
                }


            } catch (Exception e) {
                // Internal Server error -> 500
                response = ExceptionHandling.setInternalError() ;
                return response;
            }
        }
        LOGGER.info("[eCMR SERVICE] NewEcmr finished");
        return response;
    }

    /**
     * Reads the json to get the templateName and creates a new Template with given name.
     * @param jsonBody JSON Request Body containing relevant content for creating eCMR
     * @return A new Template with name taken from jsonBody
     */
    private Template getNewTemplate(Map<String, Object> jsonBody) {
        String templateName = "";
        for(var entry : jsonBody.entrySet()) {
            if(entry.getKey().equals("templateName")) {
                templateName = entry.getValue().toString();
            }
        }

        return new Template(templateName);
    }

    private String getSignature(String sessionToken, Map<String, Object> jsonBody) {
        String signature;
        try {
            // Check given Signature
            // construct signature from hex
            signature =
                    SignatureController.signatureArrayToSignatureString(
                            (ArrayList<Integer>) jsonBody.get(EcmrConstants.SIGNATURE_KEY));
            // construct string for hashing
            var concatFields = SignatureController.mapToString((Map<String, Object>) jsonBody.get(EcmrConstants.ECMR_FIELD_KEY));
            // get public key associated with given token
            var pubkey = connectionService.getCurrentPubKey(sessionToken);

            // check signature
            var signatureVerified = SignatureController.verify(signature, concatFields, pubkey);
            if (!signatureVerified) {
                LOGGER.info("Signature Error");
                signature = null;
            }
        } catch (Exception e) {
            LOGGER.info("Error in sig");
            signature = null;
        }
        return signature;
    }

    /**
     * Update a eCMR or Template for a given {id} (only if ecmr: by increasing its version number) and the status
     *
     * @param ecmrId   of the eCMR
     * @param isEcmr Is used to determine if a Tempalte or Ecmr should be updated
     * @param jsonBody JSON Request Body containing relevant content for updating eCMR
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> updateEcmrById(String ecmrId, boolean isEcmr, Map<String, Object> jsonBody) {
        LOGGER.info("[eCMR SERVICE] UpdateEcmrByID start");
        ResponseEntity<Object> response;
        // JSON must contain ecmrId and Signature
        if (jsonBody == null
                || !jsonBody.containsKey(EcmrConstants.SIGNATURE_KEY)
                || (jsonBody.containsKey(EcmrConstants.SIGNATURE_KEY) && jsonBody.get(EcmrConstants.SIGNATURE_KEY) == null)) {
            response = ExceptionHandling.setBadRequestError();
            return response;
        } else {
            try {
                var writingUser = userService.getById(usermanagement.getCurrentUserId());

                var passedFields = EcmrFieldEntryData.fromJson((Map<String, Object>) jsonBody.get(EcmrConstants.ECMR_FIELD_KEY));
                var ecmrExisting = ecmrService.checkEcmrByID(ecmrId);
                var signature = "";
                if (useSsl.equals("true")) {
                    signature = getSignature(usermanagement.getCurrentToken(), jsonBody);

                    if (signature == null) {
                        response = ExceptionHandling.setCorruptedSignatureError();
                        LOGGER.info("[eCMR SERVICE] Failed signature check by user: " + writingUser);
                        return response;
                    }
                }

                if (ecmrExisting) {
                    // Passed ecmrId is not null -> update a existing one
                    var ecmrStatus = ecmrService.getEcmrStatus(ecmrId);
                    var userRoles = userService.getUserRoles(writingUser.getId());
                    int currentVersion = ecmrVersionService.getCurrentVersion(ecmrId);
                    if (statusService.allowedToChangeEcmr(ecmrStatus, userRoles)) {
                        if (ecmrUserService.checkEcmrUser(ecmrId, writingUser.getId())) {
                            var fields = ecmrFieldService.getEcmrFieldIds(ecmrId, currentVersion);
                            var actualFields = new ArrayList<>();
                            for (EcmrFieldEntryData passedFieldEntry : passedFields) {
                                var fieldContained = false;
                                var entryContained = false;
                                for (EcmrFieldEntryData field : fields) {
                                    if (passedFieldEntry.getFieldId() == field.getFieldId()) {
                                        fieldContained = true;
                                        if (passedFieldEntry.getEntryId() == field.getEntryId()) {
                                            // put passed values to fieldentry
                                            entryContained = true;
                                            field.setValue(passedFieldEntry.getValue());
                                            actualFields.add(field.getFieldId());
                                        }
                                    }
                                }
                                if (fieldContained && !entryContained) {
                                    //new entry to an existing field has been added
                                    fields.add(passedFieldEntry);
                                    actualFields.add(passedFieldEntry.getFieldId());
                                }
                            }

                            var mandatoryFields =
                                    userMandatoryFieldService.getUserMandatoryFieldIds(writingUser.getUserRoles());
                            var mandatoryMissing = false;
                            for (Long mandatoryId : mandatoryFields) {
                                if (!actualFields.contains(mandatoryId)) {
                                    mandatoryMissing = true;
                                }
                            }
                            if (!mandatoryMissing) {
                                if(isEcmr) {
                                    var currentEcmr = ecmrService.getLatestEcmr(ecmrId);
                                    var newEcmr = ecmrService.updateEcmr(ecmrId, writingUser.getId());

                                    ecmrFieldService.duplicateEcmrFields(currentEcmr, newEcmr);
                                    ecmrFieldService.updateEcmrFields(newEcmr, fields);

                                    var signatureFields = ecmrFieldService.getSignatures(newEcmr);

                                    var newStatus = statusService.getNextStatus(ecmrStatus, signatureFields);
                                    newEcmr.setStatus(newStatus);
                                    ecmrService.setEcmrStatus(
                                            ecmrId, newStatus);

                                    ecmrVersionService.setNewEcmrVersion(
                                            new EcmrVersion(ecmrId, currentVersion + 1, writingUser.getId(), signature, false));


                                    // Notify host if necessary
                                    if(ecmrOriginService.isExternal(ecmrId))
                                    {
                                        externalEcmrService.notifyEcmrOrigin(newEcmr);
                                    }
                                    //if host notify other participants
                                    else {
                                        // TODO more efficient
                                        EcmrData ecmrData = new EcmrData(newEcmr);
                                        ecmrData.setLastUsername(writingUser.getUserName());
                                        ecmrData.setFields(ecmrFieldService.getEcmrFields(newEcmr.getEcmrId(), newEcmr.getVersion()));
                                        EcmrUpdateObject ecmrUpdateObject = new EcmrUpdateObject(ecmrData, ecmrOriginService.getCurrentAddress());
                                        externalEcmrService.notifyParticipants(ecmrUpdateObject);
                                    }

                                    // write on BC
                                    var bcMetaData =
                                            new BCMetaData(newEcmr.getVersion(), ecmrOriginService.getEcmrOriginById(ecmrId).getOrigin());
                                    bcController.write(ecmrId, passedFields, bcMetaData);
                                } else {
                                    var currentEcmr = ecmrService.getLatestEcmr(ecmrId);

                                    ecmrFieldService.updateEcmrFields(currentEcmr, fields);

                                    var signatureFields = ecmrFieldService.getSignatures(currentEcmr);

                                    var newStatus = statusService.getNextStatus(ecmrStatus, signatureFields);
                                    currentEcmr.setStatus(newStatus);
                                    ecmrService.setEcmrStatus(
                                            ecmrId, newStatus);


                                    // Notify host if necessary
                                    if(ecmrOriginService.isExternal(ecmrId))
                                    {
                                        externalEcmrService.notifyEcmrOrigin(currentEcmr);
                                    }
                                    //if host notify other participants
                                    else {
                                        // TODO more efficient
                                        EcmrData ecmrData = new EcmrData(currentEcmr);
                                        ecmrData.setLastUsername(writingUser.getUserName());
                                        ecmrData.setFields(ecmrFieldService.getEcmrFields(currentEcmr.getEcmrId(), currentEcmr.getVersion()));
                                        EcmrUpdateObject ecmrUpdateObject = new EcmrUpdateObject(ecmrData, ecmrOriginService.getCurrentAddress());
                                        externalEcmrService.notifyParticipants(ecmrUpdateObject);
                                    }

                                    //write on BC
                                    var bcMetaData =
                                            new BCMetaData(currentEcmr.getVersion(), ecmrOriginService.getEcmrOriginById(ecmrId).getOrigin());
                                    bcController.write(ecmrId, passedFields, bcMetaData);
                                }
                            } else {
                                LOGGER.info("[eCMR SERVICE] MandatoryFields missing");
                                // Bad Request --> 400
                                response = ExceptionHandling.setMandatoryMissingError();
                                return response;
                            }
                        } else {
                            LOGGER.info(E_CMR_SERVICE_USER_IS_NOT_ALLOWED);
                            // Not allowed -> 403
                            response = ExceptionHandling.setUserNotAllowedError();
                            return response;
                        }
                    } else {
                        LOGGER.info("[eCMR SERVICE] Changes to eCMR not allowed because of Status");
                        // Not allowed -> 403
                        response = ExceptionHandling.setNotAllowedByStatusError();
                        return response;
                    }
                    // because of PUT the status must be 204/NO Content
                    response = new ResponseEntity<>(StringUtils.EMPTY, HttpStatus.NO_CONTENT);
                    LOGGER.info("[eCMR SERVICE] UpdateEcmrByID finished");
                } else {
                    LOGGER.info("[eCMR SERVICE] Invalid eCMRid in setEcmrId");
                    // Resource not found -> 404
                    response = ExceptionHandling.setNotFoundError();
                    return response;
                }

            } catch (Exception e) {
                // Internal Server error -> 500
                response = ExceptionHandling.setInternalError();
                return response;
            }
        }
        return response;
    }

    private ResponseEntity<Object> writeEcmr(ArrayList<EcmrFieldEntryData> passedFields, UserFacade assignedUser, Ecmr newEcmr, List<String> userRoles, String signature) {
        ResponseEntity<Object> response;
        LOGGER.info("[eCMR SERVICE] writeEcmr start");
        var fieldIds = fieldService.getTemplateFieldIds();
        List<EcmrFieldEntryData> actualFields = new ArrayList<>();
        List<Long> realEntries = new ArrayList<>();
        for (Long fieldId : fieldIds) {
            var contained = false;
            for (EcmrFieldEntryData fieldentry : passedFields) {
                if (fieldentry.getFieldId() == fieldId) {
                    // set value from passed map
                    actualFields.add(fieldentry);
                    realEntries.add(fieldId);
                    contained = true;
                }
            }
            if (!contained) {
                // if not passed, insert default
                actualFields.add(new EcmrFieldEntryData(fieldId, 1, ""));
            }
        }
        var mandatoryMissing = false;
        if (userRoles != null && !userRoles.isEmpty()) {
            // get mandatory fields for user and check if all were passed
            var mandatoryFields =
                    userMandatoryFieldService.getUserMandatoryFieldIds(assignedUser.getUserRoles());
            for (Long mandatoryId : mandatoryFields) {
                if (!realEntries.contains(mandatoryId)) {
                    mandatoryMissing = true;
                    break;
                }
            }
        }
        // if all necessary fields are passed, save eCMR
        if (!mandatoryMissing || userRoles.isEmpty()) {
            // Create eCMR Object and save values
            var responseData = ecmrService.getEcmrResponseData(newEcmr);
            response = new ResponseEntity<>(responseData, HttpStatus.CREATED);
            ecmrFieldService.createEcmrFields(
                    newEcmr, actualFields);
            // Set User to eCMR
            ecmrUserService.addEcmrUser(responseData.getEcmrId(), assignedUser.getId());

            LOGGER.info("UserID: {}", assignedUser.getId());
            // Add inital version to DB
            ecmrVersionService.setNewEcmrVersion(
                    new EcmrVersion(responseData.getEcmrId(), 1, assignedUser.getId(), signature, false));

            // add IML admin for piloting
            //ecmrUserService.addEcmrUser(responseData.getEcmrId(), ecmrUserService.getImlAdminId());

            var bcMetaData = new BCMetaData(newEcmr.getVersion(), ecmrOriginService.getEcmrOriginById(responseData.getEcmrId()).getOrigin());

            bcController.write(
                    responseData.getEcmrId(), passedFields, bcMetaData);

        } else {
            LOGGER.info("[eCMR SERVICE] MandatoryFields missing");
            // Bad request -> 400
            response = ExceptionHandling.setMandatoryMissingError();
            return response;
        }
        LOGGER.info("[eCMR SERVICE] writeEcmr finished");

        return response;
    }

    /**
     * Interface for deleting a eCMR by id
     *
     * @param ecmrID ID for deleting eCMR
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> deleteEcmrbyId(String ecmrID, boolean isEcmr) {
        LOGGER.info("[eCMR SERVICE] DeleteECMR started");
        ResponseEntity<Object> response;
        String localEcmrId = ecmrID;

        if ("-1".equals(localEcmrId)) {
            LOGGER.info("[eCMR SERVICE] Invalid ecmrId");
            response = ExceptionHandling.setNotFoundError();
            return response;
        } else if ("null".equals(localEcmrId)) {
            LOGGER.info("[eCMR SERVICE] Invalid ecmrId");
            response = ExceptionHandling.setBadRequestError();
            return response;
        }

        String userId = usermanagement.getCurrentUserId();
        // Check user -> should be disponent
        List<String> userRoles = userService.getUserRoles(userId);
        if (!userRoles.contains(RoleService.roleNames.get(0)) && !roleService.isSuperUser(userRoles)) {
            LOGGER.info("[eCMR SERVICE] Invalid User");
            response = ExceptionHandling.setUnauthorizedError();
            return response;
        }

        if(!isEcmr) {
            LOGGER.info("[eCMR SERVICE] Deleting Template with id: " + localEcmrId);
            Template template = templateService.getById(Long.parseLong(localEcmrId)).iterator().next();
            localEcmrId = template.getEcmr().getEcmrId();

            ecmrFieldService.deleteEcmrFields(localEcmrId);
            ecmrVersionService.deleteEcmr(localEcmrId);
            ecmrUserService.deleteEcmrUserConnections(localEcmrId);
            templateService.deleteTemplateById(template.getTemplateId());
            LOGGER.info("[eCMR SERVICE] Finished deleting Template");
        } else {
            // Check eCMR -> should be NEW
            String status = ecmrService.getEcmrStatus(localEcmrId);
            if (!status.equals(StatusService.statusPrioList.get(0))) {
                LOGGER.info("[eCMR SERVICE] Invalid Status");
                response = ExceptionHandling.setNotFoundError();
                return response;
            }
            // Set status to deleted
            ecmrService.setEcmrStatus(localEcmrId, "DELETED");
            // Delete Fields
            ecmrFieldService.deleteEcmrFields(localEcmrId);
            // Delete versions
            ecmrVersionService.deleteEcmr(localEcmrId);
            // Delete eCMR_User connection
            ecmrUserService.deleteEcmrUserConnections(localEcmrId);


            // Update entry on BC
            // write on BC
            // Only host can delete ecmr -> Address is given by host address
            var bcMetaData =
                    new BCMetaData(-1, ecmrOriginService.getCurrentAddress());
            bcController.write(localEcmrId, new ArrayList<>(), bcMetaData);
        }


        // set successful response
        response = new ResponseEntity<>(HttpStatus.OK);
        LOGGER.info("[eCMR SERVICE] Delete successful");
        return response;
    }

    /**
     * Returns all information of a existing eCMR with {id}. A version will be returned.
     *
     * @param ecmrId of the eCMR
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> getEcmrbyIdandVersion(String ecmrId, String version) {
        LOGGER.info("[eCMR SERVICE] GetEcmrByIdAndVersion started");
        ResponseEntity<Object> response;

        try {

            var userId = usermanagement.getCurrentUserId();
            Integer ecmrVersion;
            // Check if specific version of eCMR is passed
            ecmrVersion = Integer.parseInt(version);
            // If requested eCMR actually exists, return as response
            if (ecmrService.checkEcmr(ecmrId, ecmrVersion)) {
                // read eCMR fields
                var ecmr = ecmrService.getEcmr(ecmrId, ecmrVersion);
                if (ecmr != null) {
                    if (ecmrUserService.checkEcmrUser(ecmr.getEcmrId(), userId)) {
                        String username = userService.getUsername(ecmr.getLastUserId());
                        ecmr.setLastUsername(username);
                        ecmr.setFields(
                                ecmrFieldService.getEcmrFields(ecmr.getEcmrId(), ecmr.getVersion()));
                    } else {
                        LOGGER.info(E_CMR_SERVICE_USER_IS_NOT_ALLOWED1);
                        // User not allowed -> 403
                        response = ExceptionHandling.setUserNotAllowedError();
                        return response;
                    }
                }
                // Everything ok, return eCMR + 200
                response = new ResponseEntity<>(ecmr, HttpStatus.OK);
                LOGGER.info("[eCMR SERVICE] GetEcmrByIdAndVersion finished");
            } else {
                // requested resource does not exist -> 404
                LOGGER.info("[eCMReCMR SERVICE] Invalid eCMRid in GetecmrId");
                response = ExceptionHandling.setNotFoundError();
                return response;
            }

        } catch (Exception e) {
            // Internal error occurred -> 500
            response = ExceptionHandling.setInternalError();
            return response;
        }
        return response;
    }

    /**
     * Returns all top level information (no field values) of all versions of a eCMR that the user is connected to as a list.
     *
     * @param ecmrId of the eCMR to read the eCMR history for
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> getEcmrHistoryById(String ecmrId) {
        LOGGER.info("[eCMR SERVICE] getEcmrHistoryById started");
        ResponseEntity<Object> response;
        // check if needed ecmrId is passed
        try {

            var userId = usermanagement.getCurrentUserId();

            // read eCMR list
            var ecmr = ecmrService.getEcmrHistory(ecmrId);
            List<EcmrListData> allowedEcmr = new ArrayList<>();
            var allowedIds = ecmrUserService.getEcmrList(userId);
            // Add only those eCMRs which are allowed for given user
            for (EcmrListData d : ecmr) {
                if (allowedIds.contains(d.getEcmrId())) {
                    allowedEcmr.add(d);
                }
            }
            List<EcmrField> prefetchedFields = ecmrFieldService.prefetchByIds(allowedIds);
            for (EcmrListData d : allowedEcmr) {
                String username;
                if (ecmrVersionService.isVersionExternal(d.getEcmrId(), d.getVersion())) {
                    d.setLastUsername(d.getLastUserId() + " (external)");
                } else {

                    username = userService.getUsername(d.getLastUserId());
                    d.setLastUsername(username);
                }
                List<String> sc = ecmrFieldService.getSenderAndConsignee(d.getEcmrId(), d.getVersion(), prefetchedFields);
                d.setSenderName(sc.get(0));
                d.setConsigneeName(sc.get(1));
                d.setReferenceNr(ecmrFieldService.getReferenceNr(d.getEcmrId(), prefetchedFields));
            }
            // return eCMR History -> 200
            response = new ResponseEntity<>(allowedEcmr, HttpStatus.OK);

            LOGGER.info("[eCMR SERVICE] getEcmrHistoryById finished");

        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            response = ExceptionHandling.setInternalError();
            return response;
        }
        return response;
    }

    /**
     * Creates a new connection eCMR↔Users and updates the eCMR status.
     *
     * @param ecmrId   id of eCMR
     * @param jsonBody contains the relevant information like userId
     * @return Response including code and message
     */
    public ResponseEntity<Object> setUsersAtEcmr(String ecmrId, Map<String, Object> jsonBody) {
        LOGGER.info("[eCMR SERVICE] setUsersAtEcmr started");
        ResponseEntity<Object> response;

        // JSON must not be null and contain key users
        if (!(jsonBody.containsKey("users"))) {
            LOGGER.info("[eCMR SERVICE] setUsersAtEcmr users is missing in jsonBody");
            response = ExceptionHandling.setBadRequestError();
        } else {
            try {
                response = new ResponseEntity<>(HttpStatus.OK);
                ResponseEntity<Object> setUserAtEcmrResponse;

                List<HashMap<String, String>> users = (List<HashMap<String, String>>) jsonBody.get("users");

                for (HashMap<String, String> user : users) {
                    LOGGER.info("Ecmr to user {}", user.get("userId"));
                    setUserAtEcmrResponse = setUserAtEcmr(user.get("userId"), ecmrId);
                    // An error occurred using setUserAtEcmr
                    if (!setUserAtEcmrResponse.getStatusCode().equals(HttpStatus.NO_CONTENT)) {
                        response = setUserAtEcmrResponse;
                    }
                }
                LOGGER.info("[eCMR SERVICE] setUsersAtEcmr finished");
            } catch (Exception e) {
                // Internal error -> 500
                response = ExceptionHandling.setInternalError();
            }
        }
        return response;
    }

    /**
     * Creates a new connection eCMR↔User and updates the eCMR status.
     *
     * @param userIdToSet id of user
     * @param ecmrId      id of eCMR
     * @return Response including code and message
     */
    //@Transactional
    public ResponseEntity<Object> setUserAtEcmr(String userIdToSet, String ecmrId) {
        LOGGER.info("[eCMR SERVICE] setUserAtEcmr started");
        ResponseEntity<Object> response;

        try {
            if (!ecmrService.checkEcmrByID(ecmrId)) {
                LOGGER.info("[eCMR SERVICE] eCMR does not exist");
                response = ExceptionHandling.setNotFoundError();
                return response;
            }

            var userId = usermanagement.getCurrentUserId();
            var ecmrsAlreadySetToNewUser = ecmrUserService.getEcmrList(userIdToSet);
            var permissionAdded = false;
            for (String i : ecmrsAlreadySetToNewUser) {
                if (i.equals(ecmrId)) {
                    permissionAdded = true;
                }
            }
            if (!permissionAdded) {
                // if no permission is active, add one
                ecmrUserService.addEcmrUser(ecmrId, userIdToSet);
                // update amount of contacts + 1
                contactService.updateAmountOfContacts(userId, userIdToSet);
                // Entity and amount updated -> 204
                LOGGER.info("[eCMR SERVICE] addEcmrUser and updateAmountOfContacts finished");
            } else {
                LOGGER.info("[eCMR SERVICE] User already associated");
            }
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);
            LOGGER.info("[eCMR SERVICE] setUserAtEcmr finished");

        } catch (Exception e) {
            // Internal error -> 500
            response = ExceptionHandling.setInternalError();
            return response;
        }
        return response;
    }

    /**
     * Creates a new connection eCMR↔User and updates the eCMR status.
     *
     * @param ecmrid id of eCMR
     * @return Response including code and message
     */
    public ResponseEntity<Object> setCurrentUserAtEcmr(String ecmrid) {
        LOGGER.info("[eCMR SERVICE] setCurrentUserAtEcmr started");
        ResponseEntity<Object> response;

        try {

            if (!ecmrService.checkEcmrByID(ecmrid)) {
                LOGGER.info("[eCMR SERVICE] eCMR does not exist");
                response = ExceptionHandling.setNotFoundError();
                return response;
            }
            var userId = usermanagement.getCurrentUserId();
            var allowedIds = ecmrUserService.getEcmrList(userId);
            boolean permissionAdded = false;
            for (String i : allowedIds) {
                if (i.equals(ecmrid)) {
                    permissionAdded = true;
                    break;
                }
            }
            if (!permissionAdded) {
                // if no permission is active, add one
                ecmrUserService.addEcmrUser(ecmrid, userId);
                // Update eCMR Status
                // Entity updated -> 204
            } else {
                LOGGER.info("[eCMR SERVICE] User already associated");
            }
            response = new ResponseEntity<>(HttpStatus.NO_CONTENT);

            LOGGER.info("[eCMR SERVICE] setCurrentUserAtEcmr finished");

        } catch (Exception e) {
            // Internal error -> 500
            response = ExceptionHandling.setInternalError();
            return response;
        }

        return response;
    }

    /**
     * Interface for generating a eCMR by external systems e.g. WMS/TMW
     *
     * @param jsonBody json containing relevant informations
     * @return Response including code and message
     */
    public ResponseEntity<Object> autogenerateEcmr(Map<String, Object> jsonBody) {

        ResponseEntity<Object> response;
        LOGGER.info("[eCMR SERVICE] autogenerateEcmr started");

        // check if request is valid
        if (
                jsonBody == null ||
                        !jsonBody.containsKey(EcmrConstants.ECMR_FIELD_KEY)
        ) {
            response = ExceptionHandling.setBadRequestError();
            return response;
        } else if (!jsonBody.containsKey("creator")) {
            response = ExceptionHandling.setBadRequestError();
            return response;
        } else {

            UserFacade assignedUser;
            try {
                //var userId = usermanagement.getCurrentUserId();
                String userMail = (String) jsonBody.get("creator");
                assignedUser = userService.getById(userService.getUserIDByEmail(userMail));
            } catch (EntityNotFoundException enfe) {
                return ExceptionHandling.setNotFoundError();
            }

            ArrayList<EcmrFieldEntryData> passedFields = EcmrFieldEntryData.fromJson((Map<String, Object>) jsonBody.get(EcmrConstants.ECMR_FIELD_KEY));
            try {
                var newEcmr = ecmrService.createEcmr(assignedUser, StatusService.statusPrioList.get(0));
                List<String> userRoles = Collections.emptyList();
                ecmrOriginService.addNewOrigin(newEcmr.getEcmrId());

                response = writeEcmr(passedFields, assignedUser, newEcmr, userRoles, null);

                // publish new ecmr to colleagues!
                try {
                    List<String> assignees = (List<String>) jsonBody.get("assignees");
                    for (String email : assignees) {
                        LOGGER.info("Assign to " + email);
                        ecmrUserService.addEcmrUser(newEcmr.getEcmrId(), userService.getUserIDByEmail(email));
                    }
                } catch (Exception e) {
                    // ignore
                    LOGGER.info("Could not assign user");
                    e.printStackTrace(System.out);
                }

            } catch (NoSuchAlgorithmException nsae) {
                return ExceptionHandling.setNotFoundError();
            }

        }
        LOGGER.info("[eCMR SERVICE] autogenerateEcmr finished [" + response.getStatusCode() + "]");
        return response;

    }

    /**
     * Returns all field definitions that are used within a system
     *
     * @return Response including code and message
     */
    public ResponseEntity<Object> getFields() {
        LOGGER.info("[eCMR SERVICE] getFields not used yet");
        return ExceptionHandling.setNotFoundError();
    }

    /**
     * Returns all field definitions for a single eCMR.
     *
     * @param id eCMR id
     * @return Response including code and message
     */
    public ResponseEntity<Object> getFieldsById(String id) {
        LOGGER.info("[eCMR SERVICE] getFieldsById not used yet");
        return ExceptionHandling.setNotFoundError();
    }

    /**
     * Returns all information of a existing eCMR with {id}. The latest version will be returned.
     *
     * @param ecmrId of the eCMR
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> getEcmrbyId(String ecmrId) {
        LOGGER.info("[eCMR SERVICE] getEcmrbyId started");
        ResponseEntity<Object> response;

        try {
            var userId = usermanagement.getCurrentUserId();
            Integer ecmrVersion = null;
            var ecmrExisting = false;
            // Check if specific version of eCMR is passed
            ecmrExisting = ecmrService.checkEcmrByID(ecmrId);

            // If requested eCMR actually exists, return as response
            if (ecmrExisting) {
                // read eCMR fields
                var ecmr = ecmrService.getEcmr(ecmrId, ecmrVersion);
                if (ecmr != null) {
                    if (ecmrUserService.checkEcmrUser(ecmr.getEcmrId(), userId)) {
                        ecmr.setLastUserId(userId);

                        ecmr.setHost(ecmrOriginService.getEcmrOriginById(ecmrId).getOrigin());
                        // Check if ecmr is external for User association
                        if (ecmrOriginService.isExternal(ecmrId)) {

                            ecmr.setLastUsername(ecmr.getLastUserId());
                        } else {
                            String username = userService.getUsername(ecmr.getLastUserId());
                            ecmr.setLastUsername(username);
                        }
                        LOGGER.info("Start fetching fields");
                        ecmr.setFields(
                                ecmrFieldService.getEcmrFields(ecmr.getEcmrId(), ecmr.getVersion()));
                        LOGGER.info("finished fetching fields");
                    } else {
                        LOGGER.info(E_CMR_SERVICE_USER_IS_NOT_ALLOWED);
                        // User not allowed -> 403
                        response = ExceptionHandling.setUserNotAllowedError();
                        return response;
                    }
                }
                // Everything ok, return eCMR + 200
                response = new ResponseEntity<>(ecmr, HttpStatus.OK);
                LOGGER.info("[eCMR SERVICE] Get eCMRbyID finished");
            } else {
                // requested resource does not exist -> 404
                LOGGER.info("[eCMReCMR SERVICE] Invalid eCMRid in GetecmrId");
                response = ExceptionHandling.setNotFoundError();
                return response;
            }

        } catch (NumberFormatException nfe) {
            // parsing error because of ugly eCMRId
            response = ExceptionHandling.setBadRequestError();
            return response;
        } catch (Exception e) {
            // Internal error occurred -> 500
            response = ExceptionHandling.setInternalError();
            return response;
        }

        return response;
    }

    /**
     * Returns all information of a existing eCMR with {id}. The latest version will be returned.
     *
     * @param ecmrId of the eCMR
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> getEcmrbyIdAndTan(String ecmrId, String tan) {
        LOGGER.info("[eCMR SERVICE] getEcmrbyId started");
        ResponseEntity<Object> response;

        try {
            Integer ecmrVersion = null;
            var ecmrExisting = false;
            // Check if specific version of eCMR is passed
            ecmrExisting = ecmrService.checkEcmrByID(ecmrId);

            // If requested eCMR actually exists, return as response
            if (ecmrExisting) {
                // read eCMR fields
                var ecmr = ecmrService.getEcmr(ecmrId, ecmrVersion);
                if (ecmr != null) {
                    if (ecmrUserService.isTanCorrect(ecmrId, tan)) {

                        ecmr.setHost(ecmrOriginService.getEcmrOriginById(ecmrId).getOrigin());
                        // Check if ecmr is external for User association
                        if (ecmrOriginService.isExternal(ecmrId)) {

                            ecmr.setLastUsername(ecmr.getLastUserId());
                        } else {
                            String username = userService.getUsername(ecmr.getLastUserId());
                            ecmr.setLastUsername(username);
                        }
                        LOGGER.info("Start fetching fields");
                        ecmr.setFields(
                                ecmrFieldService.getEcmrFields(ecmr.getEcmrId(), ecmr.getVersion()));
                        LOGGER.info("finished fetching fields");
                    } else {
                        LOGGER.info(E_CMR_SERVICE_USER_IS_NOT_ALLOWED);
                        // User not allowed -> 403
                        response = ExceptionHandling.setUserNotAllowedError();
                        return response;
                    }
                }
                // Everything ok, return eCMR + 200
                response = new ResponseEntity<>(ecmr, HttpStatus.OK);
                LOGGER.info("[eCMR SERVICE] Get eCMRbyID finished");
            } else {
                // requested resource does not exist -> 404
                LOGGER.info("[eCMReCMR SERVICE] Invalid eCMRid in GetecmrId");
                response = ExceptionHandling.setNotFoundError();
                return response;
            }

        } catch (NumberFormatException nfe) {
            // parsing error because of ugly eCMRId
            response = ExceptionHandling.setBadRequestError();
            return response;
        } catch (Exception e) {
            // Internal error occurred -> 500
            response = ExceptionHandling.setInternalError();
            return response;
        }

        return response;
    }

    /**
     * Returns all information of a existing eCMR with {id}. The latest version will be returned.
     *
     * @param templateId ID of the template
     * @return Response including code and message
     */
    @Transactional
    public ResponseEntity<Object> getTemplateById(Long templateId) {
        Template template = templateService.getById(templateId).iterator().next();
        return new ResponseEntity<>(template, HttpStatus.OK);
    }


}
