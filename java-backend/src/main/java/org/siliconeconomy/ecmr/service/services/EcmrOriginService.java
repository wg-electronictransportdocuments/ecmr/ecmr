/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.EcmrOrigin;
import org.siliconeconomy.ecmr.service.models.ExportEcmr;
import org.siliconeconomy.ecmr.service.repositories.EcmrOriginRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;


@PropertySource("classpath:application.properties")
@Service
public class EcmrOriginService {

    @Autowired
    private EcmrOriginRepository ecmrOriginRepository;


    @Value("${ecmr.be.address}")
    private String hostAddress;

    @PersistenceContext
    private EntityManager em;

    public void addEcmrOrigin(ExportEcmr exportEcmr)
    {

        ecmrOriginRepository.save(new EcmrOrigin(exportEcmr.getEcmrData().get(0).getEcmrId(), exportEcmr.getOrigin().getOrigin()));
    }

    /**
     * Get EcmrOrigin for List of EcmrIds.
     * @param allowedIds List of eCMR-Ids
     * @return List of all EcmrOrigins for given eCMR-Ids
     */
    public List<EcmrOrigin> prefetchEcmrVersions(List<String> allowedIds)
    {
        Query q = em.createQuery("SELECT DISTINCT e from EcmrOrigin e where e.ecmrId in :ids");
        q.setParameter("ids",allowedIds);
        return q.getResultList();
    }

    /**
     * Get EcmrOrigin for given eCMR-Id
     * @param ecmrId Id of eCMR
     * @return List of EcmrOrigins in DB
     */
    public EcmrOrigin getEcmrOriginById(String ecmrId)
    {
        Query q = em.createQuery("SELECT DISTINCT e from EcmrOrigin e where e.ecmrId = :id");
        q.setParameter("id",ecmrId);
        return (EcmrOrigin) q.getSingleResult();
    }

    /**
     * Get EcmrOrigin for given eCMR-Id based on prefetched EcmrOrigins
     * @param ecmrId List of EcmrOrigins for given Id
     * @param prefetchedOrigins List of EcmrOrigins
     * @return List of EcmrOrigins for given Id
     */
    public List<EcmrOrigin> getEcmrOrigins(String ecmrId, List<EcmrOrigin> prefetchedOrigins)
    {
        List<EcmrOrigin> result = new ArrayList<>();
        for (EcmrOrigin evo:prefetchedOrigins) {
            if(evo.getEcmrId().equals(ecmrId))
            {
                result.add(evo);
            }
        }
        return result;
    }

    /**
     * Creates new EcmrOrigin for every update
     * @param ecmrId Id of eCMR
     */
    public void addNewOrigin(String ecmrId)
    {
        EcmrOrigin newOrigin = new EcmrOrigin(ecmrId, hostAddress);
        ecmrOriginRepository.save(newOrigin);
    }

    public boolean isExternal(String ecmrId)
    {
        try {
            String origin = getEcmrOriginById(ecmrId).getOrigin();
            return !origin.equals(hostAddress);
        }
        catch (NoResultException nre)
        {
            // ecmr was not found in origin table --> external
            return true;
        }
    }

    public String getCurrentAddress()
    {
        return hostAddress;
    }

    public boolean isCurrentInstance(String otherHost)
    {
        return otherHost.equals(hostAddress);
    }


}
