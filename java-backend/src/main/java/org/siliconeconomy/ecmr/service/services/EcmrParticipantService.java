/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.EcmrParticipant;
import org.siliconeconomy.ecmr.service.repositories.EcmrParticipantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Service
public class EcmrParticipantService {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private EcmrParticipantRepository ecmrParticipantRepository;

    public List<EcmrParticipant> getParticipantsByEcmrId(String ecmrId)
    {
        Query q = em.createQuery("SELECT DISTINCT ep from EcmrParticipant ep where ep.ecmrId = :id");
        q.setParameter("id",ecmrId);
        return q.getResultList();
    }

    public boolean isAlreadyParticipant(String participantOrigin, String ecmrId)
    {
        Query q = em.createQuery("SELECT DISTINCT ep from EcmrParticipant ep where ep.ecmrId = :id and ep.participantOrigin = :origin");
        q.setParameter("id",ecmrId);
        q.setParameter("origin",participantOrigin);
        return q.getResultList().size() > 0;
    }


    public void addNewParticipant(EcmrParticipant ecmrParticipant)
    {
        if(!isAlreadyParticipant(ecmrParticipant.ecmrId,ecmrParticipant.getParticipantOrigin())) {
            ecmrParticipantRepository.save(new EcmrParticipant(ecmrParticipant.getParticipantOrigin(), ecmrParticipant.getEcmrId()));
        }
    }
}
