/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.*;
import org.siliconeconomy.ecmr.service.repositories.EcmrRepository;
import org.siliconeconomy.ecmr.service.repositories.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * This class defines a service that allows to connect to a Repository of Ecmr.java
 *
 */
@Service
public class EcmrService {

    @PersistenceContext
    private EntityManager em;
    @Autowired
    private EcmrRepository ecmrRepository;
    @Autowired
    private TemplateRepository templateRepository;

    /**
     * Searches eCMR db table for entries by ecmrId
     * @param ecmrId id of eCMR
     * @return list of found objects
     */
    public List<Ecmr> getById(String ecmrId) {
        Query q = em.createQuery("SELECT DISTINCT e from Ecmr e where e.ecmrId = :id");
        q.setParameter("id", ecmrId);
        return q.getResultList();
    }

    /**
     * Searches eCMR db table for entries by ecmrId and version number
     * @param ecmrId id of eCMR
     * @param version version of eCMR
     * @return list of found objects
     */
    public List<Ecmr> getByIdAndVersion(String ecmrId, int version) {
        Query q = em.createQuery("SELECT DISTINCT e from Ecmr e where e.ecmrId = :id and e.version = :version");
        q.setParameter("id", ecmrId);
        q.setParameter("version", version);
        return q.getResultList();
    }

    public List<Ecmr> prefetchEcmrs(List<String> ecmrIds, boolean archived) {
        return ecmrRepository.getEcmrByEcmrIdInAndArchived(ecmrIds, archived);
    }

    /**
     * Filters all eCMRs by Templates. Only gets the eCMR id, if a Template for the given eCMR is NOT present.
     * @param ecmrIds IDs of the eCMRs to get
     * @return A list of eCMR ids that do not have a Template, therefore real eCMRs and not Templates
     */
    public List<String> filterTemplates(List<String> ecmrIds) {
        Query q = em.createQuery("SELECT DISTINCT e.ecmrId from Ecmr e left join e.template t where e.ecmrId in :ids and t is null");
        q.setParameter("ids", ecmrIds);
        return q.getResultList();
    }

    /**
     * Filters all eCMRs by Templates. Only gets the eCMR id, if a Template for the given eCMR is present.
     * @param ecmrIds IDs of the eCMRs to get
     * @return A list of eCMRs which are Templates
     */
    public List<String> filterEcmrs(List<String> ecmrIds) {
        Query q = em.createQuery("SELECT DISTINCT e.ecmrId from Ecmr e left join e.template t where e.ecmrId in :ids and t is not null");
        q.setParameter("ids", ecmrIds);
        return q.getResultList();
    }

    /**
     * Checks, whether a eCMR exists.
     * <p>
     * This method checks of a single version of a eCMR exists.
     *
     * @param id      the eCMRid for the eCMR that is searched
     * @param version the version for the eCMR that is searched
     * @return boolean, true, when eCMR is found; false otherwise
     */
    public boolean checkEcmr(String id, int version) {
        return getByIdAndVersion(id, version).size() > 0;
    }

    /**
     * Checks, whether a eCMR exists.
     * <p>
     * This method checks if a eCMR exists at all.
     *
     * @param id the eCMRid for the eCMR that is searched
     * @return boolean, true, when eCMR is found; false otherwise
     */
    public boolean checkEcmrByID(String id) {
        var maxversion = -1;
        List<Ecmr> relevantList = getById(id);
        for (Ecmr d1 : relevantList) {
            if (d1.getVersion() > maxversion) {
                maxversion = d1.getVersion();
                break;
            }
        }
        for (Ecmr d : relevantList) {
            if (d.getEcmrId().equals(id) && d.getVersion() == maxversion) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates a new eCMR
     * <p>
     * This method creates a new eCMR in the repository. The new eCMRs id is created based on current time and userid, the version is 1.
     *
     * @param user the userid that creates the eCMR
     * @return ecmrIdData, that combines all two identifiers of the new eCMR (eCMRid, version)
     */
    public Ecmr createEcmr(UserFacade user) throws NoSuchAlgorithmException {
        var d = new Ecmr(user);
        ecmrRepository.save(d);

        return d;
    }

    public Ecmr createEcmr(UserFacade user, String status) throws NoSuchAlgorithmException {
        var d = new Ecmr(user, status);
        ecmrRepository.save(d);
        return d;
    }

    /**
     * Creates a new Template by creating a normal eCMR and attaching the Template to it.
     * @param user The userid that creates the eCMR
     * @param template The Template to create
     * @return The newly created eCMR
     * @throws NoSuchAlgorithmException
     */
    public Ecmr createTemplate(UserFacade user, Template template) throws NoSuchAlgorithmException {
        Ecmr newEcmr = new Ecmr(user, template);
        template.setEcmr(newEcmr);
        templateRepository.save(template);

        return newEcmr;
    }

    /**
     * Updates a existing eCMR by creating a new eCMR version.
     * <p>
     * This method updates a existing eCMR in the repository.
     * The old eCMR remains existing, duplicated entries for eCMR and eCMRfield are created with a increased version.
     *
     * @param ecmrId the eCMRid that decribes the existing eCMR
     * @param userId the userid that creates the eCMR
     * @return ecmrIdData, that combines all two identifiers of the updated eCMR / new eCMR version (eCMRid, version)
     */
    public Ecmr updateEcmr(String ecmrId, String userId) throws NoSuchAlgorithmException {
        var maxversion = -1;
        List<Ecmr> relevantList = getById(ecmrId);
        for (Ecmr d1 : relevantList) {
            if (d1.getVersion() > maxversion) {
                maxversion = d1.getVersion();
            }
        }
        for (Ecmr d : relevantList) {
            if (d.getEcmrId().equals(ecmrId) && d.getVersion() == maxversion) {
                Ecmr d1 = d.clone();
                // as we are creating the eCMR, its not external
                d1.setLastUserId(userId);
                d1.updateLastUpdated();
                d1.countUpVersion();
                ecmrRepository.save(d1);
                return d1;
            }
        }
        return null;
    }

    public EcmrIdData getEcmrResponseData(Ecmr ecmr) {
        return new EcmrIdData(ecmr.getEcmrId(), ecmr.getVersion());
    }

    /**
     * Gets an existing eCMR.
     * <p>
     * This method returns an existing eCMR. Depending on the set input variables, the eCMR is searched by id+version.
     *
     * @param id      the eCMRid that decribes the existing eCMR
     * @param version the version of the existing eCMR
     * @return eCMRData, that encapsulates all eCMR information in a eCMRData object
     */
    public EcmrData getEcmr(String id, Integer version) {
        if (id != null) {
            List<Ecmr> relevantList = getById(id);
            if (version == null) {
                int maxversion = -1;
                for (Ecmr d1 : relevantList) {
                    if (d1.getVersion() > maxversion) {
                        maxversion = d1.getVersion();
                    }
                }
                version = maxversion;
            }
            for (Ecmr e : relevantList) {
                if (e.getVersion() == version) {
                    return new EcmrData(e);
                }
            }
            return null;
        } else
            return null;
    }

    /**
     * Gets a list of all eCMR versions of a single eCMR.
     * <p>
     * This method returns a list of all existing eCMR version for a single eCMR.
     *
     * @param ecmrId the eCMRid that is searched for
     * @return ArrayList of eCMRListData, that encapsulates all eCMR list information in a eCMRListData object
     */
    public List<EcmrListData> getEcmrHistory(String ecmrId) {
        ArrayList<EcmrListData> list = new ArrayList<>();
        List<Ecmr> relevantList = getById(ecmrId);

        for (Ecmr d : relevantList) {
            list.add(new EcmrListData(d));
        }
        return list;
    }

    /**
     * Gets the status of a single eCMR.
     * <p>
     * This method returns the status of an existing eCMR. Only the current version of each eCMR is used.
     *
     * @param ecmrId the eCMRid, that is searched for
     * @return String, status as a String, see enum in Status.java
     */
    public String getEcmrStatus(String ecmrId) {
        var returnstring = "";
        var maxversion = -1;
        List<Ecmr> relevantEcmrs = getById(ecmrId);
        for (Ecmr d1 : relevantEcmrs) {
            if (d1.getVersion() > maxversion) {
                maxversion = d1.getVersion();
            }
        }
        for (Ecmr d : relevantEcmrs) {
            if (d.getVersion() == maxversion) {
                returnstring = d.getStatus();
            }
        }
        return returnstring;
    }

    public String getEcmrStatus(String ecmrId, List<Ecmr> prefetchedEcmrs) {
        var returnstring = "";
        var maxversion = -1;
        for (Ecmr d1 : prefetchedEcmrs) {
            if (d1.getEcmrId().equals(ecmrId) && d1.getVersion() > maxversion) {
                maxversion = d1.getVersion();
            }
        }
        for (Ecmr d : prefetchedEcmrs) {
            if (d.getEcmrId().equals(ecmrId) && d.getVersion() == maxversion) {
                returnstring = d.getStatus();
            }
        }
        return returnstring;
    }

    /**
     * Sets the status for an existing eCMR.
     * <p>
     * This method sets the status for an existing eCMR. Only the current version is updated.
     *
     * @param ecmrId, the eCMRid of the eCMR that should be updated
     * @param status, the new status
     */
    public void setEcmrStatus(String ecmrId, String status) {
        var maxversion = -1;
        List<Ecmr> relevantList = getById(ecmrId);

        for (Ecmr d1 : relevantList) {
            if (d1.getVersion() > maxversion) {
                maxversion = d1.getVersion();
            }
        }
        for (Ecmr d : relevantList) {
            if (d.getVersion() == maxversion) {
                d.setStatus(status);
                ecmrRepository.save(d);
            }
        }
    }

    /**
     * Gets newest version of given eCMR in DB
     *
     * @param ecmrId id for searched eCMR
     * @return newest eCMR object
     */
    public Ecmr getLatestEcmr(String ecmrId) {
        var maxVersion = -1;
        Ecmr resultEcmr = null;
        List<Ecmr> relevantEcmr = getById(ecmrId);
        for (Ecmr d : relevantEcmr) {
            if (d.getEcmrId().equals(ecmrId) && d.getVersion() > maxVersion) {
                resultEcmr = d;
                maxVersion = d.getVersion();
            }
        }
        return resultEcmr;
    }

    public Ecmr getLatestEcmr(String ecmrId, List<Ecmr> prefetchedEcmrs) {
        var maxVersion = -1;
        Ecmr resultEcmr = null;
        for (Ecmr d : prefetchedEcmrs) {
            if (d.getEcmrId().equals(ecmrId) && d.getVersion() > maxVersion) {
                resultEcmr = d;
                maxVersion = d.getVersion();
            }
        }
        return resultEcmr;
    }

    public EcmrListData getEcmrListDataByID(String ecmrId, List<Ecmr> prefetchedEcmrs) {
        var d = getLatestEcmr(ecmrId, prefetchedEcmrs);
        return new EcmrListData(d);
    }

    /**
     * Gets all info of a Template, including the eCMR
     * @param ecmrId The id of the eCMR that the template is attached to
     * @param prefetchedEcmrs List of all versions of the eCMR.
     * @return All data of a Template with given eCMR.
     */
    public TemplateListData getTemplateListDataByID(String ecmrId, List<Ecmr> prefetchedEcmrs) {
        var d = getLatestEcmr(ecmrId, prefetchedEcmrs);
        return new TemplateListData(d);
    }

    public List<Ecmr> saveExternal(ExportEcmr exportEcmr) {
        List<Ecmr> ecmrs = new ArrayList<>();
        Ecmr ecmr;
        for (EcmrData ecmrData : exportEcmr.getEcmrData()) {
            ecmr = new Ecmr(ecmrData);
            ecmrs.add(ecmr);
            ecmrRepository.save(ecmr);
        }
        return ecmrs;

    }

    public void archiveFinishedEcmr() {
        List<Ecmr> ecmrs = ecmrRepository.getEcmrByStatus("ARRIVED AT DESTINATION");
        List<Ecmr> ecmrToArchive = ecmrRepository.getAllByEcmrIdIn(ecmrs.stream().map(Ecmr::getEcmrId).collect(Collectors.toList()));
        ecmrToArchive.forEach(ecmr -> {
            ecmr.setArchived(true);
            ecmrRepository.save(ecmr);
        });
    }
}
