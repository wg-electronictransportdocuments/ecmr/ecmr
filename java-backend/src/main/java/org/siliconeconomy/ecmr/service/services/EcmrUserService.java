/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.EcmrGuest;
import org.siliconeconomy.ecmr.service.models.EcmrUser;
import org.siliconeconomy.ecmr.service.repositories.EcmrGuestRepository;
import org.siliconeconomy.ecmr.service.repositories.EcmrUserRepository;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.tan.MessageProvider;
import org.siliconeconomy.ecmr.service.services.tan.MessageProviderException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * This class defines a service that allows to connect to a Repository of EcmrUser.java
 *
 */
@Service
public class EcmrUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EcmrUserService.class);

    @Autowired
    private EcmrUserRepository ecmrUserRepository;
    @Autowired
    UserManagementService userManagementService;
    @Autowired
    RoleService roleService;
    @Autowired
    EcmrGuestRepository ecmrGuestRepository;
    @Autowired
    MessageProvider messageProvider;
    @Value("${ecmr.fe.address}")
    String frontendAddress;

    @PersistenceContext
    private EntityManager em;

    /**
     * Searches DB for EcmrUser entries by ecmrId and userId
     * @param ecmrId Id of eCMR
     * @param userId id of user
     * @return List of objects found in DB
     */
    private List<EcmrUser> getByEcmrAndUser(String ecmrId, String userId) {
        Query q = em.createQuery("SELECT DISTINCT eu from EcmrUser eu where eu.ecmrId = :ecmrId and eu.userId = :userId");
        q.setParameter("ecmrId", ecmrId);
        q.setParameter("userId", userId);
        return q.getResultList();
    }

    /**
     * Searches DB for EcmrUser entries by userId
     * @param userId Id of user
     * @return List of objects found in DB
     */
    private List<EcmrUser> getByUser(String userId) {
        Query q = em.createQuery("SELECT DISTINCT eu from EcmrUser eu where eu.userId = :userId");
        q.setParameter("userId", userId);

        return q.getResultList();
    }

    /**
     * Searches DB for EcmrUser entries by ecmrId
     * @param ecmrId Id of ecmr
     * @return List of objects found in DB
     */
    private List<EcmrUser> getByEcmr(String ecmrId) {
        Query q = em.createQuery("SELECT DISTINCT eu from EcmrUser eu where eu.ecmrId = :ecmrId");
        q.setParameter("ecmrId", ecmrId);
        return q.getResultList();
    }

    /**
     * Checks, if a user is connected to a eCMR.
     * <p>
     * This method returns if a user and a eCMR are connected (the eCMR got shared with the user in the past).
     *
     * @param ecmrId, the eCMRid of the searched eCMR
     * @param userid, the userid of the searched user
     * @return boolean, true if user and eCMR are connected; false otherwise
     */
    public boolean checkEcmrUser(String ecmrId, String userid) {

        List<EcmrUser> relevantList = getByEcmrAndUser(ecmrId, userid);
        for (EcmrUser d : relevantList) {
            if (d.getEcmrId().equals(ecmrId) && userid.equals(d.getUserId())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Gets a list of eCMRids, that a user is connect to
     * <p>
     * This method a list of eCMRids. Only the eCMRids that are connected to the user are returned.
     *
     * @param userId, the userId of the searched user
     * @return ArrayList<UUID>, a list containing all connected eCMRids
     */
    public List<String> getEcmrList(String userId) {
        List<String> returnlist = new ArrayList<>();
        List<EcmrUser> relevantList = getByUser(userId);
        for (EcmrUser d : relevantList) {
            returnlist.add(d.getEcmrId());
        }
        return returnlist;
    }

    /**
     * Creates a new connection between an user and a eCMR.
     * <p>
     * This method creates a new connection between an user and a eCMR.
     *
     * @param ecmrId, the eCMRid of the searched eCMR
     * @param userid, the userid of the searched user
     * @return boolean, true if both exist and connection is created; false otherwise
     */
    public boolean addEcmrUser(String ecmrId, String userid) {
        var found = false;
        List<EcmrUser> relevantList = getByEcmrAndUser(ecmrId, userid);
        if (relevantList.size() > 0) {
            found = true;
        }
        if (!found) {

            var d = new EcmrUser(ecmrId, userid);
            ecmrUserRepository.save(d);
            return true;
        } else
            return false;
    }

    /**
     * Delete all occurrences of a user to a certain eCMR
     *
     * @param ecmrId relevant id of eCMR
     */
    public void deleteEcmrUserConnections(String ecmrId) {

        List<EcmrUser> relevantList = getByEcmr(ecmrId);
        ecmrUserRepository.deleteAll(relevantList);
    }

    public void addGuestCarrier(String ecmrId, String carrierFirstName, String carrierLastName, String carrierCompany, String carrierPhoneNumber) throws MessageProviderException {
        EcmrGuest ecmrGuest = new EcmrGuest();
        ecmrGuest.setEcmrId(ecmrId);
        ecmrGuest.setFirstName(carrierFirstName);
        ecmrGuest.setLastName(carrierLastName);
        ecmrGuest.setCompany(carrierCompany);
        ecmrGuest.setPhoneNumber(carrierPhoneNumber);
        ecmrGuest.setTan(RandomStringUtils.randomNumeric(6));
        ecmrGuest.setTanValidForSigningUntil(Timestamp.from(Instant.now().plus(1, ChronoUnit.DAYS)));
        ecmrGuest.setCreatedAt(Timestamp.from(Instant.now()));
        ecmrGuestRepository.save(ecmrGuest);
        String ecmrLink = this.frontendAddress + "/edit-ecmr/{ecmrId}/tan/{tan}".replace("{ecmrId}", ecmrId).replace("{tan}", ecmrGuest.getTan());
        String tanMessage = "Your tan code is " + ecmrGuest.getTan() + " Please enter your code or click on the following link: " + ecmrLink;
        this.messageProvider.sendMessage(carrierPhoneNumber, tanMessage);
    }

    public boolean isTanCorrect(String ecmrId, String tan) {
        Optional<EcmrGuest> ecmrGuestOpt = this.ecmrGuestRepository.findByEcmrIdAndTan(ecmrId, tan);
        return ecmrGuestOpt.map(ecmrGuest -> ecmrGuest.getTanValidForSigningUntil().after(Timestamp.from(Instant.now()))).orElse(false);

    }

    public String getImlAdminId() {
        LOGGER.info("Looking for admin user...");
        var id = StringUtils.EMPTY;
        for (UserFacade kcu : userManagementService.getEcmrUsers()) {
            if (roleService.isAdminRole(kcu.getUserRoles())) {
                id = kcu.getId();
                LOGGER.info("...found {}", id);
                break;
            }
        }
        return id;
    }
}
