/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.EcmrData;
import org.siliconeconomy.ecmr.service.models.EcmrVersion;
import org.siliconeconomy.ecmr.service.models.ExportEcmr;
import org.siliconeconomy.ecmr.service.repositories.EcmrVersionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * This class defines a service that allows to connect to a Repository of ECMRVersion.java
 *
 */
@Service
public class EcmrVersionService {

    @PersistenceContext
    private EntityManager em;



    public List<EcmrVersion> getVersionsById(String ecmrId)
    {
        Query q = em.createQuery("SELECT DISTINCT ev from EcmrVersion ev where ev.ecmrId = :id ");
        q.setParameter("id", ecmrId);
        return q.getResultList();
    }

    public EcmrVersion getVersionByEcmrIdAndVersion(String ecmrId, int version)
    {
        Query q = em.createQuery("SELECT DISTINCT ev from EcmrVersion ev where ev.ecmrId = :id and ev.ecmrVersion = :version");
        q.setParameter("id", ecmrId);
        q.setParameter("version",version);
        return (EcmrVersion) q.getSingleResult();
    }

    @Autowired
    private EcmrVersionRepository ecmrVersionRepository;

    /**
     * Gets the current Version of a eCMR.
     * <p>
     * This method returns the current (highest) version of a eCMR.
     *
     * @param ecmrId, the eCMRid of the searched eCMR
     * @return int, the current version number
     */
    public int getCurrentVersion(String ecmrId) {
        var maxVersion = 0;
        for (EcmrVersion d : getVersionsById(ecmrId)) {
            if (d.getEcmrVersion() > maxVersion) {
                maxVersion = d.getEcmrVersion();
            }
        }
        return maxVersion;
    }

    public boolean isCurrentVersionExternal(String ecmrId) {
        var maxVersion = 0;
        boolean createdExternally = false;
        for (EcmrVersion d : ecmrVersionRepository.findAll()) {
            if (d.getEcmrId().equals(ecmrId) && d.getEcmrVersion() > maxVersion) {
                maxVersion = d.getEcmrVersion();
                createdExternally = d.isCreatedExternally();
            }
        }
        return createdExternally;
    }

    public boolean isVersionExternal(String ecmrId, int version) {
        return getVersionByEcmrIdAndVersion(ecmrId, version).isCreatedExternally();
    }

    /**
     * Delets all occurrences of version for a certain eCMR
     *
     * @param ecmrId relevant id of eCMR
     */
    public void deleteEcmr(String ecmrId) {
        for (EcmrVersion d : ecmrVersionRepository.findAll()) {
            if (d.getEcmrId().equals(ecmrId)) {
                ecmrVersionRepository.delete(d);
            }
        }
    }

    /**
     * Sets a new eCMR version
     * <p>
     * This method sets a new eCMR version for an existing eCMR.
     *
     * @param version, the eCMRversion object the searched eCMR
     * @return int, the current version number
     */
    public void setNewEcmrVersion(EcmrVersion version) {
        ecmrVersionRepository.save(version);
    }


    public void importEcmrVersions(ExportEcmr exportEcmr)
    {
        for(EcmrData ecmrData: exportEcmr.getEcmrData())
        {
            EcmrVersion ecmrVersion = new EcmrVersion(ecmrData.getEcmrId(), ecmrData.getVersion() ,ecmrData.getLastUsername(), "", true);
            ecmrVersionRepository.save(ecmrVersion);
        }
    }


}
