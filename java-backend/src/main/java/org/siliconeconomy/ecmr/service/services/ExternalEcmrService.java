/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.models.*;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * This service is used to handle eCMR management with external instances, including importing/exporting/updating
 *
 */

@Service
public class ExternalEcmrService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EcmrManagementService.class);

    @Autowired
    private EcmrService ecmrService;

    @Autowired
    private EcmrVersionService ecmrVersionService;

    @Autowired
    private UserService userService;

    @Autowired
    private EcmrFieldService ecmrFieldService;

    @Autowired
    private EcmrOriginService ecmrOriginService;

    @Autowired
    private EcmrUserService ecmrUserService;

    @Autowired
    private EcmrParticipantService ecmrParticipantService;

    @Autowired
    private UserManagement usermanagement;

    @Autowired
    private EcmrManagementService ecmrManagementService;

    @Autowired
    public RestTemplate restTemplate;

    @Transactional
    public List<EcmrData> getExportEcmr(String ecmrId)
    {
        List<EcmrData> resultData = new ArrayList<>();
        boolean ecmrExisting = ecmrService.checkEcmrByID(ecmrId);
        if(ecmrExisting) {
            int maxVersion = ecmrVersionService.getCurrentVersion(ecmrId);

            for(int i = 1; i <= maxVersion; i++) {
                EcmrData ecmr = ecmrService.getEcmr(ecmrId, i);
                if (ecmr != null) {
                    // Check if ecmr is external for User association
                    String username = userService.getUsername(ecmr.getLastUserId());
                    ecmr.setLastUsername(username);
                    ecmr.setFields(ecmrFieldService.getEcmrFields(ecmr.getEcmrId(), ecmr.getVersion()));
                }
                resultData.add(ecmr);
            }

        }
        return resultData;

    }

    /**
     * Method for exporting eCMR hosted on current instance
     * @param ecmrId
     * @param ecmrParticipant
     * @return
     */
    @Transactional
    public ResponseEntity<Object> exportEcmr(String ecmrId, EcmrParticipant ecmrParticipant) {

        LOGGER.info("[eCMR EXTERNAL SERVICE] exportEcmr started");
        ResponseEntity<Object> response = null;
        ArrayList<EcmrData> result = (ArrayList<EcmrData>) getExportEcmr(ecmrId);
        if(result.size() == 0)
        {
            LOGGER.info("[eCMR EXTERNAL SERVICE] Export eCMR is empty/ could not be found.");
            response = ExceptionHandling.setNotFoundError();
        }
        else {
            LOGGER.info("[eCMR EXTERNAL SERVICE] Gathered all eCMRs with matching ID");
            EcmrOrigin origin = ecmrOriginService.getEcmrOriginById(ecmrId);
            ExportEcmr exportEcmr = new ExportEcmr(result, origin);
            HashMap<String, Object> res = new HashMap();
            res.put("exportEcmr", exportEcmr);

            //Save new participant
            ecmrParticipantService.addNewParticipant(ecmrParticipant);

            response = new ResponseEntity<>(res, HttpStatus.OK);
            LOGGER.info("[eCMR EXTERNAL SERVICE] exportEcmr finished");
        }
        return response;

    }


    /**
     * Method for importing external eCMR.
     * Gets called when user scans QR-code in frontend
     * @param jsonBody
     * @return
     */
    @Transactional
    public ResponseEntity<Object> importEcmr(Map<String, Object> jsonBody) {

        LOGGER.info("[eCMR EXTERNAL SERVICE] importEcmr started");

        ResponseEntity<Object> response = null;
        try {

            var userId = usermanagement.getCurrentUserId();


            String exportDomain = (String) jsonBody.get("host");
            String ecmrId = (String) jsonBody.get("ecmrId");

            if (ecmrOriginService.isCurrentInstance(exportDomain)) {
                LOGGER.info("[eCMR EXTERNAL SERVICE] Trying to import from own instance, share instead");
                return ecmrManagementService.setUserAtEcmr(userId, ecmrId);
            }
            else if(ecmrService.checkEcmr(ecmrId, 1))
            {
                LOGGER.info("[eCMR EXTERNAL SERVICE] Trying to import an already existing eCMR, abort");
                return new ResponseEntity<>(
                        new ResponseData("eCMR already imported!"), HttpStatus.CONFLICT);
            }
            final String uri = exportDomain + EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_EXPORT_ECMR.replace("{id}", ecmrId);

            EcmrParticipant ecmrParticipant = new EcmrParticipant(ecmrOriginService.getCurrentAddress(), ecmrId);

            try {
                LOGGER.info("[eCMR EXTERNAL SERVICE] Trying to import from foreign instance");
                String result = restTemplate.postForObject(uri, ecmrParticipant, String.class);
                JSONParser jsonParser = new JSONParser();
                JSONObject json = (JSONObject) jsonParser.parse(result);
                ExportEcmr exportEcmr = (new ObjectMapper()).readValue(json.get("exportEcmr").toString(), ExportEcmr.class);
                LOGGER.info("[eCMR EXTERNAL SERVICE] Fetching successful");

                // create eCMR entries
                List<Ecmr> ecmrs = ecmrService.saveExternal(exportEcmr);
                // safe origins
                ecmrOriginService.addEcmrOrigin(exportEcmr);
                ecmrVersionService.importEcmrVersions(exportEcmr);

                ecmrFieldService.importExternalEcmr(exportEcmr, ecmrs);
                // Add calling user to user list
                ecmrUserService.addEcmrUser(exportEcmr.getEcmrData().get(0).getEcmrId(), userId);


                response = new ResponseEntity<>("", HttpStatus.CREATED);
                LOGGER.info("[eCMR EXTERNAL SERVICE] importEcmr finished");
            } catch (Exception e) {
                LOGGER.error(e.getMessage());
                response = new ResponseEntity<>("", HttpStatus.NOT_FOUND);

            }
        }
        catch (Exception e) {
            // Internal error -> 500
            response = ExceptionHandling.setInternalError();
            LOGGER.error(e.getMessage());
            return response;
        }

        return response;
    }

    /**
     * Method for notifying eCMR origin about update.
     * @param ecmr New eCMR update
     */
    @Transactional
    public boolean notifyEcmrOrigin(Ecmr ecmr)
    {
        try {
            LOGGER.info("[eCMR EXTERNAL SERVICE] Notify origin");
            // Construct eCMR to send
            String ecmrId = ecmr.getEcmrId();
            EcmrData ecmrData = new EcmrData(ecmr);
            ecmrData.setFields(ecmrFieldService.getEcmrFields(ecmr.getEcmrId(), ecmr.getVersion()));
            String username = userService.getUsername(ecmr.getLastUserId());
            ecmrData.setLastUsername(username);
            EcmrUpdateObject ecmrUpdateObject = new EcmrUpdateObject(ecmrData, ecmrOriginService.getCurrentAddress());

            // construct update url
            String origin = ecmrOriginService.getEcmrOriginById(ecmrId).getOrigin().replace("\"", "");
            final String uri = origin + EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.EXTERNAL_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId);

            // Make PUT call to origin instance
            restTemplate.put(uri, ecmrUpdateObject);
            return true;
        }
        catch (Exception e)
        {
            LOGGER.error("[eCMR SERVICE] Failure while notifying origin ("+e.getMessage()+")");
            return false;
        }

    }


    /**
     * Method for updating eCMR which is owned by current instance
     * @param ecmrId UUID of eCMR
     * @param ecmrUpdateObject Object containing update information
     * @return ResponseEntity for calling instance
     * @throws NoSuchAlgorithmException
     */
    @Transactional
    public ResponseEntity<Object> updateEcmrByExternal(String ecmrId, EcmrUpdateObject ecmrUpdateObject) throws NoSuchAlgorithmException {

        LOGGER.info("[eCMR EXTERNAL SERVICE] updateEcmrByExternal started");

        ResponseEntity<Object> response;
        //Check eCMR
        boolean ecmrExistingAndHost = ecmrService.checkEcmrByID(ecmrId);
        boolean host = !ecmrOriginService.isExternal(ecmrId);
        //Save eCMR
        if(ecmrExistingAndHost)
        {
            Ecmr currentEcmr = ecmrService.getLatestEcmr(ecmrId);
            int currentVersion = ecmrVersionService.getCurrentVersion(ecmrId);
            EcmrData newEcmrData = ecmrUpdateObject.getEcmrData();
            Ecmr newEcmr = ecmrService.updateEcmr(ecmrId, newEcmrData.getLastUsername());

            // TODO: move to EcmrFactory
            ecmrFieldService.duplicateEcmrFields(currentEcmr, newEcmr);
            var passedFields = EcmrFieldEntryData.fromJson(newEcmrData.getFields());
            var fields = ecmrFieldService.getEcmrFieldIds(ecmrId, currentVersion);
            var actualFields = new ArrayList<>();
            for (EcmrFieldEntryData passedFieldEntry : passedFields) {
                var fieldContained = false;
                var entryContained = false;
                for (EcmrFieldEntryData field : fields) {
                    if (passedFieldEntry.getFieldId() == field.getFieldId()) {
                        fieldContained = true;
                        if (passedFieldEntry.getEntryId() == field.getEntryId()) {
                            // put passed values to fieldentry
                            entryContained = true;
                            field.setValue(passedFieldEntry.getValue());
                            actualFields.add(field.getFieldId());
                        }
                    }
                }
                if (fieldContained && !entryContained) {
                    //new entry to an existing field has been added
                    fields.add(passedFieldEntry);
                    actualFields.add(passedFieldEntry.getFieldId());
                }
            }
            ecmrFieldService.updateEcmrFields(newEcmr, passedFields);
            ecmrService.setEcmrStatus(
                    ecmrId, newEcmrData.getStatus());
            ecmrVersionService.setNewEcmrVersion(
                    new EcmrVersion(ecmrId, currentVersion + 1 , newEcmrData.getLastUserId(), "", true));
            //Notify participant
            if(host) {
                notifyParticipants(ecmrUpdateObject);

            }
            LOGGER.info("[eCMR EXTERNAL SERVICE] updateEcmrByExternal finished");
            response = new ResponseEntity<>("", HttpStatus.OK);

        }
        else {
            response = ExceptionHandling.setNotFoundError();
        }



        return response;
    }

    /**
     * Method for notifying eCMR participants
     *
     * @param ecmrUpdateObject
     */
    @Transactional
    public void notifyParticipants(EcmrUpdateObject ecmrUpdateObject)
    {
        LOGGER.info("[eCMR EXTERNAL SERVICE] Update external eCMR participants");
        String ecmrId = ecmrUpdateObject.getEcmrData().getEcmrId();
        List<EcmrParticipant> participants = ecmrParticipantService.getParticipantsByEcmrId(ecmrId);
        LOGGER.info("[eCMR EXTERNAL SERVICE] Involved participants: "+ participants.size());
        for(EcmrParticipant participant : participants)
        {
            if(ecmrUpdateObject.getParticipant().equals(participant.getParticipantOrigin())) {
                LOGGER.info("[eCMR EXTERNAL SERVICE] Same participant as update came from, skip.");
                continue;
            }
            String origin = participant.getParticipantOrigin();
            final String uri = origin + EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.EXTERNAL_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId);
            LOGGER.info("[eCMR EXTERNAL SERVICE] Try to reach "+ uri);
            try {
                // Make PUT call to origin instance
                restTemplate.put(uri, ecmrUpdateObject);
            }
            catch(RestClientException rce)
            {
                LOGGER.error(rce.toString());
                LOGGER.info("[eCMR EXTERNAL SERVICE] Could not notify "+participant.getParticipantOrigin());
            }
            catch (Exception e)
            {
                LOGGER.error("[eCMR EXTERNAL SERVICE] Failure while notifying participant ("+e.getMessage()+")");
            }
        }

    }
}
