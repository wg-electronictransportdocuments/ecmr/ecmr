/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.Field;
import org.siliconeconomy.ecmr.service.repositories.FieldRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

/** 
 * This class defines a service that allows to connect to a Repository of Field.java
 *
 */
@Service
public class FieldService {
  @Autowired private FieldRepository fieldRepository;

  @PersistenceContext
  private EntityManager em;


  /**
   * Get all Field objects in DB. Uses EM call instead of JPA for faster searching
   * @return List of all field objets
   */
  private List<Field> getAll()
  {
    Query q = em.createQuery("SELECT f from Field f");
    return q.getResultList();
  }

  /**
   * Gets the fielddescriptions for a list of fields.
   *
   * This method returns a list of field descriptions, for a list of inputfields.
   * 
   * @param fieldids, the list of fieldids
   * @return List<String>, the list of field descriptions as String
   */
  public List<String> getFieldDescriptions(List<Integer> fieldids) {
    List<String> fieldDescrptions = new ArrayList<>();
    List<Field> allFields = getAll();
    for (Integer fieldid : fieldids)
      for (Field field : allFields) {
        if (field.getFieldId() == fieldid.intValue()) {
          fieldDescrptions.add(field.getDescription());
        }
      }
    return fieldDescrptions;
  }

  
  /**
   * Gets the fielddescriptions for a base field template.
   *
   * This method returns a list of field descriptions, for a template of inputfields.
   * In this version, all existing fields are returned as a full template.

   * @return List<String>, the list of field descriptions as String
   */
  public List<String> getTemplateFieldDescriptions() {
    List<String> fieldDescrptions = new ArrayList<>();
    List<Field> allFields = getAll();
    for (Field field : allFields) {
      fieldDescrptions.add(field.getDescription());
    }

    return fieldDescrptions;
  }

  /**
   * Gets the fieldids for a base field template.
   *
   * This method returns a list of field ids, for a template of inputfields.
   * In this version, all existing fields are returned as a full template.
   * 
   * @return List<String>, the list of field descriptions as String
   */
  public List<Long> getTemplateFieldIds() {
    List<Field> allFields = getAll();
    List<Long> fieldIds = new ArrayList<>();
    for (Field field : allFields) {
      fieldIds.add(field.getFieldId());
    }

    return fieldIds;
  }
}
