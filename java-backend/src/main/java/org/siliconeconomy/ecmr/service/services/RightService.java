/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.Right;
import org.siliconeconomy.ecmr.service.repositories.RightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/** 
 * This class defines a service that allows to connect to a Repository of Right.java
 *
 */
@Service
public class RightService {

  @Autowired private RightRepository rightRepository;

  /**
   * Gets the rightnames for a list of rightids.
   *
   * This method returns the corresponding right names to an input list of right ids.
   * 
   * @param rightids, the list of rightids
   * @return List<String>, the list of right names as String
   */
  public List<String> getRightNames(List<Integer> rightids) {
    List<String> rightNames = new ArrayList<>();
    for (Integer rightid : rightids)
      for (Right right : rightRepository.findAll()) {
        if (right.getRightId() == rightid.intValue()) {
          rightNames.add(right.getRightName());
        }
      }
    return rightNames;
  }
}
