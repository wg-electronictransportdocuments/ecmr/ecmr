/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.siliconeconomy.ecmr.service.models.Template;
import org.siliconeconomy.ecmr.service.repositories.TemplateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * TemplateService for basic persistence tasks.
 */
@Service
public class TemplateService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private TemplateRepository templateRepository;

    /**
     * Gets a Template as List based on given id
     *
     * @param templateId ID to search for
     * @return List of found objects
     */
    public List<Template> getById(Long templateId)
    {
        Query q = em.createQuery("SELECT DISTINCT t from Template t where t.templateId = :id");
        q.setParameter("id",templateId);
        return q.getResultList();
    }

    /**
     * Delete a Template permanently from DB.
     * @param templateId ID of Template to delete
     */
    public void deleteTemplateById(Long templateId)
    {
        templateRepository.deleteById(templateId);
    }
}
