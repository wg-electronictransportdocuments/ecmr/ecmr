/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.services;

import org.apache.commons.lang3.StringUtils;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.*;
import org.siliconeconomy.ecmr.service.repositories.CompanyRepository;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.*;

/**
 * This service provides the logic used to handle user management in the eCMR projekt.
 *
 */
@Service
public class UserManagementService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserManagementService.class);
    public static List<UserFacade> storedUsers = Collections.synchronizedList(new ArrayList<UserFacade>());


    /**
     * Key for generating JWT Tokens
     */
    public static String SECRETKEY;

    @Autowired
    private ContactService contactService;
    @Autowired
    private UserManagement usermanagement;
    @Autowired
    private ConnectionService connectionService;

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    RoleService roleService;

    /**
     * Returns the contacts of a given user.
     *
     * @return Response including code and message
     */
    public ResponseEntity<Object> getContactsForUser() {
        LOGGER.info("getContactsForUser started");
        ResponseEntity<Object> response;

        String id = usermanagement.getCurrentUserId();

        // Check if UserId was passed
        if (StringUtils.isBlank(id)) {
            response = ExceptionHandling.setBadRequestError();
            return response;
        } else {
            try {

                List<CurrentUserContact> userContacts = contactService.getCurrentUserContacts(id);
                if (null == userContacts) {
                    LOGGER.info("No Contacts found.");
                    return ExceptionHandling.setNotFoundError();
                }
                response = new ResponseEntity<>(userContacts, HttpStatus.OK);
            } catch (Exception e) {
                // Internal error occurred -> 500
                LOGGER.info("Something went wrong. {}", e.getMessage());
                return ExceptionHandling.setInternalError();
            }
        }
        return response;
    }

    /**
     * Returns the fields of a  user.
     *
     * @return Response including code and message
     */
    public ResponseEntity<Object> getFieldsForUser() {
        LOGGER.info("getFieldsForUser started");
        ResponseEntity<Object> response;

        String id = usermanagement.getCurrentUserId();

        // Check if UserId was passed
        if (StringUtils.isBlank(id)) {
            response = ExceptionHandling.setBadRequestError();
            return response;
        } else {
            try {
                var fields = new CurrentUserFields();

                List<String> userRoles = getUserById(id).getUserRoles();
                for (String role : userRoles){
                    fields.getMandatoryFields().addAll(roleService.getRoleByName(role).getMandatoryFields());
                    fields.getOptionalFields().addAll(roleService.getRoleByName(role).getOptionalFields());
                }

                response = new ResponseEntity<>(fields, HttpStatus.OK);
            } catch (Exception e) {
                // Internal error occurred -> 500
                return ExceptionHandling.setInternalError();
            }
        }

        return response;
    }

    public ResponseEntity<Object> setPubKeyToUserConnection(String sessionToken, String userId, String pubKey) {

        try {

            var connection = new Connection(
                    sessionToken,
                    pubKey,
                    userId
            );

            connectionService.addConnection(connection);
        } catch (Exception e) {
            return new ResponseEntity<>(new ResponseData("Could not create connection."), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        LOGGER.info("[]");
        return new ResponseEntity<>(new ResponseData("Connection created!"), HttpStatus.OK);
    }

    /**
     * Get the userID of a user, found by its email
     *
     * @param email email of a user
     * @return user ID
     */
    public String getUserIdByEmail(String email) {
        try {
            for (UserFacade user : storedUsers) {
                if (email.equals(user.getEmail())) {
                    return user.getId();
                }
            }
            List<UserFacade> users = usermanagement.getUsersList();
            for (UserFacade user : users) {
                if (email.equals(user.getEmail())) {
                    return user.getEmail();
                }
            }
        } catch (Exception e) {
            LOGGER.error("Could not find user {} by email. {}", email, e.getMessage());
        }
        return null;
    }

    /**
     * Get a  user by id from the static user list
     *
     * @param userId   ID of a user
     * @return a User
     */
    public UserFacade getUserById(String userId) {
        UserFacade result = null;
        try {
            for (UserFacade user : storedUsers) {
                if (userId.equals(user.getId()) || userId.equals(user.getEmail())) {
                    result = user;
                    break;
                }
            }

        } catch (Exception e) {
            LOGGER.error("Could not find user {}. {}", userId, e.getMessage());
        }

        if (null == result) {
            LOGGER.error("Could not find user {}", userId);
        }

        return result;
    }

    /**
     * Get a specific attribute attached to a  user
     *
     * @param kcu User
     * @param attributeName name of a attribute
     * @return a Map of all Attributes attached to a  user
     */
    public List<String> getAttributeByNameFromUserByID(UserFacade kcu, String attributeName) {
        LOGGER.debug("Get user {} attributes from Server with id {}", attributeName, kcu.getId());
        List<String> attribute = Collections.emptyList();

        try {
            Map<String, List<String>> attributes = kcu.getAttributes();
            if (null != attributes) {
                if (!attributes.containsKey(attributeName)) {
                    LOGGER.debug("No attribute {} found", attributeName);
                }
                attribute = attributes.get(attributeName);
            }

        } catch (Exception e) {
            LOGGER.error("Could not retrieve user attribute {} from Server with id {}", attributeName, kcu.getId());
        }
        return attribute;
    }

    /**
     * Get list of users from the KC server
     *
     * @return List of UserRepresentations
     */
    public List<UserFacade> getEcmrUsersFromServer() {
        LOGGER.debug("Get users from Server");
        return usermanagement.getUsersList();
    }

    public String getToken() {
        return usermanagement.getCurrentToken();
    }

    public String getUserIdFromToken() {
        return usermanagement.getCurrentUserId();
    }

    /**
     * Get list of Users. If static list is not filled, a fresh list will be requested from KC server
     *
     * @return List of  Users
     */
    public List<UserFacade> getEcmrUsers() {
        if (storedUsers.isEmpty()) {
            usermanagement.createUserFacades();
        }
        return storedUsers;
    }

}
