/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.UserMandatoryField;
import org.siliconeconomy.ecmr.service.repositories.UserMandatoryFieldRepository;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This class defines a service that allows to connect to a Repository of UserMandatoryField.java
 *
 */
@Service
public class UserMandatoryFieldService {

    @Autowired
    private RoleService roleService;

    /**
     * Gets a list of all mandatory fieldids for a user as Integer.
     * <p>
     * This method returns list of fieldids, that are mandatory for a given user.
     *
     * @param userRoles, the user that is searched for
     * @return List<Integer>, the list of mandatory field ids as Integer
     */
    public List<Long> getUserMandatoryFieldIds(List<String> userRoles) {
        List<Long> userMandatoryFields = new ArrayList<>();
        for(String roleName : userRoles) {
            List<String> mandatoryFieldIdsString = roleService.getRoleByName(roleName).getMandatoryFields();
            for(String mandatoryFieldIdString : mandatoryFieldIdsString) {
                long mandatoryFieldId = Long.parseLong(mandatoryFieldIdString);
                if(!userMandatoryFields.contains(mandatoryFieldId)){
                    userMandatoryFields.add(mandatoryFieldId);
                }

            }
        }
        return userMandatoryFields;
    }
}
