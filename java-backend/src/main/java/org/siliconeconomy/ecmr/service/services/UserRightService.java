/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.UserRight;
import org.siliconeconomy.ecmr.service.repositories.UserRightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This class defines a service that allows to connect to a Repository of UserRight.java
 */
@Service
public class UserRightService {

    @Autowired
    private UserRightRepository userRightRepository;

    /**
     * Gets a list of all rightids that a user is connected to.
     * <p>
     * This method returns list of rightids, that are set for a given user.
     *
     * @param userRoles, the user that is searched for
     * @return List<Integer>, the list of rightids
     */
    public List<Long> getUserRightIds(List<String> userRoles) {
        List<Long> userRights = new ArrayList<>();
        for (UserRight userright : userRightRepository.findAll()) {
            if (null != userright &&
                    !userright.getKeycloakRoles().isEmpty() && userright.getKeycloakRoles().stream().anyMatch(element -> userRoles.contains(element)) ) {
                userRights.add(userright.getRightId());
            }
        }
        return userRights;
    }
}
