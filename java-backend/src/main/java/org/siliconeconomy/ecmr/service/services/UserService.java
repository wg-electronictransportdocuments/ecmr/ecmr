/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.controller.SignatureController;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.CurrentUserContact;
import org.siliconeconomy.ecmr.service.models.User;
import org.siliconeconomy.ecmr.service.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * This class defines a service that allows to connect to a Repository of UserService.java
 *
 */
@Service
public class UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired private UserRepository userRepository;

    @Autowired private SignatureController signatureController;

    @Autowired
    UserManagementService userManagementService;

    /**
     * Gets the user name for a given userid.
     * <p>
     * This method returns the corresponding username to an input userid.
     *
     * @param userId, the userid that is searched for
     * @return String, the corresponding username if found; null otherwise
     */
    public String getUsername(String userId) {

        String result = null;

        try {
            result = getById(userId).getUserName();
        } catch (Exception e) {
            // ignore
        }
        return result;
    }

    /**
     * Gets the user type for a given userid.
     * <p>
     * This method returns the corresponding usertype to an input userid.
     *
     * @param userId, the userid that is searched for
     * @return Integer, the corresponding usertype if found; null otherwise
     */
    public List<String> getUserRoles(String userId) {
        return userManagementService.getUserById(userId).getUserRoles();

    }

    public UserFacade getById(String userId) {
        UserFacade result = null;
        try {
            result = userManagementService.getUserById(userId);

        } catch (Exception e) {
            // Logger.info
        }
        return result;
    }

    public String getUserIDByEmail(String email) {
        String result = null;
        try {

            result = userManagementService.getUserIdByEmail(email);

        } catch (Exception e) {
            LOGGER.debug("Could not get user by email.");
        }
        return result;
    }

    /**
     * Adds all userinformation to a initialized CurrentUserContact list.
     *
     * This method takes a CurrentUserContact list input and adds missing user information, where exists.
     *
     * @param contacts, the input list that has initialized contacts but missing user information
     * @return List<CurrentUserContact>, the same list with added user information.
     */
    public List<CurrentUserContact> addUserInformation(List<CurrentUserContact> contacts) {
        for (CurrentUserContact c : contacts) {
            for (User u : userRepository.findAll()) {
                if (u.getUuid().equals(c.getContactId())) {
                    c.setContactName(u.getUserName());
                    c.setContactType(u.getUserRoles().get(0)); // ggf aufbohren
                    c.setContactEmail(u.getEmail());
                }
            }
        }
        return contacts;
    }

}
