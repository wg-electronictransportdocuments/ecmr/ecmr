/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.services;

import org.siliconeconomy.ecmr.service.models.CurrentUserContact;
import org.siliconeconomy.ecmr.service.models.Usertype;
import org.siliconeconomy.ecmr.service.repositories.UsertypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/** 
 * This class defines a service that allows to connect to a Repository of Usertype.java
 *
 */
@Service
public class UsertypeService {
  @Autowired private UsertypeRepository usertypeRepository;

  /**
   * Adds all usertypeinformation to an initialized CurrentUserContact list.
   *
   * This method takes a CurrentUserContact list input and adds missing usertype information, where exists.
   * 
   * @param contacts, the input list that has initialized contacts but missing usertype information
   * @return List<CurrentUserContact>, the same list with added usertype information.
   */
  public List<CurrentUserContact> addUsertypeInformation(List<CurrentUserContact> contacts) {
    for (CurrentUserContact c : contacts) {
      for (Usertype u : usertypeRepository.findAll()) {
        if (u.getUserTypeId() == Long.parseLong(c.getContactType())) {
          c.setContactType(u.getUserTypeName());
          break;
        }
      }
    }
    return contacts;
  }

}
