/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.services.tan;

import org.springframework.stereotype.Service;

@Service
public class DummyMessageProvider implements MessageProvider {
    @Override
    public void sendMessage(String recipientIdentifier, String message) {
        System.out.println("MESSAGE TO: '" + recipientIdentifier + "', MESSAGE: '" + message + "'");
    }
}
