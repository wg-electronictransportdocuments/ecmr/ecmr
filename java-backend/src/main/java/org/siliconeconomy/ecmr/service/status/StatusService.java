/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.status;

import java.util.*;

/**
 * This class represents the eCMR status definition, that describes the global eCMR usecase swim lane.
 * Depending on status, only some usertypes are allowed to make changes (XChangesUsertypes lists).
 * Depending on status, only some usertypes are allowed to share the eCMR (XShareableUsertypes
 * lists). Depending on status, the transition to the next swimlane status can only by triggered by
 * certain usertypes (XTriggerUsertypes lists) and (optional) by certain fields (XFields lists). The
 * int values within the list correspond to the relating usertype-/fieldid. In this version, the
 * "archived" status is not yet enabled.
 *
 */
public interface StatusService {

    List<String> statusPrioList = Collections.synchronizedList(new ArrayList<>());
    Map<String,String> signaturesFields = Collections.synchronizedMap(new HashMap<>());

    boolean allowedToChangeEcmr(String oldstatus, List<String> userRoles);

    boolean allowedToShareEcmr(String oldstatus, List<String> userRoles);

    boolean allowedToDeleteEcmr(String status, List<String> userRoles);

    String getNextStatus(String oldstatus, List<Long> signatureList);

    void generateStatus();

    void getSignatureFieldStatus();

    boolean allowedToCreateEcmr(List<String> userRoles);
}
