/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */


package org.siliconeconomy.ecmr.service.status.impl;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.siliconeconomy.ecmr.service.facade.RoleFacade;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.*;

/**
 * This class represents the eCMR status definition, that describes the global eCMR usecase swim lane.
 * Depending on status, only some usertypes are allowed to make changes (XChangesUsertypes lists).
 * Depending on status, only some usertypes are allowed to share the eCMR (XShareableUsertypes
 * lists). Depending on status, the transition to the next swimlane status can only by triggered by
 * certain usertypes (XTriggerUsertypes lists) and (optional) by certain fields (XFields lists). The
 * int values within the list correspond to the relating usertype-/fieldid. In this version, the
 * "archived" status is not yet enabled.
 *
 */
public class KeycloakStatusService implements StatusService {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeycloakStatusService.class);

    @Value("${usermanagement.config.filename}")
    public String configFileName;

    @Autowired
    UserManagement userManagementService;

    @Autowired
    RoleService roleService;

    @Autowired
    private ResourceLoader resourceLoader;
    /**
     * Checks if a certain usertype can change eCMR based on the current eCMR status.
     *
     * <p>This method uses the defined XChangesUsertypes lists to determine whether a usertype is
     * allowed to change a eCMR given the current status.
     *
     * @param oldstatus, the current eCMR status
     * @param userRoles, the usertype that is checked
     * @return boolean, that is true if user is able to change eCMR; false otherwise
     */
    @Override
    public boolean allowedToChangeEcmr(String oldstatus, List<String> userRoles) {
        return roleService.isSuperUser(userRoles) || oneUserRoleContainsStatus(oldstatus, userRoles);
    }

    /**
     * Checks if a certain usertype can share a eCMR based on the current eCMR status.
     *
     * <p>This method uses the defined XShareableUsertypes lists to determine whether a usertype is
     * allowed to share a eCMR given the current status.
     *
     * @param oldstatus, the current eCMR status
     * @param userRoles, the usertype that is checked
     * @return boolean, that is true if user is able to share eCMR; false otherwise
     */
    @Override
    public boolean allowedToShareEcmr(String oldstatus, List<String> userRoles) {
        return roleService.isSuperUser(userRoles) || oneUserRoleContainsStatus(oldstatus, userRoles);
    }
    @Override
    public boolean allowedToDeleteEcmr(String status, List<String> userRoles) {
        return status.equals(statusPrioList.get(0)) && oneOfRolesCanDeleteOrIsSuperUser(userRoles);
    }

    /**
     * Checks what the new status will be after a usertype changed a list of eCMR fields
     *
     * <p>This method uses the defined XTriggerUsertypes and XFields lists to determine whether the
     * status changes or remains after a usertype edited a list of fields.
     *
     * @param oldstatus,     the current eCMR status
     * @param signatureList, an ArrayList of all changed fieldids
     * @return String, returns the new status String (from enum)
     */
    @Override
    public String getNextStatus(String oldstatus, List<Long> signatureList) {
            if (signatureList.isEmpty()) {
                return statusPrioList.get(0);
            }

            // check for "highest" signature received
            long maxSignature = Collections.max(signatureList);
            if (maxSignature == 69L) {
                return signaturesFields.get("69");
            } else if (maxSignature == 70L) {
                return signaturesFields.get("70");
            } else if (maxSignature == 72L) {
                return signaturesFields.get("72");
            }

            // No signature -> oldStatus = newStatus = NEW
            else return oldstatus;
    }

    private boolean oneOfRolesCanDeleteOrIsSuperUser(List<String> userRoles) {
        var result = Boolean.FALSE;
        for(var roleName : userRoles){
            RoleFacade rf = roleService.getRoleByName(roleName);
            if(rf.getIsSuperuser() || rf.getCanDelete()){
                result = Boolean.TRUE;
                break;
            }
        }
        return result;
    }

    private boolean oneUserRoleContainsStatus(String status, List<String> userRoles) {
        var result = Boolean.FALSE;
        for(var roleName : userRoles){
            RoleFacade rf = roleService.getRoleByName(roleName);
            if(rf.getCanChange().contains(status)){
                result = Boolean.TRUE;
                break;
            }
        }

        return result;
    }

    @Override
    public void generateStatus() {
        List<String> status = null;
        //Resource resource = new ClassPathResource(configFileName);
        Resource resource = resourceLoader.getResource("classpath:/"+configFileName);

        try (var reader = new InputStreamReader(resource.getInputStream())){
            JSONParser jsonParser = new JSONParser();
            Object jsonObj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) jsonObj;
            status = (List<String>) jsonObject.get("status");
        } catch (Exception e) {
            LOGGER.error("[KEYCLOAK STATUS SERVICE] Pls provide a valid config.json file!");
        }

        if (null != status) {

            try {
                StatusService.statusPrioList.clear();
                StatusService.statusPrioList.addAll(status);
            } catch (Exception e) {
                LOGGER.error("[KEYCLOAK STATUS SERVICE] Pls provide a valid config.json file!");
            }

        }
    }
    @Override
    public void getSignatureFieldStatus() {
        Map<String, String> signatureFieldStatus = null;
        //Resource resource = new ClassPathResource(configFileName);
        Resource resource = resourceLoader.getResource("classpath:/"+configFileName);

        try (var reader = new InputStreamReader(resource.getInputStream())){
            JSONParser jsonParser = new JSONParser();
            Object jsonObj = jsonParser.parse(reader);
            JSONObject jsonObject = (JSONObject) jsonObj;
            signatureFieldStatus = (HashMap<String, String>) jsonObject.get("signatureFieldStatus");
        } catch (Exception e) {
            LOGGER.error("[KEYCLOAK STATUS SERVICE] Pls provide a valid config.json file!");
        }

        if (null != signatureFieldStatus) {
            try {
                StatusService.signaturesFields.clear();
                for (String sig : signatureFieldStatus.keySet()) {
                    StatusService.signaturesFields.put(sig,signatureFieldStatus.get(sig) );
                }
            } catch (Exception e) {
                LOGGER.error("[KEYCLOAK STATUS SERVICE] Pls provide a valid config.json file!");
            }

        }
    }

    @Override
    public boolean allowedToCreateEcmr(List<String> userRoles) {
        var result = Boolean.FALSE;
        for(String role: userRoles){
            RoleFacade rf = roleService.getRoleByName(role);
            if (rf.getCanGenerate()){
                result = Boolean.TRUE;
                break;
            }
        }
        return result;
    }

}
