/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.usermanagement;

import org.siliconeconomy.ecmr.service.facade.UserFacade;

import java.util.List;

public interface UserManagement {

    List<UserFacade> getUsersList();

    String getCurrentUserId();

    String getCurrentToken();

    void createUserFacades();

    void initNewUser(String user);

}
