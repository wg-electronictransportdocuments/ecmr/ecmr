/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.usermanagement.impl;

import org.apache.commons.lang3.StringUtils;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.adapters.springsecurity.token.KeycloakAuthenticationToken;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.resource.RoleResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.Company;
import org.siliconeconomy.ecmr.service.models.Contact;
import org.siliconeconomy.ecmr.service.repositories.CompanyRepository;
import org.siliconeconomy.ecmr.service.repositories.ContactRepository;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;

import java.security.Principal;
import java.sql.Timestamp;
import java.util.*;

public class KeycloakUserManagement implements UserManagement {

    private static final Logger LOGGER = LoggerFactory.getLogger(KeycloakUserManagement.class);

    @Autowired
    private CompanyRepository companyRepository;

    @Value("${usermanagement.config.filename}")
    public String configFileName;

    @Autowired
    Keycloak keycloak;

    @Autowired
    UserManagementService userManagementService;

    @Autowired
    private ContactRepository contactRepository;

    @Autowired
    RoleService roleService;

    @Value("${ecmr.keycloak.user.role:ecmr-user}")
    private String userRole;

    @Value("${keycloak.realm}")
    public String realm;

    @Value("${keycloak.auth-server-url}")
    public String serverUrl;

    private Map<String, Set<UserRepresentation>> roleNameToUserRepresentationsCache = new HashMap<>();

    // BEGIN-NOSCAN
    @Override
    public List<UserFacade> getUsersList() {
        if(UserManagementService.storedUsers.isEmpty()) {
            createUserFacades();
        }
        return UserManagementService.storedUsers;
    }
    // END-NOSCAN

    // BEGIN-NOSCAN
    @Override
    public void createUserFacades() {

        final Set<UserRepresentation> roleUserMembers = keycloak.realm(realm).roles().get(userRole).getRoleUserMembers();
        List<UserFacade> result = new ArrayList<>();
        if(null != roleUserMembers && !roleUserMembers.isEmpty()) {
            for (UserRepresentation ur : roleUserMembers) {
                result.add(
                        createUserFacade(
                                ur,
                                Long.parseLong(
                                        getAttributeByNameFromUser(ur, EcmrConstants.COMPANY_ID).isEmpty()? "0" :getAttributeByNameFromUser(ur, EcmrConstants.COMPANY_ID).get(0)
                                )
                        )
                );
            }
        }
        UserManagementService.storedUsers.clear();
        UserManagementService.storedUsers.addAll(result);
        contactRepository.deleteAll();
            for (int i = 0; i < UserManagementService.storedUsers.size(); i++) {
                for (int j = 0; j < UserManagementService.storedUsers.size(); j++) {
                    if (i != j) {
                        contactRepository.save(
                                new Contact(
                                        UserManagementService.storedUsers.get(i).getId(),
                                        UserManagementService.storedUsers.get(j).getId(),
                                        0,
                                        new Timestamp(System.currentTimeMillis())
                                )
                        );
                    }
            }
        }
    }

    @Override
    public void initNewUser(String user) {

        UserRepresentation ur = null;
        final Set<UserRepresentation> roleUserMembers = keycloak.realm(realm).roles().get(userRole).getRoleUserMembers();
        for(UserRepresentation u : roleUserMembers)
        {
            if(u.getId().equals(user))
            {
                ur = u;
                break;
            }
        }
        if(ur==null)
        {
            LOGGER.info("[UserManagementService] Could not find new user in keycloak roles. Abort onboarding.");
            return;
        }
        // Add initial contacts to everyone in keycloak
        for (int i = 0; i < UserManagementService.storedUsers.size(); i++) {
                contactRepository.save(
                        new Contact(
                                user,
                                UserManagementService.storedUsers.get(i).getId(),
                                0,
                                new Timestamp(System.currentTimeMillis())
                        )
                );
                contactRepository.save(
                        new Contact(
                                UserManagementService.storedUsers.get(i).getId(),
                                user,
                                0,
                                new Timestamp(System.currentTimeMillis())
                        )
                );
            }
            // Get company of user and add to stored users
            UserManagementService.storedUsers.add(createUserFacade(ur,
                    Long.parseLong(
                            getAttributeByNameFromUser(ur, EcmrConstants.COMPANY_ID).isEmpty()? "0" :getAttributeByNameFromUser(ur, EcmrConstants.COMPANY_ID).get(0)
                    )));
    }


    // END-NOSCAN

    /**
     * Get ID of current user from Auth Token
     *
     * @return userID as String
     */
    public String getCurrentUserId() {

    LOGGER.info("Get userId from KeycloakAuthenticationToken");
    String userIdByToken = StringUtils.EMPTY;
    try {

        var authentication =
                (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
        var principal = (Principal) authentication.getPrincipal();
        LOGGER.debug("Principal received");
        if (principal instanceof KeycloakPrincipal) {
            LOGGER.debug("Principal is keycloak principal");
            var kPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) principal;

            userIdByToken = kPrincipal.getKeycloakSecurityContext().getToken().getSubject();
            LOGGER.debug("ID from token: {}", userIdByToken);

        }

    } catch (Exception e) {
        LOGGER.error("Could not retrieve userID from Keycloak-Token");
    }
    return userIdByToken;
}

    /**
     * Get accessToken for current User
     *
     * @return Response accessToken as String
     */
    public String getCurrentToken() {
        LOGGER.info("Get token from KeycloakAuthenticationToken");
        String token = "";
        try {

            var authentication =
                    (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
            var principal = (Principal) authentication.getPrincipal();
            LOGGER.debug("Principal received");
            if (principal instanceof KeycloakPrincipal) {
                LOGGER.debug("Principal is keycloak principal");
                var kPrincipal = (KeycloakPrincipal<KeycloakSecurityContext>) principal;
                token = kPrincipal.getKeycloakSecurityContext().getTokenString(); // accessToken
                LOGGER.debug("access Token: {}", token);
            }
        } catch (Exception e) {
            LOGGER.error("Could not retrieve access Token");
        }
        return token;
    }

    /**

     * Get a specific attribute attached to a keycloak user

     *

     * @param user KeycloakUser

     * @param attributeName name of a attribute

     * @return a Map of all Attributes attached to a keycloak user

     */
    // BEGIN-NOSCAN
    public List<String> getAttributeByNameFromUser(UserRepresentation user, String attributeName) {
        LOGGER.debug("Get user {} attributes from Keycloak with id {}", attributeName, user.getId());
        List<String> attribute = Collections.emptyList();
        try {
            Map<String, List<String>> attributes = user.getAttributes();
            if (null != attributes) {
                if (!attributes.containsKey(attributeName)) {
                    LOGGER.debug("No attribute {} found", attributeName);
                }
                attribute = attributes.get(attributeName);
            }
        } catch (Exception e) {
            LOGGER.error("Could not retrieve user attribute {} from Keycloak with id {}", attributeName, user.getId());
        }
        return attribute;
    }
    // END-NOSCAN

    // BEGIN-NOSCAN
    private UserFacade createUserFacade(UserRepresentation ur, long companyID) {
        List<String> userRoles = mapRolesToUser(ur.getId());
        return new UserFacade(
                ur,
                userRoles,
                companyRepository.findById(companyID).orElse(new Company("No Company","Dortmund"))
        );
    }
    // END-NOSCAN
    // BEGIN-NOSCAN
    private List<String> mapRolesToUser(String userId) {
        List<String> result = new ArrayList<>();
        for (String role : RoleService.roleNames) {
            try {
                if(!roleNameToUserRepresentationsCache.containsKey(role)) {
                    RoleResource roleResource = getRoleByName(role);
                    roleNameToUserRepresentationsCache.put(role, roleResource.getRoleUserMembers());
                }
                Set<UserRepresentation> userRepresentations = roleNameToUserRepresentationsCache.get(role);
                for (UserRepresentation ur : userRepresentations) {
                    if (userId.equals(ur.getId())) {
                        result.add(role);
                        break;
                    }
                }
            } catch (Exception e) {
                //ignore
            }
        }
        return result;
    }
    // END-NOSCAN
    private RoleResource getRoleByName(String rolename) {
        return keycloak.realm(realm).roles().get(rolename);
    }


}
