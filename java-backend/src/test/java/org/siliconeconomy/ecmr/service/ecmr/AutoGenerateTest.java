/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withSuccess;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains methods for testing the AutoGenerate
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class AutoGenerateTest {

    private final String exampleSystemId = "1";
    private final String examplefieldvalue = "setECMR_Test_Carrier";
    private final Map<String, Object> ecmrFields = new HashMap<>();

    /**
     * Maps representing different ECMRs
     */
    private final HashMap<String, Object> initEcmr = new HashMap<>();
    private final HashMap<String, Object> modEcmr = new HashMap<>();
    @Autowired
    ObjectMapper objectMapper;
    /**
     * Mock objects for ECMR and User Controller. Letter is checking Signing functionality
     */
    private MockMvc mockMvc;
    @Autowired
    @InjectMocks
    private EcmrManagementController controller;
    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;





    /**
     * Set up test by creating mock controllers and ECMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        for (int i = 0; i < 67; i++) {
            initEcmr.put(Integer.toString(i), "abcdef");
        }
        for (int i = 67; i < 70; i++) {
            modEcmr.put(Integer.toString(i), "abcdef");
        }
        modEcmr.put("69", Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
    }


    @Test
    @WithMockUser(username = "123-456-789-1")
    public void autoGenerateTest() throws Exception {
        Map<String, String> userElements = new HashMap<>();

        String user = "silvia.pagels@ecmr.de";

        // Test for successful request
        try {
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", user)
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).doesNotContain("GENERATED");
                            });

            Map<String, Object> elements = new HashMap<>();
            String json = "";

            // Try with no parameters
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_AUTOGENERATE_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*"))
                    .andExpect(status().isBadRequest());


            // Try with only User Id
            elements.clear();
            elements.put(EcmrConstants.USER_ID_KEY, UserManagementService.storedUsers.get(0).getId());
            json = objectMapper.writeValueAsString(elements);

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_AUTOGENERATE_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*"))
                    .andExpect(status().isBadRequest());

            // Bad request
            elements.clear();
            json = objectMapper.writeValueAsString(elements);
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_AUTOGENERATE_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*"))
                    .andExpect(status().isBadRequest());

            // Valid request
            elements.clear();
            elements.put("creator", user);
            elements.put("assignees", new ArrayList<>(Arrays.asList("sep@superuser.de","verona@verlader.de" )));
            elements.put(EcmrConstants.ECMR_FIELD_KEY, initEcmr);
            json = objectMapper.writeValueAsString(elements);

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_AUTOGENERATE_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*"))
                    .andExpect(status().isCreated());


            when(userManagement.getCurrentUserId()).thenReturn("123-456-789-6");
            // Test for successful request
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "silvia.pagels@ecmr.de")
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("NEW");
                            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

}
