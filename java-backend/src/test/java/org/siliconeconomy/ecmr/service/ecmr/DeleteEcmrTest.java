/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;


import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.keycloak.WithMockKeycloakAuth;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.controller.UserManagementController;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains methods for testing the ECMR Controller
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class DeleteEcmrTest {
    /**
     * Maps representing different ECMRs
     */
    private final HashMap<String, Object> initEcmr = new HashMap<>();
    private final HashMap<String, Object> modEcmr = new HashMap<>();
    @Autowired
    ObjectMapper objectMapper;
    private MockMvc mockMvc;
    private MockMvc mockUserMvc;

    @Autowired
    @InjectMocks
    private EcmrManagementController controller;
    @Autowired
    @InjectMocks
    private UserManagementController usmController;
    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;

    /**
     * Set up test by creating mock controllers and ECMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        this.mockUserMvc =
                MockMvcBuilders.standaloneSetup(usmController)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        for (int i = 0; i < 67; i++) {
            initEcmr.put(Integer.toString(i), "abcdef");
        }
        for (int i = 67; i < 70; i++) {
            modEcmr.put(Integer.toString(i), "abcdef");
        }
        modEcmr.put("69", Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
    }

    @Test
    @WithMockKeycloakAuth(
            authorities = {"USER", "AUTHORIZED_PERSONNEL"},
            claims = @OpenIdClaims(
                    sub = "123-456-789-1",
                    jti = "123-456-789-1", // userId: look @TestDataInjector
                    nbf = "2030-11-18T20:38:00Z",
                    sessionState = "987-654-321",
                    email = "dana@disponent.de",
                    emailVerified = true,
                    nickName = "dana@disponent.de",
                    preferredUsername = "dana@disponent.de")
    )
    public void deleteEcmr() throws Exception {

        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));
        Map<String, Object> elements = new HashMap<>();
        Map<String, Object> ecmrFields = new HashMap<>();


        elements.put(EcmrConstants.ECMR_ID_KEY, "0");
        ecmrFields.clear();
        int[] fields = {
                1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 41, 42, 43, 44,
                45, 46, 47, 48, 49, 50, 51, 52, 65, 66
        };
        for (int i = 0; i < fields.length; i++) {
            ecmrFields.put(String.valueOf(fields[i]), "test");
        }

        elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
        elements.put("signature", "ekhcsePcraM");
        // Test successful update
        String json = objectMapper.writeValueAsString(elements);
        this.mockMvc
                .perform(
                        // Create eCMR
                        MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_ECMR)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isCreated())
                .andDo(
                        mvcResult -> {
                            assertThat(mvcResult.getResponse().getContentAsString()).contains(EcmrConstants.ECMR_ID_KEY);
                            assertThat(mvcResult.getResponse().getContentAsString()).contains("version");

                            String ecrmId = EcmrUtils.parseResult(mvcResult.getResponse().getContentAsString(), EcmrConstants.ECMR_ID_KEY);

                            //invalid ecmrId
                            this.mockMvc
                                    .perform(
                                            MockMvcRequestBuilders.delete(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.DELETE_ECMR_BY_ID.replace("{id}", "-1"))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .header("Origin", "*")
                                    )
                                    .andExpect(status().isNotFound());
                            //test jsonBody==null
                            this.mockMvc
                                    .perform(
                                            MockMvcRequestBuilders.delete(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.DELETE_ECMR_BY_ID.replace("{id}", "null"))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .header("Origin", "*")
                                    )
                                    .andExpect(status().isBadRequest());

                            // TODO still looking for a way
//                            KeycloakAuthenticationToken authentication =
//                                    (KeycloakAuthenticationToken) SecurityContextHolder.getContext().getAuthentication();
//                            authentication.setAuthenticated(Boolean.FALSE);
//                            elements.clear();
//                            elements.put(EcmrConstants.ECMR_ID_KEY, ecrmId);
//                            String json3 = objectMapper.writeValueAsString(elements);
//                            this.mockMvc
//                                    .perform(
//                                            MockMvcRequestBuilders.delete(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.DELETE_ECMR_BY_ID.replace("{id}", "" + ecrmId))
//                                                    .contentType(MediaType.APPLICATION_JSON)
//                                                    .header("Origin", "*") //wrong user id pls
//                                    )
//                                    .andExpect(status().isUnauthorized());
//                            authentication.setAuthenticated(Boolean.TRUE);

                            // delete eCMR
                            this.mockMvc
                                    .perform(
                                            MockMvcRequestBuilders.delete(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.DELETE_ECMR_BY_ID.replace("{id}", "" + ecrmId))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .header("Origin", "*")
                                    )
                                    .andExpect(status().isOk());
                            // Try to get deleted eCMR
                            this.mockMvc
                                    .perform(
                                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", "" + ecrmId))
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .header("Origin", "*")
                                    );
                            // TODO: what status is the right one. will there be a successful response with a eCMR with eCMR status deleted?
                            //.andExpect(status().isNotFound());
                        });

    }

}
