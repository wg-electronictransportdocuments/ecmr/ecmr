/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.junit.jupiter.api.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.models.*;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.net.URI;
import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.keycloak.util.JsonSerialization.mapper;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ExternalTest {



    @Autowired
    ObjectMapper objectMapper;
    private MockMvc mockMvc;
    @Autowired
    @InjectMocks
    private EcmrManagementController controller;
    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;

    private MockRestServiceServer mockServer;


    private String ecmrId;
    private String newEcmrId;

    private EcmrData ecmrData;

    private ExportEcmr exportEcmr;

    @Autowired
    private RestTemplate restTemplate;


    private String mockHostUrl = "https://myecmrtesturl.com";
    @BeforeAll
    public void setUp() {

        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        mockServer = MockRestServiceServer.createServer(restTemplate);

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
    }

    @Test
    @Order(1)
    @WithMockUser(username = "123-456-789-1")
    public void getEcmrId() throws Exception {

        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS  + "?isArchive=false" ))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();

        List<EcmrListData> ecmrListData = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<EcmrListData>>() {});

        ecmrId = ecmrListData.get(3).getEcmrId();
    }

    @Test
    @Order(2)
    public void importEcmr() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));
        EcmrParticipant ecmrParticipant = new EcmrParticipant(mockHostUrl, ecmrId);
        String json = objectMapper.writeValueAsString(ecmrParticipant);

        // with false ecmrID
        mockMvc.perform(
                        MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_EXPORT_ECMR.replace("{id}", "false_id"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json))
                .andExpect(status().isNotFound())
                .andReturn();

        MvcResult result = mockMvc.perform(
                MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_EXPORT_ECMR.replace("{id}", ecmrId))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(json))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        HashMap<String, Object> resMap = mapper.readValue(result.getResponse().getContentAsString(),  HashMap.class);
        exportEcmr = mapper.convertValue(resMap.get("exportEcmr"),  ExportEcmr.class);
        assert exportEcmr.getEcmrData().get(0).getEcmrId().equals(ecmrId);

        ecmrData = exportEcmr.getEcmrData().get(exportEcmr.getEcmrData().size()-1);




    }

    @Test
    @Order(3)
    public void writeEcmrByExternal() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));

        // Update first field
        ecmrData.getFields().put("1","testabc");

        EcmrUpdateObject ecmrUpdateObject = new EcmrUpdateObject(ecmrData, mockHostUrl);

        // Test successful update
        String json = objectMapper.writeValueAsString(ecmrUpdateObject);

        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.EXTERNAL_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace(
                                "{id}", ecmrId).replace("{isEcmr}","true"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isOk())
                .andDo(
                        mvcResult -> {
                            assertThat(mvcResult.getResponse().getContentAsString()).isEmpty();
                        });
    }


    @Test
    @Order(4)
    @WithMockUser(username = "123-456-789-1")
    public void importTest() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));

        newEcmrId = String.valueOf(UUID.randomUUID());
        HashMap<String, String> elements = new HashMap<>();
        Map<String, Object> updateElements = new HashMap<>();

        updateElements.put("exportEcmr", exportEcmr);

        EcmrOrigin mockEcmrOrigin = new EcmrOrigin(newEcmrId, mockHostUrl);
        ecmrData.setEcmrId(newEcmrId);

        exportEcmr.setOrigin(mockEcmrOrigin);
        exportEcmr.setEcmrData(Arrays.asList(ecmrData));


        mockServer.expect(ExpectedCount.once(),
                        requestTo(new URI(mockHostUrl+EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.GET_EXPORT_ECMR.replace("{id}", "wrong_id"))))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.NOT_FOUND)
                );

        mockServer.expect(ExpectedCount.once(),
                requestTo(new URI(mockHostUrl+EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.GET_EXPORT_ECMR.replace("{id}", newEcmrId))))
                .andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.OK)
                        .contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(updateElements))
                );

        mockServer.expect(ExpectedCount.once(),
                        requestTo(new URI(mockHostUrl+EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.EXTERNAL_PATH+EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", newEcmrId).replace("{isEcmr}","true"))))
                .andExpect(method(HttpMethod.PUT))
                .andRespond(withStatus(HttpStatus.OK)
                );
        mockServer.expect(ExpectedCount.once(),
                        requestTo(new URI(mockHostUrl+EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.EXTERNAL_PATH+EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId).replace("{isEcmr}","true"))))
                .andExpect(method(HttpMethod.PUT))
                .andRespond(withStatus(HttpStatus.OK)
                );

        elements.put("ecmrId", "wrong_id");
        elements.put("host", mockHostUrl);
        String json = objectMapper.writeValueAsString(elements);
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.POST_IMPORT)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isNotFound())
                .andDo(
                        mvcResult -> {
                            assertThat(mvcResult.getResponse().getContentAsString()).isEmpty();
                        });

        elements.clear();
        elements.put("ecmrId", newEcmrId);
        elements.put("host", mockHostUrl);
        json = objectMapper.writeValueAsString(elements);
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.POST_IMPORT)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isCreated())
                .andDo(
                        mvcResult -> {
                            assertThat(mvcResult.getResponse().getContentAsString()).isEmpty();
                        });



    }


    @Test
    @Order(5)
    @WithMockUser(username = "123-456-789-1")
    public void updateExternalEcmrTest() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));

        Map<String, Object> updateElements = new HashMap<>();

        // update imported ecmr
        updateElements.put("fields", ecmrData.getFields());
        updateElements.put("signature", "");
        String json = objectMapper.writeValueAsString(updateElements);


        // Update external ecmr on own instance
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", newEcmrId).replace(
                                "{isEcmr}","true"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isNoContent());
    }

    @Test
    @Order(6)
    @WithMockUser(username = "123-456-789-1")
    public void notifyParticipantsTest() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));
        mockServer.reset(); //Clear previous expectations from other tests

        Map<String, Object> updateElements = new HashMap<>();

        // update imported ecmr
        updateElements.put("fields", ecmrData.getFields());
        updateElements.put("signature", "");
        String json = objectMapper.writeValueAsString(updateElements);

        // Update external ecmr on own instance
        this.mockMvc
                .perform(
                        MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH+EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId).replace(
                                "{isEcmr}","true"))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isNoContent());

        mockServer.verify();
    }
}
