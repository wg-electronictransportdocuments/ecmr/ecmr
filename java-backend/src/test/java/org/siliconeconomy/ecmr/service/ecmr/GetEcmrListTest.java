/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.controller.UserManagementController;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.EcmrManagementService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/**
 * This class contains methods for testing the ECMR Controller
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)


public class GetEcmrListTest {
    /**
     * Maps representing different ECMRs
     */
    @Autowired
    ObjectMapper objectMapper;
    private MockMvc mockMvc;
    private MockMvc mockUserMvc;
    @Autowired
    @InjectMocks
    private EcmrManagementController controller;
    @Autowired
    @InjectMocks
    private UserManagementController usmController;
    @Autowired
    private EcmrManagementService ecmrManagementService;
    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;

    /**
     * Set up test by creating mock controllers and ECMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        this.mockUserMvc =
                MockMvcBuilders.standaloneSetup(usmController)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();
        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
        EcmrUtils.generateTestDataEcmrs(ecmrManagementService);
    }

    /**
     * Tests for getECMRList-interface
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-1")
    void getEcmrList() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);

        // setup mock data
        Map<String, String> elements = new HashMap<>();

        // Test for successful request
        try {
            String json = objectMapper.writeValueAsString(elements);
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains(EcmrConstants.ECMR_ID_KEY);
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("consigneeName");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("senderName");
                            });
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
