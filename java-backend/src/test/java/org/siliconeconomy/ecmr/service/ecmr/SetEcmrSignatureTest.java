/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;


import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.keycloak.WithMockKeycloakAuth;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.controller.SignatureController;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.controller.UserManagementController;
import org.siliconeconomy.ecmr.service.models.EcmrListData;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.EcmrManagementService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.security.KeyPair;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains methods for testing the ECMR Controller
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)


public class SetEcmrSignatureTest {
    @Autowired
    ObjectMapper objectMapper;
    private MockMvc mockMvc;
    @Autowired
    @InjectMocks
    private EcmrManagementController controller;
    @Autowired
    private EcmrManagementService ecmrManagementService;
    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;

    /**
     * Set up test by creating mock controllers and ECMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
        EcmrUtils.generateTestDataEcmrs(ecmrManagementService);
    }

    /**
     * Tests for Signature
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-1")
    void setEcmrSignature() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false"))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();
        List<EcmrListData> ecmrListData = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<EcmrListData>>() {});

        String ecmrId = ecmrListData.get(0).getEcmrId();
        String wrongId = "910719142430450001";
        String examplefieldvalue = "setECMR_Test_Carrier";

        KeyPair kp = SignatureController.generateKeys();
        String pbPEM = SignatureController.toPem(kp.getPublic());


        try {

            Map<String, Object> elements = new HashMap<>();
            Map<String, Object> ecmrFields = new HashMap<>();
            // right id
            elements.put(EcmrConstants.ECMR_ID_KEY, ecmrId);
            ecmrFields.put("0", examplefieldvalue);
            for(int i = 1; i<75; i++){
                ecmrFields.put(Integer.toString(i), examplefieldvalue + i);
            }
            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            elements.put(
                    "signature",
                    SignatureController.sign(kp, SignatureController.mapToString(ecmrFields)));

            try {
                String json = objectMapper.writeValueAsString(elements);
                this.mockMvc
                        .perform(
                                MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId).replace("{isEcmr}","true"))
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(json)
                                        .header("Origin", "*")
                        )
                        .andExpect(status().isNoContent());

                elements.put(EcmrConstants.ECMR_ID_KEY, wrongId);
                elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
                json = objectMapper.writeValueAsString(elements);
                this.mockMvc
                        .perform(
                                MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", "12345").replace("{isEcmr}","true"))
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(json)
                                        .header("Origin", "*")
                        )
                        .andExpect(status().isNotFound())
                        .andDo(
                                mvcResult -> {
                                    assertThat(mvcResult.getResponse().getContentAsString())
                                            .contains("Resource does not exist");
                                });

                elements.clear();
                json = objectMapper.writeValueAsString(elements);
                this.mockMvc
                        .perform(
                                MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId).replace("{isEcmr}","true"))
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(json)
                                        .header("Origin", "*")
                        )
                        .andExpect(status().isBadRequest())
                        .andDo(
                                mvcResult -> {
                                    assertThat(mvcResult.getResponse().getContentAsString())
                                            .contains("Invalid parameters");
                                });

                Map<String, String> elementsOfWrongType = new HashMap<String, String>();

                elementsOfWrongType.put(EcmrConstants.ECMR_ID_KEY, "ABC");
                elementsOfWrongType.put("signature", "ABC");
                json = objectMapper.writeValueAsString(elementsOfWrongType);
                this.mockMvc
                        .perform(
                                MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId).replace("{isEcmr}","true"))
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .content(json)
                                        .header("Origin", "*")
                        )
                        .andExpect(status().isInternalServerError())
                        .andDo(
                                mvcResult -> {
                                    assertThat(mvcResult.getResponse().getContentAsString())
                                            .contains("Internal error");
                                });

            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
}
