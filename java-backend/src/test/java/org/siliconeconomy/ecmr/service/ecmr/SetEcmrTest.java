/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;


import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.keycloak.WithMockKeycloakAuth;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.controller.SignatureController;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.controller.UserManagementController;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains methods for testing the ECMR Controller
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)


public class SetEcmrTest {
    /**
     * Maps representing different ECMRs
     */
    private final HashMap<String, Object> initEcmr = new HashMap<>();
    private final HashMap<String, Object> modEcmr = new HashMap<>();
    @Autowired
    ObjectMapper objectMapper;
    private MockMvc mockMvc;
    private MockMvc mockUserMvc;
    @Autowired
    @InjectMocks
    private EcmrManagementController controller;
    @Autowired
    @InjectMocks
    private UserManagementController usmController;
    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;


    /**
     * Set up test by creating mock controllers and ECMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        this.mockUserMvc =
                MockMvcBuilders.standaloneSetup(usmController)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();
        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        for (int i = 0; i < 67; i++) {
            initEcmr.put(Integer.toString(i), "abcdef");
        }
        for (int i = 67; i < 70; i++) {
            modEcmr.put(Integer.toString(i), "abcdef");
        }
        modEcmr.put("69", Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
    }

    /**
     * Tests for SetECMR-interface
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockKeycloakAuth(
            authorities = {"USER", "AUTHORIZED_PERSONNEL"},
            claims = @OpenIdClaims(
                    sub = "123-456-789-1",
                    jti = "123-456-789-1", // userId: look @TestDataInjector
                    nbf = "2030-11-18T20:38:00Z",
                    sessionState = "987-654-321",
                    email = "dana@disponent.de",
                    emailVerified = true,
                    nickName = "dana@disponent.de",
                    preferredUsername = "dana@disponent.de")
    )
    void setEcmr() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));
        // Setting up mock data
        String id = "1629903275000001";
        String secondid = "1629903276000001";
        String wrongId = "910719142430450001";
        int examplefieldname = 2;
        int exampleentryname = 1;
        String examplefieldvalue = "setECMR_Test_Carrier";
        int userid = 1;

        Map<String, Object> elements = new HashMap<>();
        Map<String, Object> ecmrFields = new HashMap<>();

        // right id
        // elements.put(EcmrConstants.ECMR_ID_KEY, id); // will be generated at newEcmr

        elements.put("ecmrId", "0");
        ecmrFields.put("1", "Fraunhofer Institute (IML)");
        ecmrFields.put("2", "Mr. Mueller");
//        ecmrFields.put("3", "Joseph-von-Fraunhofer-Straße 2-");
        ecmrFields.put("4", "44227");
        ecmrFields.put("5", "Dortmund");
        ecmrFields.put("6", "Germany");
        ecmrFields.put("7", "Fraunhofer-Gesellschaft");
        ecmrFields.put("8", "Mr. Fischer");
        ecmrFields.put("9", "80686");
        ecmrFields.put("10", "Hansastraße 27c");
        ecmrFields.put("11", "Munich");
        ecmrFields.put("12", "DE");
        ecmrFields.put("13", "Germany");
        ecmrFields.put("14", "Mr. Fischer");
        ecmrFields.put("15", "13/09/2021");
        ecmrFields.put("16", "10/09/2021");
        ecmrFields.put("17", "Warehouse");
        ecmrFields.put("18", "Mon: 9am - 5pm Tue: 9am - 5pm Wed:9am - 5pm Thu:9am - 5pm");
        ecmrFields.put("19", "None");
        ecmrFields.put("20", "560");
        ecmrFields.put("40", "Delivery note No: 16538987");
        ecmrFields.put("41", "DHJ");
        ecmrFields.put("42", "156742");
        ecmrFields.put("43", "20");
        ecmrFields.put("44", "254236");
        ecmrFields.put("45", "Euro pallet");
        ecmrFields.put("46", "165");
        ecmrFields.put("47", "125434");
        ecmrFields.put("48", "40");
        ecmrFields.put("49", "Wasser");
        ecmrFields.put("50", "1210");
        ecmrFields.put("51", "1.44");
        ecmrFields.put("52", "Handle with care");
        ecmrFields.put("65", "Placeholder");

        elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
        elements.put("signature", "");

        try {
            String json = objectMapper.writeValueAsString(elements);
            // Test with missing mandatory field
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().is4xxClientError())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("Bad format. Mandatory field is missing");
                            });

            elements.put(EcmrConstants.ECMR_ID_KEY, wrongId);
            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            elements.put("signature", "");
            json = objectMapper.writeValueAsString(elements);
            // Test updating an non-existent ECMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", wrongId).replace("{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNotFound())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("Resource does not exist");
                            });

            // Test with missing mandatory field
            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            elements.put("signature", "");
            json = objectMapper.writeValueAsString(elements);
            // new ECMR
            Map<String, Object> elementsForNewEcmr = new HashMap<>();
            Map<String, Object> ecmrFieldsForNewEcmr = new HashMap<>();

            // build a valid fields for creating new ecmr
            int[] fieldsForNewEcmr = {
                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 41, 42, 43, 44,
                    45, 46, 47, 48, 49, 50, 51, 52, 65, 66
            };
            for (int i = 0; i < fieldsForNewEcmr.length; i++) {
                ecmrFieldsForNewEcmr.put(String.valueOf(fieldsForNewEcmr[i]), "test");
            }
            elementsForNewEcmr.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFieldsForNewEcmr);
            elementsForNewEcmr.put("signature", "ekhcsePcraM");
            // Test successful update
            String jsonForNewEcmr = objectMapper.writeValueAsString(elementsForNewEcmr);
            MvcResult resultMvc = mockMvc
                    .perform(
                            // Create eCMR
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(jsonForNewEcmr)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isCreated())
                    .andReturn();
            String result = resultMvc.getResponse().getContentAsString();
            assertThat(resultMvc.getResponse().getContentAsString().contains("ecmrId"));
            assertThat(resultMvc.getResponse().getContentAsString().contains("version"));
            String ecmrId = EcmrUtils.parseResult(resultMvc.getResponse().getContentAsString(), EcmrConstants.ECMR_ID_KEY);

            json = objectMapper.writeValueAsString(elements);

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId).replace(
                                    "{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isBadRequest())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("Bad format. Mandatory field is missing");
                            });

            elements.put(EcmrConstants.ECMR_ID_KEY, "0");
            ecmrFields.clear();
            int[] fields = {
                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 44,
                    46, 47, 48, 52, 65, 66
            };
            int[] listFields = {41, 42, 43, 45, 49, 50, 51};
            for (int i : fields) {
                ecmrFields.put(Integer.toString(i), "test");
            }
            Map<String, String> packingLists = new HashMap<>();
            for (int i : listFields) {
                packingLists.put(Integer.toString(i), "listTest");
            }
            ecmrFields.put("positions", new ArrayList<>(List.of(packingLists)));

            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            elements.put("signature", SignatureController.mapToString(ecmrFields));

            // Test successful update
            json = objectMapper.writeValueAsString(elements);
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId).replace(
                                    "{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).isEmpty();
                            });

            elements.clear();
            json = objectMapper.writeValueAsString(elements);

            // Test setting a ECMR with invalid parameters (none in this case)
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}",
                                            "" + elements.get(EcmrConstants.ECMR_ID_KEY)).replace("{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isBadRequest())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("Invalid parameters");
                            });

            Map<String, String> elementsOfWrongType = new HashMap<>();

            elementsOfWrongType.put(EcmrConstants.ECMR_ID_KEY, "ABC");
            elementsOfWrongType.put("signature", "");
            json = objectMapper.writeValueAsString(elementsOfWrongType);
            // Testing for internal error
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}",
                                            "" + elements.get(EcmrConstants.ECMR_ID_KEY)).replace("{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isInternalServerError())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("Internal error");
                            });
//            // userid = 4; // see below TODO
//            userid = 1;
//            elements = new HashMap<>();
//            ecmrFields = new HashMap<>();
//            // right id
//            elements.put(EcmrConstants.ECMR_ID_KEY, secondid);
//            ecmrFields.put("71",examplefieldvalue);
//            ecmrFields.put("72", examplefieldvalue);
//            ecmrFields.put("74", examplefieldvalue);
//            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
//            elements.put("signature", "");
//            jwttokenbuilder =
//                    Jwts.builder()
//                            .setIssuedAt(now)
//                            .signWith(signingKey, signatureAlgorithm)
//                            .setSubject(Integer.toString(userid))
//                            .claim(
//                                    "authorities",
//                                    grantedAuthorities.stream()
//                                            .map(GrantedAuthority::getAuthority)
//                                            .collect(Collectors.toList()))
//                            .setExpiration(new Date(nowMillis + 1800000));
//
//            json = objectMapper.writeValueAsString(elements);
//
//            // Test for successful update with other role
//            // TODO review: The update role is implemented in the STATUS class and is fix. -> Change temporarily UserID to 1
//
//            ecmrFields.clear();
//            int[] fields1 = {
//                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 41, 42, 43, 44,
//                    45, 46, 47, 48, 49, 50, 51, 52, 65, 66
//            };
//            for (int i : fields1) {
//                ecmrFields.put(Integer.toString(i),"test");
//            }
//            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
//            json = objectMapper.writeValueAsString(elements);
//            this.mockMvc
//                    .perform(
//                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", ecmrId))
//                                    .contentType(MediaType.APPLICATION_JSON)
//                                    .content(json)
//                                    .header("Origin", "*")
//                                    )
//                    .andExpect(status().isNoContent())
//                    .andDo(
//                            mvcResult -> {
//                                assertThat(mvcResult.getResponse().getContentAsString()).isEmpty();
//                            });
//
//            elements.clear();
//
//            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
//            elements.put("signature", "");
//            json = objectMapper.writeValueAsString(elements);
//            userid = 8;
//            jwttokenbuilder =
//                    Jwts.builder()
//                            .setIssuedAt(now)
//                            .signWith(signingKey, signatureAlgorithm)
//                            .setSubject(Integer.toString(userid))
//                            .claim(
//                                    "authorities",
//                                    grantedAuthorities.stream()
//                                            .map(GrantedAuthority::getAuthority)
//                                            .collect(Collectors.toList()))
//                            .setExpiration(new Date(nowMillis + 1800000));
//
//            // Test with role without necessary permissions
//            this.mockMvc
//                    .perform(
//                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}" , "1629903276000001"))
//                                    .contentType(MediaType.APPLICATION_JSON)
//                                    .content(json)
//                                    .header("Origin", "*")
//                                    )
//                    .andExpect(status().isForbidden())
//                    .andDo(
//                            mvcResult -> {
//                                assertThat(mvcResult.getResponse().getContentAsString())
//                                        .contains("User not allowed");
//                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test of endpoints who are not used yet -> code coverage
     *
     * @throws Exception thrown if test fails
     */
    @Test
    @WithMockKeycloakAuth(
            authorities = {"USER", "AUTHORIZED_PERSONNEL"},
            claims = @OpenIdClaims(
                    sub = "123-456-789-1",
                    jti = "123-456-789-1", // userId: look @TestDataInjector
                    nbf = "2030-11-18T20:38:00Z",
                    sessionState = "987-654-321",
                    email = "dana@disponent.de",
                    emailVerified = true,
                    nickName = "dana@disponent.de",
                    preferredUsername = "dana@disponent.de")
    )
    public void checkUserContacts() throws Exception {
        try {

            Map<String, Object> elements = new HashMap<>();
            elements.put("signature", "");
            String json = objectMapper.writeValueAsString(elements);

            // test valid request
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_FIELDS)
                                    .contentType(MediaType.APPLICATION_JSON)

                                    .header("Origin", "*"))
                    .andExpect(status().isNotFound())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("Resource does not exist");
                            });
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_FIELD_BY_ID.replace("{id}", "abcd"))
                                    .contentType(MediaType.APPLICATION_JSON)

                                    .header("Origin", "*"))
                    .andExpect(status().isNotFound())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("Resource does not exist");
                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
