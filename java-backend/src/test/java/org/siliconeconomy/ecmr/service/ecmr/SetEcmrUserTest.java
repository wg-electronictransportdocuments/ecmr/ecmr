/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.models.EcmrListData;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains methods for testing the ECMR Controller
 */
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class SetEcmrUserTest {
    /**
     * Maps representing different ECMRs
     */
    private final HashMap<String, Object> initEcmr = new HashMap<>();
    private final HashMap<String, Object> modEcmr = new HashMap<>();

    @Autowired
    ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Autowired
    private EcmrManagementController controller;

    private String globalEcmrId;

    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;


    /**
     * Set up test by creating mock controllers and ECMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        for (int i = 0; i < 67; i++) {
            initEcmr.put(Integer.toString(i), "abcdef");
        }
        for (int i = 67; i < 70; i++) {
            modEcmr.put(Integer.toString(i), "abcdef");
        }
        modEcmr.put("69", Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
    }

    /**
     * Tests for SetECMRUser-interface
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-1")
    void setEcmrUser() throws Exception {

        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false"))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();

        List<EcmrListData> ecmrListData = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<EcmrListData>>() {
        });


        String ecmrId = ecmrListData.get(0).getEcmrId();

        try {
            // Test user which already has necessary permissions
            String s = EcmrEndpoints.ECMR_BASE_PATH +
                    EcmrEndpoints.PUT_USER_AT_ECMR_BY_IDS.replace("{ecmrId}", ecmrId).replace("{userId}", "dana@disponent.de");
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent());
            // Test user which already has necessary permissions
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent());


        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests for SetECMRUser-interface
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-1")
    void claimEcmrByCurrentUser() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));

        try {

            // new eCMR
            Map<String, Object> elementsForNewEcmr = new HashMap<>();
            Map<String, Object> ecmrFieldsForNewEcmr = new HashMap<>();

            // build a valid fields for creating new ecmr
            int[] fieldsForNewEcmr = {
                    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 40, 41, 42, 43, 44,
                    45, 46, 47, 48, 49, 50, 51, 52, 65, 66
            };
            for (int i = 0; i < fieldsForNewEcmr.length; i++) {
                ecmrFieldsForNewEcmr.put(String.valueOf(fieldsForNewEcmr[i]), "test");
            }
            elementsForNewEcmr.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFieldsForNewEcmr);
            elementsForNewEcmr.put("signature", "ekhcsePcraM");
            // Test successful update
            String jsonForNewEcmr = objectMapper.writeValueAsString(elementsForNewEcmr);
            MvcResult resultMvc = mockMvc
                    .perform(
                            // Create eCMR
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(jsonForNewEcmr)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isCreated())
                    .andReturn();
            String result = resultMvc.getResponse().getContentAsString();
            assertThat(resultMvc.getResponse().getContentAsString().contains("ecmrId"));
            assertThat(resultMvc.getResponse().getContentAsString().contains("version"));

            globalEcmrId = EcmrUtils.parseResult(result, EcmrConstants.ECMR_ID_KEY);

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Tests for SetECMRUser-interface
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-4")
    void claimEcmrByCurrentUserNext() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-4");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);

        try {

            String s = EcmrEndpoints.ECMR_BASE_PATH +
                    EcmrEndpoints.PUT_USER_AT_ECMR_BY_IDS.replace("{ecmrId}", globalEcmrId)
                            .replace("{userId}", "123-456-789-4");

            // claim eCMR by user 2
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString().isEmpty());
                            });
            // already assosiated error
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH +
                                            EcmrEndpoints.PUT_CURRENTUSER_AT_ECMR_BY_ID.replace("{id}", globalEcmrId))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString().equals("User has already been associated"));
                            });
            // no ecmr found
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH +
                                            EcmrEndpoints.PUT_CURRENTUSER_AT_ECMR_BY_ID.replace("{id}", "4321"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNotFound())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString().equals("Resource does not exist"));
                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
