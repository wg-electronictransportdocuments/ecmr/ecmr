/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */

package org.siliconeconomy.ecmr.service.ecmr;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.models.EcmrListData;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.EcmrManagementService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains methods for testing the ECMR Controller
 */
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class SetEcmrUsersTest {
    @Autowired
    ObjectMapper objectMapper;

    private MockMvc mockMvc;

    @Autowired
    private EcmrManagementController controller;
    @Autowired
    private EcmrManagementService ecmrManagementService;
    @MockBean
    private StatusService statusService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private UserManagement userManagement;

    /**
     * Set up test by creating mock controllers and ECMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
        EcmrUtils.generateTestDataEcmrs(ecmrManagementService);
    }

    /**
     * Tests for SetECMRUsers-interface
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-1")
    void setEcmrUsers() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);

        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false"))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();

        List<EcmrListData> ecmrListData = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<EcmrListData>>() {
        });

        String ecmrId = ecmrListData.get(0).getEcmrId();

        try {
            // Test jsonBody is missing
            String s = EcmrEndpoints.ECMR_BASE_PATH +
                    EcmrEndpoints.PUT_USERS_AT_ECMR_BY_IDS.replace("{ecmrId}", ecmrId);
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isBadRequest());

            // Test jsonBody is null
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(String.valueOf((Object) null))
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isBadRequest());

            // Test invalid request, users is missing {"ecmrId":"552aca83-128e-4ac7-8e9e-f84ecdca3c6b","users":[{"userId":"123-456-789-3","response":""},{"userId":"123-456-789-4","response":""},{"userId":"123-456-789-5","response":""}]}
            JsonFactory factory = new JsonFactory();
            StringWriter stringWriter = new StringWriter();
            JsonGenerator generator = factory.createGenerator(stringWriter);
            generator.writeStartObject(); //{
            generator.writeStringField("ecmrId", ecmrId);
            generator.writeFieldName("uIsMissing");
            generator.writeStartArray(); //[
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-1");
            generator.writeEndObject(); //}
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-2");
            generator.writeEndObject(); //}
            generator.writeEndArray(); //]
            generator.writeEndObject(); //}
            generator.flush();
            generator.close();

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(String.valueOf(stringWriter))
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isBadRequest());

            // Test valid request {"ecmrId":"552aca83-128e-4ac7-8e9e-f84ecdca3c6b","users":[{"userId":"123-456-789-3","response":""},{"userId":"123-456-789-4","response":""},{"userId":"123-456-789-5","response":""}]}
            stringWriter = new StringWriter();
            generator = factory.createGenerator(stringWriter);
            generator.writeStartObject(); //{
            generator.writeStringField("ecmrId", ecmrId);
            generator.writeFieldName("users");
            generator.writeStartArray(); //[
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-3");
            generator.writeEndObject(); //}
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-4");
            generator.writeEndObject(); //}
            generator.writeEndArray(); //]
            generator.writeEndObject(); //}
            generator.flush();
            generator.close();

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(String.valueOf(stringWriter))
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk());

            // Test valid request, but one wrong user
            stringWriter = new StringWriter();
            generator = factory.createGenerator(stringWriter);
            generator.writeStartObject(); //{
            generator.writeStringField("ecmrId", ecmrId);
            generator.writeFieldName("users");
            generator.writeStartArray(); //[
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-1");
            generator.writeEndObject(); //}
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-99");
            generator.writeEndObject(); //}
            generator.writeEndArray(); //]
            generator.writeEndObject(); //}
            generator.flush();
            generator.close();

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(String.valueOf(stringWriter))
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk());

            // Test valid request, but wrong ecmrId
            s = EcmrEndpoints.ECMR_BASE_PATH +
                    EcmrEndpoints.PUT_USERS_AT_ECMR_BY_IDS.replace("{ecmrId}", "ABC");

            stringWriter = new StringWriter();
            generator = factory.createGenerator(stringWriter);
            generator.writeStartObject(); //{
            generator.writeStringField("ecmrId", "ABC");
            generator.writeFieldName("users");
            generator.writeStartArray(); //[
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-1");
            generator.writeEndObject(); //}
            generator.writeStartObject(); //{
            generator.writeStringField("userId", "123-456-789-4");
            generator.writeEndObject(); //}
            generator.writeEndArray(); //]
            generator.writeEndObject(); //}
            generator.flush();
            generator.close();

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(s)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(String.valueOf(stringWriter))
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNotFound());

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
