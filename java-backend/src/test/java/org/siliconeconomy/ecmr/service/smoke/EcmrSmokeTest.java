/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.smoke;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.Connection;
import org.siliconeconomy.ecmr.service.models.CurrentUserContact;
import org.siliconeconomy.ecmr.service.models.Right;
import org.siliconeconomy.ecmr.service.models.User;
import org.siliconeconomy.ecmr.service.models.UserRight;
import org.siliconeconomy.ecmr.service.repositories.ConnectionRepository;
import org.siliconeconomy.ecmr.service.repositories.UserRepository;
import org.siliconeconomy.ecmr.service.repositories.UserRightRepository;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.ConnectionService;
import org.siliconeconomy.ecmr.service.services.FieldService;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.siliconeconomy.ecmr.service.services.UserMandatoryFieldService;
import org.siliconeconomy.ecmr.service.services.UserRightService;
import org.siliconeconomy.ecmr.service.services.UserService;
import org.siliconeconomy.ecmr.service.services.UsertypeService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * This class contains tests for unreachable methods
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class EcmrSmokeTest {

    @Autowired
    UserRightService userRightService;
    @Autowired
    UserService userService;
    @Autowired
    ConnectionService connectionService;
    @Autowired
    UserManagementService userManagementService;
    @Autowired
    UserMandatoryFieldService userMandatoryFieldService;
    @Autowired
    FieldService fieldService;
    @Autowired
    UserRightRepository userRightRepository;
    @Autowired
    ConnectionRepository connectionRepository;
    @Autowired
    UsertypeService usertypeService;
    @Autowired
    UserRepository userRepository;
    @MockBean
    StatusService statusService;
    @MockBean
    UserManagement userManagement;
    @MockBean
    RoleService roleService;

    @BeforeEach
    public void beforeEach() {
        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
    }

    /**
     * Touch unreachable methods
     *
     * @throws Exception thrown if test fails
     */
    @Test
    @Transactional
    @WithMockUser(username = "123-456-789-1")
    public void smokeTesting() throws Exception {
        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");
        when(statusService.getNextStatus(anyString(), anyList())).thenReturn("NEW");
        when(statusService.allowedToCreateEcmr(anyList())).thenReturn(true);
        when(statusService.allowedToChangeEcmr(anyString(), anyList())).thenReturn(true);
        when(roleService.getRoleByName(anyString())).thenReturn(RoleService.storedRoles.get(0));

        var right = new Right("TestRight", "Only for testing");
        var userRight = new UserRight(right, Arrays.asList("ecmr-disponent","ecmr-verlader"));
        userRightRepository.save(userRight);
        userRightService.getUserRightIds(Arrays.asList("ecmr-disponent","ecmr-verlader"));

        var userfacade = userService.getById("123-456-789-1");
        var user = new User(userfacade);
        userRepository.save(user);
        userService.getUserIDByEmail("dana@disponent.de");
        List<CurrentUserContact> contacts = new ArrayList<>();
        var cuc = new CurrentUserContact("1234", "1234", "1234", "1234",
                1, new Timestamp(System.currentTimeMillis()));
        contacts.add(cuc);
        userService.addUserInformation(contacts);

        connectionRepository.save(new Connection("1234567890", "123-456-789-1", "1234" ));

        userManagementService.getUserIdByEmail("dana@disponent.de");
        userManagementService.getFieldsForUser();
        userManagementService.getAttributeByNameFromUserByID(new UserFacade("1234","1234",
                "dana@disponent.de", Arrays.asList("ecmr_disponent"),null, null),
                "company");

        userMandatoryFieldService.getUserMandatoryFieldIds(Arrays.asList("ecmr-disponent","ecmr-verlader"));

        fieldService.getFieldDescriptions(Arrays.asList(69,70));
        fieldService.getTemplateFieldDescriptions();

        usertypeService.addUsertypeInformation(Arrays.asList(cuc));
    }
}
