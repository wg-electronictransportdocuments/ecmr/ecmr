/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.user;


import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.keycloak.WithMockKeycloakAuth;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.UserManagementController;
import org.siliconeconomy.ecmr.service.repositories.ContactRepository;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.siliconeconomy.ecmr.service.usermanagement.UserManagement;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains tests for user management
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@EnableWebMvc
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class LoginTest {

    public static final String ID = "{id}";
    private final String fakePubKey =
            "-----BEGIN PUBLIC KEY-----\n"
                    + "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEI+itrrhydD7/JrEEtrizQ/HxRlQ3\n"
                    + "dAoAP4f2+bo+M9XYFS39W5/vXw7m+QQkuzEXICymckgUMiQnq7coYGuSQw==\n"
                    + "-----END PUBLIC KEY-----";
    @Autowired
    ObjectMapper objectMapper;

    private MockMvc mockMvc;
    @Autowired
    @InjectMocks
    private UserManagementController controller;
    @Autowired
    private ContactRepository contactRepository;
    @MockBean
    StatusService statusService;
    @MockBean
    UserManagement userManagement;
    @MockBean
    RoleService roleService;

    /**
     * Setup method for creating mock objects
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();
        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        EcmrUtils.generateTestRoles();
        EcmrUtils.generateTestUsers();
        EcmrUtils.generateTestStatus();
        EcmrUtils.generateTestContacts(contactRepository, "123-456-789-1");
    }

    /**
     * Test for getting correct username for id
     *
     * @throws Exception thrown if test fails
     */
    @Test
    @WithMockUser(username = "123-456-789-1")
    public void checkUserId() throws Exception {

        Map<String, String> elements;

        // valid login
        elements = new HashMap<>();
        elements.put(EcmrConstants.USER_ID_KEY, UserManagementService.storedUsers.get(0).getId());

        try {
            // test valid request
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.USER_BASE_PATH + EcmrEndpoints.GET_USER_BY_ID.replace(ID, UserManagementService.storedUsers.get(0).getId()))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*"))
                    .andExpect(status().isOk());
//                    .andDo(
//                            mvcResult -> {
//                                assertThat(mvcResult.getResponse().getContentAsString()).contains("Dana");
//                            });

            elements.put(EcmrConstants.USER_ID_KEY, "32423");
            // test for invalid userid
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.USER_BASE_PATH + EcmrEndpoints.GET_USER_BY_ID.replace(ID, "32432"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*"))
                    .andExpect(status().isNotFound())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("Resource does not exist");
                            });
            elements.clear();

            Map<String, Integer> elementsOfWrongType = new HashMap<>();
            // test internal error
            elementsOfWrongType.put(EcmrConstants.USER_ID_KEY, 1);
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.USER_BASE_PATH + EcmrEndpoints.GET_USER_BY_ID.replace(ID, "peter"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*"))
                    .andExpect(status().isNotFound())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("Resource does not exist");
                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test for getting contacts of a user for id
     *
     * @throws Exception thrown if test fails
     */
    @Test
    @WithMockUser(username = "123-456-789-1")

    public void checkUserContacts() throws Exception {

        when(userManagement.getCurrentUserId()).thenReturn("123-456-789-1");

        try {
            // test valid request
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.USER_BASE_PATH + EcmrEndpoints.GET_USERS_CONTACTS)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*"))
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("contactId");
                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Test for getting contacts of a user for id
     *
     * @throws Exception thrown if test fails
     */
    @Test
    @WithMockUser(username = "123-456-789-1")

    public void testAddConnection() throws Exception {

        Map<String, Object> elements = new HashMap<String, Object>();
        elements.put("pubKey", fakePubKey);
        String json = objectMapper.writeValueAsString(elements);
        try {
            // test valid request
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.post(EcmrEndpoints.USER_BASE_PATH + EcmrEndpoints.POST_USER_PUBKEY)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*"))
                    .andExpect(status().isOk());

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }
}
