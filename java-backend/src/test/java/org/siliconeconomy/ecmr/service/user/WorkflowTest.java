/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.user;

import com.c4_soft.springaddons.security.oauth2.test.annotations.OpenIdClaims;
import com.c4_soft.springaddons.security.oauth2.test.annotations.keycloak.WithMockKeycloakAuth;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.siliconeconomy.ecmr.service.exception.ExceptionHandling;
import org.siliconeconomy.ecmr.service.controller.SignatureController;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.controller.EcmrManagementController;
import org.siliconeconomy.ecmr.service.controller.UserManagementController;
import org.siliconeconomy.ecmr.service.services.EcmrService;
import org.siliconeconomy.ecmr.service.services.EcmrUserService;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.siliconeconomy.ecmr.service.utils.EcmrUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.security.*;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * This class contains tests representing a typical workflow
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@EnableWebMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Disabled
public class WorkflowTest {

    public static final String E_CMRID = "{ecmrId}";
    public static final String USER_ID = "{userId}";
    public static final String ID = "{id}";
    private static final Logger LOGGER = LoggerFactory.getLogger(WorkflowTest.class);
    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EcmrService ecmrService;
    @Autowired
    EcmrUserService ecmrUserService;

    /**
     * Maps representing different eCMRs
     */
    HashMap<String, Object> initEcmr = new HashMap<>();
    HashMap<String, Object> modEcmr = new HashMap<>();
    String globalEcmrId = StringUtils.EMPTY;
    /**
     * Mock objects for eCMR and User Controller. Letter is checking Signing functionality
     */
    private MockMvc mockMvc;
    private MockMvc mockUserMvc;
    @Autowired
    @InjectMocks
    private EcmrManagementController controller;
    @Autowired
    @InjectMocks
    private UserManagementController usmController;

    /**
     * Set up test by creating mock controllers and eCMRs
     */
    @BeforeAll
    public void setUp() {
        this.mockMvc =
                MockMvcBuilders.standaloneSetup(controller)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();

        this.mockUserMvc =
                MockMvcBuilders.standaloneSetup(usmController)
                        .setControllerAdvice(new ExceptionHandling())
                        .build();
        MockitoAnnotations.openMocks(this);
        objectMapper = new ObjectMapper();

        for (int i = 0; i < 67; i++) {
            initEcmr.put(Integer.toString(i), "abcdef");
        }
        for (int i = 67; i < 70; i++) {
            modEcmr.put(Integer.toString(i), "abcdef");
        }
        modEcmr.put("69", Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));

    }

    /**
     * Checks the happy path from Dana
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-1")
    void testADana() throws Exception {
        LOGGER.info("Found {} Testusers.", UserManagementService.storedUsers.size());

        //Set up the login data from Dana
        String user = UserManagementService.storedUsers.get(0).getEmail();
        KeyPair kp = SignatureController.generateKeys();
        String pbPEM = SignatureController.toPem(kp.getPublic());

        Map<String, Object> loginElements = new HashMap<String, Object>();
        loginElements.put("email", user);
        loginElements.put("pubKey", pbPEM);
        try {

            Map<String, Object> elements = new HashMap<String, Object>();
            Map<String, Object> ecmrFields = new HashMap<>();

            elements.put("ecmrId", "0");
            ecmrFields.put("1", "Fraunhofer Institute (IML)");
            ecmrFields.put("2", "Mr. Mueller");
            ecmrFields.put("3", "Joseph-von-Fraunhofer-Straße 2-");
            ecmrFields.put("4", "44227");
            ecmrFields.put("5", "Dortmund");
            ecmrFields.put("6", "Germany");
            ecmrFields.put("7", "Fraunhofer-Gesellschaft");
            ecmrFields.put("8", "Mr. Fischer");
            ecmrFields.put("9", "80686");
            ecmrFields.put("10", "Hansastraße 27c");
            ecmrFields.put("11", "Munich");
            ecmrFields.put("12", "DE");
            ecmrFields.put("13", "Germany");
            ecmrFields.put("14", "Mr. Fischer");
            ecmrFields.put("15", "13/09/2021");
            ecmrFields.put("16", "10/09/2021");
            ecmrFields.put("17", "Warehouse");
            ecmrFields.put("18", "Mon: 9am - 5pm Tue: 9am - 5pm Wed:9am - 5pm Thu:9am - 5pm");
            ecmrFields.put("19", "None");
            ecmrFields.put("20", "560");
            ecmrFields.put("40", "Delivery note No: 16538987");
            ecmrFields.put("41", "DHJ");
            ecmrFields.put("42", "156742");
            ecmrFields.put("43", "20");
            ecmrFields.put("44", "254236");
            ecmrFields.put("45", "Euro pallet");
            ecmrFields.put("46", "165");
            ecmrFields.put("47", "125434");
            ecmrFields.put("48", "40");
            ecmrFields.put("49", "Wasser");
            ecmrFields.put("50", "1210");
            ecmrFields.put("51", "1.44");
            ecmrFields.put("52", "Handle with care");
            ecmrFields.put("65", "Placeholder");
            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            elements.put(EcmrConstants.SIGNATURE_KEY, SignatureController.sign(kp, SignatureController.mapToString(ecmrFields)));
            String json = objectMapper.writeValueAsString(elements);
            //Testing of creating a eCMR
            MvcResult resultMvc = mockMvc
                    .perform(
                            MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_ECMR)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isCreated())
                    .andReturn();
            String result = resultMvc.getResponse().getContentAsString();
            assertThat(resultMvc.getResponse().getContentAsString().contains("ecmrId"));
            assertThat(resultMvc.getResponse().getContentAsString().contains("version"));

            globalEcmrId = EcmrUtils.parseResult(result, EcmrConstants.ECMR_ID_KEY);

            //Testing of getting a specific eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", globalEcmrId))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "dana@disponent.de")

                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("fields");

                            });

            //Testing of setting a User to an actual eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH +
                                            StringUtils.replaceEach(
                                                    EcmrEndpoints.PUT_USER_AT_ECMR_BY_IDS,
                                                    new String[]{E_CMRID, USER_ID},
                                                    new String[]{globalEcmrId, UserManagementService.storedUsers.get(1).getId()})) //userid=2
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "dana@disponent.de")

                    )
                    .andExpect(status().isNoContent());

            //Testing of getting the List of all eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS  + "?isArchive=false")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "dana@disponent.de")
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("consigneeName");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("senderName");
                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }

    /**
     * Checks the happy path from Verona
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-2")
    void testBVerona() throws Exception {
        //Set up the login data from Verona
        LOGGER.info("Found {} Testusers.", UserManagementService.storedUsers.size());

        //Set up the login data from Verona
        String user = UserManagementService.storedUsers.get(1).getEmail();
        KeyPair kp = SignatureController.generateKeys();
        String pbPEM = SignatureController.toPem(kp.getPublic());

        Map<String, Object> loginElements = new HashMap<String, Object>();
        loginElements.put("email", user);
        loginElements.put("pubKey", pbPEM);

        try {

            // set next user to ecmr
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_CURRENTUSER_AT_ECMR_BY_ID.replace(ID,
                                            globalEcmrId).replace("{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Authorization", "verona@verlader.de")
                                    .header("Origin", "*")
                    );

            Map<String, Object> elements = new HashMap<String, Object>();
            Map<String, Object> ecmrFields = new HashMap<>();

            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            //Testing of getting a specific eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMR_BY_ID.replace(ID, globalEcmrId))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("fields");

                            });

            elements.clear();

            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            ecmrFields.put("40", "Delivery note No: 16538987");
            ecmrFields.put("41", "DHJ");
            ecmrFields.put("42", "156742");
            ecmrFields.put("43", "20");
            ecmrFields.put("45", "Euro pallet");
            ecmrFields.put("49", "Wasser");
            ecmrFields.put("50", "1210");
            ecmrFields.put("51", "1.44");
            ecmrFields.put("65", "Placeholder");
            ecmrFields.put("69", "Placeholder");
            elements.put(EcmrConstants.SIGNATURE_KEY, SignatureController.sign(kp, SignatureController.mapToString(ecmrFields)));
            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            String json = objectMapper.writeValueAsString(elements);

            //Testing of updating a eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace(ID, globalEcmrId).replace("{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                                    .header("Authorization", "verona@verlader.de")

                    )
                    .andExpect(status().isNoContent())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString().isEmpty());
                            });

            elements.clear();
            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            elements.put(EcmrConstants.USER_ID_KEY, UserManagementService.storedUsers.get(2).getId());
            //Testing of setting a User to an actual eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + StringUtils.replaceEach(
                                            EcmrEndpoints.PUT_USER_AT_ECMR_BY_IDS,
                                            new String[]{E_CMRID, USER_ID},
                                            new String[]{globalEcmrId, UserManagementService.storedUsers.get(2).getId()}))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "verona@verlader.de")

                    )
                    .andExpect(status().isNoContent())
                    .andDo(

                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("");

                            });

            elements.clear();
            //Testing of getting the List of all eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "verona@verlader.de")

                    )
                    .andExpect(status().isOk())
                    .andDo(

                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("consigneeName");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("senderName");

                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks the happy path from Freddy
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-3")
    void testCFreddy() throws Exception {
        //Set up the login data from Freddy
        String user = UserManagementService.storedUsers.get(2).getId();
        KeyPair kp = SignatureController.generateKeys();
        String pbPEM = SignatureController.toPem(kp.getPublic());

        Map<String, Object> loginElements = new HashMap<String, Object>();
        loginElements.put("email", user);
        loginElements.put("pubKey", pbPEM);
        try {
            //Test login from Freddy

            Map<String, Object> elements = new HashMap<String, Object>();
            Map<String, Object> ecmrFields = new HashMap<>();
            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            //Testing of getting a specific eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", globalEcmrId))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "freddy@fahrer.de")
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("fields");


                            });

            elements.clear();

            ecmrFields.put("21", "Spedition Weber");
            ecmrFields.put("22", "Mr. Wagner");
            ecmrFields.put("23", "45139");
            ecmrFields.put("24", "Leopoldstraße 55");
            ecmrFields.put("25", "Essen");
            ecmrFields.put("26", "DE");
            ecmrFields.put("27", "Germany");
            ecmrFields.put("28", "NRW");
            ecmrFields.put("29", "E-GH-1053");
            ecmrFields.put("30", "Schneider transports");
            ecmrFields.put("31", "Mr. Schneider");
            ecmrFields.put("32", "12423");
            ecmrFields.put("33", "Virchowstraße 23");
            ecmrFields.put("34", "Essen");
            ecmrFields.put("35", "DE");
            ecmrFields.put("36", "Germany");
            ecmrFields.put("37", "10/09/2021");
            ecmrFields.put("38", "Mr. Schneider");
            ecmrFields.put("67", "Dortmund");
            ecmrFields.put("68", "10/09/2021");
            ecmrFields.put("70", "Placeholder");
            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            elements.put(EcmrConstants.SIGNATURE_KEY, SignatureController.sign(kp, SignatureController.mapToString(ecmrFields)));
            String json = objectMapper.writeValueAsString(elements);
            //Testing of updating a eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", globalEcmrId).replace("{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                                    .header("Authorization", "freddy@fahrer.de")
                    )
                    .andExpect(status().isNoContent())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString().isEmpty());
                            });


            elements.clear();
            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            elements.put(EcmrConstants.USER_ID_KEY, UserManagementService.storedUsers.get(3).getId());
            String loginJson2 = objectMapper.writeValueAsString(elements);
            //Testing of setting a User to an actual eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + StringUtils.replaceEach(
                                            EcmrEndpoints.PUT_USER_AT_ECMR_BY_IDS,
                                            new String[]{E_CMRID, USER_ID},
                                            new String[]{globalEcmrId, UserManagementService.storedUsers.get(3).getId()}))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "freddy@fahrer.de")

                    )
                    .andExpect(status().isNoContent())
                    .andDo(

                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("");

                            });

            elements.clear();
            //Testing of getting the List of all eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                                    .header("Authorization", "freddy@fahrer.de")
                    )
                    .andExpect(status().isOk())
                    .andDo(

                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("consigneeName");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("senderName");

                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks the happy path from Ernie
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-4")
    void testDErnie() throws Exception {
        //Set up the login data from Ernie
        KeyPair kp = SignatureController.generateKeys();
        String pbPEM = SignatureController.toPem(kp.getPublic());

        Map<String, Object> loginElements = new HashMap<String, Object>();
        loginElements.put("email", UserManagementService.storedUsers.get(3).getId());
        loginElements.put("pubKey", pbPEM);
        try {
            //Test login from Ernie

            Map<String, Object> elements = new HashMap<String, Object>();
            Map<String, Object> ecmrFields = new HashMap<>();

            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            //Testing of getting a specific eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", globalEcmrId))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("fields");

                            });

            elements.clear();

            ecmrFields.put("71", "Placeholder");
            ecmrFields.put("72", "Placeholder");
            ecmrFields.put("74", "Placeholder");
            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
            elements.put(EcmrConstants.SIGNATURE_KEY, SignatureController.sign(kp, SignatureController.mapToString(ecmrFields)));
            String json = objectMapper.writeValueAsString(elements);
            //Testing of updating a eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}", globalEcmrId).replace("{isEcmr}","true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString().isEmpty());
                            });


            elements.clear();
            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            elements.put(EcmrConstants.USER_ID_KEY, UserManagementService.storedUsers.get(5).getId());
            json = objectMapper.writeValueAsString(elements);
            //Testing of setting a User to an actual eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH + StringUtils.replaceEach(
                                            EcmrEndpoints.PUT_USER_AT_ECMR_BY_IDS,
                                            new String[]{E_CMRID, USER_ID},
                                            new String[]{globalEcmrId, UserManagementService.storedUsers.get(5).getId()}))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent())
                    .andDo(

                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("");
                            });

            elements.clear();

            //Testing of getting the List of all eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS + "?isArchive=false")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk())
                    .andDo(

                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("consigneeName");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("senderName");

                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    /**
     * Checks the happy path from Vanni
     *
     * @throws Exception thrown when test is failed
     */
    /*
    @Test
    @WithMockUser(username = "vanni@verwaltung.de")
    void testEVanni() throws Exception {
        //Set up the login data from Vanni
        KeyPair kp = SignatureController.generateKeys();
        String pbPEM = SignatureController.toPem(kp.getPublic());

        Map<String, Object> loginElements = new HashMap<String, Object>();
        loginElements.put("email", UserManagementService.storedUsers.get(4).getId());
        loginElements.put("pubKey", pbPEM);
        try {
            String loginJson = objectMapper.writeValueAsString(loginElements);

            Map<String, Object> elements = new HashMap<>();

            elements.put(EcmrConstants.ECMR_ID_KEY, globalEcmrId);
            //Testing of getting a specific eCMR
            String json = objectMapper.writeValueAsString(elements);

            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", globalEcmrId))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk())
                    .andDo(
                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("fields");

                            });


            elements.clear();
            //Testing of getting the List of all eCMR
            this.mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS)
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk())
                    .andDo(

                            mvcResult -> {
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("ecmrId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("version");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("status");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUserId");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("lastUpdated");
                                assertThat(mvcResult.getResponse().getContentAsString()).contains("consigneeName");
                                assertThat(mvcResult.getResponse().getContentAsString())
                                        .contains("senderName");

                            });

        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

    }
*/

    /**
     * Checks the happy path for Superuser
     *
     * @throws Exception thrown when test is failed
     */
    @Test
    @WithMockUser(username = "123-456-789-5")
    void testSuperUser() throws Exception {
        //Generate Keys for Sep
        KeyPair kp = SignatureController.generateKeys();


        Map<String, Object> elements = new HashMap<>();
        Map<String, Object> ecmrFields = new HashMap<>();

        elements.put("ecmrId", "0");


        for(int i = 0; i < 75; i++)
        {
            if(!SignatureController.SIGNATURE_FIELD_IDS.contains(Integer.toString(i))) {
                ecmrFields.put(Integer.toString(i), "Arbitrary Text");
            }
        }
        elements.put(EcmrConstants.ECMR_FIELD_KEY, ecmrFields);
        elements.put(EcmrConstants.SIGNATURE_KEY, SignatureController.sign(kp, SignatureController.mapToString(ecmrFields)));

        String json = objectMapper.writeValueAsString(elements);

        //Testing of creating a eCMR
        MvcResult resultMvc = mockMvc
                .perform(
                        MockMvcRequestBuilders.post(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.POST_ECMR)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isCreated())
                .andReturn();
        assertThat(resultMvc.getResponse().getContentAsString().contains("ecmrId"));
        HashMap<String, String> createdEcmr = objectMapper.readValue(resultMvc.getResponse().getContentAsString(), HashMap.class);

        // Get eCMR information
        resultMvc = mockMvc
                .perform(
                        MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH +  EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", createdEcmr.get("ecmrId")))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(json)
                                .header("Origin", "*")
                )
                .andExpect(status().isOk())
                .andReturn();

        assertThat(resultMvc.getResponse().getContentAsString().contains("ecmrId"));
        HashMap<String, String> ecmrResponseData = objectMapper.readValue(resultMvc.getResponse().getContentAsString(), HashMap.class);

        // Check if status is new (since no signature is given)
        String s = ecmrResponseData.get("status");
        assertThat(s.contains("NEW"));

        // Sort signature fields as they have no order in a set
        Object[] signatureFields = Arrays.stream(SignatureController.SIGNATURE_FIELD_IDS.toArray()).sorted().toArray();
        String[] relevantStatuses = new String[]{"LOADING","TRANSPORT","ARRIVED"};
        //iterate over signatures
        for(int i = 0; i < signatureFields.length; i++)
        {
            // add first signature
            ecmrFields.clear();
            // sort set because it has no order -> get first signature field
            ecmrFields.put((String) signatureFields[i], "Signature");
            elements.put("ecmrId", createdEcmr.get("ecmrId"));
            json = objectMapper.writeValueAsString(elements);

            // update eCMR information
            mockMvc
                    .perform(
                            MockMvcRequestBuilders.put(EcmrEndpoints.ECMR_BASE_PATH +  EcmrEndpoints.PUT_ECMR_BY_ID.replace("{id}",
                                            createdEcmr.get("ecmrId")).replace("{isEcmr}", "true"))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isNoContent())
                    .andReturn();


            // Get eCMR information
            resultMvc = mockMvc
                    .perform(
                            MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH +  EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", createdEcmr.get("ecmrId")))
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(json)
                                    .header("Origin", "*")
                    )
                    .andExpect(status().isOk())
                    .andReturn();

            assertThat(resultMvc.getResponse().getContentAsString().contains("ecmrId"));
            ecmrResponseData = objectMapper.readValue(resultMvc.getResponse().getContentAsString(), HashMap.class);

            // Check if status is new (since no signature is given)
            assertThat(ecmrResponseData.get("status").contains(relevantStatuses[i]));
        }

    }

}
