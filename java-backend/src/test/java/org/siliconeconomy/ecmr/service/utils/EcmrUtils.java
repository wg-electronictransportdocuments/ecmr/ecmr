/*
 * Copyright Open Logistics Foundation
 *
 * Licensed under the Open Logistics Foundation License 1.3.
 * For details on the licensing terms, see the LICENSE file.
 * SPDX-License-Identifier: OLFL-1.3
 */
package org.siliconeconomy.ecmr.service.utils;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.nio.charset.StandardCharsets;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.siliconeconomy.ecmr.service.constants.EcmrConstants;
import org.siliconeconomy.ecmr.service.constants.EcmrEndpoints;
import org.siliconeconomy.ecmr.service.facade.RoleFacade;
import org.siliconeconomy.ecmr.service.facade.UserFacade;
import org.siliconeconomy.ecmr.service.models.Company;
import org.siliconeconomy.ecmr.service.models.Contact;
import org.siliconeconomy.ecmr.service.models.EcmrData;
import org.siliconeconomy.ecmr.service.models.EcmrListData;
import org.siliconeconomy.ecmr.service.repositories.ContactRepository;
import org.siliconeconomy.ecmr.service.role.RoleService;
import org.siliconeconomy.ecmr.service.services.EcmrManagementService;
import org.siliconeconomy.ecmr.service.services.UserManagementService;
import org.siliconeconomy.ecmr.service.status.StatusService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EcmrUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(EcmrUtils.class);

    public static String parseResult(String result, String key) {

        try {
            Object ob = new JSONParser().parse(result);
            // typecasting ob to JSONObject
            JSONObject js = (JSONObject) ob;
            return (String) js.get(key);
        } catch (Exception e) {
            LOGGER.info("No value for key {} found.", key);
        }
        return null;
    }

    @WithMockUser
    public static List<EcmrListData> getCurrentEcmrsForUser(MockMvc mockMvc, String user) throws Exception {
        // get current Ecmrs
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMRS)
                        .with(user(user)))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();

        List<EcmrListData> ecmrListData = mapper.readValue(result.getResponse().getContentAsString(), new TypeReference<List<EcmrListData>>() {
        });

        return ecmrListData;
    }

    public static EcmrData getEcmrForUser(MockMvc mockMvc, String user, String ecmrId) throws Exception {
        // get Ecmr by id
        MvcResult result = mockMvc.perform(MockMvcRequestBuilders.get(EcmrEndpoints.ECMR_BASE_PATH + EcmrEndpoints.GET_ECMR_BY_ID.replace("{id}", ecmrId))
                        .header("Authorization", user))
                .andExpect(status().isOk())
                .andReturn();

        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(result.getResponse().getContentAsString(), EcmrData.class);
    }

    public static void generateTestStatus() {
        StatusService.statusPrioList.addAll(List.of("NEW", "LOADING", "IN TRANSPORT", "ARRIVED AT DESTINATION"));
    }

    public static void generateTestRoles() {
        RoleService.roleNames.clear();
        RoleService.roleNames.add("ecmr-disponent");
        RoleService.roleNames.add("ecmr-verlader");
        RoleService.roleNames.add("ecmr-fahrer");
        RoleService.roleNames.add("ecmr-empfaenger");
        RoleService.roleNames.add("ecmr-superuser");

        RoleFacade role = new RoleFacade("1", "ecmr-verlader");
        role.setMandatoryFields(List.of("1", "3", "4", "5", "6", "7", "9", "10", "11", "13", "14", "17", "18", "65"));
        RoleService.storedRoles.add(role);
    }

    public static void generateTestUsers() {
        UserManagementService.storedUsers.clear();
        List<UserFacade> testUsers = new ArrayList<>();
        testUsers.add(new UserFacade(
                "123-456-789-1",
                "nils.vetter@ecmr.de",
                "nils.vetter@ecmr.de",
                List.of(RoleService.roleNames.get(0)),
                new Company("No Company", "Dortmund"),
                Map.of(EcmrConstants.COMPANY_ID, List.of("1"))
        ));
        testUsers.add(new UserFacade(
                "123-456-789-2",
                "annette.greiner@ecmr.de",
                "annette.greiner@ecmr.de",
                List.of(RoleService.roleNames.get(0)),
                new Company("No Company", "Dortmund"),
                Map.of(EcmrConstants.COMPANY_ID, List.of("2"))
        ));
        testUsers.add(new UserFacade(
                "123-456-789-3",
                "franz.pankerl@ecmr.de",
                "franz.pankerl@ecmr.de",
                List.of(RoleService.roleNames.get(0)),
                new Company("No Company", "Dortmund"),
                Map.of(EcmrConstants.COMPANY_ID, List.of("3"))
        ));
        testUsers.add(new UserFacade(
                "123-456-789-4",
                "uwe.senft@ecmr.de",
                "uwe.senft@ecmr.de",
                List.of(RoleService.roleNames.get(0)),
                new Company("No Company", "Dortmund"),
                Map.of(EcmrConstants.COMPANY_ID, List.of("4"))
        ));
        testUsers.add(new UserFacade(
                "123-456-789-5",
                "judith.leineweber@ecmr.de",
                "judith.leineweber@ecmr.de",
                List.of(RoleService.roleNames.get(0)),
                new Company("No Company", "Dortmund"),
                Map.of(EcmrConstants.COMPANY_ID, List.of("4"))
        ));
        testUsers.add(new UserFacade(
                "123-456-789-6",
                "silvia.pagels@ecmr.de",
                "silvia.pagels@ecmr.de",
                List.of(RoleService.roleNames.get(0)),
                new Company("No Company", "Dortmund"),
                Map.of(EcmrConstants.COMPANY_ID, List.of("4"))
        ));
        UserManagementService.storedUsers.addAll(testUsers);
    }

    public static void generateTestDataEcmrs(EcmrManagementService ecmrManagementService){
        HashMap<String, Object> ecmr = new HashMap<>();
        for(int j = 0; j < 5; j++) {
            ecmr.clear();
            for (int i = 0; i < 67; i++) {
                // Create Random string
                byte[] array = new byte[7]; // length is bounded by 7
                new Random().nextBytes(array);
                String generatedString = new String(array, StandardCharsets.UTF_8);
                // put it in ecmr
                ecmr.put(Integer.toString(i), "ABC");
            }

            HashMap<String, Object> autogenInput = new HashMap<>();
            autogenInput.put(EcmrConstants.ECMR_FIELD_KEY, ecmr);
            autogenInput.put("creator", "nils.vetter@ecmr.de");
            autogenInput.put("assignees", new ArrayList<>(Arrays.asList("annette.greiner@ecmr.de")));

            ecmrManagementService.autogenerateEcmr(autogenInput);
        }
    }

    public static void generateTestContacts(ContactRepository contactRepository, String userId) {
        Contact contact = new Contact(userId, userId, 1, Timestamp.from(Instant.now()));
        contactRepository.save(contact);
    }
}
