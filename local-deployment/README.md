## Setting up a local deployment for testing purpose

This directory provides environments and configurations for setting up local testing deployments. There are two different docker compose setups provided.
Used third party software is listed in [`./third-party-licenses/third-party-licenses.csv`](./third-party-licenses/third-party-licenses.csv)

---

### Environment Variables

The following table provides an overview of the environment variables used in the example configurations. Some Variables are recommended to match between modules of the same instance. All eCMR specific variables are described in the respective README files.

| Backend                        | Frontend                     | Keycloak                | Postgres                    | General              |
|--------------------------------| ---------------------------- | ----------------------- | --------------------------- | -------------------- |
| ECMR_INSTANCE_SSL              | INSTANCE_SSL                 | KC_DB_ADDR              | POSTGRES_MULTIPLE_DATABASES | INSTANCE_INDEX_COLOR |
| ECMR_BACKEND_ADDRESS           | BACKEND_URL                  | KC_DB_DATABASE          | POSTGRES_USER               | INSTANCE_INDEX_TITLE |
| KEYCLOAK_AUTH_SERVER_URL       | KEYCLOAK_URL                 | KC_DB_SCHEMA            | POSTGRES_PASSWORD           | INSTANCE_INDEX       |
| KEYCLOAK_ENABLED               | INSTANCE_USERMANAGEMENT_TYPE | KC_DB_PASSWORD          |                             |                      |
| KEYCLOAK_REALM                 | KEYCLOAK_REALM               | KC_DB_URL               |                             |                      |
| KEYCLOAK_RESOURCE              | KEYCLOAK_FRONTEND_RESOURCE   | KC_DB_USERNAME          |                             |                      |
| ECMR_BACKEND_PORT              | INSTANCE_TITLE               | KC_HEALTH_ENABLED       |                             |                      |
| ECMR_KEYCLOAK_ADMIN_RESOURCE   | INSTANCE_COLOR_THEME         | KEYCLOAK_ADMIN          |                             |                      |
| ECMR_KEYCLOAK_ADMIN_SECRET_KEY | FRONTEND_URL                 | KEYCLOAK_ADMIN_PASSWORD |                             |                      |
| DATASOURCE_URL                 |                              |                         |                             |                      |
| DATASOURCE_USERNAME            |                              |
| DATASOURCE_PASSWORD            |                              |
| DATASOURCE_DRIVERCLASSNAME     |                              |
| DATASOURCE_PLATFORM            |                              |
| BC_MS_URL                      |                              |

---

### Lite environment

The lite environment provides a simplified eCMR Instance Infrastructure. It is meant to be used for fast and easy demonstration purposes and/or testing. It sets up a single back- and frontend instance, exposed on `localhost:8080` resp. `:4200`. It makes use of an embedded H2 Database which is reset after each session.

Also, it configures both applications to use the `localdev` user management profile. `localdev` brings a less sophisticated feature set than i.e. `keycloak` and only provides some preconfigured users which can be selected by dropdown instead of implementing a complex login functionality. It omits any kind of pki in order to make the application more accessible for testing.

Running the lite environment:
```
# assuming Linux as OS and working directory of /ecmr/local-deployment/
docker compose -f ./docker-compose.lite.yaml up --build
```

---
### Single environment

The default environment is meant for providing a more full-fetched demo experience. It provides the eCMR application, a dedicated postgres container as well as a postgres-based keycloak.

It is configured to build the keycloak instance exposed on `localhost:8090`, the preconfigured postgreSQL DBMS on `localhost:5432` and the eCMR application, consisting of a backend which is exposed on `localhost:8080` and a frontend application exposed on `localhost:4200`.

The postgreSQL DBMS comes with a dedicated DB for keycloak (DB named `keycloak`) and one for the eCMR Instance (named `ecmr`)

The keycloak container contains a separate realm for the application instance and can be configured by accessing the administration console using the default username:password combination of `admin:admin`.

The single environment, provided through `docker-compose.single.yaml` can be used to build and set up the eCMR environment by calling:
```
# assuming Linux as OS and working directory of /ecmr/local-deployment/
docker compose -f ./docker-compose.single.yaml up --build
```
**At the moment**, it is crucial to set the users role to `ecmr-user` to have them recognized by the respective backend instance and `ecmr-superuser` to give them read and write access.
To map a keycloak user to a company from the database, a keycloak attribute `CompanyID` has to be created with the company's index as its value.
**Users without correct role-mappings won't be found by the backend service**
If a configured user is still missing after registration and correct mapping of roles try to restart the respective backend's docker container.


### Multi environment

The multi environment is meant for providing a more full-fetched demo experience. It provides several eCMR application instances, a dedicated postgres container as well as a postgres-based keycloak.

It is configured to build the keycloak instance exposed on `localhost:8090`, the preconfigured postgreSQL DBMS on `localhost:5432` and four independent eCMR Instances, each consisting of a backend which is exposed on `localhost:808X` and a frontend application exposed on `localhost:420X`, where `X` stands for the Instance Index (`0` to `3`).

The postgreSQL DBMS comes with a dedicated DB for keycloak (DB named `keycloak`) and one for each eCMR Instance (named `ecmrX`, where `X` also stands for the Instance Index.)

The keycloak container contains a separate realm for each application instance and can be configured by accessing the administration console using the default username:password combination of `admin:admin`.

The multi environment, provided through `docker-compose.multi.yaml` can be used to build and set up said multi instance eCMR environment by calling:

```
# assuming Linux as OS and working directory of /ecmr/local-deployment/
docker compose -f ./docker-compose.multi.yaml up --build
```

To get started on testing the mulit-instance version of the eCMR Service, the admin should at first create keycloak users for all instances to be used.
By accessing http://localhost:8090/ you're directed to the keycloak admin panel where you have to create a fitting user structure for each instance-corresponding realm (eg. if you're up to testing the service with Instance A/0 on `localhost:4200/8080` and B/1 on `localhost:4201/8081`, you have to create users on realm `ecmr-keycloak-0` and `-1`).

**At the moment**, it is crucial to set the users role to `ecmr-user` to have them recognized by the respective backend instance and `ecmr-superuser` to give them read and write access.
To map a keycloak user to a company from the database, a keycloak attribute `CompanyID` has to be created with the company's index as its value.
**Users without correct role-mappings won't be found by the backend service**
If a configured user is still missing after registration and correct mapping of roles try to restart the respective backend's docker container.

The following deployment view diagram visualizes relations and concepts for the concurrent docker compose service components:

![](./documentation/images/multi-env-deployment-view.drawio.svg)

---

### Demo/Testing on multiple devices in a local network

The provided environment configurations are designed to be modified for testing in a local network.

> **NOTE:**
> When executed in a non-secure context (without SSL encryption on serving), some frontend-based features like device media usage (camera) and key generation are, for security reasons, not available by default.
